
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCardActivityType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chCardActivityType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TRANSFER"/>
 *     &lt;enumeration value="BILL_PAYMENT"/>
 *     &lt;enumeration value="PURCHASE"/>
 *     &lt;enumeration value="DEPOSIT"/>
 *     &lt;enumeration value="WITHDRAWAL"/>
 *     &lt;enumeration value="CHARGE_BACK"/>
 *     &lt;enumeration value="CHANGE_SECOND_PIN"/>
 *     &lt;enumeration value="CHANGE_FIRST_PIN"/>
 *     &lt;enumeration value="UNBLOCK"/>
 *     &lt;enumeration value="CAPTURED"/>
 *     &lt;enumeration value="STATEMENT"/>
 *     &lt;enumeration value="BALANCE"/>
 *     &lt;enumeration value="RETURN"/>
 *     &lt;enumeration value="OTHER"/>
 *     &lt;enumeration value="CREDIT_DEBIT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chCardActivityType")
@XmlEnum
public enum ChCardActivityType {

    TRANSFER,
    BILL_PAYMENT,
    PURCHASE,
    DEPOSIT,
    WITHDRAWAL,
    CHARGE_BACK,
    CHANGE_SECOND_PIN,
    CHANGE_FIRST_PIN,
    UNBLOCK,
    CAPTURED,
    STATEMENT,
    BALANCE,
    RETURN,
    OTHER,
    CREDIT_DEBIT;

    public String value() {
        return name();
    }

    public static ChCardActivityType fromValue(String v) {
        return valueOf(v);
    }

}
