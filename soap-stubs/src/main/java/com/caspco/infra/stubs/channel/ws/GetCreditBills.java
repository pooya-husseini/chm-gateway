
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCreditBills complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCreditBills">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBaseCreditBillSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBaseCreditBillSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCreditBills", propOrder = {
    "chBaseCreditBillSearchRequestBean"
})
public class GetCreditBills
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBaseCreditBillSearchRequestBean chBaseCreditBillSearchRequestBean;

    /**
     * Gets the value of the chBaseCreditBillSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBaseCreditBillSearchRequestBean }
     *     
     */
    public ChBaseCreditBillSearchRequestBean getChBaseCreditBillSearchRequestBean() {
        return chBaseCreditBillSearchRequestBean;
    }

    /**
     * Sets the value of the chBaseCreditBillSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBaseCreditBillSearchRequestBean }
     *     
     */
    public void setChBaseCreditBillSearchRequestBean(ChBaseCreditBillSearchRequestBean value) {
        this.chBaseCreditBillSearchRequestBean = value;
    }

}
