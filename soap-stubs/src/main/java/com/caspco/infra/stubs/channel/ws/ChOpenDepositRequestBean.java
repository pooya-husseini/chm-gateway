
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chOpenDepositRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chOpenDepositRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addressId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chSecondPasswordType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSecondPasswordType" minOccurs="0"/>
 *         &lt;element name="cif" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="depositBranchCode" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="depositGroup" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositGroupType" minOccurs="0"/>
 *         &lt;element name="depositType" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="investmentDeposit" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chInvestmentDepositBean" minOccurs="0"/>
 *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reportDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="reportDurationUnit" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chInaugurateDepositDurationUnit" minOccurs="0"/>
 *         &lt;element name="sameSignatureDepositNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="savingDeposit" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSavingDepositBean" minOccurs="0"/>
 *         &lt;element name="secondPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="withdrawableAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="withdrawableDeposit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chOpenDepositRequestBean", propOrder = {
    "addressId",
    "chSecondPasswordType",
    "cif",
    "currency",
    "depositBranchCode",
    "depositGroup",
    "depositType",
    "email",
    "fax",
    "investmentDeposit",
    "mobile",
    "reportDate",
    "reportDurationUnit",
    "sameSignatureDepositNumber",
    "savingDeposit",
    "secondPassword",
    "withdrawableAmount",
    "withdrawableDeposit"
})
public class ChOpenDepositRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String addressId;
    @XmlSchemaType(name = "string")
    protected ChSecondPasswordType chSecondPasswordType;
    protected String cif;
    protected String currency;
    protected Long depositBranchCode;
    @XmlSchemaType(name = "string")
    protected ChDepositGroupType depositGroup;
    protected Long depositType;
    protected String email;
    protected String fax;
    protected ChInvestmentDepositBean investmentDeposit;
    protected String mobile;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date reportDate;
    @XmlSchemaType(name = "string")
    protected ChInaugurateDepositDurationUnit reportDurationUnit;
    protected String sameSignatureDepositNumber;
    protected ChSavingDepositBean savingDeposit;
    protected String secondPassword;
    protected BigDecimal withdrawableAmount;
    protected String withdrawableDeposit;

    /**
     * Gets the value of the addressId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressId() {
        return addressId;
    }

    /**
     * Sets the value of the addressId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressId(String value) {
        this.addressId = value;
    }

    /**
     * Gets the value of the chSecondPasswordType property.
     * 
     * @return
     *     possible object is
     *     {@link ChSecondPasswordType }
     *     
     */
    public ChSecondPasswordType getChSecondPasswordType() {
        return chSecondPasswordType;
    }

    /**
     * Sets the value of the chSecondPasswordType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSecondPasswordType }
     *     
     */
    public void setChSecondPasswordType(ChSecondPasswordType value) {
        this.chSecondPasswordType = value;
    }

    /**
     * Gets the value of the cif property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCif() {
        return cif;
    }

    /**
     * Sets the value of the cif property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCif(String value) {
        this.cif = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the depositBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDepositBranchCode() {
        return depositBranchCode;
    }

    /**
     * Sets the value of the depositBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDepositBranchCode(Long value) {
        this.depositBranchCode = value;
    }

    /**
     * Gets the value of the depositGroup property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositGroupType }
     *     
     */
    public ChDepositGroupType getDepositGroup() {
        return depositGroup;
    }

    /**
     * Sets the value of the depositGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositGroupType }
     *     
     */
    public void setDepositGroup(ChDepositGroupType value) {
        this.depositGroup = value;
    }

    /**
     * Gets the value of the depositType property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDepositType() {
        return depositType;
    }

    /**
     * Sets the value of the depositType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDepositType(Long value) {
        this.depositType = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the fax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Gets the value of the investmentDeposit property.
     * 
     * @return
     *     possible object is
     *     {@link ChInvestmentDepositBean }
     *     
     */
    public ChInvestmentDepositBean getInvestmentDeposit() {
        return investmentDeposit;
    }

    /**
     * Sets the value of the investmentDeposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChInvestmentDepositBean }
     *     
     */
    public void setInvestmentDeposit(ChInvestmentDepositBean value) {
        this.investmentDeposit = value;
    }

    /**
     * Gets the value of the mobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Sets the value of the mobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobile(String value) {
        this.mobile = value;
    }

    /**
     * Gets the value of the reportDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getReportDate() {
        return reportDate;
    }

    /**
     * Sets the value of the reportDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportDate(Date value) {
        this.reportDate = value;
    }

    /**
     * Gets the value of the reportDurationUnit property.
     * 
     * @return
     *     possible object is
     *     {@link ChInaugurateDepositDurationUnit }
     *     
     */
    public ChInaugurateDepositDurationUnit getReportDurationUnit() {
        return reportDurationUnit;
    }

    /**
     * Sets the value of the reportDurationUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChInaugurateDepositDurationUnit }
     *     
     */
    public void setReportDurationUnit(ChInaugurateDepositDurationUnit value) {
        this.reportDurationUnit = value;
    }

    /**
     * Gets the value of the sameSignatureDepositNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSameSignatureDepositNumber() {
        return sameSignatureDepositNumber;
    }

    /**
     * Sets the value of the sameSignatureDepositNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSameSignatureDepositNumber(String value) {
        this.sameSignatureDepositNumber = value;
    }

    /**
     * Gets the value of the savingDeposit property.
     * 
     * @return
     *     possible object is
     *     {@link ChSavingDepositBean }
     *     
     */
    public ChSavingDepositBean getSavingDeposit() {
        return savingDeposit;
    }

    /**
     * Sets the value of the savingDeposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSavingDepositBean }
     *     
     */
    public void setSavingDeposit(ChSavingDepositBean value) {
        this.savingDeposit = value;
    }

    /**
     * Gets the value of the secondPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondPassword() {
        return secondPassword;
    }

    /**
     * Sets the value of the secondPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondPassword(String value) {
        this.secondPassword = value;
    }

    /**
     * Gets the value of the withdrawableAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWithdrawableAmount() {
        return withdrawableAmount;
    }

    /**
     * Sets the value of the withdrawableAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWithdrawableAmount(BigDecimal value) {
        this.withdrawableAmount = value;
    }

    /**
     * Gets the value of the withdrawableDeposit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWithdrawableDeposit() {
        return withdrawableDeposit;
    }

    /**
     * Sets the value of the withdrawableDeposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWithdrawableDeposit(String value) {
        this.withdrawableDeposit = value;
    }

}
