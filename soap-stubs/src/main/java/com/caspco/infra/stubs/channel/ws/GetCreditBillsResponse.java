
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCreditBillsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCreditBillsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBillResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCreditBillsResponse", propOrder = {
    "chBillResponseBean"
})
public class GetCreditBillsResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBillResponseBean chBillResponseBean;

    /**
     * Gets the value of the chBillResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillResponseBean }
     *     
     */
    public ChBillResponseBean getChBillResponseBean() {
        return chBillResponseBean;
    }

    /**
     * Sets the value of the chBillResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillResponseBean }
     *     
     */
    public void setChBillResponseBean(ChBillResponseBean value) {
        this.chBillResponseBean = value;
    }

}
