package com.caspco.channel.configuration;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/16/17
 * Time: 11:07 AM
 */
public class SwaggerInput {
    private List<String> operations;

    @NotNull(message = "You should fill operation")
    public List<String> getOperations() {
        return operations;
    }

    public void setOperations(List<String> operations) {
        this.operations = operations;
    }
}
