
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chInaugurateDepositDurationUnit.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chInaugurateDepositDurationUnit">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DAILY"/>
 *     &lt;enumeration value="WEEKLY"/>
 *     &lt;enumeration value="MONTHLY"/>
 *     &lt;enumeration value="TWO_MONTH"/>
 *     &lt;enumeration value="THREE_MONTH"/>
 *     &lt;enumeration value="FOUR_MONTH"/>
 *     &lt;enumeration value="SIX_MONTH"/>
 *     &lt;enumeration value="YEAR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chInaugurateDepositDurationUnit")
@XmlEnum
public enum ChInaugurateDepositDurationUnit {

    DAILY,
    WEEKLY,
    MONTHLY,
    TWO_MONTH,
    THREE_MONTH,
    FOUR_MONTH,
    SIX_MONTH,
    YEAR;

    public String value() {
        return name();
    }

    public static ChInaugurateDepositDurationUnit fromValue(String v) {
        return valueOf(v);
    }

}
