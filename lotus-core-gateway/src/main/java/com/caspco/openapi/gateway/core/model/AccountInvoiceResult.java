package com.caspco.openapi.gateway.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/23/17
 *         Time: 10:39 AM
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountInvoiceResult implements Serializable {
    private static final long serialVersionUID = -2694957980703310111L;
    private String dealReference;
    private Date bookDate;
    private BigDecimal amount;
    private Integer operatorBranchCode;
    private String narrative1;
    private String currencyCode;
    private String businessTime;
    private String chequeNumber;
    private String receiptNumber;
    private BigDecimal bookBalance;
    private String printOperationCode;
    private Date bookDateTime;

    public AccountInvoiceResult() {
    }

    public String getDealReference() {
        return this.dealReference;
    }

    public void setDealReference(String dealReference) {
        this.dealReference = dealReference;
    }

    public Date getBookDate() {
        return this.bookDate;
    }

    public void setBookDate(Date bookDate) {
        this.bookDate = bookDate;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getOperatorBranchCode() {
        return this.operatorBranchCode;
    }

    public void setOperatorBranchCode(Integer operatorBranchCode) {
        this.operatorBranchCode = operatorBranchCode;
    }

    public String getNarrative1() {
        return this.narrative1;
    }

    public void setNarrative1(String narrative1) {
        this.narrative1 = narrative1;
    }

    public String getCurrencyCode() {
        return this.currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getBusinessTime() {
        return this.businessTime;
    }

    public void setBusinessTime(String businessTime) {
        this.businessTime = businessTime;
    }

    public String getChequeNumber() {
        return this.chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public String getReceiptNumber() {
        return this.receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public BigDecimal getBookBalance() {
        return this.bookBalance;
    }

    public void setBookBalance(BigDecimal bookBalance) {
        this.bookBalance = bookBalance;
    }

    public String getPrintOperationCode() {
        return this.printOperationCode;
    }

    public void setPrintOperationCode(String printOperationCode) {
        this.printOperationCode = printOperationCode;
    }

    public Date getBookDateTime() {
        return this.bookDateTime;
    }

    public void setBookDateTime(Date bookDateTime) {
        this.bookDateTime = bookDateTime;
    }
}
