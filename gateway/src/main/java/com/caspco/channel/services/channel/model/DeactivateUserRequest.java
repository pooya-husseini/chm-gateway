package com.caspco.channel.services.channel.model;

import com.caspco.infra.stubs.channel.ws.ChannelType;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/10/17
 * Time: 4:02 PM
 */
public class DeactivateUserRequest {
    private String username;
    private ChannelType channelType;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }
}
