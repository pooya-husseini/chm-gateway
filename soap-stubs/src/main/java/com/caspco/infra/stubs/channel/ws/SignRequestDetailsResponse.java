
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for signRequestDetailsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="signRequestDetailsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSignRequestDetailResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSignRequestDetailResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "signRequestDetailsResponse", propOrder = {
    "chSignRequestDetailResponseBean"
})
public class SignRequestDetailsResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSignRequestDetailResponseBean chSignRequestDetailResponseBean;

    /**
     * Gets the value of the chSignRequestDetailResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChSignRequestDetailResponseBean }
     *     
     */
    public ChSignRequestDetailResponseBean getChSignRequestDetailResponseBean() {
        return chSignRequestDetailResponseBean;
    }

    /**
     * Sets the value of the chSignRequestDetailResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSignRequestDetailResponseBean }
     *     
     */
    public void setChSignRequestDetailResponseBean(ChSignRequestDetailResponseBean value) {
        this.chSignRequestDetailResponseBean = value;
    }

}
