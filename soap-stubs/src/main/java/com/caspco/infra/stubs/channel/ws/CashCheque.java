
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cashCheque complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cashCheque">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCashChequeRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCashChequeRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cashCheque", propOrder = {
    "chCashChequeRequestBean"
})
public class CashCheque
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCashChequeRequestBean chCashChequeRequestBean;

    /**
     * Gets the value of the chCashChequeRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCashChequeRequestBean }
     *     
     */
    public ChCashChequeRequestBean getChCashChequeRequestBean() {
        return chCashChequeRequestBean;
    }

    /**
     * Sets the value of the chCashChequeRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCashChequeRequestBean }
     *     
     */
    public void setChCashChequeRequestBean(ChCashChequeRequestBean value) {
        this.chCashChequeRequestBean = value;
    }

}
