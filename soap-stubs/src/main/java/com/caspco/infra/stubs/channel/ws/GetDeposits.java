
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDeposits complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDeposits">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDepositSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDeposits", propOrder = {
    "chDepositSearchRequestBean"
})
public class GetDeposits
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDepositSearchRequestBean chDepositSearchRequestBean;

    /**
     * Gets the value of the chDepositSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositSearchRequestBean }
     *     
     */
    public ChDepositSearchRequestBean getChDepositSearchRequestBean() {
        return chDepositSearchRequestBean;
    }

    /**
     * Sets the value of the chDepositSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositSearchRequestBean }
     *     
     */
    public void setChDepositSearchRequestBean(ChDepositSearchRequestBean value) {
        this.chDepositSearchRequestBean = value;
    }

}
