package com.caspco.channel.swagger;

import java.lang.reflect.Parameter;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/11/17
 * Time: 4:51 PM
 */
public interface MethodParameterFilter {
    boolean isValid(Parameter parameter);
}
