
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBatchBillPaymentBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chBatchBillPaymentBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="doneCompletely" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="problemType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chProblemType" minOccurs="0"/>
 *         &lt;element name="response" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillPaymentResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chBatchBillPaymentBean", propOrder = {
    "doneCompletely",
    "problemType",
    "response"
})
public class ChBatchBillPaymentBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected boolean doneCompletely;
    @XmlSchemaType(name = "string")
    protected ChProblemType problemType;
    protected ChBillPaymentResponseBean response;

    /**
     * Gets the value of the doneCompletely property.
     * 
     */
    public boolean isDoneCompletely() {
        return doneCompletely;
    }

    /**
     * Sets the value of the doneCompletely property.
     * 
     */
    public void setDoneCompletely(boolean value) {
        this.doneCompletely = value;
    }

    /**
     * Gets the value of the problemType property.
     * 
     * @return
     *     possible object is
     *     {@link ChProblemType }
     *     
     */
    public ChProblemType getProblemType() {
        return problemType;
    }

    /**
     * Sets the value of the problemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChProblemType }
     *     
     */
    public void setProblemType(ChProblemType value) {
        this.problemType = value;
    }

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillPaymentResponseBean }
     *     
     */
    public ChBillPaymentResponseBean getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillPaymentResponseBean }
     *     
     */
    public void setResponse(ChBillPaymentResponseBean value) {
        this.response = value;
    }

}
