
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBanks complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBanks">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBankSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBankSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBanks", propOrder = {
    "chBankSearchRequestBean"
})
public class GetBanks
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBankSearchRequestBean chBankSearchRequestBean;

    /**
     * Gets the value of the chBankSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBankSearchRequestBean }
     *     
     */
    public ChBankSearchRequestBean getChBankSearchRequestBean() {
        return chBankSearchRequestBean;
    }

    /**
     * Sets the value of the chBankSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBankSearchRequestBean }
     *     
     */
    public void setChBankSearchRequestBean(ChBankSearchRequestBean value) {
        this.chBankSearchRequestBean = value;
    }

}
