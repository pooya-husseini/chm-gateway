/**
 * ChNormalAchTransferResultBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

import java.math.BigDecimal;
import java.util.Date;

public class NormalAchTransferResultDto  implements java.io.Serializable {

    private BigDecimal amount;

    private String currency;

    private String description;

    private String factorNumber;

    private String ibanNumber;

    private String ownerName;

    private String referenceId;

    private String sourceIbanNumber;

    private Date transactionDate;

    private DestinationTransactionStatus transactionStatus;

    private String transferDescription;

    private AchTransferStatus transferStatus;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFactorNumber() {
        return factorNumber;
    }

    public void setFactorNumber(String factorNumber) {
        this.factorNumber = factorNumber;
    }

    public String getIbanNumber() {
        return ibanNumber;
    }

    public void setIbanNumber(String ibanNumber) {
        this.ibanNumber = ibanNumber;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getSourceIbanNumber() {
        return sourceIbanNumber;
    }

    public void setSourceIbanNumber(String sourceIbanNumber) {
        this.sourceIbanNumber = sourceIbanNumber;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public DestinationTransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(DestinationTransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransferDescription() {
        return transferDescription;
    }

    public void setTransferDescription(String transferDescription) {
        this.transferDescription = transferDescription;
    }

    public AchTransferStatus getTransferStatus() {
        return transferStatus;
    }

    public void setTransferStatus(AchTransferStatus transferStatus) {
        this.transferStatus = transferStatus;
    }
}
