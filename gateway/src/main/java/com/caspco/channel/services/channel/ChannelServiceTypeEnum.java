package com.caspco.channel.services.channel;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.HashMap;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 8/15/17
 * Time: 2:20 PM
 */
public @io.swagger.annotations.ApiModel enum ChannelServiceTypeEnum {
    INTERNET_BANK,
    MOBILE_BANK,
    TELEPHONE_BANK,
    THIRDPARTY,
    BOURSE;


    @JsonCreator
    public static ChannelServiceTypeEnum fromStringOperator(HashMap<String, String> map) {
        if (map != null) {
            for (ChannelServiceTypeEnum operator : ChannelServiceTypeEnum.values()) {
                if (map.get("value").equals(operator.name())) {
                    return operator;
                }
            }
        }
        return null;
    }
}
