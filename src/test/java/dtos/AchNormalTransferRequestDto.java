/**
 * ChAchNormalTransferRequestBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class AchNormalTransferRequestDto  implements java.io.Serializable {

    private java.math.BigDecimal amount;

    @ApiModelProperty(value = "secondPasswordType")
    private com.caspco.channel.dtos.SecondPasswordType chSecondPasswordType;

    private java.lang.String cif;

    private java.lang.String description;

    private java.lang.String email;

    private java.lang.String factorNumber;

    private java.lang.String ibanNumber;

    private java.lang.String ownerName;

    private java.lang.String secondPassword;

    private java.lang.String sourceDepositNumber;

    private java.lang.String transferDescription;


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public SecondPasswordType getChSecondPasswordType() {
        return chSecondPasswordType;
    }

    public void setChSecondPasswordType(SecondPasswordType chSecondPasswordType) {
        this.chSecondPasswordType = chSecondPasswordType;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFactorNumber() {
        return factorNumber;
    }

    public void setFactorNumber(String factorNumber) {
        this.factorNumber = factorNumber;
    }

    public String getIbanNumber() {
        return ibanNumber;
    }

    public void setIbanNumber(String ibanNumber) {
        this.ibanNumber = ibanNumber;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getSecondPassword() {
        return secondPassword;
    }

    public void setSecondPassword(String secondPassword) {
        this.secondPassword = secondPassword;
    }

    public String getSourceDepositNumber() {
        return sourceDepositNumber;
    }

    public void setSourceDepositNumber(String sourceDepositNumber) {
        this.sourceDepositNumber = sourceDepositNumber;
    }

    public String getTransferDescription() {
        return transferDescription;
    }

    public void setTransferDescription(String transferDescription) {
        this.transferDescription = transferDescription;
    }
}
