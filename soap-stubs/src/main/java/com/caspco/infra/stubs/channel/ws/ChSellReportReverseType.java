
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chSellReportReverseType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chSellReportReverseType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CUSTOM_REVERSED"/>
 *     &lt;enumeration value="SYSTEM_REVERSED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chSellReportReverseType")
@XmlEnum
public enum ChSellReportReverseType {

    CUSTOM_REVERSED,
    SYSTEM_REVERSED;

    public String value() {
        return name();
    }

    public static ChSellReportReverseType fromValue(String v) {
        return valueOf(v);
    }

}
