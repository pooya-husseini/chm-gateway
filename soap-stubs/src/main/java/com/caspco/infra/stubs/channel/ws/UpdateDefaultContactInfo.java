
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateDefaultContactInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateDefaultContactInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chUpdateDefaultContactInfoRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chUpdateDefaultContactInfoRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateDefaultContactInfo", propOrder = {
    "chUpdateDefaultContactInfoRequestBean"
})
public class UpdateDefaultContactInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChUpdateDefaultContactInfoRequestBean chUpdateDefaultContactInfoRequestBean;

    /**
     * Gets the value of the chUpdateDefaultContactInfoRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChUpdateDefaultContactInfoRequestBean }
     *     
     */
    public ChUpdateDefaultContactInfoRequestBean getChUpdateDefaultContactInfoRequestBean() {
        return chUpdateDefaultContactInfoRequestBean;
    }

    /**
     * Sets the value of the chUpdateDefaultContactInfoRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChUpdateDefaultContactInfoRequestBean }
     *     
     */
    public void setChUpdateDefaultContactInfoRequestBean(ChUpdateDefaultContactInfoRequestBean value) {
        this.chUpdateDefaultContactInfoRequestBean = value;
    }

}
