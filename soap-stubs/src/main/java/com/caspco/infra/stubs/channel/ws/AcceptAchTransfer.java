
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for acceptAchTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="acceptAchTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAcceptAchTransferRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAcceptAchTransferRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "acceptAchTransfer", propOrder = {
    "chAcceptAchTransferRequestBean"
})
public class AcceptAchTransfer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAcceptAchTransferRequestBean chAcceptAchTransferRequestBean;

    /**
     * Gets the value of the chAcceptAchTransferRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAcceptAchTransferRequestBean }
     *     
     */
    public ChAcceptAchTransferRequestBean getChAcceptAchTransferRequestBean() {
        return chAcceptAchTransferRequestBean;
    }

    /**
     * Sets the value of the chAcceptAchTransferRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAcceptAchTransferRequestBean }
     *     
     */
    public void setChAcceptAchTransferRequestBean(ChAcceptAchTransferRequestBean value) {
        this.chAcceptAchTransferRequestBean = value;
    }

}
