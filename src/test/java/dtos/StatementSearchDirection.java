/**
 * ChStatementSearchDirection.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

public @io.swagger.annotations.ApiModel enum StatementSearchDirection {
    START_TO_END,
    END_TO_START

}
