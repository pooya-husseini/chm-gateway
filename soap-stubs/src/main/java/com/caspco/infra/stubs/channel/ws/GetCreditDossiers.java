
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCreditDossiers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCreditDossiers">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSearchCreditDossierRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSearchCreditDossierRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCreditDossiers", propOrder = {
    "chSearchCreditDossierRequestBean"
})
public class GetCreditDossiers
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSearchCreditDossierRequestBean chSearchCreditDossierRequestBean;

    /**
     * Gets the value of the chSearchCreditDossierRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChSearchCreditDossierRequestBean }
     *     
     */
    public ChSearchCreditDossierRequestBean getChSearchCreditDossierRequestBean() {
        return chSearchCreditDossierRequestBean;
    }

    /**
     * Sets the value of the chSearchCreditDossierRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSearchCreditDossierRequestBean }
     *     
     */
    public void setChSearchCreditDossierRequestBean(ChSearchCreditDossierRequestBean value) {
        this.chSearchCreditDossierRequestBean = value;
    }

}
