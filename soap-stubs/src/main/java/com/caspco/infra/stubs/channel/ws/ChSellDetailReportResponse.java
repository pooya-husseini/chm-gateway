
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chSellDetailReportResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chSellDetailReportResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sellDetailReportResponseResults" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSellDetailReportResponseResult" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="totalRecord" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chSellDetailReportResponse", propOrder = {
    "sellDetailReportResponseResults",
    "totalRecord"
})
public class ChSellDetailReportResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(nillable = true)
    protected List<ChSellDetailReportResponseResult> sellDetailReportResponseResults;
    protected Long totalRecord;

    /**
     * Gets the value of the sellDetailReportResponseResults property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sellDetailReportResponseResults property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSellDetailReportResponseResults().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChSellDetailReportResponseResult }
     * 
     * 
     */
    public List<ChSellDetailReportResponseResult> getSellDetailReportResponseResults() {
        if (sellDetailReportResponseResults == null) {
            sellDetailReportResponseResults = new ArrayList<ChSellDetailReportResponseResult>();
        }
        return this.sellDetailReportResponseResults;
    }

    /**
     * Gets the value of the totalRecord property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTotalRecord() {
        return totalRecord;
    }

    /**
     * Sets the value of the totalRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTotalRecord(Long value) {
        this.totalRecord = value;
    }

}
