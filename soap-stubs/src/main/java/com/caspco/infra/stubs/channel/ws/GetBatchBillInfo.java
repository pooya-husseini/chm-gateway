
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBatchBillInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBatchBillInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBatchBillInfoSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBatchBillInfoSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBatchBillInfo", propOrder = {
    "chBatchBillInfoSearchRequestBean"
})
public class GetBatchBillInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBatchBillInfoSearchRequestBean chBatchBillInfoSearchRequestBean;

    /**
     * Gets the value of the chBatchBillInfoSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBatchBillInfoSearchRequestBean }
     *     
     */
    public ChBatchBillInfoSearchRequestBean getChBatchBillInfoSearchRequestBean() {
        return chBatchBillInfoSearchRequestBean;
    }

    /**
     * Sets the value of the chBatchBillInfoSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBatchBillInfoSearchRequestBean }
     *     
     */
    public void setChBatchBillInfoSearchRequestBean(ChBatchBillInfoSearchRequestBean value) {
        this.chBatchBillInfoSearchRequestBean = value;
    }

}
