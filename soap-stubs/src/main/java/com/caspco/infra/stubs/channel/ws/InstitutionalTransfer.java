
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for institutionalTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="institutionalTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chInstitutionalTransferRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chInstitutionalTransferRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "institutionalTransfer", propOrder = {
    "chInstitutionalTransferRequestBean"
})
public class InstitutionalTransfer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChInstitutionalTransferRequestBean chInstitutionalTransferRequestBean;

    /**
     * Gets the value of the chInstitutionalTransferRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChInstitutionalTransferRequestBean }
     *     
     */
    public ChInstitutionalTransferRequestBean getChInstitutionalTransferRequestBean() {
        return chInstitutionalTransferRequestBean;
    }

    /**
     * Sets the value of the chInstitutionalTransferRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChInstitutionalTransferRequestBean }
     *     
     */
    public void setChInstitutionalTransferRequestBean(ChInstitutionalTransferRequestBean value) {
        this.chInstitutionalTransferRequestBean = value;
    }

}
