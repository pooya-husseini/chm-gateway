
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCardStatusCause.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chCardStatusCause">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OK"/>
 *     &lt;enumeration value="STOLEN_CARD"/>
 *     &lt;enumeration value="LOST_CARD"/>
 *     &lt;enumeration value="ARBITRATION_FIAT"/>
 *     &lt;enumeration value="REPLICATED_CARD"/>
 *     &lt;enumeration value="EXPIRED_CARD"/>
 *     &lt;enumeration value="PIN_TRYIES_EXCCEDED"/>
 *     &lt;enumeration value="REPLICATED_CARD_CAPTURED"/>
 *     &lt;enumeration value="OTHER"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chCardStatusCause")
@XmlEnum
public enum ChCardStatusCause {

    OK,
    STOLEN_CARD,
    LOST_CARD,
    ARBITRATION_FIAT,
    REPLICATED_CARD,
    EXPIRED_CARD,
    PIN_TRYIES_EXCCEDED,
    REPLICATED_CARD_CAPTURED,
    OTHER;

    public String value() {
        return name();
    }

    public static ChCardStatusCause fromValue(String v) {
        return valueOf(v);
    }

}
