
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for convertDepositNumberToIban complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="convertDepositNumberToIban">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDepositNumberToIbanRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositNumberToIbanRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "convertDepositNumberToIban", propOrder = {
    "chDepositNumberToIbanRequestBean"
})
public class ConvertDepositNumberToIban
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDepositNumberToIbanRequestBean chDepositNumberToIbanRequestBean;

    /**
     * Gets the value of the chDepositNumberToIbanRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositNumberToIbanRequestBean }
     *     
     */
    public ChDepositNumberToIbanRequestBean getChDepositNumberToIbanRequestBean() {
        return chDepositNumberToIbanRequestBean;
    }

    /**
     * Sets the value of the chDepositNumberToIbanRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositNumberToIbanRequestBean }
     *     
     */
    public void setChDepositNumberToIbanRequestBean(ChDepositNumberToIbanRequestBean value) {
        this.chDepositNumberToIbanRequestBean = value;
    }

}
