
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardPurchaseResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardPurchaseResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardPurchaseResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardPurchaseResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardPurchaseResponse", propOrder = {
    "chCardPurchaseResponseBean"
})
public class CardPurchaseResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardPurchaseResponseBean chCardPurchaseResponseBean;

    /**
     * Gets the value of the chCardPurchaseResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardPurchaseResponseBean }
     *     
     */
    public ChCardPurchaseResponseBean getChCardPurchaseResponseBean() {
        return chCardPurchaseResponseBean;
    }

    /**
     * Sets the value of the chCardPurchaseResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardPurchaseResponseBean }
     *     
     */
    public void setChCardPurchaseResponseBean(ChCardPurchaseResponseBean value) {
        this.chCardPurchaseResponseBean = value;
    }

}
