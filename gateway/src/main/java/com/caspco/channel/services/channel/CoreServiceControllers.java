package com.caspco.channel.services.channel;

import com.caspco.channel.configuration.SmartTransformer;
import com.caspco.channel.services.channel.model.DeactivateUserRequest;
import com.caspco.channel.services.channel.model.LoginDto;
import com.caspco.channel.services.channel.model.LoginResponseDto;
import com.caspco.infra.stubs.channel.ws.*;
import com.caspco.infra.stubs.channel.ws.ChPaymentServiceType;
import com.caspco.infra.stubs.channel.ws.ChPaymentServicesResponseBean;
import com.caspian.moderngateway.core.channelmanagerinfrastructure.exception.ChannelManagerException;
import com.caspian.moderngateway.core.coreservice.dto.*;
import com.caspian.moderngateway.core.coreservice.dto.ChLoanDetailResponseBean;
import com.caspian.moderngateway.core.coreservice.dto.ChLoanDetailSearchRequestBean;
import com.caspian.moderngateway.core.coreservice.dto.ChUserInfoRequestBean;
import com.caspian.moderngateway.core.message.*;
import com.caspian.moderngateway.core.message.sample.EchoMsg;
import com.caspian.moderngateway.spi.service.ChannelManagerProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.rmi.RemoteException;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 8/15/17
 * Time: 12:51 PM
 */

@RestController
@RequestMapping(path = "/core")
public class CoreServiceControllers /*implements CoreServices */ {
    private final SmartTransformer transformer;
    private final CoreService coreService;
    private final ConcurrentHashMap<String,String> hashToken = createMap();
    @Autowired
    private ChannelManagerProvider provider;

//    @Autowired
//    private JmsService jmsService;

    @Autowired
    public CoreServiceControllers(CoreService coreService, SmartTransformer transformer) {
        this.coreService = coreService;
        this.transformer = transformer;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/login")
    public LoginResponseDto login(HttpServletRequest request, @Valid @RequestBody LoginDto loginDto) throws RemoteException, WebserviceGatewayFaultException_Exception {
        com.caspco.infra.stubs.channel.ws.ChLoginResponseBean responseBean = coreService.getService(request).login(loginDto.getRequestBean(), loginDto.getChannelServiceType());
        LoginResponseDto loginResponseDto = transformer.convert(responseBean, LoginResponseDto.class);
        try {
            hashToken.put(loginResponseDto.getSessionId(),setMobileToken(loginDto));
        } catch (ChannelManagerException e) {
            e.printStackTrace();
        }
        return loginResponseDto;
    }


/*    @RequestMapping(method = RequestMethod.POST, path = "/channelLogin")
    public LoginResponseDto channelLogin(HttpServletRequest request, @Valid @RequestBody ChUserInfoRequestBean loginDto) throws RemoteException, WebserviceGatewayFaultException_Exception, ChannelManagerException {
        LoginStaticMsg.Inbound inbound=new LoginStaticMsg.Inbound();
        inbound.setChUserInfoRequestBean(loginDto);
        LoginStaticMsg.Outbound outbound = this.provider.execute(inbound, LoginStaticMsg.Outbound.class);
        return transformer.convert(outbound.getChLoginResponseBean(), LoginResponseDto.class);
    }*/


//    @RequestMapping(method = RequestMethod.POST, path = "/getCustomerInfo")
//    public UserResponse getCustomerInfo(HttpServletRequest servletRequest, @RequestBody UserRequest customerInfoDto)
//            throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCustomerInfo(transformer.convert(customerInfoDto, com.caspco.infra.stubs.channel.ws.ChUserRequestBean.class)), UserResponse.class);
//    }
//
//    private void setHeader(HttpServletRequest servletRequest) {
//        String token = servletRequest.getHeader("token");
//        if (token == null) {
//            token = servletRequest.getParameter("token");
//        }
//        Map<String, String> maps = new Hashtable<>();
//        maps.put("token", token);
//        ((Stub) bankingService)._setProperty(HTTPConstants.REQUEST_HEADERS, maps);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getDeposits")
//    public DepositResponse getDeposits(HttpServletRequest servletRequest, @Valid @RequestBody DepositSearchRequest getDepositsDto) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getDeposits(transformer.convert(getDepositsDto, com.caspco.infra.stubs.channel.ws.ChDepositSearchRequestBean.class)), DepositResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getCardStatementInquiry")
//    public List<CardStatement> getCardStatementInquiry(HttpServletRequest servletRequest, @RequestBody CardStatementRequest dto) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCardStatementInquiry(transformer.convert(dto, com.caspco.infra.stubs.channel.ws.ChCardStatementRequestBean.class)), CardStatement.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getStatement")
//    public StatementResponse getStatement(HttpServletRequest servletRequest, @RequestBody StatementSearchRequest dto) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getStatement(transformer.convert(dto, com.caspco.infra.stubs.channel.ws.ChStatementSearchRequestBean.class)), StatementResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/normalTransfer")
//    public NormalTransferResponse normalTransfer(HttpServletRequest servletRequest, @RequestBody NormalTransferRequest dto) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.normalTransfer(transformer.convert(dto, com.caspco.infra.stubs.channel.ws.ChNormalTransferRequestBean.class)), NormalTransferResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/achNormalTransfer")
//    public NormalAchTransferResponse achNormalTransfer(HttpServletRequest servletRequest, @RequestBody AchNormalTransferRequest dto) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.achNormalTransfer(transformer.convert(dto, com.caspco.infra.stubs.channel.ws.ChAchNormalTransferRequestBean.class)), NormalAchTransferResponse.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/cardIssuingAllowedDepositsToOpen")
//    public List<CardIssuingAllowedDepositsToOpenResponse> cardIssuingAllowedDepositsToOpen(HttpServletRequest servletRequest, CardIssuingAllowedDepositsToOpenRequest chCardIssuingAllowedDepositsToOpenRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.cardIssuingAllowedDepositsToOpen(transformer.convert(chCardIssuingAllowedDepositsToOpenRequest, ChCardIssuingAllowedDepositsToOpenRequestBean.class)), CardIssuingAllowedDepositsToOpenResponse.class);
//    }
//
//    //
//    @RequestMapping(method = RequestMethod.POST, path = "/reportAchTransferSummary")
//    public AchTransferSummery reportAchTransferSummary(HttpServletRequest servletRequest, @RequestBody AchTransferSummeryFilter dto) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.reportAchTransferSummary(transformer.convert(dto, com.caspco.infra.stubs.channel.ws.ChAchTransferSummeryFilterBean.class)), AchTransferSummery.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/getCardProducts")
//    public List<CardProductsResponse> getCardProducts(HttpServletRequest servletRequest, CardProductsRequest chCardProductsRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCardProducts(transformer.convert(chCardProductsRequest,ChCardProductsRequestBean.class)), CardProductsResponse.class);
//    }
//
//    //
//    @RequestMapping(method = RequestMethod.POST, path = "/rtgsNormalTransfer")
//    public NormalRtgsTransferResponse rtgsNormalTransfer(HttpServletRequest servletRequest, @RequestBody RtgsNormalTransferRequest dto) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.rtgsNormalTransfer(transformer.convert(dto, com.caspco.infra.stubs.channel.ws.ChRtgsNormalTransferRequestBean.class)), NormalRtgsTransferResponse.class);
//    }
//
//    //
//    @RequestMapping(method = RequestMethod.POST, path = "/rtgsTransferReport")
//    public RtgsTransferResponse rtgsTransferReport(HttpServletRequest servletRequest, @RequestBody RtgTransferSearchRequest dto) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.rtgsTransferReport(transformer.convert(dto, com.caspco.infra.stubs.channel.ws.ChRtgTransferSearchRequestBean.class)), RtgsTransferResponse.class);
//    }
//
//    //
//    @RequestMapping(method = RequestMethod.POST, path = "/cardTransfer")
//    public FinancialServiceResponse cardTransfer(HttpServletRequest servletRequest, @RequestBody FundTransferRequest dto) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.cardTransfer(transformer.convert(dto, com.caspco.infra.stubs.channel.ws.ChFundTransferRequestBean.class)), FinancialServiceResponse.class);
//    }
//
//    //
//    @RequestMapping(method = RequestMethod.POST, path = "/getCardTransactions")
//    public CardTransactionsResponse getCardTransactions(HttpServletRequest servletRequest, @RequestBody CardTransactionsRequest dto) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCardTransactions(transformer.convert(dto, com.caspco.infra.stubs.channel.ws.ChCardTransactionsRequestBean.class)), CardTransactionsResponse.class);
//    }
//
//    //
//    @RequestMapping(method = RequestMethod.POST, path = "/hotCard")
//    public void hotCard(HttpServletRequest servletRequest, @RequestBody PanRequest dto) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.hotCard(transformer.convert(dto, com.caspco.infra.stubs.channel.ws.ChPanRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/achAutoTransfer")
//    public AchAutoTransferResponse achAutoTransfer(HttpServletRequest servletRequest, @RequestBody AutoAchTransferRequest dto) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.achAutoTransfer(this.transformer.convert(dto, ChAutoAchTransferRequestBean.class)), AchAutoTransferResponse.class);
//    }
//
//
//    ///******************
//
//    @RequestMapping(method = RequestMethod.POST, path = "/achBatchTransfer")
//    public AchBatchTransferResponse achBatchTransfer(HttpServletRequest servletRequest, @Valid @RequestBody BatchAchTransferRequest chBatchAchTransferRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.achBatchTransfer(transformer.convert(chBatchAchTransferRequest, ChBatchAchTransferRequestBean.class)), AchBatchTransferResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getTransferChequeList")
//    public TransferChequesResponse getTransferChequeList(HttpServletRequest servletRequest, @Valid @RequestBody SearchTransferChequeRequest chSearchTransferChequeRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getTransferChequeList(transformer.convert(chSearchTransferChequeRequest, ChSearchTransferChequeRequestBean.class)), TransferChequesResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getAtmStatus")
//    public List<AtmStatusResponse> getAtmStatus(HttpServletRequest servletRequest, @Valid @RequestBody AtmStatusRequest chAtmStatusRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getAtmStatus(transformer.convert(chAtmStatusRequest, ChAtmStatusRequestBean.class)), AtmStatusResponse.class);
//    }
//
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getSellReport")
//    public SellReportResponse getSellReport(HttpServletRequest servletRequest, @Valid @RequestBody SellReportRequest chSellReportRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getSellReport(transformer.convert(chSellReportRequest, ChSellReportRequestBean.class)), SellReportResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/payBill")
//    public BillPaymentResponse payBill(HttpServletRequest servletRequest, @Valid @RequestBody BillPaymentRequest chBillPaymentRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.payBill(transformer.convert(chBillPaymentRequest, ChBillPaymentRequestBean.class)), BillPaymentResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getMerchantInfo")
//    public MerchantInfoResponse getMerchantInfo(HttpServletRequest servletRequest, @Valid @RequestBody MerchantInfoSearchRequest chMerchantInfoSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getMerchantInfo(transformer.convert(chMerchantInfoSearchRequest, ChMerchantInfoSearchRequestBean.class)), MerchantInfoResponse.class);
//    }
//
////    @RequestMapping(method = RequestMethod.POST, path = "/cardLessIssueVoucher")
////    public CardLessIssueVoucherResponse cardLessIssueVoucher(HttpServletRequest servletRequest, @Valid @RequestBody CardLessIssueVoucherRequest chCardLessIssueVoucherRequest) throws RemoteException {
////        setHeader(servletRequest);
////        return transformer.convert(bankingService.cardLessIssueVoucher(transformer.convert(chCardLessIssueVoucherRequest, ChCardLessIssueVoucherRequestBean.class)), CardLessIssueVoucherResponse.class);
////    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/sendMail")
//    public java.lang.String sendMail(HttpServletRequest servletRequest, @Valid @RequestBody MailRequest chMailRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return bankingService.sendMail(transformer.convert(chMailRequest, ChMailRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getRequestStatus")
//    public ReqStatusResponses getRequestStatus(HttpServletRequest servletRequest, @Valid @RequestBody ReqStatusRequest chReqStatusRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getRequestStatus(transformer.convert(chReqStatusRequest, ChReqStatusRequestBean.class)), ReqStatusResponses.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getSignedDeposits")
//    public SignedDepositResponse getSignedDeposits(HttpServletRequest servletRequest, @Valid @RequestBody SignedDepositRequest chSignedDepositRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getSignedDeposits(transformer.convert(chSignedDepositRequest, ChSignedDepositRequestBean.class)), SignedDepositResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/convertDepositNumberToIban")
//    public java.lang.String convertDepositNumberToIban(HttpServletRequest servletRequest, @Valid @RequestBody DepositNumberToIbanRequest chDepositNumberToIbanRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return bankingService.convertDepositNumberToIban(transformer.convert(chDepositNumberToIbanRequest, ChDepositNumberToIbanRequestBean.class));
//
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/suspendAchTransfer")
//    public void suspendAchTransfer(HttpServletRequest servletRequest, @Valid @RequestBody AchTransferKeyRequest chAchTransferKeyRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.suspendAchTransfer(transformer.convert(chAchTransferKeyRequest, ChAchTransferKeyRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getTransferConstraintInfoPerService")
//    public BriefTransferConstraintInfoResponse getTransferConstraintInfoPerService(HttpServletRequest servletRequest, @Valid @RequestBody TransferConstraintRequest transferConstraintRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getTransferConstraintInfoPerService(transformer.convert(transferConstraintRequest, ChTransferConstraintRequestBean.class)), BriefTransferConstraintInfoResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/saveOrUpdateSecondPassword")
//    public void saveOrUpdateSecondPassword(HttpServletRequest servletRequest, @Valid @RequestBody SecondPasswordRequest chSecondPasswordRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.saveOrUpdateSecondPassword(transformer.convert(chSecondPasswordRequest, ChSecondPasswordRequestBean.class));
//    }
//
//
//    @RequestMapping(method = RequestMethod.POST, path = "/addPeriodicBalanceRequest")
//    public java.lang.Long addPeriodicBalanceRequest(HttpServletRequest servletRequest, @Valid @RequestBody PeriodicRequest chPeriodicRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return bankingService.addPeriodicBalanceRequest(transformer.convert(chPeriodicRequest, ChPeriodicRequestBean.class));
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/getBranchAllowedDepositsToOpen")
//    public List<BranchAllowedDepositsToOpenResponse> getBranchAllowedDepositsToOpen(HttpServletRequest servletRequest, BranchAllowedDepositsToOpenRequest chBranchAllowedDepositsToOpenRequest) throws RemoteException {
//        setHeader(servletRequest);
//        ChBranchAllowedDepositsToOpenResponseBean[] response = bankingService.getBranchAllowedDepositsToOpen(transformer.convert(chBranchAllowedDepositsToOpenRequest, ChBranchAllowedDepositsToOpenRequestBean.class));
//        return transformer.convert(response, BranchAllowedDepositsToOpenResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getCreditBillTransactions")
//    public TransactionResponse getCreditBillTransactions(HttpServletRequest servletRequest, @Valid @RequestBody BillTransactionSearchRequest chBillTransactionSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCreditBillTransactions(transformer.convert(chBillTransactionSearchRequest, ChBillTransactionSearchRequestBean.class)), TransactionResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/doCustomRetrun")
//    public CustomReturnResponse doCustomRetrun(HttpServletRequest servletRequest, @Valid @RequestBody CustomReturnRequest chCustomReturnRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.doCustomRetrun(transformer.convert(chCustomReturnRequest, ChCustomReturnRequestBean.class)), CustomReturnResponse.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/createNaturalCustomer")
//    public CreateNaturalCustomerResponse createNaturalCustomer(HttpServletRequest servletRequest, CreateNaturalCustomerRequest chCreateNaturalCustomerRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.createNaturalCustomer(transformer.convert(chCreateNaturalCustomerRequest, ChCreateNaturalCustomerRequestBean.class)), CreateNaturalCustomerResponse.class);
//    }
//
////    @RequestMapping(method = RequestMethod.POST, path = "/getCardLessListVouchersSearch")
////    public CardLessListVouchersSearchResponse getCardLessListVouchersSearch(HttpServletRequest servletRequest, @Valid @RequestBody CardLessListVoucherSearchRequest chCardLessListVoucherSearchRequest) throws RemoteException {
////        setHeader(servletRequest);
////        return transformer.convert(bankingService.getCardLessListVouchersSearch(transformer.convert(chCardLessListVoucherSearchRequest, ChCardLessListVoucherSearchRequestBean.class)), CardLessListVouchersSearchResponse.class);
////    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/switchFundTransferFromVirtualTerminal")
//    public CardTransferResponse switchFundTransferFromVirtualTerminal(HttpServletRequest servletRequest, @Valid @RequestBody VirtualCardTransferRequest chVirtualCardTransferRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.switchFundTransferFromVirtualTerminal(transformer.convert(chVirtualCardTransferRequest, ChVirtualCardTransferRequest.class)), CardTransferResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/switchFundTransferFromPhysicalTerminal")
//    public CardTransferResponse switchFundTransferFromPhysicalTerminal(HttpServletRequest servletRequest, @Valid @RequestBody PhysicalCardTransferRequest chPhysicalCardTransferRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.switchFundTransferFromPhysicalTerminal(transformer.convert(chPhysicalCardTransferRequest, ChPhysicalCardTransferRequest.class)), CardTransferResponse.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/cardExtend")
//    public CardExtendResponse cardExtend(HttpServletRequest servletRequest, CardExtendRequest chCardExtendRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.cardExtend(transformer.convert(chCardExtendRequest, ChCardExtendRequestBean.class)), CardExtendResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getUserBillSetting")
//    public BillSettingResponse getUserBillSetting(HttpServletRequest servletRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getUserBillSetting(), BillSettingResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/blockCheque")
//    public List<BlockingStatus> blockCheque(HttpServletRequest servletRequest, @Valid @RequestBody BlockChequeRequest chBlockChequeRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.blockCheque(transformer.convert(chBlockChequeRequest, ChBlockChequeRequestBean.class)), BlockingStatus.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getTransferConstraintInfo")
//    public BriefTransferConstraintInfoResponse getTransferConstraintInfo(HttpServletRequest servletRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getTransferConstraintInfo(), BriefTransferConstraintInfoResponse.class);
//    }
//
//
//    @RequestMapping(method = RequestMethod.POST, path = "/loginWithMobileNumber")
//    public LoginResponse loginWithMobileNumber(HttpServletRequest servletRequest, @Valid @RequestBody MobileLoginRequest chMobileLoginRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.loginWithMobileNumber(transformer.convert(chMobileLoginRequest, ChMobileLoginRequestBean.class)), LoginResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/achTransferReport")
//    public AchTransfersResponse achTransferReport(HttpServletRequest servletRequest, @Valid @RequestBody AchTransferSearchRequest chAchTransferSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.achTransferReport(transformer.convert(chAchTransferSearchRequest, ChAchTransferSearchRequestBean.class)), AchTransfersResponse.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/inquiryCardIssuing")
//    public InquiryCardIssuingResponse inquiryCardIssuing(HttpServletRequest servletRequest, InquiryCardIssuingRequest chInquiryCardIssuingRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.inquiryCardIssuing(transformer.convert(chInquiryCardIssuingRequest, ChInquiryCardIssuingRequestBean.class)), InquiryCardIssuingResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/signRequestDetails")
//    public SignRequestDetailResponse signRequestDetails(HttpServletRequest servletRequest, @Valid @RequestBody SignRequest chSignRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.signRequestDetails(transformer.convert(chSignRequest, ChSignRequestBean.class)), SignRequestDetailResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/cardChangePin")
//    public void cardChangePin(HttpServletRequest servletRequest, @Valid @RequestBody CardChangePinRequest chCardChangePinRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.cardChangePin(transformer.convert(chCardChangePinRequest, ChCardChangePinRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getPaymentServicesByType")
//    public List<PaymentServiceGroup> getPaymentServicesByType(HttpServletRequest servletRequest, @Valid @RequestBody GenericRequest<PaymentServiceType> payload) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getPaymentServicesByType(transformer.convert(payload.getPayload(), ChPaymentServiceType.class)), PaymentServiceGroup.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getOwnerDepositName")
//    public java.lang.String getOwnerDepositName(HttpServletRequest servletRequest, @Valid @RequestBody OwnerDepositRequest chOwnerDepositRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return bankingService.getOwnerDepositName(transformer.convert(chOwnerDepositRequest, ChOwnerDepositRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/convertIbanToDepositNumber")
//    public java.lang.String convertIbanToDepositNumber(HttpServletRequest servletRequest, @Valid @RequestBody IbanToDepositRequest chIbanToDepositRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return bankingService.convertIbanToDepositNumber(transformer.convert(chIbanToDepositRequest, ChIbanToDepositRequestBean.class));
//    }
//
//
//    @RequestMapping(method = RequestMethod.POST, path = "/logout")
//    public void logout(HttpServletRequest servletRequest, @Valid @RequestBody GenericRequest<ChannelServiceType> channelServiceType) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.logout(transformer.convert(channelServiceType.getPayload(), com.caspco.infra.stubs.channel.ws.ChannelServiceType.class));
//    }
//
//
//    @RequestMapping(method = RequestMethod.POST, path = "/autoTransfer")
//    public AutoTransferResponse autoTransfer(HttpServletRequest servletRequest, @Valid @RequestBody AutoTransferRequest chAutoTransferRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.autoTransfer(transformer.convert(chAutoTransferRequest, ChAutoTransferRequestBean.class)), AutoTransferResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/addCardDeposit")
//    public void addCardDeposit(HttpServletRequest servletRequest, @Valid @RequestBody AddCardDepositRequest chAddCardDepositRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.addCardDeposit(transformer.convert(chAddCardDepositRequest, ChAddCardDepositRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/payBillByAccount")
//    public BillPaymentResponse payBillByAccount(HttpServletRequest servletRequest, @Valid @RequestBody BillPaymentAccountRequest chBillPaymentAccountRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.payBillByAccount(transformer.convert(chBillPaymentAccountRequest, ChBillPaymentAccountRequestBean.class)), BillPaymentResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/institutionalTransfer")
//    public InstitutionalTransferResponse institutionalTransfer(HttpServletRequest servletRequest, @Valid @RequestBody InstitutionalTransferRequest chInstitutionalTransferRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.institutionalTransfer(transformer.convert(chInstitutionalTransferRequest, ChInstitutionalTransferRequestBean.class)), InstitutionalTransferResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getAutoTransferList")
//    public AutoTransferDetailsResponse getAutoTransferList(HttpServletRequest servletRequest, @Valid @RequestBody SearchAutoTransferRequest chSearchAutoTransferRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getAutoTransferList(transformer.convert(chSearchAutoTransferRequest, ChSearchAutoTransferRequestBean.class)), AutoTransferDetailsResponse.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/inquiryNaturalPersonOpenSingleDeposit")
//    public InquiryNaturalPersonOpenSingleDepositResponse inquiryNaturalPersonOpenSingleDeposit(HttpServletRequest servletRequest, InquiryNaturalPersonOpenSingleDepositRequest chInquiryNaturalPersonOpenSingleDepositRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.inquiryNaturalPersonOpenSingleDeposit(transformer.convert(chInquiryNaturalPersonOpenSingleDepositRequest, ChInquiryNaturalPersonOpenSingleDepositRequestBean.class)), InquiryNaturalPersonOpenSingleDepositResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/cancelAchTransfer")
//    public void cancelAchTransfer(HttpServletRequest servletRequest, @Valid @RequestBody AchTransferKeyRequest chAchTransferKeyRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.cancelAchTransfer(transformer.convert(chAchTransferKeyRequest, ChAchTransferKeyRequestBean.class));
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/cardInfoRetrievation")
//    public CardInfoRetrievationResponse cardInfoRetrievation(HttpServletRequest servletRequest, CardInfoRetrievationRequest chCardInfoRetrievationRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.inquiryNaturalPersonOpenSingleDeposit(transformer.convert(chCardInfoRetrievationRequest, ChInquiryNaturalPersonOpenSingleDepositRequestBean.class)), CardInfoRetrievationResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getCards")
//    public List<Card> getCards(HttpServletRequest servletRequest, @Valid @RequestBody CardsSearchRequest chCardsSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCards(transformer.convert(chCardsSearchRequest, ChCardsSearchRequestBean.class)), Card.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getBranchBalance")
//    public BranchBalance getBranchBalance(HttpServletRequest servletRequest, @Valid @RequestBody BranchBalanceSearch chBranchBalanceSearch) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getBranchBalance(transformer.convert(chBranchBalanceSearch, ChBranchBalanceSearchBean.class)), BranchBalance.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getBanks")
//    public BankResponse getBanks(HttpServletRequest servletRequest, @Valid @RequestBody BankSearchRequest chBankSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getBanks(transformer.convert(chBankSearchRequest, ChBankSearchRequestBean.class)), BankResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getEcho")
//    public java.lang.Boolean getEcho(HttpServletRequest servletRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return bankingService.getEcho();
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/topupChargeFromVirtualTerminal")
//    public CardTransferResponse topupChargeFromVirtualTerminal(HttpServletRequest servletRequest, @Valid @RequestBody TopupChargeRequest chTopupChargeRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.topupChargeFromVirtualTerminal(transformer.convert(chTopupChargeRequest, ChTopupChargeRequest.class)), CardTransferResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/cashCheque")
//    public CashChequeResponse cashCheque(HttpServletRequest servletRequest, @Valid @RequestBody CashChequeRequest chCashChequeRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.cashCheque(transformer.convert(chCashChequeRequest, ChCashChequeRequestBean.class)), CashChequeResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getGuarantyList")
//    public GuarantiesResponse getGuarantyList(HttpServletRequest servletRequest, @Valid @RequestBody GuarantiesRequest chGuarantiesRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getGuarantyList(transformer.convert(chGuarantiesRequest, ChGuarantiesRequestBean.class)), GuarantiesResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getCardLimitations")
//    public List<CardLimitationResponse> getCardLimitations(HttpServletRequest servletRequest, @Valid @RequestBody CardLimitRequest chCardLimitRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCardLimitations(transformer.convert(chCardLimitRequest, ChCardLimitRequestBean.class)), CardLimitationResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getDepositCustomer")
//    public DepositCustomerResponse getDepositCustomer(HttpServletRequest servletRequest, @Valid @RequestBody DepositCustomerRequest chDepositCustomerRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getDepositCustomer(transformer.convert(chDepositCustomerRequest, ChDepositCustomerRequestBean.class)), DepositCustomerResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getBeOpenedDeposits")
//    public BeOpenedDepositResponse getBeOpenedDeposits(HttpServletRequest servletRequest, @Valid @RequestBody BeOpenedDepositRequest chBeOpenedDepositRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getBeOpenedDeposits(transformer.convert(chBeOpenedDepositRequest, ChBeOpenedDepositRequestBean.class)), BeOpenedDepositResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/changePassword")
//    public void changePassword(HttpServletRequest servletRequest, @Valid @RequestBody ChangePasswordRequest chChangePasswordRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.changePassword(transformer.convert(chChangePasswordRequest, ChChangePasswordRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getDefaultBillStatement")
//    public DefaultStatementResponse getDefaultBillStatement(HttpServletRequest servletRequest, @Valid @RequestBody DefaultStatementRequest chDefaultStatementRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getDefaultBillStatement(transformer.convert(chDefaultStatementRequest, ChDefaultStatementRequestBean.class)), DefaultStatementResponse.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/cardCreatePassword")
//    public CardCreatePasswordResponse cardCreatePassword(HttpServletRequest servletRequest, CardCreatePasswordRequest chCardCreatePasswordRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.cardCreatePassword(transformer.convert(chCardCreatePasswordRequest, ChCardCreatePasswordRequestBean.class)), CardCreatePasswordResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getSellDetailReport")
//    public SellDetailReportResponse getSellDetailReport(HttpServletRequest servletRequest, @Valid @RequestBody SellDetailRequest chSellDetailRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getSellDetailReport(transformer.convert(chSellDetailRequest, ChSellDetailRequestBean.class)), SellDetailReportResponse.class);
//    }
//
//
//    @RequestMapping(method = RequestMethod.POST, path = "/registerChequeBookRequest")
//    public void registerChequeBookRequest(HttpServletRequest servletRequest, @Valid @RequestBody ChequeBookRequest chChequeBookRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.registerChequeBookRequest(transformer.convert(chChequeBookRequest, ChChequeBookRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/addPeriodicBillRequest")
//    public java.lang.Long addPeriodicBillRequest(HttpServletRequest servletRequest, @Valid @RequestBody PeriodicRequest chPeriodicRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return bankingService.addPeriodicBillRequest(transformer.convert(chPeriodicRequest, ChPeriodicRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/cardPurchase")
//    public CardPurchaseResponse cardPurchase(HttpServletRequest servletRequest, @Valid @RequestBody CardPurchaseRequest chCardPurchaseRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.cardPurchase(transformer.convert(chCardPurchaseRequest, ChCardPurchaseRequestBean.class)), CardPurchaseResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/resumeAchTransaction")
//    public void resumeAchTransaction(HttpServletRequest servletRequest, @Valid @RequestBody AchTransactionKeysRequest chAchTransactionKeysRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.resumeAchTransaction(transformer.convert(chAchTransactionKeysRequest, ChAchTransactionKeysRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getCardOwner")
//    public CardOwnerResponse getCardOwner(HttpServletRequest servletRequest, @Valid @RequestBody CardOwnerRequest chCardOwnerRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCardOwner(transformer.convert(chCardOwnerRequest, ChCardOwnerRequestBean.class)), CardOwnerResponse.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/updateDefaultContactInfo")
//    public void updateDefaultContactInfo(HttpServletRequest servletRequest, UpdateDefaultContactInfoRequest chUpdateDefaultContactInfoRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.cardCreatePassword(transformer.convert(chUpdateDefaultContactInfoRequest, ChCardCreatePasswordRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getServiceConstraintList")
//    public List<ServiceConstraintInformation> getServiceConstraintList(HttpServletRequest servletRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getServiceConstraintList(), ServiceConstraintInformation.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getBranches")
//    public BranchResponse getBranches(HttpServletRequest servletRequest, @Valid @RequestBody BranchSearchRequest chBranchSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getBranches(transformer.convert(chBranchSearchRequest, ChBranchSearchRequestBean.class)), BranchResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getChequeBookList")
//    public ChequeBooksResponse getChequeBookList(HttpServletRequest servletRequest, @Valid @RequestBody BaseSearchChequeBookRequest chBaseSearchChequeBookRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getChequeBookList(transformer.convert(chBaseSearchChequeBookRequest, ChBaseSearchChequeBookRequestBean.class)), ChequeBooksResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getLoanDetail")
//    public LoanDetailResponse getLoanDetail(HttpServletRequest servletRequest, @Valid @RequestBody LoanDetailSearchRequest chLoanDetailSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getLoanDetail(transformer.convert(chLoanDetailSearchRequest, ChLoanDetailSearchRequestBean.class)), LoanDetailResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/cancelAchTransaction")
//    public void cancelAchTransaction(HttpServletRequest servletRequest, @Valid @RequestBody AchTransactionKeysRequest chAchTransactionKeysRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.cancelAchTransaction(transformer.convert(chAchTransactionKeysRequest, ChAchTransactionKeysRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getBatchBillInfo")
//
//    public List<BatchBillInfo> getBatchBillInfo(HttpServletRequest servletRequest, @Valid @RequestBody List<BillInfoSearchRequest> chBatchBillInfoSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        List<ChBillInfoSearchRequestBean> convert = transformer.convert(chBatchBillInfoSearchRequest, ChBillInfoSearchRequestBean.class);
//        return transformer.convert(bankingService.getBatchBillInfo(convert.toArray(new ChBillInfoSearchRequestBean[convert.size()])), BatchBillInfo.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/updateUserBillSetting")
//    public void updateUserBillSetting(HttpServletRequest servletRequest, @Valid @RequestBody BillSettingRequest chBillSettingRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.updateUserBillSetting(transformer.convert(chBillSettingRequest, ChBillSettingRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getUserInfo")
//    public UserResponse getUserInfo(HttpServletRequest servletRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getUserInfo(), UserResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getPaymentPreview")
//    public PaymentPreviewResponse getPaymentPreview(HttpServletRequest servletRequest, @Valid @RequestBody PaymentPreviewRequest chPaymentPreviewRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getPaymentPreview(transformer.convert(chPaymentPreviewRequest, ChPaymentPreviewRequestBean.class)), PaymentPreviewResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/payCreditBill")
//    public CreditBillPaymentResponse payCreditBill(HttpServletRequest servletRequest, @Valid @RequestBody CreditBillPaymentRequest chCreditBillPaymentRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.payCreditBill(transformer.convert(chCreditBillPaymentRequest, ChCreditBillPaymentRequestBean.class)), CreditBillPaymentResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/cardBalanceInquiry")
//    public CardBalanceResponse cardBalanceInquiry(HttpServletRequest servletRequest, @Valid @RequestBody CardBalanceRequest chCardBalanceRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.cardBalanceInquiry(transformer.convert(chCardBalanceRequest, ChCardBalanceRequestBean.class)), CardBalanceResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getFavoriteAccountSetting")
//    public List<FavoriteDepositNumber> getFavoriteAccountSetting(HttpServletRequest servletRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getFavoriteAccountSetting(), FavoriteDepositNumber.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/changeUsername")
//    public void changeUsername(HttpServletRequest servletRequest, @Valid @RequestBody ChangeUsernameRequest chChangeUsernameRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.changeUsername(transformer.convert(chChangeUsernameRequest, ChChangeUsernameRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getUserSMSSetting")
//    public SMSSettingResponse getUserSMSSetting(HttpServletRequest servletRequest, @Valid @RequestBody SMSSettingRequest chSMSSettingRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getUserSMSSetting(transformer.convert(chSMSSettingRequest, ChSMSSettingRequestBean.class)), SMSSettingResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getFavoriteDeposits")
//    public List<CompleteDeposit> getFavoriteDeposits(HttpServletRequest servletRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getFavoriteDeposits(), CompleteDeposit.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getCustomerAddress")
//    public CustomerAddressResponse getCustomerAddress(HttpServletRequest servletRequest, @Valid @RequestBody CustomerAddressRequest chCustomerAddressRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCustomerAddress(transformer.convert(chCustomerAddressRequest, ChCustomerAddressRequestBean.class)), CustomerAddressResponse.class);
//    }
//
@RequestMapping(method = RequestMethod.POST, path = "/getDepositOwnerName")
public GetDepositOwnerNameResponse getDepositOwnerName(HttpServletRequest servletRequest, @Valid @RequestBody com.caspco.infra.stubs.channel.ws.ChDepositOwnerRequestBean chDepositOwnerRequest) throws RemoteException, WebserviceGatewayFaultException_Exception {
    String depositOwnerName = coreService.getService(servletRequest).getDepositOwnerName(chDepositOwnerRequest);
    com.caspco.infra.stubs.channel.ws.GetDepositOwnerNameResponse responseBean = new com.caspco.infra.stubs.channel.ws.GetDepositOwnerNameResponse();
    responseBean.setDepositOwnerName(depositOwnerName);
    return transformer.convert(responseBean, GetDepositOwnerNameResponse.class);

}
//
//    @RequestMapping(method = RequestMethod.POST, path = "/openDeposit")
//    public java.lang.String openDeposit(HttpServletRequest servletRequest, @Valid @RequestBody OpenDepositRequest chOpenDepositRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return bankingService.openDeposit(transformer.convert(chOpenDepositRequest, ChOpenDepositRequestBean.class));
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/naturalPersonOpenSingleDeposit")
//    public NaturalPersonOpenSingleDepositResponse naturalPersonOpenSingleDeposit(HttpServletRequest servletRequest, NaturalPersonOpenSingleDepositRequest chNaturalPersonOpenSingleDepositRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.naturalPersonOpenSingleDeposit(transformer.convert(chNaturalPersonOpenSingleDepositRequest, ChNaturalPersonOpenSingleDepositRequestBean.class)), NaturalPersonOpenSingleDepositResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/payBatchBill")
//    public List<BatchBillPayment> payBatchBill(HttpServletRequest servletRequest, @Valid @RequestBody BatchBillPaymentRequest chBatchBillPaymentRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.payBatchBill(transformer.convert(chBatchBillPaymentRequest, ChBatchBillPaymentRequestBean.class)), BatchBillPayment.class);
//    }
//
////    @RequestMapping(method = RequestMethod.POST, path = "/cardLessAmendVoucher")
////    public void cardLessAmendVoucher(HttpServletRequest servletRequest, @Valid @RequestBody CardLessAmendVoucherRequest chCardLessAmendVoucherRequest) throws RemoteException {
////        setHeader(servletRequest);
////        bankingService.cardLessAmendVoucher(transformer.convert(chCardLessAmendVoucherRequest, ChCardLessAmendVoucherRequestBean.class));
////    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getInstitutions")
//    public InstitutionsResponse getInstitutions(HttpServletRequest servletRequest, @Valid @RequestBody InstitutionRequest chInstitutionRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getInstitutions(transformer.convert(chInstitutionRequest, ChInstitutionRequestBean.class)), InstitutionsResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getCurrencyRate")
//    public CurrencyRateResponse getCurrencyRate(HttpServletRequest servletRequest, @Valid @RequestBody CurrencyRateRequest chCurrencyRateRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCurrencyRate(transformer.convert(chCurrencyRateRequest, ChCurrencyRateRequestBean.class)), CurrencyRateResponse.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/getCustomerInfoListByModifyDate")
//    public List<FullCustomerInfoResponse> getCustomerInfoListByModifyDate(HttpServletRequest servletRequest, FullCustomerInfoRequest chFullCustomerInfoRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCustomerInfoListByModifyDate(transformer.convert(chFullCustomerInfoRequest, ChFullCustomerInfoRequestBean.class)), FullCustomerInfoResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/batchTransfer")
//    public BatchTransferResponse batchTransfer(HttpServletRequest servletRequest, @Valid @RequestBody BatchTransferRequest batchTransferRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.batchTransfer(transformer.convert(batchTransferRequest, ChBatchTransferRequestBean.class)), BatchTransferResponse.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/cardCorrectLinkedAccounts")
//    public CardCorrectLinkedAccountsResponse cardCorrectLinkedAccounts(HttpServletRequest servletRequest, CardCorrectLinkedAccountsRequest chCardCorrectLinkedAccountsRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.cardCorrectLinkedAccounts(transformer.convert(chCardCorrectLinkedAccountsRequest, ChCardCorrectLinkedAccountsRequestBean.class)), CardCorrectLinkedAccountsResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/rtgsTransferDetailReport")
//    public RtgsTransferDetailResponse rtgsTransferDetailReport(HttpServletRequest servletRequest, @Valid @RequestBody RtgTransferDetailSearchRequest chRtgTransferDetailSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.rtgsTransferDetailReport(transformer.convert(chRtgTransferDetailSearchRequest, ChRtgTransferDetailSearchRequestBean.class)), RtgsTransferDetailResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/registerCheque")
//    public void registerCheque(HttpServletRequest servletRequest, @Valid @RequestBody RegisterChequeRequest chRegisterChequeRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.registerCheque(transformer.convert(chRegisterChequeRequest, ChRegisterChequeRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/disableTransfer")
//    public short disableTransfer(HttpServletRequest servletRequest, @Valid @RequestBody DisableTransferRequest chDisableTransferRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return bankingService.disableTransfer(transformer.convert(chDisableTransferRequest, ChDisableTransferRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/hotCardByPin")
//    public void hotCardByPin(HttpServletRequest servletRequest, @Valid @RequestBody HotCardByPinRequest chHotCardByPinRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.hotCardByPin(transformer.convert(chHotCardByPinRequest, ChHotCardByPinRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getPaymentServices")
//    public List<PaymentServiceGroup> getPaymentServices(HttpServletRequest servletRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getPaymentServices(), PaymentServiceGroup.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/customerExistenceInquiry")
//    public CustomerExistenceInquiryResponse customerExistenceInquiry(HttpServletRequest servletRequest, CustomerExistenceInquiryRequest chCustomerExistenceInquiryRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.customerExistenceInquiry(transformer.convert(chCustomerExistenceInquiryRequest, ChCustomerExistenceInquiryRequestBean.class)), CustomerExistenceInquiryResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/removeCardDeposit")
//    public void removeCardDeposit(HttpServletRequest servletRequest, @Valid @RequestBody RemoveCardDepositRequest chRemoveCardDepositRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.removeCardDeposit(transformer.convert(chRemoveCardDepositRequest, ChRemoveCardDepositRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getCreditBills")
//    public BillResponse getCreditBills(HttpServletRequest servletRequest, @Valid @RequestBody BaseCreditBillSearchRequest chBaseCreditBillSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCreditBills(transformer.convert(chBaseCreditBillSearchRequest, ChBaseCreditBillSearchRequestBean.class)), BillResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/resumeAchTransfer")
//    public void resumeAchTransfer(HttpServletRequest servletRequest, @Valid @RequestBody ResumeAchTransferRequest chResumeAchTransferRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.resumeAchTransfer(transformer.convert(chResumeAchTransferRequest, ChResumeAchTransferRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getDestinationCardOwner")
//    public DestinationCardOwnerResponse getDestinationCardOwner(HttpServletRequest servletRequest, @Valid @RequestBody DestinationCardOwnerRequest chDestinationCardOwnerRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getDestinationCardOwner(transformer.convert(chDestinationCardOwnerRequest, ChDestinationCardOwnerRequest.class)), DestinationCardOwnerResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getCheque")
//    public ChequeSheetsResponse getCheque(HttpServletRequest servletRequest, @Valid @RequestBody ChequeSearchRequest chChequeSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCheque(transformer.convert(chChequeSearchRequest, ChChequeSearchRequestBean.class)), ChequeSheetsResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getPaymentBillStatement")
//    public PaymentBillStatementsResponse getPaymentBillStatement(HttpServletRequest servletRequest, @Valid @RequestBody PaymentBillStatementRequest chPaymentBillStatementRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getPaymentBillStatement(transformer.convert(chPaymentBillStatementRequest, ChPaymentBillStatementRequestBean.class)), PaymentBillStatementsResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/suspendAchTransaction")
//    public void suspendAchTransaction(HttpServletRequest servletRequest, @Valid @RequestBody AchTransactionKeysRequest chAchTransactionKeysRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.suspendAchTransaction(transformer.convert(chAchTransactionKeysRequest, ChAchTransactionKeysRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/ibanEnquiry")
//    public IBANEnquiryResponse ibanEnquiry(HttpServletRequest servletRequest, @Valid @RequestBody IBANEnquiryRequest message02) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.ibanEnquiry(transformer.convert(message02, ChIBANEnquiryRequest.class)), IBANEnquiryResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/verifyTransaction")
//    public VerifyTransactionResponse verifyTransaction(HttpServletRequest servletRequest, @Valid @RequestBody VerifyTransactionRequest chVerifyTransactionRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.verifyTransaction(transformer.convert(chVerifyTransactionRequest, ChVerifyTransactionRequestBean.class)), VerifyTransactionResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/updateFavoriteAccountSetting")
//    public void updateFavoriteAccountSetting(HttpServletRequest servletRequest, @Valid @RequestBody List<FavoriteDepositNumber> chFavoriteDepositNumberRequests) throws RemoteException {
//        setHeader(servletRequest);
//        List<ChFavoriteDepositNumberBean> convert = transformer.convert(chFavoriteDepositNumberRequests, ChFavoriteDepositNumberBean.class);
//        bankingService.updateFavoriteAccountSetting(convert.toArray(new ChFavoriteDepositNumberBean[convert.size()]));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getBillInfo")
//    public BillInfoResponse getBillInfo(HttpServletRequest servletRequest, @Valid @RequestBody BillInfoSearchRequest chBillInfoSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getBillInfo(transformer.convert(chBillInfoSearchRequest, ChBillInfoSearchRequestBean.class)), BillInfoResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/doPayment")
//    public PaymentResponse doPayment(HttpServletRequest servletRequest, @Valid @RequestBody PaymentRequest chPaymentRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.doPayment(transformer.convert(chPaymentRequest, ChPaymentRequestBean.class)), PaymentResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/chargeCard")
//    public List<ChargeCardResponse> chargeCard(HttpServletRequest servletRequest, @Valid @RequestBody ChargeCardRequest chChargeCardRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.chargeCard(transformer.convert(chChargeCardRequest, ChChargeCardRequestBean.class)), ChargeCardResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getCardDeposits")
//    public CardDepositResponse getCardDeposits(HttpServletRequest servletRequest, @Valid @RequestBody CardDepositRequest chCardDepositRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCardDeposits(transformer.convert(chCardDepositRequest, ChCardDepositRequestBean.class)), CardDepositResponse.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/blockDepositAmount")
//    public CoreBlockDepositAmountResponse blockDepositAmount(HttpServletRequest servletRequest, CoreBlockDepositAmountRequest arg0) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.blockDepositAmount(transformer.convert(arg0, ChCoreBlockDepositAmountRequestBean.class)), CoreBlockDepositAmountResponse.class);
//    }
//
////    @RequestMapping(method = RequestMethod.POST, path = "/cardLessCancelVoucher")
////    public CardLessCancelVoucherResponse cardLessCancelVoucher(HttpServletRequest servletRequest, @Valid @RequestBody CardLessCancelVoucherRequest chCardLessCancelVoucherRequest) throws RemoteException {
////        setHeader(servletRequest);
////        return transformer.convert(bankingService.cardLessCancelVoucher(transformer.convert(chCardLessCancelVoucherRequest, ChCardLessCancelVoucherRequestBean.class)), CardLessCancelVoucherResponse.class);
////    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/achTransactionReport")
//    public AchTransactionsResponse achTransactionReport(HttpServletRequest servletRequest, @Valid @RequestBody AchTransactionSearchRequest chAchTransactionSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.achTransactionReport(transformer.convert(chAchTransactionSearchRequest, ChAchTransactionSearchRequestBean.class)), AchTransactionsResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getCreditDossiers")
//    public CreditDossierResponses getCreditDossiers(HttpServletRequest servletRequest, @Valid @RequestBody SearchCreditDossierRequest chSearchCreditDossierRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getCreditDossiers(transformer.convert(chSearchCreditDossierRequest, ChSearchCreditDossierRequestBean.class)), CreditDossierResponses.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/defineCustomerSignatureSample")
//    public DefineCustomerSignatureSampleResponse defineCustomerSignatureSample(HttpServletRequest servletRequest, DefineCustomerSignatureSampleRequest chDefineCustomerSignatureSampleRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.defineCustomerSignatureSample(transformer.convert(chDefineCustomerSignatureSampleRequest, ChDefineCustomerSignatureSampleRequestBean.class)), DefineCustomerSignatureSampleResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/searchThirdParty")
//    public List<PayedThirdPartyResponse> searchThirdParty(HttpServletRequest servletRequest, @Valid @RequestBody ThirdPartySearchRequest chThirdPartySearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.searchThirdParty(transformer.convert(chThirdPartySearchRequest, ChThirdPartySearchRequestBean.class)), PayedThirdPartyResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getLoans")
//    public LoansResponse getLoans(HttpServletRequest servletRequest, @Valid @RequestBody LoansSearchRequest chLoansSearchRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getLoans(transformer.convert(chLoansSearchRequest, ChLoansSearchRequestBean.class)), LoansResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/getDepositRates")
//    public DepositRateInquiryResponse getDepositRates(HttpServletRequest servletRequest, @Valid @RequestBody DepositRateInquiryRequest chDepositRateInquiryRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.getDepositRates(transformer.convert(chDepositRateInquiryRequest, ChDepositRateInquiryRequestBean.class)), DepositRateInquiryResponse.class);
//    }
//
//    @Override
//    @RequestMapping(method = RequestMethod.POST, path = "/cardIssuing")
//    public CardIssuingResponse cardIssuing(HttpServletRequest servletRequest, CardIssuingRequest chCardIssuingRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.cardIssuing(transformer.convert(chCardIssuingRequest, ChCardIssuingRequestBean.class)), CardIssuingResponse.class);
//    }
//
//
//    @RequestMapping(method = RequestMethod.POST, path = "/acceptAchTransfer")
//    public void acceptAchTransfer(HttpServletRequest servletRequest, @Valid @RequestBody AcceptAchTransferRequest chAcceptAchTransferRequest) throws RemoteException {
//        setHeader(servletRequest);
//        bankingService.acceptAchTransfer(transformer.convert(chAcceptAchTransferRequest, ChAcceptAchTransferRequestBean.class));
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/payLoan")
//    public LoanPaymentResponse payLoan(HttpServletRequest servletRequest, @Valid @RequestBody LoanPaymentRequest chLoanPaymentRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.payLoan(transformer.convert(chLoanPaymentRequest, ChLoanPaymentRequestBean.class)), LoanPaymentResponse.class);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/extendSMSSetting")
//    public UserSMSSettingResponse extendSMSSetting(HttpServletRequest servletRequest, @Valid @RequestBody UserSMSSettingRequest chUserSMSSettingRequest) throws RemoteException {
//        setHeader(servletRequest);
//        return transformer.convert(bankingService.extendSMSSetting(transformer.convert(chUserSMSSettingRequest, ChUserSMSSettingRequestBean.class)), UserSMSSettingResponse.class);
//    }

    //    @RequestMapping(method = RequestMethod.POST, path = "/logout")
//    public void logout(@RequestBody ChCardStatementRequestBean dto) throws RemoteException {
//        bankingService.logout(dto);
//    }
    @RequestMapping(method = RequestMethod.POST, path = "/deactivateModernGatewayUser")
    public void deactivateModernGatewayUser(HttpServletRequest request, @Valid @RequestBody DeactivateUserRequest dto) throws RemoteException, WebserviceGatewayFaultException_Exception {
        coreService.getService(request).deactivateModernGatewayUser(dto.getUsername(), dto.getChannelType());
    }

    @RequestMapping(method = RequestMethod.POST, path = "/getPaymentServicesByType")
    public ChPaymentServicesResponseBean getPaymentServicesByType(HttpServletRequest request, @Valid @RequestBody GenericRequest<ChPaymentServiceType> dto) throws RemoteException, WebserviceGatewayFaultException_Exception {
        return coreService.getService(request).getPaymentServicesByType(dto.getPayload());
    }

    @RequestMapping(method = RequestMethod.POST, path = "/logout")
    public void logout(HttpServletRequest request, @Valid @RequestBody GenericRequest<ChannelServiceType> dto) throws RemoteException, WebserviceGatewayFaultException_Exception {
        coreService.getService(request).logout(dto.getPayload());
    }

    //    @RequestMapping(method = RequestMethod.POST, path = "/getStatement")
//    public ChStatementResponseBean getStatement(HttpServletRequest servletRequest, @RequestBody ChStatementSearchRequestBean dto) throws RemoteException {
////        setHeader(servletRequest);
////        return transformer.convert(bankingService.getStatement(transformer.convert(dto, com.caspco.infra.stubs.channel.ws.ChStatementSearchRequestBean.class)), StatementResponse.class);
//
//        return new ChStatementResponseBean();
//    }
    @RequestMapping(method = RequestMethod.POST, path = "/cardLessListVoucherSearch")
    public ChCardLessListVouchersSearchResponseBean cardLessListVoucherSearch(HttpServletRequest request, @Valid @RequestBody ChCardLessListVoucherSearchRequestBean dto) throws ChannelManagerException {
        CardLessListVoucherSearchMsg.Inbound input = new CardLessListVoucherSearchMsg.Inbound();
        input.setRequestBean(dto);
        String token = getToken(request);
        CardLessListVoucherSearchMsg.Outbound outbound = provider.execute(
                input,
                CardLessListVoucherSearchMsg.Outbound.class, hashToken.get(token));
        return outbound.getResponseBean();
    }

    @RequestMapping(method = RequestMethod.POST, path = "/cardLessIssueVoucher")
    public ChCardLessIssueVoucherResponseBean cardLessIssueVoucher(HttpServletRequest request, @Valid @RequestBody ChCardLessIssueVoucherRequestBean dto) throws ChannelManagerException {
        CardLessIssueVoucherMsg.Inbound input = new CardLessIssueVoucherMsg.Inbound();
        input.setRequestBean(dto);
        String token = getToken(request);
        CardLessIssueVoucherMsg.Outbound outbound = provider.execute(input,
                CardLessIssueVoucherMsg.Outbound.class,hashToken.get(token));
        return outbound.getResponseBean();
    }

    @RequestMapping(method = RequestMethod.POST, path = "/cardLessCancelVoucher")
    public ChCardLessCancelVoucherResponseBean cardLessCancelVoucher(HttpServletRequest request, @Valid @RequestBody ChCardLessCancelVoucherRequestBean dto) throws ChannelManagerException {
        CardLessCancelVoucherMsg.Inbound input = new CardLessCancelVoucherMsg.Inbound();
        input.setRequestBean(dto);
        String token = getToken(request);
        CardLessCancelVoucherMsg.Outbound outbound = provider.execute(
                input,
                CardLessCancelVoucherMsg.Outbound.class,hashToken.get(token));
        return outbound.getResponseBean();
    }

    @RequestMapping(method = RequestMethod.POST, path = "/cardLessAmendVoucher")
    public CardLessAmendVoucherMsg.Outbound cardLessAmendVoucher(HttpServletRequest request, @Valid @RequestBody ChCardLessAmendVoucherRequestBean dto) throws ChannelManagerException {
        CardLessAmendVoucherMsg.Inbound input = new CardLessAmendVoucherMsg.Inbound();
        input.setRequestBean(dto);
        String token = getToken(request);
        CardLessAmendVoucherMsg.Outbound outbound = provider.execute(
                input,
                CardLessAmendVoucherMsg.Outbound.class,hashToken.get(token));
        return outbound;
    }


    @RequestMapping(method = RequestMethod.POST, path = "/getLoanOwnerName")
    public com.caspian.moderngateway.core.coreservice.dto.ChLoanDetailResponseBean getLoanOwnerName(HttpServletRequest request, @Valid @RequestBody com.caspian.moderngateway.core.coreservice.dto.ChLoanDetailSearchRequestBean dto) throws ChannelManagerException {
        GetLoanDetailMsg.Inbound input = new GetLoanDetailMsg.Inbound();
        input.setRequestBean(dto);
        String token = getToken(request);
        GetLoanDetailMsg.Outbound outbound = provider.execute(
                input,
                GetLoanDetailMsg.Outbound.class,hashToken.get(token));
        return outbound.getResponseBean();
    }


    @RequestMapping(method = RequestMethod.POST, path = "/bookTurn")
    public BookTurnMsg.Outbound bookTurn(HttpServletRequest request, @Valid @RequestBody BookTurnMsg.Inbound dto) throws ChannelManagerException {
        BookTurnMsg.Inbound input = new BookTurnMsg.Inbound();
        String token = getToken(request);
        BookTurnMsg.Outbound outbound = provider.execute(
                input,
                BookTurnMsg.Outbound.class,hashToken.get(token));
        return outbound;
    }

/*    @RequestMapping(method = RequestMethod.POST, path = "/echoMsg")
    public EchoMsg.Outbound echoMsg(HttpServletRequest request, @Valid @RequestBody EchoMsg.Inbound dto) throws ChannelManagerException {
        EchoMsg.Inbound input = new EchoMsg.Inbound();
        EchoMsg.Outbound outbound = provider.execute(
                input,
                EchoMsg.Outbound.class);
        return outbound;
    }*/

    public String getToken(HttpServletRequest request) {
        String token = request.getHeader("token");
        if (token == null) {
            token = request.getParameter("token");
        }
        return token;
    }


    private String setMobileToken(LoginDto loginDto) throws ChannelManagerException {
        LoginStaticMsg.Inbound inbound=new LoginStaticMsg.Inbound();
        ChUserInfoRequestBean dto  = new ChOtpLoginRequestBean();
        dto.setUsername(loginDto.getRequestBean().getUsername());
        dto.setPassword(loginDto.getRequestBean().getPassword());
        inbound.setChUserInfoRequestBean(dto);
        LoginStaticMsg.Outbound outbound = this.provider.execute(inbound, LoginStaticMsg.Outbound.class);
        LoginResponseDto loginResponseDto = transformer.convert(outbound.getChLoginResponseBean(), LoginResponseDto.class);
        return loginResponseDto.getSessionId();
    }

    private static ConcurrentHashMap<String, String> createMap() {
        ConcurrentHashMap<String, String> concurrentHashMap = new ConcurrentHashMap<String, String>();
        return concurrentHashMap;
    }


}