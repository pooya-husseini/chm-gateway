
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCreditDossiersResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCreditDossiersResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCreditDossierResponseBeans" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCreditDossierResponseBeans" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCreditDossiersResponse", propOrder = {
    "chCreditDossierResponseBeans"
})
public class GetCreditDossiersResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCreditDossierResponseBeans chCreditDossierResponseBeans;

    /**
     * Gets the value of the chCreditDossierResponseBeans property.
     * 
     * @return
     *     possible object is
     *     {@link ChCreditDossierResponseBeans }
     *     
     */
    public ChCreditDossierResponseBeans getChCreditDossierResponseBeans() {
        return chCreditDossierResponseBeans;
    }

    /**
     * Sets the value of the chCreditDossierResponseBeans property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCreditDossierResponseBeans }
     *     
     */
    public void setChCreditDossierResponseBeans(ChCreditDossierResponseBeans value) {
        this.chCreditDossierResponseBeans = value;
    }

}
