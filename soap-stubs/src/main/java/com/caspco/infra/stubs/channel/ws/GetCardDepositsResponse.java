
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardDepositsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardDepositsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardDepositResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardDepositResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardDepositsResponse", propOrder = {
    "chCardDepositResponseBean"
})
public class GetCardDepositsResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardDepositResponseBean chCardDepositResponseBean;

    /**
     * Gets the value of the chCardDepositResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardDepositResponseBean }
     *     
     */
    public ChCardDepositResponseBean getChCardDepositResponseBean() {
        return chCardDepositResponseBean;
    }

    /**
     * Sets the value of the chCardDepositResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardDepositResponseBean }
     *     
     */
    public void setChCardDepositResponseBean(ChCardDepositResponseBean value) {
        this.chCardDepositResponseBean = value;
    }

}
