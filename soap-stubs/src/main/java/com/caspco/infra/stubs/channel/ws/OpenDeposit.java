
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for openDeposit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="openDeposit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chOpenDepositRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chOpenDepositRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "openDeposit", propOrder = {
    "chOpenDepositRequestBean"
})
public class OpenDeposit
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChOpenDepositRequestBean chOpenDepositRequestBean;

    /**
     * Gets the value of the chOpenDepositRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChOpenDepositRequestBean }
     *     
     */
    public ChOpenDepositRequestBean getChOpenDepositRequestBean() {
        return chOpenDepositRequestBean;
    }

    /**
     * Sets the value of the chOpenDepositRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChOpenDepositRequestBean }
     *     
     */
    public void setChOpenDepositRequestBean(ChOpenDepositRequestBean value) {
        this.chOpenDepositRequestBean = value;
    }

}
