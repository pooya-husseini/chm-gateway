
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCardIssueCause.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chCardIssueCause">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FIRST_ISSUE"/>
 *     &lt;enumeration value="FAILURED"/>
 *     &lt;enumeration value="REDUPLICATE"/>
 *     &lt;enumeration value="PRE_PAID"/>
 *     &lt;enumeration value="SALED_PRE_PAID"/>
 *     &lt;enumeration value="WEB_GOODS_CARD"/>
 *     &lt;enumeration value="NO_NAME_CHARGABLE"/>
 *     &lt;enumeration value="FUEL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chCardIssueCause")
@XmlEnum
public enum ChCardIssueCause {

    FIRST_ISSUE,
    FAILURED,
    REDUPLICATE,
    PRE_PAID,
    SALED_PRE_PAID,
    WEB_GOODS_CARD,
    NO_NAME_CHARGABLE,
    FUEL;

    public String value() {
        return name();
    }

    public static ChCardIssueCause fromValue(String v) {
        return valueOf(v);
    }

}
