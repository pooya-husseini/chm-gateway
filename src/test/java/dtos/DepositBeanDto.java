/**
 * ChDepositBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

public class DepositBeanDto  implements java.io.Serializable {

    private java.math.BigDecimal availableBalance;

    private java.math.BigDecimal balance;

    private java.math.BigDecimal blockedAmount;

    private java.lang.String branchCode;

    private java.lang.String creditDeposit;

    private java.math.BigDecimal creditLoanRemainAmount;

    private java.lang.Double creditRateAmount;

    private java.math.BigDecimal creditRemainAmount;

    private java.lang.String currency;

    private java.lang.String dayOfDepositInterest;

    private java.lang.String depositNumber;

    @ApiModelProperty(value = "depositStatus")
    private com.caspco.channel.dtos.DepositStatus depositStatus;

    private java.lang.String depositTitle;

    private java.util.Date expireDate;

    private java.math.BigDecimal extraAvailableBalance;

    @ApiModelProperty(value = "depositGroupType")
    private com.caspco.channel.dtos.DepositGroupType group;

    private java.util.Date inaugurationDate;

    private java.lang.String interestAccount;

    private java.lang.Boolean lotusFlag;

    private java.math.BigDecimal maximumBalance;

    private java.math.BigDecimal minimumBalance;

    @ApiModelProperty(value = "depositOwnerType")
    private com.caspco.channel.dtos.DepositOwnerType owner;

    private java.lang.String owners;

    @ApiModelProperty(value = "personalityType")
    private com.caspco.channel.dtos.PersonalityType personality;

    @ApiModelProperty(value = "signatureOwnerStatus")
    private com.caspco.channel.dtos.SignatureOwnerStatus signature;

    private java.lang.String supportCurrency;

    private java.lang.String supportDepositNumber;

    @ApiModelProperty(value = "depositStatus")
    private com.caspco.channel.dtos.DepositStatus supportDepositStatus;

    @ApiModelProperty(value = "supportStatus")
    private com.caspco.channel.dtos.SupportStatus supportStatus;

    @ApiModelProperty(value = "withdrawalOption")
    private com.caspco.channel.dtos.WithdrawalOption withdrawalOption;


    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBlockedAmount() {
        return blockedAmount;
    }

    public void setBlockedAmount(BigDecimal blockedAmount) {
        this.blockedAmount = blockedAmount;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getCreditDeposit() {
        return creditDeposit;
    }

    public void setCreditDeposit(String creditDeposit) {
        this.creditDeposit = creditDeposit;
    }

    public BigDecimal getCreditLoanRemainAmount() {
        return creditLoanRemainAmount;
    }

    public void setCreditLoanRemainAmount(BigDecimal creditLoanRemainAmount) {
        this.creditLoanRemainAmount = creditLoanRemainAmount;
    }

    public Double getCreditRateAmount() {
        return creditRateAmount;
    }

    public void setCreditRateAmount(Double creditRateAmount) {
        this.creditRateAmount = creditRateAmount;
    }

    public BigDecimal getCreditRemainAmount() {
        return creditRemainAmount;
    }

    public void setCreditRemainAmount(BigDecimal creditRemainAmount) {
        this.creditRemainAmount = creditRemainAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDayOfDepositInterest() {
        return dayOfDepositInterest;
    }

    public void setDayOfDepositInterest(String dayOfDepositInterest) {
        this.dayOfDepositInterest = dayOfDepositInterest;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public DepositStatus getDepositStatus() {
        return depositStatus;
    }

    public void setDepositStatus(DepositStatus depositStatus) {
        this.depositStatus = depositStatus;
    }

    public String getDepositTitle() {
        return depositTitle;
    }

    public void setDepositTitle(String depositTitle) {
        this.depositTitle = depositTitle;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public BigDecimal getExtraAvailableBalance() {
        return extraAvailableBalance;
    }

    public void setExtraAvailableBalance(BigDecimal extraAvailableBalance) {
        this.extraAvailableBalance = extraAvailableBalance;
    }

    public DepositGroupType getGroup() {
        return group;
    }

    public void setGroup(DepositGroupType group) {
        this.group = group;
    }

    public Date getInaugurationDate() {
        return inaugurationDate;
    }

    public void setInaugurationDate(Date inaugurationDate) {
        this.inaugurationDate = inaugurationDate;
    }

    public String getInterestAccount() {
        return interestAccount;
    }

    public void setInterestAccount(String interestAccount) {
        this.interestAccount = interestAccount;
    }

    public Boolean getLotusFlag() {
        return lotusFlag;
    }

    public void setLotusFlag(Boolean lotusFlag) {
        this.lotusFlag = lotusFlag;
    }

    public BigDecimal getMaximumBalance() {
        return maximumBalance;
    }

    public void setMaximumBalance(BigDecimal maximumBalance) {
        this.maximumBalance = maximumBalance;
    }

    public BigDecimal getMinimumBalance() {
        return minimumBalance;
    }

    public void setMinimumBalance(BigDecimal minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

    public DepositOwnerType getOwner() {
        return owner;
    }

    public void setOwner(DepositOwnerType owner) {
        this.owner = owner;
    }

    public String getOwners() {
        return owners;
    }

    public void setOwners(String owners) {
        this.owners = owners;
    }

    public PersonalityType getPersonality() {
        return personality;
    }

    public void setPersonality(PersonalityType personality) {
        this.personality = personality;
    }

    public SignatureOwnerStatus getSignature() {
        return signature;
    }

    public void setSignature(SignatureOwnerStatus signature) {
        this.signature = signature;
    }

    public String getSupportCurrency() {
        return supportCurrency;
    }

    public void setSupportCurrency(String supportCurrency) {
        this.supportCurrency = supportCurrency;
    }

    public String getSupportDepositNumber() {
        return supportDepositNumber;
    }

    public void setSupportDepositNumber(String supportDepositNumber) {
        this.supportDepositNumber = supportDepositNumber;
    }

    public DepositStatus getSupportDepositStatus() {
        return supportDepositStatus;
    }

    public void setSupportDepositStatus(DepositStatus supportDepositStatus) {
        this.supportDepositStatus = supportDepositStatus;
    }

    public SupportStatus getSupportStatus() {
        return supportStatus;
    }

    public void setSupportStatus(SupportStatus supportStatus) {
        this.supportStatus = supportStatus;
    }

    public WithdrawalOption getWithdrawalOption() {
        return withdrawalOption;
    }

    public void setWithdrawalOption(WithdrawalOption withdrawalOption) {
        this.withdrawalOption = withdrawalOption;
    }
}
