
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCurrencyRateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCurrencyRateResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCurrencyRateResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCurrencyRateResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCurrencyRateResponse", propOrder = {
    "chCurrencyRateResponseBean"
})
public class GetCurrencyRateResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCurrencyRateResponseBean chCurrencyRateResponseBean;

    /**
     * Gets the value of the chCurrencyRateResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCurrencyRateResponseBean }
     *     
     */
    public ChCurrencyRateResponseBean getChCurrencyRateResponseBean() {
        return chCurrencyRateResponseBean;
    }

    /**
     * Sets the value of the chCurrencyRateResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCurrencyRateResponseBean }
     *     
     */
    public void setChCurrencyRateResponseBean(ChCurrencyRateResponseBean value) {
        this.chCurrencyRateResponseBean = value;
    }

}
