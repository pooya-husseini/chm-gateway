
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for signRequestDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="signRequestDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSignRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSignRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "signRequestDetails", propOrder = {
    "chSignRequestBean"
})
public class SignRequestDetails
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSignRequestBean chSignRequestBean;

    /**
     * Gets the value of the chSignRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChSignRequestBean }
     *     
     */
    public ChSignRequestBean getChSignRequestBean() {
        return chSignRequestBean;
    }

    /**
     * Sets the value of the chSignRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSignRequestBean }
     *     
     */
    public void setChSignRequestBean(ChSignRequestBean value) {
        this.chSignRequestBean = value;
    }

}
