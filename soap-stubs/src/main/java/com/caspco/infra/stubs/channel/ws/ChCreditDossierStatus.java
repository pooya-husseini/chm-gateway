
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCreditDossierStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chCreditDossierStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="GENERATED"/>
 *     &lt;enumeration value="FEE_PAID"/>
 *     &lt;enumeration value="APPROVED"/>
 *     &lt;enumeration value="CARD_REQUESTED"/>
 *     &lt;enumeration value="ACTIVE"/>
 *     &lt;enumeration value="BLOCKED"/>
 *     &lt;enumeration value="SETTLEMENT_REQUESTED"/>
 *     &lt;enumeration value="CLOSED"/>
 *     &lt;enumeration value="EXPIRED"/>
 *     &lt;enumeration value="CONTRACT_ISSUED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chCreditDossierStatus")
@XmlEnum
public enum ChCreditDossierStatus {

    GENERATED,
    FEE_PAID,
    APPROVED,
    CARD_REQUESTED,
    ACTIVE,
    BLOCKED,
    SETTLEMENT_REQUESTED,
    CLOSED,
    EXPIRED,
    CONTRACT_ISSUED;

    public String value() {
        return name();
    }

    public static ChCreditDossierStatus fromValue(String v) {
        return valueOf(v);
    }

}
