
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getTransferConstraintInfoPerService complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTransferConstraintInfoPerService">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transferConstraintRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chTransferConstraintRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransferConstraintInfoPerService", propOrder = {
    "transferConstraintRequestBean"
})
public class GetTransferConstraintInfoPerService
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChTransferConstraintRequestBean transferConstraintRequestBean;

    /**
     * Gets the value of the transferConstraintRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChTransferConstraintRequestBean }
     *     
     */
    public ChTransferConstraintRequestBean getTransferConstraintRequestBean() {
        return transferConstraintRequestBean;
    }

    /**
     * Sets the value of the transferConstraintRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChTransferConstraintRequestBean }
     *     
     */
    public void setTransferConstraintRequestBean(ChTransferConstraintRequestBean value) {
        this.transferConstraintRequestBean = value;
    }

}
