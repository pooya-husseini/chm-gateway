/**
 * ChAchTransferStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.HashMap;

public @io.swagger.annotations.ApiModel enum AchTransferStatus implements java.io.Serializable {
    WAIT_FOR_CUSTOMER_ACCEPT,
    WAIT_FOR_BRANCH_ACCEPT,
    BRANCH_REJECT,
    READY_TO_TRANSFER,
    SUSPEND,
    CANCEL,
    PROCESSED;

    @JsonCreator
    public static AchTransferStatus fromStringOperator(HashMap<String, String> map) {
        if (map != null) {
            for (AchTransferStatus operator : AchTransferStatus.values()) {
                if (map.get("value").equals(operator.name())) {
                    return operator;
                }
            }
        }
        return null;
    }
}
