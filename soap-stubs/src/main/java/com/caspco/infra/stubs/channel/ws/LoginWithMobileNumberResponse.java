
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for loginWithMobileNumberResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loginWithMobileNumberResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chLoginResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chLoginResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loginWithMobileNumberResponse", propOrder = {
    "chLoginResponseBean"
})
public class LoginWithMobileNumberResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChLoginResponseBean chLoginResponseBean;

    /**
     * Gets the value of the chLoginResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChLoginResponseBean }
     *     
     */
    public ChLoginResponseBean getChLoginResponseBean() {
        return chLoginResponseBean;
    }

    /**
     * Sets the value of the chLoginResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChLoginResponseBean }
     *     
     */
    public void setChLoginResponseBean(ChLoginResponseBean value) {
        this.chLoginResponseBean = value;
    }

}
