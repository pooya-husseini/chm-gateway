
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardDeposits complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardDeposits">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardDepositRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardDepositRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardDeposits", propOrder = {
    "chCardDepositRequestBean"
})
public class GetCardDeposits
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardDepositRequestBean chCardDepositRequestBean;

    /**
     * Gets the value of the chCardDepositRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardDepositRequestBean }
     *     
     */
    public ChCardDepositRequestBean getChCardDepositRequestBean() {
        return chCardDepositRequestBean;
    }

    /**
     * Sets the value of the chCardDepositRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardDepositRequestBean }
     *     
     */
    public void setChCardDepositRequestBean(ChCardDepositRequestBean value) {
        this.chCardDepositRequestBean = value;
    }

}
