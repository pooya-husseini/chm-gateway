
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for changeUsername complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="changeUsername">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chChangeUsernameRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chChangeUsernameRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeUsername", propOrder = {
    "chChangeUsernameRequestBean"
})
public class ChangeUsername
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChChangeUsernameRequestBean chChangeUsernameRequestBean;

    /**
     * Gets the value of the chChangeUsernameRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChChangeUsernameRequestBean }
     *     
     */
    public ChChangeUsernameRequestBean getChChangeUsernameRequestBean() {
        return chChangeUsernameRequestBean;
    }

    /**
     * Sets the value of the chChangeUsernameRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChChangeUsernameRequestBean }
     *     
     */
    public void setChChangeUsernameRequestBean(ChChangeUsernameRequestBean value) {
        this.chChangeUsernameRequestBean = value;
    }

}
