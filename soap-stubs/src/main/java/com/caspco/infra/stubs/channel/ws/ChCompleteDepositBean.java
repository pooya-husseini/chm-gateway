
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCompleteDepositBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chCompleteDepositBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="branch" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBranchBean" minOccurs="0"/>
 *         &lt;element name="creditDeposit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="creditDepositRemainAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="creditLoanRemainAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="creditProfitRemainAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="creditRateAmount" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="creditRemainAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="deposit" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositDetailBean" minOccurs="0"/>
 *         &lt;element name="depositTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="group" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositGroupType" minOccurs="0"/>
 *         &lt;element name="interestPayDay" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="owner" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositOwnerType" minOccurs="0"/>
 *         &lt;element name="paymentPeriod" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="personality" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPersonalityType" minOccurs="0"/>
 *         &lt;element name="signature" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSignatureOwnerStatus" minOccurs="0"/>
 *         &lt;element name="support" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositDetailBean" minOccurs="0"/>
 *         &lt;element name="supportGroupType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositGroupType" minOccurs="0"/>
 *         &lt;element name="supportStatus" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSupportStatus" minOccurs="0"/>
 *         &lt;element name="supportVisibility" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="withdrawalOption" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chWithdrawalOption" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chCompleteDepositBean", propOrder = {
    "branch",
    "creditDeposit",
    "creditDepositRemainAmount",
    "creditLoanRemainAmount",
    "creditProfitRemainAmount",
    "creditRateAmount",
    "creditRemainAmount",
    "deposit",
    "depositTitle",
    "group",
    "interestPayDay",
    "owner",
    "paymentPeriod",
    "personality",
    "signature",
    "support",
    "supportGroupType",
    "supportStatus",
    "supportVisibility",
    "withdrawalOption"
})
public class ChCompleteDepositBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBranchBean branch;
    protected String creditDeposit;
    protected BigDecimal creditDepositRemainAmount;
    protected BigDecimal creditLoanRemainAmount;
    protected BigDecimal creditProfitRemainAmount;
    protected Double creditRateAmount;
    protected BigDecimal creditRemainAmount;
    protected ChDepositDetailBean deposit;
    protected String depositTitle;
    @XmlSchemaType(name = "string")
    protected ChDepositGroupType group;
    protected Long interestPayDay;
    @XmlSchemaType(name = "string")
    protected ChDepositOwnerType owner;
    protected Long paymentPeriod;
    @XmlSchemaType(name = "string")
    protected ChPersonalityType personality;
    @XmlSchemaType(name = "string")
    protected ChSignatureOwnerStatus signature;
    protected ChDepositDetailBean support;
    @XmlSchemaType(name = "string")
    protected ChDepositGroupType supportGroupType;
    @XmlSchemaType(name = "string")
    protected ChSupportStatus supportStatus;
    protected Boolean supportVisibility;
    @XmlSchemaType(name = "string")
    protected ChWithdrawalOption withdrawalOption;

    /**
     * Gets the value of the branch property.
     * 
     * @return
     *     possible object is
     *     {@link ChBranchBean }
     *     
     */
    public ChBranchBean getBranch() {
        return branch;
    }

    /**
     * Sets the value of the branch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBranchBean }
     *     
     */
    public void setBranch(ChBranchBean value) {
        this.branch = value;
    }

    /**
     * Gets the value of the creditDeposit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditDeposit() {
        return creditDeposit;
    }

    /**
     * Sets the value of the creditDeposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditDeposit(String value) {
        this.creditDeposit = value;
    }

    /**
     * Gets the value of the creditDepositRemainAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditDepositRemainAmount() {
        return creditDepositRemainAmount;
    }

    /**
     * Sets the value of the creditDepositRemainAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditDepositRemainAmount(BigDecimal value) {
        this.creditDepositRemainAmount = value;
    }

    /**
     * Gets the value of the creditLoanRemainAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditLoanRemainAmount() {
        return creditLoanRemainAmount;
    }

    /**
     * Sets the value of the creditLoanRemainAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditLoanRemainAmount(BigDecimal value) {
        this.creditLoanRemainAmount = value;
    }

    /**
     * Gets the value of the creditProfitRemainAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditProfitRemainAmount() {
        return creditProfitRemainAmount;
    }

    /**
     * Sets the value of the creditProfitRemainAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditProfitRemainAmount(BigDecimal value) {
        this.creditProfitRemainAmount = value;
    }

    /**
     * Gets the value of the creditRateAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCreditRateAmount() {
        return creditRateAmount;
    }

    /**
     * Sets the value of the creditRateAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCreditRateAmount(Double value) {
        this.creditRateAmount = value;
    }

    /**
     * Gets the value of the creditRemainAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditRemainAmount() {
        return creditRemainAmount;
    }

    /**
     * Sets the value of the creditRemainAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditRemainAmount(BigDecimal value) {
        this.creditRemainAmount = value;
    }

    /**
     * Gets the value of the deposit property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositDetailBean }
     *     
     */
    public ChDepositDetailBean getDeposit() {
        return deposit;
    }

    /**
     * Sets the value of the deposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositDetailBean }
     *     
     */
    public void setDeposit(ChDepositDetailBean value) {
        this.deposit = value;
    }

    /**
     * Gets the value of the depositTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepositTitle() {
        return depositTitle;
    }

    /**
     * Sets the value of the depositTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepositTitle(String value) {
        this.depositTitle = value;
    }

    /**
     * Gets the value of the group property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositGroupType }
     *     
     */
    public ChDepositGroupType getGroup() {
        return group;
    }

    /**
     * Sets the value of the group property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositGroupType }
     *     
     */
    public void setGroup(ChDepositGroupType value) {
        this.group = value;
    }

    /**
     * Gets the value of the interestPayDay property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getInterestPayDay() {
        return interestPayDay;
    }

    /**
     * Sets the value of the interestPayDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setInterestPayDay(Long value) {
        this.interestPayDay = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositOwnerType }
     *     
     */
    public ChDepositOwnerType getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositOwnerType }
     *     
     */
    public void setOwner(ChDepositOwnerType value) {
        this.owner = value;
    }

    /**
     * Gets the value of the paymentPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPaymentPeriod() {
        return paymentPeriod;
    }

    /**
     * Sets the value of the paymentPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPaymentPeriod(Long value) {
        this.paymentPeriod = value;
    }

    /**
     * Gets the value of the personality property.
     * 
     * @return
     *     possible object is
     *     {@link ChPersonalityType }
     *     
     */
    public ChPersonalityType getPersonality() {
        return personality;
    }

    /**
     * Sets the value of the personality property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPersonalityType }
     *     
     */
    public void setPersonality(ChPersonalityType value) {
        this.personality = value;
    }

    /**
     * Gets the value of the signature property.
     * 
     * @return
     *     possible object is
     *     {@link ChSignatureOwnerStatus }
     *     
     */
    public ChSignatureOwnerStatus getSignature() {
        return signature;
    }

    /**
     * Sets the value of the signature property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSignatureOwnerStatus }
     *     
     */
    public void setSignature(ChSignatureOwnerStatus value) {
        this.signature = value;
    }

    /**
     * Gets the value of the support property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositDetailBean }
     *     
     */
    public ChDepositDetailBean getSupport() {
        return support;
    }

    /**
     * Sets the value of the support property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositDetailBean }
     *     
     */
    public void setSupport(ChDepositDetailBean value) {
        this.support = value;
    }

    /**
     * Gets the value of the supportGroupType property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositGroupType }
     *     
     */
    public ChDepositGroupType getSupportGroupType() {
        return supportGroupType;
    }

    /**
     * Sets the value of the supportGroupType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositGroupType }
     *     
     */
    public void setSupportGroupType(ChDepositGroupType value) {
        this.supportGroupType = value;
    }

    /**
     * Gets the value of the supportStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ChSupportStatus }
     *     
     */
    public ChSupportStatus getSupportStatus() {
        return supportStatus;
    }

    /**
     * Sets the value of the supportStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSupportStatus }
     *     
     */
    public void setSupportStatus(ChSupportStatus value) {
        this.supportStatus = value;
    }

    /**
     * Gets the value of the supportVisibility property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportVisibility() {
        return supportVisibility;
    }

    /**
     * Sets the value of the supportVisibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportVisibility(Boolean value) {
        this.supportVisibility = value;
    }

    /**
     * Gets the value of the withdrawalOption property.
     * 
     * @return
     *     possible object is
     *     {@link ChWithdrawalOption }
     *     
     */
    public ChWithdrawalOption getWithdrawalOption() {
        return withdrawalOption;
    }

    /**
     * Sets the value of the withdrawalOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChWithdrawalOption }
     *     
     */
    public void setWithdrawalOption(ChWithdrawalOption value) {
        this.withdrawalOption = value;
    }

}
