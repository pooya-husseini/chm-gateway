package com.caspco.channel.exceptions;

import javax.servlet.http.HttpServletResponse;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 11/26/17
 * Time: 5:07 PM
 */
public interface BaseExceptionHandler {
    void logError(Exception e, HttpServletResponse response);
}
