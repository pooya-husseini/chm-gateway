
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBeOpenedDepositBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chBeOpenedDepositBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="depositGroup" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositGroupType" minOccurs="0"/>
 *         &lt;element name="depositType" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="depositTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="interestDay" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="lifeCycleMonth" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="minimumApplicableAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="stampPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chBeOpenedDepositBean", propOrder = {
    "currency",
    "depositGroup",
    "depositType",
    "depositTypeName",
    "interestDay",
    "lifeCycleMonth",
    "minimumApplicableAmount",
    "stampPrice"
})
public class ChBeOpenedDepositBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String currency;
    @XmlSchemaType(name = "string")
    protected ChDepositGroupType depositGroup;
    protected Long depositType;
    protected String depositTypeName;
    protected Short interestDay;
    protected Short lifeCycleMonth;
    protected BigDecimal minimumApplicableAmount;
    protected BigDecimal stampPrice;

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the depositGroup property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositGroupType }
     *     
     */
    public ChDepositGroupType getDepositGroup() {
        return depositGroup;
    }

    /**
     * Sets the value of the depositGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositGroupType }
     *     
     */
    public void setDepositGroup(ChDepositGroupType value) {
        this.depositGroup = value;
    }

    /**
     * Gets the value of the depositType property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDepositType() {
        return depositType;
    }

    /**
     * Sets the value of the depositType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDepositType(Long value) {
        this.depositType = value;
    }

    /**
     * Gets the value of the depositTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepositTypeName() {
        return depositTypeName;
    }

    /**
     * Sets the value of the depositTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepositTypeName(String value) {
        this.depositTypeName = value;
    }

    /**
     * Gets the value of the interestDay property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getInterestDay() {
        return interestDay;
    }

    /**
     * Sets the value of the interestDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setInterestDay(Short value) {
        this.interestDay = value;
    }

    /**
     * Gets the value of the lifeCycleMonth property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getLifeCycleMonth() {
        return lifeCycleMonth;
    }

    /**
     * Sets the value of the lifeCycleMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setLifeCycleMonth(Short value) {
        this.lifeCycleMonth = value;
    }

    /**
     * Gets the value of the minimumApplicableAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinimumApplicableAmount() {
        return minimumApplicableAmount;
    }

    /**
     * Sets the value of the minimumApplicableAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinimumApplicableAmount(BigDecimal value) {
        this.minimumApplicableAmount = value;
    }

    /**
     * Gets the value of the stampPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStampPrice() {
        return stampPrice;
    }

    /**
     * Sets the value of the stampPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStampPrice(BigDecimal value) {
        this.stampPrice = value;
    }

}
