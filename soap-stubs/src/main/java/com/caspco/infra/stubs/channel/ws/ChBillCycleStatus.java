
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBillCycleStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chBillCycleStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CLOSE"/>
 *     &lt;enumeration value="OPEN"/>
 *     &lt;enumeration value="CLOSE_OUT_OF_BALANCE"/>
 *     &lt;enumeration value="CLOSE_IN_BALANCE"/>
 *     &lt;enumeration value="CLOSE_MANUALLY_IN_BALANCE"/>
 *     &lt;enumeration value="CLOSE_BATCH_ID_CONFLICT"/>
 *     &lt;enumeration value="ISSUE_STATEMENT"/>
 *     &lt;enumeration value="OPEN_LOAN"/>
 *     &lt;enumeration value="TRANSFER_TO_NEXT"/>
 *     &lt;enumeration value="STORE"/>
 *     &lt;enumeration value="OPEN_LOAN_FOR_ORIGINAL_AMOUNT"/>
 *     &lt;enumeration value="CLOSE_SUSPENDED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chBillCycleStatus")
@XmlEnum
public enum ChBillCycleStatus {

    CLOSE,
    OPEN,
    CLOSE_OUT_OF_BALANCE,
    CLOSE_IN_BALANCE,
    CLOSE_MANUALLY_IN_BALANCE,
    CLOSE_BATCH_ID_CONFLICT,
    ISSUE_STATEMENT,
    OPEN_LOAN,
    TRANSFER_TO_NEXT,
    STORE,
    OPEN_LOAN_FOR_ORIGINAL_AMOUNT,
    CLOSE_SUSPENDED;

    public String value() {
        return name();
    }

    public static ChBillCycleStatus fromValue(String v) {
        return valueOf(v);
    }

}
