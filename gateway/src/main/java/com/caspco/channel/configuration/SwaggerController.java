package com.caspco.channel.configuration;

import com.google.common.base.Strings;
import io.swagger.models.Path;
import io.swagger.models.Swagger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.UrlPathHelper;
import springfox.documentation.service.Documentation;
import springfox.documentation.spring.web.DocumentationCache;
import springfox.documentation.spring.web.json.Json;
import springfox.documentation.spring.web.json.JsonSerializer;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.mappers.ServiceModelToSwagger2Mapper;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.base.Strings.isNullOrEmpty;
import static org.springframework.util.StringUtils.hasText;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromContextPath;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/16/17
 * Time: 11:06 AM
 */

@RestController
@RequestMapping("/swagger")
public class SwaggerController {
    @Autowired
    private Environment environment;
    @Autowired
    private DocumentationCache documentationCache;
    @Autowired
    private ServiceModelToSwagger2Mapper mapper;
    @Autowired
    private JsonSerializer jsonSerializer;
    private String hostNameOverride;

    @PostConstruct
    public void init(){
        this.hostNameOverride = environment.getProperty("springfox.documentation.swagger.v2.host", "DEFAULT");
    }

    @RequestMapping(method = RequestMethod.POST, value = "/filter")
    public ResponseEntity<Json> filterSwagger(@Valid @RequestBody SwaggerInput swaggerInput, HttpServletRequest servletRequest) {
        Set<String> strings = new HashSet<>(swaggerInput.getOperations());
        return extractJson(servletRequest,path -> strings.contains(path.getKey()));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/notFilter")
    public ResponseEntity<Json> notFilterSwagger(@Valid @RequestBody SwaggerInput swaggerInput, HttpServletRequest servletRequest) {
        Set<String> strings = new HashSet<>(swaggerInput.getOperations());
        return extractJson(servletRequest,path -> !strings.contains(path.getKey()));
    }

    private ResponseEntity<Json> extractJson(HttpServletRequest servletRequest,SwaggerOperationFilter operationFilter) {
        String groupName = Docket.DEFAULT_GROUP_NAME;


        Documentation documentation = documentationCache.documentationByGroup(groupName);
        if (documentation == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Swagger swagger = mapper.mapDocumentation(documentation);
        Map<String, Path> pathMap = swagger.getPaths().entrySet().stream().filter(operationFilter::filter).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        swagger.setPaths(pathMap);
        UriComponents uriComponents = componentsFrom(servletRequest, swagger.getBasePath());
        swagger.basePath(Strings.isNullOrEmpty(uriComponents.getPath()) ? "/" : uriComponents.getPath());
        if (isNullOrEmpty(swagger.getHost())) {
            swagger.host(hostName(uriComponents));
        }
        return new ResponseEntity<>(jsonSerializer.toJson(swagger), HttpStatus.OK);
    }

    private String hostName(UriComponents uriComponents) {
        if ("DEFAULT".equals(hostNameOverride)) {
            String host = uriComponents.getHost();
            int port = uriComponents.getPort();
            if (port > -1) {
                return String.format("%s:%d", host, port);
            }
            return host;
        }
        return hostNameOverride;
    }

    private static UriComponents componentsFrom(
            HttpServletRequest request,
            String basePath) {

        ServletUriComponentsBuilder builder = fromServletMapping(request, basePath);

        UriComponents components = UriComponentsBuilder.fromHttpRequest(
                new ServletServerHttpRequest(request))
                .build();

        String host = components.getHost();
        if (!hasText(host)) {
            return builder.build();
        }

        builder.host(host);
        builder.port(components.getPort());

        return builder.build();
    }

    private static ServletUriComponentsBuilder fromServletMapping(
            HttpServletRequest request,
            String basePath) {

        ServletUriComponentsBuilder builder = fromContextPath(request);

        builder.replacePath(prependForwardedPrefix(request, basePath));
        if (hasText(new UrlPathHelper().getPathWithinServletMapping(request))) {
            builder.path(request.getServletPath());
        }

        return builder;
    }

    private static String prependForwardedPrefix(
            HttpServletRequest request,
            String path) {

        String prefix = request.getHeader("X-Forwarded-Prefix");
        if (prefix != null) {
            return prefix + path;
        } else {
            return path;
        }
    }
}
