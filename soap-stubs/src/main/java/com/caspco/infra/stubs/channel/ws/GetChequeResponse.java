
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getChequeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getChequeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chChequeSheetsResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chChequeSheetsResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getChequeResponse", propOrder = {
    "chChequeSheetsResponseBean"
})
public class GetChequeResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChChequeSheetsResponseBean chChequeSheetsResponseBean;

    /**
     * Gets the value of the chChequeSheetsResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChChequeSheetsResponseBean }
     *     
     */
    public ChChequeSheetsResponseBean getChChequeSheetsResponseBean() {
        return chChequeSheetsResponseBean;
    }

    /**
     * Sets the value of the chChequeSheetsResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChChequeSheetsResponseBean }
     *     
     */
    public void setChChequeSheetsResponseBean(ChChequeSheetsResponseBean value) {
        this.chChequeSheetsResponseBean = value;
    }

}
