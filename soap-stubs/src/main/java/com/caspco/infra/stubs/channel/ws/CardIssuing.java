
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardIssuing complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardIssuing">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardIssuingRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardIssuingRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardIssuing", propOrder = {
    "chCardIssuingRequestBean"
})
public class CardIssuing
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardIssuingRequestBean chCardIssuingRequestBean;

    /**
     * Gets the value of the chCardIssuingRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardIssuingRequestBean }
     *     
     */
    public ChCardIssuingRequestBean getChCardIssuingRequestBean() {
        return chCardIssuingRequestBean;
    }

    /**
     * Sets the value of the chCardIssuingRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardIssuingRequestBean }
     *     
     */
    public void setChCardIssuingRequestBean(ChCardIssuingRequestBean value) {
        this.chCardIssuingRequestBean = value;
    }

}
