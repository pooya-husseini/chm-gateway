
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chPaymentServiceInputParameterBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chPaymentServiceInputParameterBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="foreignTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inputStyle" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPaymentServiceInputStyleType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="isAmount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isProtected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="maxValueLength" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="minValueLength" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chPaymentServiceInputParameterBean", propOrder = {
    "foreignTitle",
    "id",
    "inputStyle",
    "isAmount",
    "isProtected",
    "maxValueLength",
    "minValueLength",
    "title"
})
public class ChPaymentServiceInputParameterBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String foreignTitle;
    protected String id;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "string")
    protected List<ChPaymentServiceInputStyleType> inputStyle;
    protected Boolean isAmount;
    protected Boolean isProtected;
    protected Short maxValueLength;
    protected Short minValueLength;
    protected String title;

    /**
     * Gets the value of the foreignTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignTitle() {
        return foreignTitle;
    }

    /**
     * Sets the value of the foreignTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignTitle(String value) {
        this.foreignTitle = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the inputStyle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inputStyle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInputStyle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChPaymentServiceInputStyleType }
     * 
     * 
     */
    public List<ChPaymentServiceInputStyleType> getInputStyle() {
        if (inputStyle == null) {
            inputStyle = new ArrayList<ChPaymentServiceInputStyleType>();
        }
        return this.inputStyle;
    }

    /**
     * Gets the value of the isAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAmount() {
        return isAmount;
    }

    /**
     * Sets the value of the isAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAmount(Boolean value) {
        this.isAmount = value;
    }

    /**
     * Gets the value of the isProtected property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsProtected() {
        return isProtected;
    }

    /**
     * Sets the value of the isProtected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsProtected(Boolean value) {
        this.isProtected = value;
    }

    /**
     * Gets the value of the maxValueLength property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getMaxValueLength() {
        return maxValueLength;
    }

    /**
     * Sets the value of the maxValueLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setMaxValueLength(Short value) {
        this.maxValueLength = value;
    }

    /**
     * Gets the value of the minValueLength property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getMinValueLength() {
        return minValueLength;
    }

    /**
     * Sets the value of the minValueLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setMinValueLength(Short value) {
        this.minValueLength = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

}
