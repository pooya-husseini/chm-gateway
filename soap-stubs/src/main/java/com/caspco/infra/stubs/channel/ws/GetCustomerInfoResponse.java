
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCustomerInfoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCustomerInfoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chUserResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chUserResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCustomerInfoResponse", propOrder = {
    "chUserResponseBean"
})
public class GetCustomerInfoResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChUserResponseBean chUserResponseBean;

    /**
     * Gets the value of the chUserResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChUserResponseBean }
     *     
     */
    public ChUserResponseBean getChUserResponseBean() {
        return chUserResponseBean;
    }

    /**
     * Sets the value of the chUserResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChUserResponseBean }
     *     
     */
    public void setChUserResponseBean(ChUserResponseBean value) {
        this.chUserResponseBean = value;
    }

}
