package com.caspco.channel.swagger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/11/17
 * Time: 8:56 AM
 */

@Service
public class DefinedRestServiceExtractor {
    @Autowired
    private RequestMappingHandlerMapping handlerMapping;

    @Cacheable("DefinedMethods")
    public Set<String> getDefinedMethods(){
        Set<RequestMappingInfo> handlerMethods = handlerMapping.getHandlerMethods().keySet();
        return  handlerMethods.stream().map(i -> i.getPatternsCondition().toString().replace("[", "").replace("]", "")).collect(Collectors.toSet());
    }
}
