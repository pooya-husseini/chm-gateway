/**
 * ChNormalRtgsTransferResponseBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public class NormalRtgsTransferResponseDto  implements java.io.Serializable {

    private java.math.BigDecimal balance;

    private String currency;

    private String id;

    /**
     * Gets the balance value for this ChNormalRtgsTransferResponseBean.
     *
     * @return balance
     */
    public java.math.BigDecimal getBalance() {
        return balance;
    }


    /**
     * Sets the balance value for this ChNormalRtgsTransferResponseBean.
     *
     * @param balance
     */
    public void setBalance(java.math.BigDecimal balance) {
        this.balance = balance;
    }


    /**
     * Gets the currency value for this ChNormalRtgsTransferResponseBean.
     *
     * @return currency
     */
    public String getCurrency() {
        return currency;
    }


    /**
     * Sets the currency value for this ChNormalRtgsTransferResponseBean.
     *
     * @param currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }


    /**
     * Gets the id value for this ChNormalRtgsTransferResponseBean.
     *
     * @return id
     */
    public String getId() {
        return id;
    }


    /**
     * Sets the id value for this ChNormalRtgsTransferResponseBean.
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }


}
