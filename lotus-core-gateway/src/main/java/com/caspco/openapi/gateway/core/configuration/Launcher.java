package com.caspco.openapi.gateway.core.configuration;

/*
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 5/22/17
 * Time: 10:51 AM
 */

import com.caspco.openapi.gateway.core.exceptions.IllegalAccountException;
import com.caspco.openapi.gateway.core.exceptions.InvalidUserException;
import com.caspco.openapi.gateway.core.model.*;
import com.caspco.openapi.gateway.core.services.CustomerService;
import com.caspco.spl.wrench.beans.transformer.SmartTransformer;
import com.caspian.banking.cif.dto.CustomerDTO;
import com.caspian.banking.deposit.account.dto.AccountDTO;
import com.caspian.banking.ebanking.dto.CardBalanceDto;
import com.caspian.banking.ebanking.dto.ChCardDto;
import com.caspian.banking.ebanking.message.MGCardBalanceInquiryMsg;
import com.caspian.banking.ebanking.message.MGGetCardDepositsMsg;
import com.caspian.moderngateway.core.coreservice.dto.ChStatementBean;
import com.caspian.moderngateway.core.coreservice.dto.ChStatementSearchRequestBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.jndi.JndiTemplate;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import javax.jms.JMSException;
import javax.naming.NamingException;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Properties;
import java.util.Set;

@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class
})
@RestController
@EnableResourceServer
@RequestMapping("/banking")
@EnableCaching
@EnableWebSecurity
//@EnableOAuth2Sso
public class Launcher extends WebSecurityConfigurerAdapter implements ApplicationRunner{
    @Autowired
    private CustomerService customerService;

    @Autowired
    private SmartTransformer basicTransformer;

    @Bean
    public JndiTemplate jndiTemplate() {
        JndiTemplate template = new JndiTemplate();
        Properties properties = new Properties();
        properties.setProperty("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
        properties.setProperty("java.naming.provider.url", "t3://192.168.247.78:7001");
        template.setEnvironment(properties);
        return template;
    }

    @RequestMapping("/profile")
    @ResponseBody
    public ResponseObject user(OAuth2Authentication user) throws JMSException, NamingException, IOException {
        String name = user.getName();
//        List<AccountDTO> strings = customerService.loadAccount(Long.valueOf(name));
//        for ( AccountDTO string : strings) {
//            AccountInvoiceMessage inbound = new AccountInvoiceMessage();
//            inbound.setAccountExternalRef(string.getExternalRef());
//            inbound.setRowNumber(10);
//
//            inbound.setFromBookdate(new Date(117,1,20));
//            inbound.setToBookdate(new Date(117,1,22));
//            customerService.loadBills(inbound);
//        }

        Long customerId = customerService.getCustomerId(name);
        CustomerDTO customerDTO = customerService.getCustomerById(customerId);
        return new ResponseObject(basicTransformer.convert(customerDTO, CustomerProfile.class));
//        return new ResponseObject(new Object());
    }

    @RequestMapping("/accounts")
    @ResponseBody
    public ResponseObject userAccounts(Principal user) throws JMSException, NamingException, IOException {
        String name = user.getName();
//        List<AccountDTO> strings = customerService.loadAccount(Long.valueOf(name));
//        for ( AccountDTO string : strings) {
//            AccountInvoiceMessage inbound = new AccountInvoiceMessage();
//            inbound.setAccountExternalRef(string.getExternalRef());
//            inbound.setRowNumber(10);
//
//            inbound.setFromBookdate(new Date(117,1,20));
//            inbound.setToBookdate(new Date(117,1,22));
//            customerService.loadBills(inbound);
//        }
        List<AccountDTO> customerDTO = customerService.getAccounts(Long.valueOf(name));
        return new ResponseObject(basicTransformer.convert(customerDTO, CustomerAccount.class));
    }

    @RequestMapping(value = "/invoices", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject userInvoice(Principal user, @RequestBody AccountInvoiceMessage message) throws JMSException, NamingException, IOException {
        String name = user.getName();
        Set<String> externalRefs = customerService.getAccountsExternalRefs(Long.valueOf(name));
        if (externalRefs == null) {
            throw new InvalidUserException();
        }
        if (externalRefs.contains(message.getDepositNumber())) {
            List<ChStatementBean> list = customerService.getStatement(this.basicTransformer.convert(message,ChStatementSearchRequestBean.class));
//            return new ResponseObject(basicTransformer.convert(list, AccountInvoiceResult.class));
            return new ResponseObject(list);
        } else {
            throw new IllegalAccountException("Account id is invalid for the current user");
        }
    }

    @RequestMapping(value = "/cards", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject getCards(Principal user) throws JMSException, NamingException, IOException {
        String name = user.getName();
        List<ChCardDto> externalRefs = customerService.getCards(Long.valueOf(name));
        return new ResponseObject(externalRefs);
    }

    @RequestMapping(value = "/cardDeposits", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject getCards(Principal user, @RequestBody CardRequestDto card) throws JMSException, NamingException, IOException {
        String name = user.getName();
        MGGetCardDepositsMsg.Outbound externalRefs = customerService.getCardDeposit(Long.valueOf(name), card.getCardNumber());
        return new ResponseObject(externalRefs);
    }

    @RequestMapping(value = "/cardBalance", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject getCardBalance(Principal user, @RequestBody CardBalanceDto cardBalance) throws JMSException, NamingException, IOException {
        MGCardBalanceInquiryMsg.Outbound externalRefs = customerService.getCardBalance(cardBalance);
        return new ResponseObject(externalRefs);
    }

    /*@Bean
    public AccessTokenConverter jwtAccessTokenConverter() {

        DefaultAccessTokenConverter tokenConverter = new DefaultAccessTokenConverter();
        tokenConverter.setUserTokenConverter(new DefaultUserAuthenticationConverter() {

            @Override
            public Authentication extractAuthentication(Map<String, ?> map) {
                Authentication authentication = super.extractAuthentication(map);
                // User is my custom UserDetails class
                return new UsernamePasswordAuthenticationToken("asdasd",
                        authentication.getCredentials(), authentication.getAuthorities());
            }

        });


        return tokenConverter;
    }*/


    public static void main(String[] args) {
        SpringApplication.run(Launcher.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        CustomerDTO customerById = customerService.getCustomerById(35136555L);
        System.out.println(customerById);
    }

//    @Override
//    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
//        endpoints
////                .tokenEnhancer(tokenEnhancer())
//                .accessTokenConverter(jwtAccessTokenConverter());
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().authenticated();
    }

}