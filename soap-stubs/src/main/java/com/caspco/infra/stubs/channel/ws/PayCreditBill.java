
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for payCreditBill complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="payCreditBill">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCreditBillPaymentRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCreditBillPaymentRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "payCreditBill", propOrder = {
    "chCreditBillPaymentRequestBean"
})
public class PayCreditBill
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCreditBillPaymentRequestBean chCreditBillPaymentRequestBean;

    /**
     * Gets the value of the chCreditBillPaymentRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCreditBillPaymentRequestBean }
     *     
     */
    public ChCreditBillPaymentRequestBean getChCreditBillPaymentRequestBean() {
        return chCreditBillPaymentRequestBean;
    }

    /**
     * Sets the value of the chCreditBillPaymentRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCreditBillPaymentRequestBean }
     *     
     */
    public void setChCreditBillPaymentRequestBean(ChCreditBillPaymentRequestBean value) {
        this.chCreditBillPaymentRequestBean = value;
    }

}
