
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCustomerInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCustomerInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chUserRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chUserRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCustomerInfo", propOrder = {
    "chUserRequestBean"
})
public class GetCustomerInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChUserRequestBean chUserRequestBean;

    /**
     * Gets the value of the chUserRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChUserRequestBean }
     *     
     */
    public ChUserRequestBean getChUserRequestBean() {
        return chUserRequestBean;
    }

    /**
     * Sets the value of the chUserRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChUserRequestBean }
     *     
     */
    public void setChUserRequestBean(ChUserRequestBean value) {
        this.chUserRequestBean = value;
    }

}
