
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chChequeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chChequeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CURRENT_CHEQUE"/>
 *     &lt;enumeration value="BANK_DRAFT_CHEQUE"/>
 *     &lt;enumeration value="GUARANTEE_CHEQUE"/>
 *     &lt;enumeration value="TRAVELS_CHEQUE"/>
 *     &lt;enumeration value="OTHERS_CHEQUE"/>
 *     &lt;enumeration value="BANK_CHEQUE"/>
 *     &lt;enumeration value="IRAN_CHEQUE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chChequeType")
@XmlEnum
public enum ChChequeType {

    CURRENT_CHEQUE,
    BANK_DRAFT_CHEQUE,
    GUARANTEE_CHEQUE,
    TRAVELS_CHEQUE,
    OTHERS_CHEQUE,
    BANK_CHEQUE,
    IRAN_CHEQUE;

    public String value() {
        return name();
    }

    public static ChChequeType fromValue(String v) {
        return valueOf(v);
    }

}
