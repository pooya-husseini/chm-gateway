/**
 * ChTransferDetailBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

import java.math.BigDecimal;
import java.util.Date;

public class TransferDetailDto  implements java.io.Serializable {

    private BigDecimal amount;

    private String branchCode;

    private String branchName;

    private BankDto destinationBank;

    private String destinationDepositNumber;

    private Date registerDate;

    private String serial;

    private String sourceDepositNumber;

    private RtgsTransferStatus status;

    private RtgsSystemCode systemCode;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public BankDto getDestinationBank() {
        return destinationBank;
    }

    public void setDestinationBank(BankDto destinationBank) {
        this.destinationBank = destinationBank;
    }

    public String getDestinationDepositNumber() {
        return destinationDepositNumber;
    }

    public void setDestinationDepositNumber(String destinationDepositNumber) {
        this.destinationDepositNumber = destinationDepositNumber;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getSourceDepositNumber() {
        return sourceDepositNumber;
    }

    public void setSourceDepositNumber(String sourceDepositNumber) {
        this.sourceDepositNumber = sourceDepositNumber;
    }

    public RtgsTransferStatus getStatus() {
        return status;
    }

    public void setStatus(RtgsTransferStatus status) {
        this.status = status;
    }

    public RtgsSystemCode getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(RtgsSystemCode systemCode) {
        this.systemCode = systemCode;
    }
}
