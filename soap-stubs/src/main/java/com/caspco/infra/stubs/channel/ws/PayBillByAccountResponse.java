
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for payBillByAccountResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="payBillByAccountResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBillPaymentResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillPaymentResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "payBillByAccountResponse", propOrder = {
    "chBillPaymentResponseBean"
})
public class PayBillByAccountResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBillPaymentResponseBean chBillPaymentResponseBean;

    /**
     * Gets the value of the chBillPaymentResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillPaymentResponseBean }
     *     
     */
    public ChBillPaymentResponseBean getChBillPaymentResponseBean() {
        return chBillPaymentResponseBean;
    }

    /**
     * Sets the value of the chBillPaymentResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillPaymentResponseBean }
     *     
     */
    public void setChBillPaymentResponseBean(ChBillPaymentResponseBean value) {
        this.chBillPaymentResponseBean = value;
    }

}
