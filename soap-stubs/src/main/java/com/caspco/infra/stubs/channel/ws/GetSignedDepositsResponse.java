
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getSignedDepositsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getSignedDepositsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSignedDepositResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSignedDepositResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSignedDepositsResponse", propOrder = {
    "chSignedDepositResponseBean"
})
public class GetSignedDepositsResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSignedDepositResponseBean chSignedDepositResponseBean;

    /**
     * Gets the value of the chSignedDepositResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChSignedDepositResponseBean }
     *     
     */
    public ChSignedDepositResponseBean getChSignedDepositResponseBean() {
        return chSignedDepositResponseBean;
    }

    /**
     * Sets the value of the chSignedDepositResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSignedDepositResponseBean }
     *     
     */
    public void setChSignedDepositResponseBean(ChSignedDepositResponseBean value) {
        this.chSignedDepositResponseBean = value;
    }

}
