
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getRequestStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getRequestStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chReqStatusRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chReqStatusRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getRequestStatus", propOrder = {
    "chReqStatusRequestBean"
})
public class GetRequestStatus
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChReqStatusRequestBean chReqStatusRequestBean;

    /**
     * Gets the value of the chReqStatusRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChReqStatusRequestBean }
     *     
     */
    public ChReqStatusRequestBean getChReqStatusRequestBean() {
        return chReqStatusRequestBean;
    }

    /**
     * Sets the value of the chReqStatusRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChReqStatusRequestBean }
     *     
     */
    public void setChReqStatusRequestBean(ChReqStatusRequestBean value) {
        this.chReqStatusRequestBean = value;
    }

}
