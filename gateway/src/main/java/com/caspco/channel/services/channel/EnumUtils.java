package com.caspco.channel.services.channel;

import java.util.HashMap;
import java.util.Map;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 9/18/17
 * Time: 12:00 PM
 */
public final class EnumUtils {

    public static  <T extends Enum<T>> T convertEnum(Object obj,Class<T> e) {
        if (obj != null) {
            if (obj instanceof Map) {
                HashMap<String, String> map = (HashMap<String, String>) obj;

                for (Object operator : e.getEnumConstants()) {
                    Enum<T> anEnum = (Enum<T>) operator;
                    if (map.get("value").equals(anEnum.name())) {
                        return (T) anEnum;
                    }
                }
            } else {
                for (Object operator : e.getEnumConstants()) {
                    Enum<T> anEnum = (Enum<T>) operator;
                    if (anEnum.name().equals(obj.toString())) {
                        return (T) anEnum;
                    }
                }
            }
        }
        return null;
    }
}