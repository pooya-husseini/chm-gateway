
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ibanEnquiryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ibanEnquiryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Message03" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chIBANEnquiryResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ibanEnquiryResponse", propOrder = {
    "message03"
})
public class IbanEnquiryResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Message03")
    protected ChIBANEnquiryResponse message03;

    /**
     * Gets the value of the message03 property.
     * 
     * @return
     *     possible object is
     *     {@link ChIBANEnquiryResponse }
     *     
     */
    public ChIBANEnquiryResponse getMessage03() {
        return message03;
    }

    /**
     * Sets the value of the message03 property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChIBANEnquiryResponse }
     *     
     */
    public void setMessage03(ChIBANEnquiryResponse value) {
        this.message03 = value;
    }

}
