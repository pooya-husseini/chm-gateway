
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPaymentPreviewResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPaymentPreviewResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chPaymentPreviewResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPaymentPreviewResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPaymentPreviewResponse", propOrder = {
    "chPaymentPreviewResponseBean"
})
public class GetPaymentPreviewResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChPaymentPreviewResponseBean chPaymentPreviewResponseBean;

    /**
     * Gets the value of the chPaymentPreviewResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChPaymentPreviewResponseBean }
     *     
     */
    public ChPaymentPreviewResponseBean getChPaymentPreviewResponseBean() {
        return chPaymentPreviewResponseBean;
    }

    /**
     * Sets the value of the chPaymentPreviewResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPaymentPreviewResponseBean }
     *     
     */
    public void setChPaymentPreviewResponseBean(ChPaymentPreviewResponseBean value) {
        this.chPaymentPreviewResponseBean = value;
    }

}
