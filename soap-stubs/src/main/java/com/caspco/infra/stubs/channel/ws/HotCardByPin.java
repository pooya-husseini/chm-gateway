
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hotCardByPin complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hotCardByPin">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chHotCardByPinRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chHotCardByPinRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hotCardByPin", propOrder = {
    "chHotCardByPinRequestBean"
})
public class HotCardByPin
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChHotCardByPinRequestBean chHotCardByPinRequestBean;

    /**
     * Gets the value of the chHotCardByPinRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChHotCardByPinRequestBean }
     *     
     */
    public ChHotCardByPinRequestBean getChHotCardByPinRequestBean() {
        return chHotCardByPinRequestBean;
    }

    /**
     * Sets the value of the chHotCardByPinRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChHotCardByPinRequestBean }
     *     
     */
    public void setChHotCardByPinRequestBean(ChHotCardByPinRequestBean value) {
        this.chHotCardByPinRequestBean = value;
    }

}
