
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBriefTransferConstraintInfoResponseBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chBriefTransferConstraintInfoResponseBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="maxPriceDaily" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maxPriceDailyToOtherDeposit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maxPriceDailyToOwnDeposit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maxPriceMonthly" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maxPriceMonthlyToOtherDeposit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maxPriceMonthlyToOwnDeposit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maxTransferAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maxTransferAmountToOtherDeposit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maxTransferAmountToOwnDeposit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="minPriceDaily" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="minTicketAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="minTransferAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="priceThatUserTransferInMonth" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="priceThatUserTransferToday" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chBriefTransferConstraintInfoResponseBean", propOrder = {
    "maxPriceDaily",
    "maxPriceDailyToOtherDeposit",
    "maxPriceDailyToOwnDeposit",
    "maxPriceMonthly",
    "maxPriceMonthlyToOtherDeposit",
    "maxPriceMonthlyToOwnDeposit",
    "maxTransferAmount",
    "maxTransferAmountToOtherDeposit",
    "maxTransferAmountToOwnDeposit",
    "minPriceDaily",
    "minTicketAmount",
    "minTransferAmount",
    "priceThatUserTransferInMonth",
    "priceThatUserTransferToday"
})
public class ChBriefTransferConstraintInfoResponseBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigDecimal maxPriceDaily;
    protected BigDecimal maxPriceDailyToOtherDeposit;
    protected BigDecimal maxPriceDailyToOwnDeposit;
    protected BigDecimal maxPriceMonthly;
    protected BigDecimal maxPriceMonthlyToOtherDeposit;
    protected BigDecimal maxPriceMonthlyToOwnDeposit;
    protected BigDecimal maxTransferAmount;
    protected BigDecimal maxTransferAmountToOtherDeposit;
    protected BigDecimal maxTransferAmountToOwnDeposit;
    protected BigDecimal minPriceDaily;
    protected BigDecimal minTicketAmount;
    protected BigDecimal minTransferAmount;
    protected BigDecimal priceThatUserTransferInMonth;
    protected BigDecimal priceThatUserTransferToday;

    /**
     * Gets the value of the maxPriceDaily property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxPriceDaily() {
        return maxPriceDaily;
    }

    /**
     * Sets the value of the maxPriceDaily property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxPriceDaily(BigDecimal value) {
        this.maxPriceDaily = value;
    }

    /**
     * Gets the value of the maxPriceDailyToOtherDeposit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxPriceDailyToOtherDeposit() {
        return maxPriceDailyToOtherDeposit;
    }

    /**
     * Sets the value of the maxPriceDailyToOtherDeposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxPriceDailyToOtherDeposit(BigDecimal value) {
        this.maxPriceDailyToOtherDeposit = value;
    }

    /**
     * Gets the value of the maxPriceDailyToOwnDeposit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxPriceDailyToOwnDeposit() {
        return maxPriceDailyToOwnDeposit;
    }

    /**
     * Sets the value of the maxPriceDailyToOwnDeposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxPriceDailyToOwnDeposit(BigDecimal value) {
        this.maxPriceDailyToOwnDeposit = value;
    }

    /**
     * Gets the value of the maxPriceMonthly property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxPriceMonthly() {
        return maxPriceMonthly;
    }

    /**
     * Sets the value of the maxPriceMonthly property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxPriceMonthly(BigDecimal value) {
        this.maxPriceMonthly = value;
    }

    /**
     * Gets the value of the maxPriceMonthlyToOtherDeposit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxPriceMonthlyToOtherDeposit() {
        return maxPriceMonthlyToOtherDeposit;
    }

    /**
     * Sets the value of the maxPriceMonthlyToOtherDeposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxPriceMonthlyToOtherDeposit(BigDecimal value) {
        this.maxPriceMonthlyToOtherDeposit = value;
    }

    /**
     * Gets the value of the maxPriceMonthlyToOwnDeposit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxPriceMonthlyToOwnDeposit() {
        return maxPriceMonthlyToOwnDeposit;
    }

    /**
     * Sets the value of the maxPriceMonthlyToOwnDeposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxPriceMonthlyToOwnDeposit(BigDecimal value) {
        this.maxPriceMonthlyToOwnDeposit = value;
    }

    /**
     * Gets the value of the maxTransferAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxTransferAmount() {
        return maxTransferAmount;
    }

    /**
     * Sets the value of the maxTransferAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxTransferAmount(BigDecimal value) {
        this.maxTransferAmount = value;
    }

    /**
     * Gets the value of the maxTransferAmountToOtherDeposit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxTransferAmountToOtherDeposit() {
        return maxTransferAmountToOtherDeposit;
    }

    /**
     * Sets the value of the maxTransferAmountToOtherDeposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxTransferAmountToOtherDeposit(BigDecimal value) {
        this.maxTransferAmountToOtherDeposit = value;
    }

    /**
     * Gets the value of the maxTransferAmountToOwnDeposit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxTransferAmountToOwnDeposit() {
        return maxTransferAmountToOwnDeposit;
    }

    /**
     * Sets the value of the maxTransferAmountToOwnDeposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxTransferAmountToOwnDeposit(BigDecimal value) {
        this.maxTransferAmountToOwnDeposit = value;
    }

    /**
     * Gets the value of the minPriceDaily property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinPriceDaily() {
        return minPriceDaily;
    }

    /**
     * Sets the value of the minPriceDaily property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinPriceDaily(BigDecimal value) {
        this.minPriceDaily = value;
    }

    /**
     * Gets the value of the minTicketAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinTicketAmount() {
        return minTicketAmount;
    }

    /**
     * Sets the value of the minTicketAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinTicketAmount(BigDecimal value) {
        this.minTicketAmount = value;
    }

    /**
     * Gets the value of the minTransferAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinTransferAmount() {
        return minTransferAmount;
    }

    /**
     * Sets the value of the minTransferAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinTransferAmount(BigDecimal value) {
        this.minTransferAmount = value;
    }

    /**
     * Gets the value of the priceThatUserTransferInMonth property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceThatUserTransferInMonth() {
        return priceThatUserTransferInMonth;
    }

    /**
     * Sets the value of the priceThatUserTransferInMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceThatUserTransferInMonth(BigDecimal value) {
        this.priceThatUserTransferInMonth = value;
    }

    /**
     * Gets the value of the priceThatUserTransferToday property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceThatUserTransferToday() {
        return priceThatUserTransferToday;
    }

    /**
     * Sets the value of the priceThatUserTransferToday property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceThatUserTransferToday(BigDecimal value) {
        this.priceThatUserTransferToday = value;
    }

}
