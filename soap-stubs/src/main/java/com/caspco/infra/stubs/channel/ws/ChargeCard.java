
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chargeCard complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chargeCard">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chChargeCardRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chChargeCardRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chargeCard", propOrder = {
    "chChargeCardRequestBean"
})
public class ChargeCard
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChChargeCardRequestBean chChargeCardRequestBean;

    /**
     * Gets the value of the chChargeCardRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChChargeCardRequestBean }
     *     
     */
    public ChChargeCardRequestBean getChChargeCardRequestBean() {
        return chChargeCardRequestBean;
    }

    /**
     * Sets the value of the chChargeCardRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChChargeCardRequestBean }
     *     
     */
    public void setChChargeCardRequestBean(ChChargeCardRequestBean value) {
        this.chChargeCardRequestBean = value;
    }

}
