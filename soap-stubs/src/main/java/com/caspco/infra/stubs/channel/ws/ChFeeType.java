
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chFeeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chFeeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TRANSACTION"/>
 *     &lt;enumeration value="TRANSACTION_PROCESSING"/>
 *     &lt;enumeration value="SURCHARGE"/>
 *     &lt;enumeration value="SUSPEND_PAY_LATEN"/>
 *     &lt;enumeration value="STAMP_TAX"/>
 *     &lt;enumeration value="UNUSED_CREDIT"/>
 *     &lt;enumeration value="TRANSFER_TO_NEXT"/>
 *     &lt;enumeration value="RETURN"/>
 *     &lt;enumeration value="GOOD_AND_SERVICE"/>
 *     &lt;enumeration value="CASH"/>
 *     &lt;enumeration value="YEARLY"/>
 *     &lt;enumeration value="CARD_ISSUE"/>
 *     &lt;enumeration value="UN_BLOCK"/>
 *     &lt;enumeration value="UN_REVOKE"/>
 *     &lt;enumeration value="CHANGES"/>
 *     &lt;enumeration value="BILLING"/>
 *     &lt;enumeration value="BILL_TYPE"/>
 *     &lt;enumeration value="PAY_LATEN"/>
 *     &lt;enumeration value="INSURANCE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chFeeType")
@XmlEnum
public enum ChFeeType {

    TRANSACTION,
    TRANSACTION_PROCESSING,
    SURCHARGE,
    SUSPEND_PAY_LATEN,
    STAMP_TAX,
    UNUSED_CREDIT,
    TRANSFER_TO_NEXT,
    RETURN,
    GOOD_AND_SERVICE,
    CASH,
    YEARLY,
    CARD_ISSUE,
    UN_BLOCK,
    UN_REVOKE,
    CHANGES,
    BILLING,
    BILL_TYPE,
    PAY_LATEN,
    INSURANCE;

    public String value() {
        return name();
    }

    public static ChFeeType fromValue(String v) {
        return valueOf(v);
    }

}
