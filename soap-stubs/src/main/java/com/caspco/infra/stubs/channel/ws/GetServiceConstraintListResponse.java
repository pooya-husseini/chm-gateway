
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getServiceConstraintListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getServiceConstraintListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chServiceConstraintInformationResponseBeans" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chServiceConstraintInformationResponseBeans" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getServiceConstraintListResponse", propOrder = {
    "chServiceConstraintInformationResponseBeans"
})
public class GetServiceConstraintListResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChServiceConstraintInformationResponseBeans chServiceConstraintInformationResponseBeans;

    /**
     * Gets the value of the chServiceConstraintInformationResponseBeans property.
     * 
     * @return
     *     possible object is
     *     {@link ChServiceConstraintInformationResponseBeans }
     *     
     */
    public ChServiceConstraintInformationResponseBeans getChServiceConstraintInformationResponseBeans() {
        return chServiceConstraintInformationResponseBeans;
    }

    /**
     * Sets the value of the chServiceConstraintInformationResponseBeans property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChServiceConstraintInformationResponseBeans }
     *     
     */
    public void setChServiceConstraintInformationResponseBeans(ChServiceConstraintInformationResponseBeans value) {
        this.chServiceConstraintInformationResponseBeans = value;
    }

}
