
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBranchAllowedDepositsToOpen complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBranchAllowedDepositsToOpen">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBranchAllowedDepositsToOpenRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBranchAllowedDepositsToOpenRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBranchAllowedDepositsToOpen", propOrder = {
    "chBranchAllowedDepositsToOpenRequestBean"
})
public class GetBranchAllowedDepositsToOpen
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBranchAllowedDepositsToOpenRequestBean chBranchAllowedDepositsToOpenRequestBean;

    /**
     * Gets the value of the chBranchAllowedDepositsToOpenRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBranchAllowedDepositsToOpenRequestBean }
     *     
     */
    public ChBranchAllowedDepositsToOpenRequestBean getChBranchAllowedDepositsToOpenRequestBean() {
        return chBranchAllowedDepositsToOpenRequestBean;
    }

    /**
     * Sets the value of the chBranchAllowedDepositsToOpenRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBranchAllowedDepositsToOpenRequestBean }
     *     
     */
    public void setChBranchAllowedDepositsToOpenRequestBean(ChBranchAllowedDepositsToOpenRequestBean value) {
        this.chBranchAllowedDepositsToOpenRequestBean = value;
    }

}
