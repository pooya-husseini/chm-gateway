
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBlockedChequeReasonType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chBlockedChequeReasonType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FORGE"/>
 *     &lt;enumeration value="ROBBERY"/>
 *     &lt;enumeration value="FRAUD"/>
 *     &lt;enumeration value="LOSS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chBlockedChequeReasonType")
@XmlEnum
public enum ChBlockedChequeReasonType {

    FORGE,
    ROBBERY,
    FRAUD,
    LOSS;

    public String value() {
        return name();
    }

    public static ChBlockedChequeReasonType fromValue(String v) {
        return valueOf(v);
    }

}
