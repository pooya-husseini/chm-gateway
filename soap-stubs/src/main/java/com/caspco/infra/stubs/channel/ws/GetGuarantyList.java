
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getGuarantyList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getGuarantyList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chGuarantiesRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chGuarantiesRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getGuarantyList", propOrder = {
    "chGuarantiesRequestBean"
})
public class GetGuarantyList
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChGuarantiesRequestBean chGuarantiesRequestBean;

    /**
     * Gets the value of the chGuarantiesRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChGuarantiesRequestBean }
     *     
     */
    public ChGuarantiesRequestBean getChGuarantiesRequestBean() {
        return chGuarantiesRequestBean;
    }

    /**
     * Sets the value of the chGuarantiesRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChGuarantiesRequestBean }
     *     
     */
    public void setChGuarantiesRequestBean(ChGuarantiesRequestBean value) {
        this.chGuarantiesRequestBean = value;
    }

}
