/**
 * ChCardDepositBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public class CardDepositDto  implements java.io.Serializable {

    private AmountDto availableBalance;

    private AmountDto ledgerBalance;

    public AmountDto getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(AmountDto availableBalance) {
        this.availableBalance = availableBalance;
    }

    public AmountDto getLedgerBalance() {
        return ledgerBalance;
    }

    public void setLedgerBalance(AmountDto ledgerBalance) {
        this.ledgerBalance = ledgerBalance;
    }
}
