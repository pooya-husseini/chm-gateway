
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCreditDossierOrderField.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chCreditDossierOrderField">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NUMBER"/>
 *     &lt;enumeration value="STATUS"/>
 *     &lt;enumeration value="AVAILABLE_FUNDS"/>
 *     &lt;enumeration value="TOTAL_FUNDS"/>
 *     &lt;enumeration value="CYCLE_LENGTH"/>
 *     &lt;enumeration value="OPENING_DATE"/>
 *     &lt;enumeration value="EXPIRE_DATE"/>
 *     &lt;enumeration value="PAN"/>
 *     &lt;enumeration value="CARD_HOLDER_NAME"/>
 *     &lt;enumeration value="CARD_HOLDER_NATIONAL_CODE"/>
 *     &lt;enumeration value="CARD_STATUS"/>
 *     &lt;enumeration value="CARD_ISSUE_CAUSE"/>
 *     &lt;enumeration value="CARD_ISSUE_DATE"/>
 *     &lt;enumeration value="CARD_EXPIRE_DATE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chCreditDossierOrderField")
@XmlEnum
public enum ChCreditDossierOrderField {

    NUMBER,
    STATUS,
    AVAILABLE_FUNDS,
    TOTAL_FUNDS,
    CYCLE_LENGTH,
    OPENING_DATE,
    EXPIRE_DATE,
    PAN,
    CARD_HOLDER_NAME,
    CARD_HOLDER_NATIONAL_CODE,
    CARD_STATUS,
    CARD_ISSUE_CAUSE,
    CARD_ISSUE_DATE,
    CARD_EXPIRE_DATE;

    public String value() {
        return name();
    }

    public static ChCreditDossierOrderField fromValue(String v) {
        return valueOf(v);
    }

}
