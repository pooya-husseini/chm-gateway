
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCardPurchaseResponseBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chCardPurchaseResponseBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="availableBalance" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAmountBean" minOccurs="0"/>
 *         &lt;element name="ledgerBalance" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAmountBean" minOccurs="0"/>
 *         &lt;element name="referenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="switchResponseRRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chCardPurchaseResponseBean", propOrder = {
    "availableBalance",
    "ledgerBalance",
    "referenceNumber",
    "switchResponseRRN"
})
public class ChCardPurchaseResponseBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAmountBean availableBalance;
    protected ChAmountBean ledgerBalance;
    protected String referenceNumber;
    protected String switchResponseRRN;

    /**
     * Gets the value of the availableBalance property.
     * 
     * @return
     *     possible object is
     *     {@link ChAmountBean }
     *     
     */
    public ChAmountBean getAvailableBalance() {
        return availableBalance;
    }

    /**
     * Sets the value of the availableBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAmountBean }
     *     
     */
    public void setAvailableBalance(ChAmountBean value) {
        this.availableBalance = value;
    }

    /**
     * Gets the value of the ledgerBalance property.
     * 
     * @return
     *     possible object is
     *     {@link ChAmountBean }
     *     
     */
    public ChAmountBean getLedgerBalance() {
        return ledgerBalance;
    }

    /**
     * Sets the value of the ledgerBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAmountBean }
     *     
     */
    public void setLedgerBalance(ChAmountBean value) {
        this.ledgerBalance = value;
    }

    /**
     * Gets the value of the referenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Sets the value of the referenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumber(String value) {
        this.referenceNumber = value;
    }

    /**
     * Gets the value of the switchResponseRRN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwitchResponseRRN() {
        return switchResponseRRN;
    }

    /**
     * Sets the value of the switchResponseRRN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwitchResponseRRN(String value) {
        this.switchResponseRRN = value;
    }

}
