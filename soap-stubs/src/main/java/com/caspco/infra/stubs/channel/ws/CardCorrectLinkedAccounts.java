
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardCorrectLinkedAccounts complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardCorrectLinkedAccounts">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardCorrectLinkedAccountsRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardCorrectLinkedAccountsRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardCorrectLinkedAccounts", propOrder = {
    "chCardCorrectLinkedAccountsRequestBean"
})
public class CardCorrectLinkedAccounts
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardCorrectLinkedAccountsRequestBean chCardCorrectLinkedAccountsRequestBean;

    /**
     * Gets the value of the chCardCorrectLinkedAccountsRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardCorrectLinkedAccountsRequestBean }
     *     
     */
    public ChCardCorrectLinkedAccountsRequestBean getChCardCorrectLinkedAccountsRequestBean() {
        return chCardCorrectLinkedAccountsRequestBean;
    }

    /**
     * Sets the value of the chCardCorrectLinkedAccountsRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardCorrectLinkedAccountsRequestBean }
     *     
     */
    public void setChCardCorrectLinkedAccountsRequestBean(ChCardCorrectLinkedAccountsRequestBean value) {
        this.chCardCorrectLinkedAccountsRequestBean = value;
    }

}
