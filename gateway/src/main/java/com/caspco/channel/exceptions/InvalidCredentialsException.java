package com.caspco.channel.exceptions;

import java.util.Map;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/22/17
 * Time: 3:55 PM
 */
public class InvalidCredentialsException extends OpenApiException {

    public InvalidCredentialsException(String message, Map<String,?> args) {
        super(message, args);
    }

    public InvalidCredentialsException(String message) {
        super(message);
    }

    public InvalidCredentialsException(ExceptionDescription description) {
        super(description);
    }

    public InvalidCredentialsException(String message, ExceptionDescription description) {
        super(message, description);
    }

    public InvalidCredentialsException(String message, Throwable cause, ExceptionDescription description) {
        super(message, cause, description);
    }

    public InvalidCredentialsException(Throwable cause, ExceptionDescription description) {
        super(cause, description);
    }

    public InvalidCredentialsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ExceptionDescription description) {
        super(message, cause, enableSuppression, writableStackTrace, description);
    }
}
