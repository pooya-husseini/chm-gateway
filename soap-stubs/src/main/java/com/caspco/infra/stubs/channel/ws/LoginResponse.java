
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for loginResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loginResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userToken" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chLoginResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loginResponse", propOrder = {
    "userToken"
})
public class LoginResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChLoginResponseBean userToken;

    /**
     * Gets the value of the userToken property.
     * 
     * @return
     *     possible object is
     *     {@link ChLoginResponseBean }
     *     
     */
    public ChLoginResponseBean getUserToken() {
        return userToken;
    }

    /**
     * Sets the value of the userToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChLoginResponseBean }
     *     
     */
    public void setUserToken(ChLoginResponseBean value) {
        this.userToken = value;
    }

}
