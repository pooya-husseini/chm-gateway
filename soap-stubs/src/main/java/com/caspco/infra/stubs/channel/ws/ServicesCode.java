
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for servicesCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="servicesCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CARD_TRANSFER"/>
 *     &lt;enumeration value="NORMAL_TRANSFER"/>
 *     &lt;enumeration value="ACH_NORMAL_TRANSFER"/>
 *     &lt;enumeration value="RTGS_NORMAL_TRANSFER"/>
 *     &lt;enumeration value="GET_STATEMENT"/>
 *     &lt;enumeration value="GET_DEPOSITS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "servicesCode")
@XmlEnum
public enum ServicesCode {

    CARD_TRANSFER,
    NORMAL_TRANSFER,
    ACH_NORMAL_TRANSFER,
    RTGS_NORMAL_TRANSFER,
    GET_STATEMENT,
    GET_DEPOSITS;

    public String value() {
        return name();
    }

    public static ServicesCode fromValue(String v) {
        return valueOf(v);
    }

}
