
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;


/**
 * <p>Java class for cardSetPassword complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardSetPassword">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardCreatePasswordRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardCreatePasswordRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardSetPassword", propOrder = {
    "chCardCreatePasswordRequestBean"
})
public class CardSetPassword
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardCreatePasswordRequestBean chCardCreatePasswordRequestBean;

    /**
     * Gets the value of the chCardCreatePasswordRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardCreatePasswordRequestBean }
     *     
     */
    public ChCardCreatePasswordRequestBean getChCardCreatePasswordRequestBean() {
        return chCardCreatePasswordRequestBean;
    }

    /**
     * Sets the value of the chCardCreatePasswordRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardCreatePasswordRequestBean }
     *     
     */
    public void setChCardCreatePasswordRequestBean(ChCardCreatePasswordRequestBean value) {
        this.chCardCreatePasswordRequestBean = value;
    }

}
