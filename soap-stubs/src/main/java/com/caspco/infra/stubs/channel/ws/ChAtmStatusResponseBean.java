
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chAtmStatusResponseBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chAtmStatusResponseBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="atmCassettes" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAtmStatusCassetteResponseBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="atmDetail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="atmModel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="atmType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="balanceDate" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="balanceTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="lastStateDate" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="lastStateTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="latitude" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="location" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="longitude" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="stateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="terminalIp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="terminalNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="terminalStatus" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="terminalStatusTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="terminalTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totalBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="userStatus" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="userStatusTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chAtmStatusResponseBean", propOrder = {
    "address",
    "atmCassettes",
    "atmDetail",
    "atmModel",
    "atmType",
    "balanceDate",
    "balanceTime",
    "lastStateDate",
    "lastStateTime",
    "latitude",
    "location",
    "longitude",
    "stateDate",
    "terminalIp",
    "terminalNumber",
    "terminalStatus",
    "terminalStatusTitle",
    "terminalTitle",
    "totalBalance",
    "userStatus",
    "userStatusTitle",
    "version"
})
public class ChAtmStatusResponseBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String address;
    @XmlElement(nillable = true)
    protected List<ChAtmStatusCassetteResponseBean> atmCassettes;
    protected String atmDetail;
    protected String atmModel;
    protected String atmType;
    protected Integer balanceDate;
    protected Integer balanceTime;
    protected Integer lastStateDate;
    protected Integer lastStateTime;
    protected Float latitude;
    protected String location;
    protected Float longitude;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date stateDate;
    protected String terminalIp;
    protected Integer terminalNumber;
    protected Integer terminalStatus;
    protected String terminalStatusTitle;
    protected String terminalTitle;
    protected BigDecimal totalBalance;
    protected Integer userStatus;
    protected String userStatusTitle;
    protected String version;

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the atmCassettes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the atmCassettes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAtmCassettes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChAtmStatusCassetteResponseBean }
     * 
     * 
     */
    public List<ChAtmStatusCassetteResponseBean> getAtmCassettes() {
        if (atmCassettes == null) {
            atmCassettes = new ArrayList<ChAtmStatusCassetteResponseBean>();
        }
        return this.atmCassettes;
    }

    /**
     * Gets the value of the atmDetail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtmDetail() {
        return atmDetail;
    }

    /**
     * Sets the value of the atmDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtmDetail(String value) {
        this.atmDetail = value;
    }

    /**
     * Gets the value of the atmModel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtmModel() {
        return atmModel;
    }

    /**
     * Sets the value of the atmModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtmModel(String value) {
        this.atmModel = value;
    }

    /**
     * Gets the value of the atmType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtmType() {
        return atmType;
    }

    /**
     * Sets the value of the atmType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtmType(String value) {
        this.atmType = value;
    }

    /**
     * Gets the value of the balanceDate property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBalanceDate() {
        return balanceDate;
    }

    /**
     * Sets the value of the balanceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBalanceDate(Integer value) {
        this.balanceDate = value;
    }

    /**
     * Gets the value of the balanceTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBalanceTime() {
        return balanceTime;
    }

    /**
     * Sets the value of the balanceTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBalanceTime(Integer value) {
        this.balanceTime = value;
    }

    /**
     * Gets the value of the lastStateDate property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLastStateDate() {
        return lastStateDate;
    }

    /**
     * Sets the value of the lastStateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLastStateDate(Integer value) {
        this.lastStateDate = value;
    }

    /**
     * Gets the value of the lastStateTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLastStateTime() {
        return lastStateTime;
    }

    /**
     * Sets the value of the lastStateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLastStateTime(Integer value) {
        this.lastStateTime = value;
    }

    /**
     * Gets the value of the latitude property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getLatitude() {
        return latitude;
    }

    /**
     * Sets the value of the latitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setLatitude(Float value) {
        this.latitude = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Gets the value of the longitude property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getLongitude() {
        return longitude;
    }

    /**
     * Sets the value of the longitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setLongitude(Float value) {
        this.longitude = value;
    }

    /**
     * Gets the value of the stateDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getStateDate() {
        return stateDate;
    }

    /**
     * Sets the value of the stateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateDate(Date value) {
        this.stateDate = value;
    }

    /**
     * Gets the value of the terminalIp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminalIp() {
        return terminalIp;
    }

    /**
     * Sets the value of the terminalIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminalIp(String value) {
        this.terminalIp = value;
    }

    /**
     * Gets the value of the terminalNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTerminalNumber() {
        return terminalNumber;
    }

    /**
     * Sets the value of the terminalNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTerminalNumber(Integer value) {
        this.terminalNumber = value;
    }

    /**
     * Gets the value of the terminalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTerminalStatus() {
        return terminalStatus;
    }

    /**
     * Sets the value of the terminalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTerminalStatus(Integer value) {
        this.terminalStatus = value;
    }

    /**
     * Gets the value of the terminalStatusTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminalStatusTitle() {
        return terminalStatusTitle;
    }

    /**
     * Sets the value of the terminalStatusTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminalStatusTitle(String value) {
        this.terminalStatusTitle = value;
    }

    /**
     * Gets the value of the terminalTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminalTitle() {
        return terminalTitle;
    }

    /**
     * Sets the value of the terminalTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminalTitle(String value) {
        this.terminalTitle = value;
    }

    /**
     * Gets the value of the totalBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalBalance() {
        return totalBalance;
    }

    /**
     * Sets the value of the totalBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalBalance(BigDecimal value) {
        this.totalBalance = value;
    }

    /**
     * Gets the value of the userStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUserStatus() {
        return userStatus;
    }

    /**
     * Sets the value of the userStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUserStatus(Integer value) {
        this.userStatus = value;
    }

    /**
     * Gets the value of the userStatusTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserStatusTitle() {
        return userStatusTitle;
    }

    /**
     * Sets the value of the userStatusTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserStatusTitle(String value) {
        this.userStatusTitle = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

}
