/**
 * ServicesCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public enum ServicesCode implements java.io.Serializable {
    CARD_TRANSFER,
    GET_DEPOSITS,
    GET_STATEMENT,
    RTGS_NORMAL_TRANSFER,
    ACH_NORMAL_TRANSFER,
    NORMAL_TRANSFER;


}
