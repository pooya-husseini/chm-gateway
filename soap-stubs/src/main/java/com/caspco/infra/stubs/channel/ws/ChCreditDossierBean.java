
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chCreditDossierBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chCreditDossierBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="availableFund" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customer" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCustomerInformationBean" minOccurs="0"/>
 *         &lt;element name="cycleLength" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDossierCycleLength" minOccurs="0"/>
 *         &lt;element name="defaultCard" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCompleteCardBean" minOccurs="0"/>
 *         &lt;element name="expireDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="openDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="status" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCreditDossierStatus" minOccurs="0"/>
 *         &lt;element name="totalFund" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chCreditDossierBean", propOrder = {
    "availableFund",
    "currency",
    "customer",
    "cycleLength",
    "defaultCard",
    "expireDate",
    "number",
    "openDate",
    "status",
    "totalFund"
})
public class ChCreditDossierBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigDecimal availableFund;
    protected String currency;
    protected ChCustomerInformationBean customer;
    @XmlSchemaType(name = "string")
    protected ChDossierCycleLength cycleLength;
    protected ChCompleteCardBean defaultCard;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date expireDate;
    protected String number;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date openDate;
    @XmlSchemaType(name = "string")
    protected ChCreditDossierStatus status;
    protected BigDecimal totalFund;

    /**
     * Gets the value of the availableFund property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAvailableFund() {
        return availableFund;
    }

    /**
     * Sets the value of the availableFund property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAvailableFund(BigDecimal value) {
        this.availableFund = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link ChCustomerInformationBean }
     *     
     */
    public ChCustomerInformationBean getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCustomerInformationBean }
     *     
     */
    public void setCustomer(ChCustomerInformationBean value) {
        this.customer = value;
    }

    /**
     * Gets the value of the cycleLength property.
     * 
     * @return
     *     possible object is
     *     {@link ChDossierCycleLength }
     *     
     */
    public ChDossierCycleLength getCycleLength() {
        return cycleLength;
    }

    /**
     * Sets the value of the cycleLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDossierCycleLength }
     *     
     */
    public void setCycleLength(ChDossierCycleLength value) {
        this.cycleLength = value;
    }

    /**
     * Gets the value of the defaultCard property.
     * 
     * @return
     *     possible object is
     *     {@link ChCompleteCardBean }
     *     
     */
    public ChCompleteCardBean getDefaultCard() {
        return defaultCard;
    }

    /**
     * Sets the value of the defaultCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCompleteCardBean }
     *     
     */
    public void setDefaultCard(ChCompleteCardBean value) {
        this.defaultCard = value;
    }

    /**
     * Gets the value of the expireDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getExpireDate() {
        return expireDate;
    }

    /**
     * Sets the value of the expireDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpireDate(Date value) {
        this.expireDate = value;
    }

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

    /**
     * Gets the value of the openDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getOpenDate() {
        return openDate;
    }

    /**
     * Sets the value of the openDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpenDate(Date value) {
        this.openDate = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link ChCreditDossierStatus }
     *     
     */
    public ChCreditDossierStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCreditDossierStatus }
     *     
     */
    public void setStatus(ChCreditDossierStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the totalFund property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalFund() {
        return totalFund;
    }

    /**
     * Sets the value of the totalFund property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalFund(BigDecimal value) {
        this.totalFund = value;
    }

}
