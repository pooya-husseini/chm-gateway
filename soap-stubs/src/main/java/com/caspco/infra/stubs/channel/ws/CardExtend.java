
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardExtend complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardExtend">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardExtendRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardExtendRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardExtend", propOrder = {
    "chCardExtendRequestBean"
})
public class CardExtend
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardExtendRequestBean chCardExtendRequestBean;

    /**
     * Gets the value of the chCardExtendRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardExtendRequestBean }
     *     
     */
    public ChCardExtendRequestBean getChCardExtendRequestBean() {
        return chCardExtendRequestBean;
    }

    /**
     * Sets the value of the chCardExtendRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardExtendRequestBean }
     *     
     */
    public void setChCardExtendRequestBean(ChCardExtendRequestBean value) {
        this.chCardExtendRequestBean = value;
    }

}
