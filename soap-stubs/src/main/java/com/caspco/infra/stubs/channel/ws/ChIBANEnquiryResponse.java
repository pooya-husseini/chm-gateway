
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chIBANEnquiryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chIBANEnquiryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestedIBan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentCodeValid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountOwner" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAccountOwner" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RequiredPaymentCode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chIBANEnquiryResponse", propOrder = {
    "requestedIBan",
    "referenceNumber",
    "accountNumber",
    "accountStatus",
    "paymentCode",
    "paymentCodeValid",
    "accountComment",
    "accountOwner",
    "requiredPaymentCode"
})
public class ChIBANEnquiryResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "RequestedIBan")
    protected String requestedIBan;
    @XmlElement(name = "ReferenceNumber")
    protected String referenceNumber;
    @XmlElement(name = "AccountNumber")
    protected String accountNumber;
    @XmlElement(name = "AccountStatus")
    protected String accountStatus;
    @XmlElement(name = "PaymentCode")
    protected String paymentCode;
    @XmlElement(name = "PaymentCodeValid")
    protected String paymentCodeValid;
    @XmlElement(name = "AccountComment")
    protected String accountComment;
    @XmlElement(name = "AccountOwner")
    protected List<ChAccountOwner> accountOwner;
    @XmlElement(name = "RequiredPaymentCode")
    protected Boolean requiredPaymentCode;

    /**
     * Gets the value of the requestedIBan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestedIBan() {
        return requestedIBan;
    }

    /**
     * Sets the value of the requestedIBan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestedIBan(String value) {
        this.requestedIBan = value;
    }

    /**
     * Gets the value of the referenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Sets the value of the referenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumber(String value) {
        this.referenceNumber = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the accountStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountStatus() {
        return accountStatus;
    }

    /**
     * Sets the value of the accountStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountStatus(String value) {
        this.accountStatus = value;
    }

    /**
     * Gets the value of the paymentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentCode() {
        return paymentCode;
    }

    /**
     * Sets the value of the paymentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentCode(String value) {
        this.paymentCode = value;
    }

    /**
     * Gets the value of the paymentCodeValid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentCodeValid() {
        return paymentCodeValid;
    }

    /**
     * Sets the value of the paymentCodeValid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentCodeValid(String value) {
        this.paymentCodeValid = value;
    }

    /**
     * Gets the value of the accountComment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountComment() {
        return accountComment;
    }

    /**
     * Sets the value of the accountComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountComment(String value) {
        this.accountComment = value;
    }

    /**
     * Gets the value of the accountOwner property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountOwner property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountOwner().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChAccountOwner }
     * 
     * 
     */
    public List<ChAccountOwner> getAccountOwner() {
        if (accountOwner == null) {
            accountOwner = new ArrayList<ChAccountOwner>();
        }
        return this.accountOwner;
    }

    /**
     * Gets the value of the requiredPaymentCode property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequiredPaymentCode() {
        return requiredPaymentCode;
    }

    /**
     * Sets the value of the requiredPaymentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequiredPaymentCode(Boolean value) {
        this.requiredPaymentCode = value;
    }

}
