
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPaymentServicesByTypeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPaymentServicesByTypeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chPaymentServicesResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPaymentServicesResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPaymentServicesByTypeResponse", propOrder = {
    "chPaymentServicesResponseBean"
})
public class GetPaymentServicesByTypeResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChPaymentServicesResponseBean chPaymentServicesResponseBean;

    /**
     * Gets the value of the chPaymentServicesResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChPaymentServicesResponseBean }
     *     
     */
    public ChPaymentServicesResponseBean getChPaymentServicesResponseBean() {
        return chPaymentServicesResponseBean;
    }

    /**
     * Sets the value of the chPaymentServicesResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPaymentServicesResponseBean }
     *     
     */
    public void setChPaymentServicesResponseBean(ChPaymentServicesResponseBean value) {
        this.chPaymentServicesResponseBean = value;
    }

}
