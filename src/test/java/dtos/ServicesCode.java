/**
 * ServicesCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;

import java.util.HashMap;

@ApiModel
public enum ServicesCode implements java.io.Serializable {
    CARD_TRANSFER,
    GET_DEPOSITS,
    GET_STATEMENT,
    RTGS_NORMAL_TRANSFER,
    ACH_NORMAL_TRANSFER,
    NORMAL_TRANSFER;

    @JsonCreator
    public static ServicesCode fromStringOperator(HashMap<String, String> map) {
        if (map != null) {
            for (ServicesCode operator : ServicesCode.values()) {
                if (map.get("value").equals(operator.name())) {
                    return operator;
                }
            }
        }
        return null;
    }



}
