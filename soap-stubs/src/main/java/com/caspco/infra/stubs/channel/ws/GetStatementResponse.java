
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStatementResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStatementResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chStatementResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chStatementResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStatementResponse", propOrder = {
    "chStatementResponseBean"
})
public class GetStatementResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChStatementResponseBean chStatementResponseBean;

    /**
     * Gets the value of the chStatementResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChStatementResponseBean }
     *     
     */
    public ChStatementResponseBean getChStatementResponseBean() {
        return chStatementResponseBean;
    }

    /**
     * Sets the value of the chStatementResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChStatementResponseBean }
     *     
     */
    public void setChStatementResponseBean(ChStatementResponseBean value) {
        this.chStatementResponseBean = value;
    }

}
