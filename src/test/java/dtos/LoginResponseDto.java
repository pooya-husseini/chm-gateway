/**
 * ChLoginResponseBean.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import io.swagger.annotations.ApiModel;

@ApiModel
public class LoginResponseDto implements java.io.Serializable {

    private java.lang.String code;

    private java.lang.Long customerNo;

    private java.lang.String foreignName;

    private com.caspco.channel.dtos.Gender gender;

    private java.util.Date lastLoginTime;


    private java.lang.String name;

    private java.lang.Boolean requiredChangeSecondPassword;

    private java.lang.String sessionId;

    private java.lang.String title;


    public LoginResponseDto() {
    }


    /**
     * Gets the activities value for this ChLoginResponseBean.
     *
     * @return activities
     */


    /**
     * Gets the code value for this ChLoginResponseBean.
     *
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this ChLoginResponseBean.
     *
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the customerNo value for this ChLoginResponseBean.
     *
     * @return customerNo
     */
    public java.lang.Long getCustomerNo() {
        return customerNo;
    }


    /**
     * Sets the customerNo value for this ChLoginResponseBean.
     *
     * @param customerNo
     */
    public void setCustomerNo(java.lang.Long customerNo) {
        this.customerNo = customerNo;
    }


    /**
     * Gets the foreignName value for this ChLoginResponseBean.
     *
     * @return foreignName
     */
    public java.lang.String getForeignName() {
        return foreignName;
    }


    /**
     * Sets the foreignName value for this ChLoginResponseBean.
     *
     * @param foreignName
     */
    public void setForeignName(java.lang.String foreignName) {
        this.foreignName = foreignName;
    }


    /**
     * Gets the gender value for this ChLoginResponseBean.
     *
     * @return gender
     */
    public com.caspco.channel.dtos.Gender getGender() {
        return gender;
    }


    /**
     * Sets the gender value for this ChLoginResponseBean.
     *
     * @param gender
     */
    public void setGender(com.caspco.channel.dtos.Gender gender) {
        this.gender = gender;
    }


    /**
     * Gets the lastLoginTime value for this ChLoginResponseBean.
     *
     * @return lastLoginTime
     */
    public java.util.Date getLastLoginTime() {
        return lastLoginTime;
    }


    /**
     * Sets the lastLoginTime value for this ChLoginResponseBean.
     *
     * @param lastLoginTime
     */
    public void setLastLoginTime(java.util.Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }


    /**
     * Gets the name value for this ChLoginResponseBean.
     *
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this ChLoginResponseBean.
     *
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the requiredChangeSecondPassword value for this ChLoginResponseBean.
     *
     * @return requiredChangeSecondPassword
     */
    public java.lang.Boolean getRequiredChangeSecondPassword() {
        return requiredChangeSecondPassword;
    }


    /**
     * Sets the requiredChangeSecondPassword value for this ChLoginResponseBean.
     *
     * @param requiredChangeSecondPassword
     */
    public void setRequiredChangeSecondPassword(java.lang.Boolean requiredChangeSecondPassword) {
        this.requiredChangeSecondPassword = requiredChangeSecondPassword;
    }


    /**
     * Gets the sessionId value for this ChLoginResponseBean.
     *
     * @return sessionId
     */
    public java.lang.String getSessionId() {
        return sessionId;
    }


    /**
     * Sets the sessionId value for this ChLoginResponseBean.
     *
     * @param sessionId
     */
    public void setSessionId(java.lang.String sessionId) {
        this.sessionId = sessionId;
    }


    /**
     * Gets the title value for this ChLoginResponseBean.
     *
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }


    /**
     * Sets the title value for this ChLoginResponseBean.
     *
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }

}
