
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getMerchantInfoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getMerchantInfoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chMerchantInfoResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chMerchantInfoResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMerchantInfoResponse", propOrder = {
    "chMerchantInfoResponseBean"
})
public class GetMerchantInfoResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChMerchantInfoResponseBean chMerchantInfoResponseBean;

    /**
     * Gets the value of the chMerchantInfoResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChMerchantInfoResponseBean }
     *     
     */
    public ChMerchantInfoResponseBean getChMerchantInfoResponseBean() {
        return chMerchantInfoResponseBean;
    }

    /**
     * Sets the value of the chMerchantInfoResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChMerchantInfoResponseBean }
     *     
     */
    public void setChMerchantInfoResponseBean(ChMerchantInfoResponseBean value) {
        this.chMerchantInfoResponseBean = value;
    }

}
