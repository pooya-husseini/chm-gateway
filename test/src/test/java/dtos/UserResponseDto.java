/**
 * ChUserResponseBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;


public class UserResponseDto  implements java.io.Serializable {
    private String cif;

    private String code;

    private String foreignName;

    private Gender gender;

    private String name;

    private String title;

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getForeignName() {
        return foreignName;
    }

    public void setForeignName(String foreignName) {
        this.foreignName = foreignName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "UserResponseDto{" +
                "cif='" + cif + '\'' +
                ", code='" + code + '\'' +
                ", foreignName='" + foreignName + '\'' +
                ", gender=" + gender +
                ", name='" + name + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
