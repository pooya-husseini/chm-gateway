
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chSellReportRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chSellReportRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amountMax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="amountMin" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="billId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="billTypes" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="customerRefNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="merchantId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="offset" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="onlyReversed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="orderField" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSellOrderField" minOccurs="0"/>
 *         &lt;element name="orderType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chOrderType" minOccurs="0"/>
 *         &lt;element name="paymentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="timeMax" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="timeMin" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="txState" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSellTransactionState" minOccurs="0"/>
 *         &lt;element name="txType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSellTransactionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chSellReportRequestBean", propOrder = {
    "amountMax",
    "amountMin",
    "billId",
    "billTypes",
    "customerRefNum",
    "length",
    "merchantId",
    "offset",
    "onlyReversed",
    "orderField",
    "orderType",
    "paymentId",
    "refNum",
    "resNum",
    "timeMax",
    "timeMin",
    "txState",
    "txType"
})
public class ChSellReportRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigDecimal amountMax;
    protected BigDecimal amountMin;
    protected String billId;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "string")
    protected List<ChBillType> billTypes;
    protected String customerRefNum;
    protected Long length;
    protected String merchantId;
    protected Long offset;
    protected boolean onlyReversed;
    @XmlSchemaType(name = "string")
    protected ChSellOrderField orderField;
    @XmlSchemaType(name = "string")
    protected ChOrderType orderType;
    protected String paymentId;
    protected String refNum;
    protected String resNum;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date timeMax;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date timeMin;
    @XmlSchemaType(name = "string")
    protected ChSellTransactionState txState;
    @XmlSchemaType(name = "string")
    protected ChSellTransactionType txType;

    /**
     * Gets the value of the amountMax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountMax() {
        return amountMax;
    }

    /**
     * Sets the value of the amountMax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountMax(BigDecimal value) {
        this.amountMax = value;
    }

    /**
     * Gets the value of the amountMin property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountMin() {
        return amountMin;
    }

    /**
     * Sets the value of the amountMin property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountMin(BigDecimal value) {
        this.amountMin = value;
    }

    /**
     * Gets the value of the billId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillId() {
        return billId;
    }

    /**
     * Sets the value of the billId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillId(String value) {
        this.billId = value;
    }

    /**
     * Gets the value of the billTypes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the billTypes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBillTypes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChBillType }
     * 
     * 
     */
    public List<ChBillType> getBillTypes() {
        if (billTypes == null) {
            billTypes = new ArrayList<ChBillType>();
        }
        return this.billTypes;
    }

    /**
     * Gets the value of the customerRefNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRefNum() {
        return customerRefNum;
    }

    /**
     * Sets the value of the customerRefNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRefNum(String value) {
        this.customerRefNum = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLength(Long value) {
        this.length = value;
    }

    /**
     * Gets the value of the merchantId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * Sets the value of the merchantId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantId(String value) {
        this.merchantId = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOffset(Long value) {
        this.offset = value;
    }

    /**
     * Gets the value of the onlyReversed property.
     * 
     */
    public boolean isOnlyReversed() {
        return onlyReversed;
    }

    /**
     * Sets the value of the onlyReversed property.
     * 
     */
    public void setOnlyReversed(boolean value) {
        this.onlyReversed = value;
    }

    /**
     * Gets the value of the orderField property.
     * 
     * @return
     *     possible object is
     *     {@link ChSellOrderField }
     *     
     */
    public ChSellOrderField getOrderField() {
        return orderField;
    }

    /**
     * Sets the value of the orderField property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSellOrderField }
     *     
     */
    public void setOrderField(ChSellOrderField value) {
        this.orderField = value;
    }

    /**
     * Gets the value of the orderType property.
     * 
     * @return
     *     possible object is
     *     {@link ChOrderType }
     *     
     */
    public ChOrderType getOrderType() {
        return orderType;
    }

    /**
     * Sets the value of the orderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChOrderType }
     *     
     */
    public void setOrderType(ChOrderType value) {
        this.orderType = value;
    }

    /**
     * Gets the value of the paymentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * Sets the value of the paymentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentId(String value) {
        this.paymentId = value;
    }

    /**
     * Gets the value of the refNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefNum() {
        return refNum;
    }

    /**
     * Sets the value of the refNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefNum(String value) {
        this.refNum = value;
    }

    /**
     * Gets the value of the resNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResNum() {
        return resNum;
    }

    /**
     * Sets the value of the resNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResNum(String value) {
        this.resNum = value;
    }

    /**
     * Gets the value of the timeMax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getTimeMax() {
        return timeMax;
    }

    /**
     * Sets the value of the timeMax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeMax(Date value) {
        this.timeMax = value;
    }

    /**
     * Gets the value of the timeMin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getTimeMin() {
        return timeMin;
    }

    /**
     * Sets the value of the timeMin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeMin(Date value) {
        this.timeMin = value;
    }

    /**
     * Gets the value of the txState property.
     * 
     * @return
     *     possible object is
     *     {@link ChSellTransactionState }
     *     
     */
    public ChSellTransactionState getTxState() {
        return txState;
    }

    /**
     * Sets the value of the txState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSellTransactionState }
     *     
     */
    public void setTxState(ChSellTransactionState value) {
        this.txState = value;
    }

    /**
     * Gets the value of the txType property.
     * 
     * @return
     *     possible object is
     *     {@link ChSellTransactionType }
     *     
     */
    public ChSellTransactionType getTxType() {
        return txType;
    }

    /**
     * Sets the value of the txType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSellTransactionType }
     *     
     */
    public void setTxType(ChSellTransactionType value) {
        this.txType = value;
    }

}
