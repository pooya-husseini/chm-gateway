
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reportAchTransferSummaryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reportAchTransferSummaryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAchTransferSummeryBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAchTransferSummeryBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reportAchTransferSummaryResponse", propOrder = {
    "chAchTransferSummeryBean"
})
public class ReportAchTransferSummaryResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAchTransferSummeryBean chAchTransferSummeryBean;

    /**
     * Gets the value of the chAchTransferSummeryBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAchTransferSummeryBean }
     *     
     */
    public ChAchTransferSummeryBean getChAchTransferSummeryBean() {
        return chAchTransferSummeryBean;
    }

    /**
     * Sets the value of the chAchTransferSummeryBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAchTransferSummeryBean }
     *     
     */
    public void setChAchTransferSummeryBean(ChAchTransferSummeryBean value) {
        this.chAchTransferSummeryBean = value;
    }

}
