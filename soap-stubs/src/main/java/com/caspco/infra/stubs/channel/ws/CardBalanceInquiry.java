
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardBalanceInquiry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardBalanceInquiry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardBalanceRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardBalanceRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardBalanceInquiry", propOrder = {
    "chCardBalanceRequestBean"
})
public class CardBalanceInquiry
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardBalanceRequestBean chCardBalanceRequestBean;

    /**
     * Gets the value of the chCardBalanceRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardBalanceRequestBean }
     *     
     */
    public ChCardBalanceRequestBean getChCardBalanceRequestBean() {
        return chCardBalanceRequestBean;
    }

    /**
     * Sets the value of the chCardBalanceRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardBalanceRequestBean }
     *     
     */
    public void setChCardBalanceRequestBean(ChCardBalanceRequestBean value) {
        this.chCardBalanceRequestBean = value;
    }

}
