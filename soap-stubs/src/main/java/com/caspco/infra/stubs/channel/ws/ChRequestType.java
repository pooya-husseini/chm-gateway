
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chRequestType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chRequestType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SIGNER"/>
 *     &lt;enumeration value="REQUESTER"/>
 *     &lt;enumeration value="BOTH"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chRequestType")
@XmlEnum
public enum ChRequestType {

    SIGNER,
    REQUESTER,
    BOTH;

    public String value() {
        return name();
    }

    public static ChRequestType fromValue(String v) {
        return valueOf(v);
    }

}
