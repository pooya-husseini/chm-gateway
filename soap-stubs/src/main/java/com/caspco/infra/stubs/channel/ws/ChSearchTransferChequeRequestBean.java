
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chSearchTransferChequeRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chSearchTransferChequeRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="branchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="depositNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deviseeBankCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fromBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="fromDueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fromNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fromPassDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fromRegisterDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="includeStatus" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chTransferChequeStatus" minOccurs="0"/>
 *         &lt;element name="includeType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chChequeType" minOccurs="0"/>
 *         &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="offset" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="orderMethod" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chChequeFieldOrder" minOccurs="0"/>
 *         &lt;element name="orderType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chOrderType" minOccurs="0"/>
 *         &lt;element name="toBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="toDueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="toNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="toPassDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="toRegisterDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chSearchTransferChequeRequestBean", propOrder = {
    "branchCode",
    "depositNumber",
    "deviseeBankCode",
    "fromBalance",
    "fromDueDate",
    "fromNumber",
    "fromPassDate",
    "fromRegisterDate",
    "includeStatus",
    "includeType",
    "length",
    "offset",
    "orderMethod",
    "orderType",
    "toBalance",
    "toDueDate",
    "toNumber",
    "toPassDate",
    "toRegisterDate"
})
public class ChSearchTransferChequeRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String branchCode;
    protected String depositNumber;
    protected String deviseeBankCode;
    protected BigDecimal fromBalance;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date fromDueDate;
    protected String fromNumber;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date fromPassDate;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date fromRegisterDate;
    @XmlSchemaType(name = "string")
    protected ChTransferChequeStatus includeStatus;
    @XmlSchemaType(name = "string")
    protected ChChequeType includeType;
    protected Long length;
    protected Long offset;
    @XmlSchemaType(name = "string")
    protected ChChequeFieldOrder orderMethod;
    @XmlSchemaType(name = "string")
    protected ChOrderType orderType;
    protected BigDecimal toBalance;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date toDueDate;
    protected String toNumber;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date toPassDate;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date toRegisterDate;

    /**
     * Gets the value of the branchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * Sets the value of the branchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchCode(String value) {
        this.branchCode = value;
    }

    /**
     * Gets the value of the depositNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepositNumber() {
        return depositNumber;
    }

    /**
     * Sets the value of the depositNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepositNumber(String value) {
        this.depositNumber = value;
    }

    /**
     * Gets the value of the deviseeBankCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviseeBankCode() {
        return deviseeBankCode;
    }

    /**
     * Sets the value of the deviseeBankCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviseeBankCode(String value) {
        this.deviseeBankCode = value;
    }

    /**
     * Gets the value of the fromBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFromBalance() {
        return fromBalance;
    }

    /**
     * Sets the value of the fromBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFromBalance(BigDecimal value) {
        this.fromBalance = value;
    }

    /**
     * Gets the value of the fromDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getFromDueDate() {
        return fromDueDate;
    }

    /**
     * Sets the value of the fromDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromDueDate(Date value) {
        this.fromDueDate = value;
    }

    /**
     * Gets the value of the fromNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromNumber() {
        return fromNumber;
    }

    /**
     * Sets the value of the fromNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromNumber(String value) {
        this.fromNumber = value;
    }

    /**
     * Gets the value of the fromPassDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getFromPassDate() {
        return fromPassDate;
    }

    /**
     * Sets the value of the fromPassDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromPassDate(Date value) {
        this.fromPassDate = value;
    }

    /**
     * Gets the value of the fromRegisterDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getFromRegisterDate() {
        return fromRegisterDate;
    }

    /**
     * Sets the value of the fromRegisterDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromRegisterDate(Date value) {
        this.fromRegisterDate = value;
    }

    /**
     * Gets the value of the includeStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ChTransferChequeStatus }
     *     
     */
    public ChTransferChequeStatus getIncludeStatus() {
        return includeStatus;
    }

    /**
     * Sets the value of the includeStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChTransferChequeStatus }
     *     
     */
    public void setIncludeStatus(ChTransferChequeStatus value) {
        this.includeStatus = value;
    }

    /**
     * Gets the value of the includeType property.
     * 
     * @return
     *     possible object is
     *     {@link ChChequeType }
     *     
     */
    public ChChequeType getIncludeType() {
        return includeType;
    }

    /**
     * Sets the value of the includeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChChequeType }
     *     
     */
    public void setIncludeType(ChChequeType value) {
        this.includeType = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLength(Long value) {
        this.length = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOffset(Long value) {
        this.offset = value;
    }

    /**
     * Gets the value of the orderMethod property.
     * 
     * @return
     *     possible object is
     *     {@link ChChequeFieldOrder }
     *     
     */
    public ChChequeFieldOrder getOrderMethod() {
        return orderMethod;
    }

    /**
     * Sets the value of the orderMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChChequeFieldOrder }
     *     
     */
    public void setOrderMethod(ChChequeFieldOrder value) {
        this.orderMethod = value;
    }

    /**
     * Gets the value of the orderType property.
     * 
     * @return
     *     possible object is
     *     {@link ChOrderType }
     *     
     */
    public ChOrderType getOrderType() {
        return orderType;
    }

    /**
     * Sets the value of the orderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChOrderType }
     *     
     */
    public void setOrderType(ChOrderType value) {
        this.orderType = value;
    }

    /**
     * Gets the value of the toBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getToBalance() {
        return toBalance;
    }

    /**
     * Sets the value of the toBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setToBalance(BigDecimal value) {
        this.toBalance = value;
    }

    /**
     * Gets the value of the toDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getToDueDate() {
        return toDueDate;
    }

    /**
     * Sets the value of the toDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToDueDate(Date value) {
        this.toDueDate = value;
    }

    /**
     * Gets the value of the toNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToNumber() {
        return toNumber;
    }

    /**
     * Sets the value of the toNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToNumber(String value) {
        this.toNumber = value;
    }

    /**
     * Gets the value of the toPassDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getToPassDate() {
        return toPassDate;
    }

    /**
     * Sets the value of the toPassDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToPassDate(Date value) {
        this.toPassDate = value;
    }

    /**
     * Gets the value of the toRegisterDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getToRegisterDate() {
        return toRegisterDate;
    }

    /**
     * Sets the value of the toRegisterDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToRegisterDate(Date value) {
        this.toRegisterDate = value;
    }

}
