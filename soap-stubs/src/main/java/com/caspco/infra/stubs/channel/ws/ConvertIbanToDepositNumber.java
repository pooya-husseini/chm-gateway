
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for convertIbanToDepositNumber complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="convertIbanToDepositNumber">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chIbanToDepositRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chIbanToDepositRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "convertIbanToDepositNumber", propOrder = {
    "chIbanToDepositRequestBean"
})
public class ConvertIbanToDepositNumber
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChIbanToDepositRequestBean chIbanToDepositRequestBean;

    /**
     * Gets the value of the chIbanToDepositRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChIbanToDepositRequestBean }
     *     
     */
    public ChIbanToDepositRequestBean getChIbanToDepositRequestBean() {
        return chIbanToDepositRequestBean;
    }

    /**
     * Sets the value of the chIbanToDepositRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChIbanToDepositRequestBean }
     *     
     */
    public void setChIbanToDepositRequestBean(ChIbanToDepositRequestBean value) {
        this.chIbanToDepositRequestBean = value;
    }

}
