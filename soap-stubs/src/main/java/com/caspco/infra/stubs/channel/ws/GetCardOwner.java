
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardOwner complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardOwner">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardOwnerRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardOwnerRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardOwner", propOrder = {
    "chCardOwnerRequestBean"
})
public class GetCardOwner
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardOwnerRequestBean chCardOwnerRequestBean;

    /**
     * Gets the value of the chCardOwnerRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardOwnerRequestBean }
     *     
     */
    public ChCardOwnerRequestBean getChCardOwnerRequestBean() {
        return chCardOwnerRequestBean;
    }

    /**
     * Sets the value of the chCardOwnerRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardOwnerRequestBean }
     *     
     */
    public void setChCardOwnerRequestBean(ChCardOwnerRequestBean value) {
        this.chCardOwnerRequestBean = value;
    }

}
