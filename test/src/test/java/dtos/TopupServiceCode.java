/**
 * TopupServiceCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public  enum TopupServiceCode implements java.io.Serializable {
    NONE,
    MCI,
    RIGHTEL,
    IRANCELL_PREPAID_SIMCARD,
    IRANCELL_MAGICAL_PREPAID_SIMCARD,
    IRANCELL_POSTPAID_SIMCARD,
    IRANCELL_PREPAID_WIMAX,
    IRANCELL_POSTPAID_WIMAX

}
