
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getOwnerDepositName complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getOwnerDepositName">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chOwnerDepositRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chOwnerDepositRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getOwnerDepositName", propOrder = {
    "chOwnerDepositRequestBean"
})
public class GetOwnerDepositName
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChOwnerDepositRequestBean chOwnerDepositRequestBean;

    /**
     * Gets the value of the chOwnerDepositRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChOwnerDepositRequestBean }
     *     
     */
    public ChOwnerDepositRequestBean getChOwnerDepositRequestBean() {
        return chOwnerDepositRequestBean;
    }

    /**
     * Sets the value of the chOwnerDepositRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChOwnerDepositRequestBean }
     *     
     */
    public void setChOwnerDepositRequestBean(ChOwnerDepositRequestBean value) {
        this.chOwnerDepositRequestBean = value;
    }

}
