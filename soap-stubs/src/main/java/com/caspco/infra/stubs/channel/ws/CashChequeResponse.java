
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cashChequeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cashChequeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCashChequeResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCashChequeResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cashChequeResponse", propOrder = {
    "chCashChequeResponseBean"
})
public class CashChequeResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCashChequeResponseBean chCashChequeResponseBean;

    /**
     * Gets the value of the chCashChequeResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCashChequeResponseBean }
     *     
     */
    public ChCashChequeResponseBean getChCashChequeResponseBean() {
        return chCashChequeResponseBean;
    }

    /**
     * Sets the value of the chCashChequeResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCashChequeResponseBean }
     *     
     */
    public void setChCashChequeResponseBean(ChCashChequeResponseBean value) {
        this.chCashChequeResponseBean = value;
    }

}
