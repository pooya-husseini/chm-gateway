
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getLoansResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getLoansResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chLoansResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chLoansResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getLoansResponse", propOrder = {
    "chLoansResponseBean"
})
public class GetLoansResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChLoansResponseBean chLoansResponseBean;

    /**
     * Gets the value of the chLoansResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChLoansResponseBean }
     *     
     */
    public ChLoansResponseBean getChLoansResponseBean() {
        return chLoansResponseBean;
    }

    /**
     * Sets the value of the chLoansResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChLoansResponseBean }
     *     
     */
    public void setChLoansResponseBean(ChLoansResponseBean value) {
        this.chLoansResponseBean = value;
    }

}
