
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chMediaType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chMediaType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FAX"/>
 *     &lt;enumeration value="EMAIL"/>
 *     &lt;enumeration value="SMS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chMediaType")
@XmlEnum
public enum ChMediaType {

    FAX,
    EMAIL,
    SMS;

    public String value() {
        return name();
    }

    public static ChMediaType fromValue(String v) {
        return valueOf(v);
    }

}
