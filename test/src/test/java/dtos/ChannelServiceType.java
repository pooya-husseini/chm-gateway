/**
 * ChannelServiceType.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public  enum ChannelServiceType {
    INTERNET_BANK,
    MOBILE_BANK,
    TELEPHONE_BANK,
    THIRDPARTY,
    BOURSE
}
