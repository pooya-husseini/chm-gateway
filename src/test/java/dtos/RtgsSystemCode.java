/**
 * ChRtgsSystemCode.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.HashMap;

public @io.swagger.annotations.ApiModel enum RtgsSystemCode implements java.io.Serializable {
    TAVASOT_KARBAR_SHOBE,
    TAVASOT_SHOBE_TAEED_KONANDE,
    TAVASOT_KARBAR_SHOBE_SANTA,
    TAVASOT_BANKDARI_MODERN,
    TAVASOT_FERESTANDE_GIRANDE_SATNA;

    @JsonCreator
    public static RtgsSystemCode fromStringOperator(HashMap<String, String> map) {
        if (map != null) {
            for (RtgsSystemCode operator : RtgsSystemCode.values()) {
                if (map.get("value").equals(operator.name())) {
                    return operator;
                }
            }
        }
        return null;
    }

}
