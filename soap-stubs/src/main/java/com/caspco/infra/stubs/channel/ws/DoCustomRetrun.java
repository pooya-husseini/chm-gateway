
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for doCustomRetrun complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="doCustomRetrun">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCustomReturnRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCustomReturnRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doCustomRetrun", propOrder = {
    "chCustomReturnRequestBean"
})
public class DoCustomRetrun
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCustomReturnRequestBean chCustomReturnRequestBean;

    /**
     * Gets the value of the chCustomReturnRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCustomReturnRequestBean }
     *     
     */
    public ChCustomReturnRequestBean getChCustomReturnRequestBean() {
        return chCustomReturnRequestBean;
    }

    /**
     * Sets the value of the chCustomReturnRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCustomReturnRequestBean }
     *     
     */
    public void setChCustomReturnRequestBean(ChCustomReturnRequestBean value) {
        this.chCustomReturnRequestBean = value;
    }

}
