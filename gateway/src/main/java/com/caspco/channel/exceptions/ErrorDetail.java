package com.caspco.channel.exceptions;

import java.util.Map;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 11/26/17
 * Time: 3:47 PM
 */
public class ErrorDetail {
    private Integer code;
    private String field;
    private Map<String, String> message;

    public ErrorDetail() {
    }

    public ErrorDetail(Integer code, String field, Map<String, String> message) {
        this.code = code;
        this.field = field;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Map<String, String> getMessage() {
        return message;
    }

    public void setMessage(Map<String, String> message) {
        this.message = message;
    }
}
