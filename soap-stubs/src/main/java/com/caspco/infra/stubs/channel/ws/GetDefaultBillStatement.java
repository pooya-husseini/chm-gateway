
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDefaultBillStatement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDefaultBillStatement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDefaultStatementRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDefaultStatementRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDefaultBillStatement", propOrder = {
    "chDefaultStatementRequestBean"
})
public class GetDefaultBillStatement
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDefaultStatementRequestBean chDefaultStatementRequestBean;

    /**
     * Gets the value of the chDefaultStatementRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDefaultStatementRequestBean }
     *     
     */
    public ChDefaultStatementRequestBean getChDefaultStatementRequestBean() {
        return chDefaultStatementRequestBean;
    }

    /**
     * Sets the value of the chDefaultStatementRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDefaultStatementRequestBean }
     *     
     */
    public void setChDefaultStatementRequestBean(ChDefaultStatementRequestBean value) {
        this.chDefaultStatementRequestBean = value;
    }

}
