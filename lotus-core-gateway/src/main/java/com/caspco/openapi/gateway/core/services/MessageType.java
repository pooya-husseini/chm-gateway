package com.caspco.openapi.gateway.core.services;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/17/17
 *         Time: 8:20 PM
 */
public enum MessageType {
    REQUEST(100),
    RESPONSE(200),
    NOTIFICATION(300);

    private final Integer code;

    MessageType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }
}
