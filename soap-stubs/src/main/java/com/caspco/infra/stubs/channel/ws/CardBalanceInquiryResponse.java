
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardBalanceInquiryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardBalanceInquiryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardBalanceResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardBalanceResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardBalanceInquiryResponse", propOrder = {
    "chCardBalanceResponseBean"
})
public class CardBalanceInquiryResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardBalanceResponseBean chCardBalanceResponseBean;

    /**
     * Gets the value of the chCardBalanceResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardBalanceResponseBean }
     *     
     */
    public ChCardBalanceResponseBean getChCardBalanceResponseBean() {
        return chCardBalanceResponseBean;
    }

    /**
     * Sets the value of the chCardBalanceResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardBalanceResponseBean }
     *     
     */
    public void setChCardBalanceResponseBean(ChCardBalanceResponseBean value) {
        this.chCardBalanceResponseBean = value;
    }

}
