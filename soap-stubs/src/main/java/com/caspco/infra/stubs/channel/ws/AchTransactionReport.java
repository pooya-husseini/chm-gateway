
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for achTransactionReport complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="achTransactionReport">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAchTransactionSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAchTransactionSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "achTransactionReport", propOrder = {
    "chAchTransactionSearchRequestBean"
})
public class AchTransactionReport
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAchTransactionSearchRequestBean chAchTransactionSearchRequestBean;

    /**
     * Gets the value of the chAchTransactionSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAchTransactionSearchRequestBean }
     *     
     */
    public ChAchTransactionSearchRequestBean getChAchTransactionSearchRequestBean() {
        return chAchTransactionSearchRequestBean;
    }

    /**
     * Sets the value of the chAchTransactionSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAchTransactionSearchRequestBean }
     *     
     */
    public void setChAchTransactionSearchRequestBean(ChAchTransactionSearchRequestBean value) {
        this.chAchTransactionSearchRequestBean = value;
    }

}
