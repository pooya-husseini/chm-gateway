
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDestinationCardOwnerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDestinationCardOwnerResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDestinationCardOwnerResponse" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDestinationCardOwnerResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDestinationCardOwnerResponse", propOrder = {
    "chDestinationCardOwnerResponse"
})
public class GetDestinationCardOwnerResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDestinationCardOwnerResponse chDestinationCardOwnerResponse;

    /**
     * Gets the value of the chDestinationCardOwnerResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ChDestinationCardOwnerResponse }
     *     
     */
    public ChDestinationCardOwnerResponse getChDestinationCardOwnerResponse() {
        return chDestinationCardOwnerResponse;
    }

    /**
     * Sets the value of the chDestinationCardOwnerResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDestinationCardOwnerResponse }
     *     
     */
    public void setChDestinationCardOwnerResponse(ChDestinationCardOwnerResponse value) {
        this.chDestinationCardOwnerResponse = value;
    }

}
