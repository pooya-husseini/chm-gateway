/**
 * ChAchTransferStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public  enum AchTransferStatus implements java.io.Serializable {
    WAIT_FOR_CUSTOMER_ACCEPT,
    WAIT_FOR_BRANCH_ACCEPT,
    BRANCH_REJECT,
    READY_TO_TRANSFER,
    SUSPEND,
    CANCEL,
    PROCESSED;

}
