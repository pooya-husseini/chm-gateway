
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chAutoTransferDetailsResponseBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chAutoTransferDetailsResponseBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="autoTransferDetailDtos" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAutoTransferDetailBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="totalRecord" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chAutoTransferDetailsResponseBean", propOrder = {
    "autoTransferDetailDtos",
    "totalRecord"
})
public class ChAutoTransferDetailsResponseBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(nillable = true)
    protected List<ChAutoTransferDetailBean> autoTransferDetailDtos;
    protected Integer totalRecord;

    /**
     * Gets the value of the autoTransferDetailDtos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the autoTransferDetailDtos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAutoTransferDetailDtos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChAutoTransferDetailBean }
     * 
     * 
     */
    public List<ChAutoTransferDetailBean> getAutoTransferDetailDtos() {
        if (autoTransferDetailDtos == null) {
            autoTransferDetailDtos = new ArrayList<ChAutoTransferDetailBean>();
        }
        return this.autoTransferDetailDtos;
    }

    /**
     * Gets the value of the totalRecord property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalRecord() {
        return totalRecord;
    }

    /**
     * Sets the value of the totalRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalRecord(Integer value) {
        this.totalRecord = value;
    }

}
