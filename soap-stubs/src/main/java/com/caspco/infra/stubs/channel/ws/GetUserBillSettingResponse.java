
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getUserBillSettingResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getUserBillSettingResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBillSettingResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillSettingResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUserBillSettingResponse", propOrder = {
    "chBillSettingResponseBean"
})
public class GetUserBillSettingResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBillSettingResponseBean chBillSettingResponseBean;

    /**
     * Gets the value of the chBillSettingResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillSettingResponseBean }
     *     
     */
    public ChBillSettingResponseBean getChBillSettingResponseBean() {
        return chBillSettingResponseBean;
    }

    /**
     * Sets the value of the chBillSettingResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillSettingResponseBean }
     *     
     */
    public void setChBillSettingResponseBean(ChBillSettingResponseBean value) {
        this.chBillSettingResponseBean = value;
    }

}
