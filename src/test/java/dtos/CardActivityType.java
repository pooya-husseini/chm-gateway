/**
 * ChCardActivityType.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.HashMap;

public @io.swagger.annotations.ApiModel enum CardActivityType implements java.io.Serializable {
    TRANSFER,
    BILL_PAYMENT,
    PURCHASE,
    DEPOSIT,
    WITHDRAWAL,
    CHARGE_BACK,
    CHANGE_SECOND_PIN,
    CHANGE_FIRST_PIN,
    UNBLOCK,
    CAPTURED,
    STATEMENT,
    BALANCE,
    RETURN,
    OTHER,
    CREDIT_DEBIT;

    @JsonCreator
    public static CardActivityType fromStringOperator(HashMap<String, String> map) {
        if (map != null) {
            for (CardActivityType operator : CardActivityType.values()) {
                if (map.get("value").equals(operator.name())) {
                    return operator;
                }
            }
        }
        return null;
    }

}
