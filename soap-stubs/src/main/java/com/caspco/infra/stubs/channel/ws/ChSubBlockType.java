
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chSubBlockType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chSubBlockType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="GARNISHEE"/>
 *     &lt;enumeration value="PLEDGE"/>
 *     &lt;enumeration value="EXCHANGE"/>
 *     &lt;enumeration value="TRADE_FINANCE"/>
 *     &lt;enumeration value="CARD_LESS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chSubBlockType")
@XmlEnum
public enum ChSubBlockType {

    GARNISHEE,
    PLEDGE,
    EXCHANGE,
    TRADE_FINANCE,
    CARD_LESS;

    public String value() {
        return name();
    }

    public static ChSubBlockType fromValue(String v) {
        return valueOf(v);
    }

}
