
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for payLoan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="payLoan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chLoanPaymentRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chLoanPaymentRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "payLoan", propOrder = {
    "chLoanPaymentRequestBean"
})
public class PayLoan
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChLoanPaymentRequestBean chLoanPaymentRequestBean;

    /**
     * Gets the value of the chLoanPaymentRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChLoanPaymentRequestBean }
     *     
     */
    public ChLoanPaymentRequestBean getChLoanPaymentRequestBean() {
        return chLoanPaymentRequestBean;
    }

    /**
     * Sets the value of the chLoanPaymentRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChLoanPaymentRequestBean }
     *     
     */
    public void setChLoanPaymentRequestBean(ChLoanPaymentRequestBean value) {
        this.chLoanPaymentRequestBean = value;
    }

}
