
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rtgsTransferDetailReport complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rtgsTransferDetailReport">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chRtgTransferDetailSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chRtgTransferDetailSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rtgsTransferDetailReport", propOrder = {
    "chRtgTransferDetailSearchRequestBean"
})
public class RtgsTransferDetailReport
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChRtgTransferDetailSearchRequestBean chRtgTransferDetailSearchRequestBean;

    /**
     * Gets the value of the chRtgTransferDetailSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChRtgTransferDetailSearchRequestBean }
     *     
     */
    public ChRtgTransferDetailSearchRequestBean getChRtgTransferDetailSearchRequestBean() {
        return chRtgTransferDetailSearchRequestBean;
    }

    /**
     * Sets the value of the chRtgTransferDetailSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChRtgTransferDetailSearchRequestBean }
     *     
     */
    public void setChRtgTransferDetailSearchRequestBean(ChRtgTransferDetailSearchRequestBean value) {
        this.chRtgTransferDetailSearchRequestBean = value;
    }

}
