
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for autoTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="autoTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChAutoTransferRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAutoTransferRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "autoTransfer", propOrder = {
    "chAutoTransferRequestBean"
})
public class AutoTransfer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ChAutoTransferRequestBean")
    protected ChAutoTransferRequestBean chAutoTransferRequestBean;

    /**
     * Gets the value of the chAutoTransferRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAutoTransferRequestBean }
     *     
     */
    public ChAutoTransferRequestBean getChAutoTransferRequestBean() {
        return chAutoTransferRequestBean;
    }

    /**
     * Sets the value of the chAutoTransferRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAutoTransferRequestBean }
     *     
     */
    public void setChAutoTransferRequestBean(ChAutoTransferRequestBean value) {
        this.chAutoTransferRequestBean = value;
    }

}
