
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addPeriodicBillRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addPeriodicBillRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chPeriodicRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPeriodicRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addPeriodicBillRequest", propOrder = {
    "chPeriodicRequestBean"
})
public class AddPeriodicBillRequest
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChPeriodicRequestBean chPeriodicRequestBean;

    /**
     * Gets the value of the chPeriodicRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChPeriodicRequestBean }
     *     
     */
    public ChPeriodicRequestBean getChPeriodicRequestBean() {
        return chPeriodicRequestBean;
    }

    /**
     * Sets the value of the chPeriodicRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPeriodicRequestBean }
     *     
     */
    public void setChPeriodicRequestBean(ChPeriodicRequestBean value) {
        this.chPeriodicRequestBean = value;
    }

}
