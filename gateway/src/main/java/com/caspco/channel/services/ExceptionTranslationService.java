package com.caspco.channel.services;

import com.caspco.spl.webactivator.services.TranslationService;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class ExceptionTranslationService {

    private static Map<String, Map<String, String>> translationsMap = new ConcurrentHashMap<>();

    @Autowired
    private TranslationService translationService;

    @PostConstruct
    public void init() {
        translationsMap = translationService.getTranslations().entrySet()
                .stream()
                .collect(Collectors.groupingBy(i -> i.getKey().substring(i.getKey().indexOf(".") + 1)))
                .entrySet()
                .stream()
                .map(i -> Pair.of(i.getKey(), i.getValue().stream().map((k -> Pair.of(k.getKey().substring(0, k.getKey().indexOf(".")), k.getValue()))).collect(Collectors.toMap(Pair::getKey, Pair::getValue))))
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }

    public static Map<String, String> getTranslations(String key) {
        return translationsMap.get(key);
    }
}