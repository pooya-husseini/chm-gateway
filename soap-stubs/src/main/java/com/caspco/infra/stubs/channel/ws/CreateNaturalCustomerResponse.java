
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createNaturalCustomerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createNaturalCustomerResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCreateNaturalCustomerResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCreateNaturalCustomerResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createNaturalCustomerResponse", propOrder = {
    "chCreateNaturalCustomerResponseBean"
})
public class CreateNaturalCustomerResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCreateNaturalCustomerResponseBean chCreateNaturalCustomerResponseBean;

    /**
     * Gets the value of the chCreateNaturalCustomerResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCreateNaturalCustomerResponseBean }
     *     
     */
    public ChCreateNaturalCustomerResponseBean getChCreateNaturalCustomerResponseBean() {
        return chCreateNaturalCustomerResponseBean;
    }

    /**
     * Sets the value of the chCreateNaturalCustomerResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCreateNaturalCustomerResponseBean }
     *     
     */
    public void setChCreateNaturalCustomerResponseBean(ChCreateNaturalCustomerResponseBean value) {
        this.chCreateNaturalCustomerResponseBean = value;
    }

}
