
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chBaseCreditBillSearchRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chBaseCreditBillSearchRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clearingStatus" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillClearingStatus" minOccurs="0"/>
 *         &lt;element name="cycleStatus" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillCycleStatus" minOccurs="0"/>
 *         &lt;element name="dossierNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fromEndCycleDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fromStartCycleDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="offset" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="showTotalCount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="toEndCycleDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="toStartCycleDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chBaseCreditBillSearchRequestBean", propOrder = {
    "clearingStatus",
    "cycleStatus",
    "dossierNumber",
    "fromEndCycleDate",
    "fromStartCycleDate",
    "length",
    "offset",
    "showTotalCount",
    "toEndCycleDate",
    "toStartCycleDate"
})
public class ChBaseCreditBillSearchRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlSchemaType(name = "string")
    protected ChBillClearingStatus clearingStatus;
    @XmlSchemaType(name = "string")
    protected ChBillCycleStatus cycleStatus;
    protected String dossierNumber;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date fromEndCycleDate;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date fromStartCycleDate;
    protected Long length;
    protected Long offset;
    protected Boolean showTotalCount;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date toEndCycleDate;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date toStartCycleDate;

    /**
     * Gets the value of the clearingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillClearingStatus }
     *     
     */
    public ChBillClearingStatus getClearingStatus() {
        return clearingStatus;
    }

    /**
     * Sets the value of the clearingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillClearingStatus }
     *     
     */
    public void setClearingStatus(ChBillClearingStatus value) {
        this.clearingStatus = value;
    }

    /**
     * Gets the value of the cycleStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillCycleStatus }
     *     
     */
    public ChBillCycleStatus getCycleStatus() {
        return cycleStatus;
    }

    /**
     * Sets the value of the cycleStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillCycleStatus }
     *     
     */
    public void setCycleStatus(ChBillCycleStatus value) {
        this.cycleStatus = value;
    }

    /**
     * Gets the value of the dossierNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDossierNumber() {
        return dossierNumber;
    }

    /**
     * Sets the value of the dossierNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDossierNumber(String value) {
        this.dossierNumber = value;
    }

    /**
     * Gets the value of the fromEndCycleDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getFromEndCycleDate() {
        return fromEndCycleDate;
    }

    /**
     * Sets the value of the fromEndCycleDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromEndCycleDate(Date value) {
        this.fromEndCycleDate = value;
    }

    /**
     * Gets the value of the fromStartCycleDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getFromStartCycleDate() {
        return fromStartCycleDate;
    }

    /**
     * Sets the value of the fromStartCycleDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromStartCycleDate(Date value) {
        this.fromStartCycleDate = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLength(Long value) {
        this.length = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOffset(Long value) {
        this.offset = value;
    }

    /**
     * Gets the value of the showTotalCount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowTotalCount() {
        return showTotalCount;
    }

    /**
     * Sets the value of the showTotalCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowTotalCount(Boolean value) {
        this.showTotalCount = value;
    }

    /**
     * Gets the value of the toEndCycleDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getToEndCycleDate() {
        return toEndCycleDate;
    }

    /**
     * Sets the value of the toEndCycleDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToEndCycleDate(Date value) {
        this.toEndCycleDate = value;
    }

    /**
     * Gets the value of the toStartCycleDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getToStartCycleDate() {
        return toStartCycleDate;
    }

    /**
     * Sets the value of the toStartCycleDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToStartCycleDate(Date value) {
        this.toStartCycleDate = value;
    }

}
