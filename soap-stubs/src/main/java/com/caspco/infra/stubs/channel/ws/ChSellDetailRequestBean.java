
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chSellDetailRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chSellDetailRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amountMax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="amountMin" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="mainTransactionId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="merchantId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="offset" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="orderType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chOrderType" minOccurs="0"/>
 *         &lt;element name="refNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sellOrderField" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSellOrderField" minOccurs="0"/>
 *         &lt;element name="timeMax" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="timeMin" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chSellDetailRequestBean", propOrder = {
    "amountMax",
    "amountMin",
    "length",
    "mainTransactionId",
    "merchantId",
    "offset",
    "orderType",
    "refNum",
    "resNum",
    "sellOrderField",
    "timeMax",
    "timeMin"
})
public class ChSellDetailRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigDecimal amountMax;
    protected BigDecimal amountMin;
    protected Long length;
    protected Long mainTransactionId;
    protected String merchantId;
    protected Long offset;
    @XmlSchemaType(name = "string")
    protected ChOrderType orderType;
    protected String refNum;
    protected String resNum;
    @XmlSchemaType(name = "string")
    protected ChSellOrderField sellOrderField;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date timeMax;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date timeMin;

    /**
     * Gets the value of the amountMax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountMax() {
        return amountMax;
    }

    /**
     * Sets the value of the amountMax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountMax(BigDecimal value) {
        this.amountMax = value;
    }

    /**
     * Gets the value of the amountMin property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountMin() {
        return amountMin;
    }

    /**
     * Sets the value of the amountMin property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountMin(BigDecimal value) {
        this.amountMin = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLength(Long value) {
        this.length = value;
    }

    /**
     * Gets the value of the mainTransactionId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMainTransactionId() {
        return mainTransactionId;
    }

    /**
     * Sets the value of the mainTransactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMainTransactionId(Long value) {
        this.mainTransactionId = value;
    }

    /**
     * Gets the value of the merchantId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * Sets the value of the merchantId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantId(String value) {
        this.merchantId = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOffset(Long value) {
        this.offset = value;
    }

    /**
     * Gets the value of the orderType property.
     * 
     * @return
     *     possible object is
     *     {@link ChOrderType }
     *     
     */
    public ChOrderType getOrderType() {
        return orderType;
    }

    /**
     * Sets the value of the orderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChOrderType }
     *     
     */
    public void setOrderType(ChOrderType value) {
        this.orderType = value;
    }

    /**
     * Gets the value of the refNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefNum() {
        return refNum;
    }

    /**
     * Sets the value of the refNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefNum(String value) {
        this.refNum = value;
    }

    /**
     * Gets the value of the resNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResNum() {
        return resNum;
    }

    /**
     * Sets the value of the resNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResNum(String value) {
        this.resNum = value;
    }

    /**
     * Gets the value of the sellOrderField property.
     * 
     * @return
     *     possible object is
     *     {@link ChSellOrderField }
     *     
     */
    public ChSellOrderField getSellOrderField() {
        return sellOrderField;
    }

    /**
     * Sets the value of the sellOrderField property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSellOrderField }
     *     
     */
    public void setSellOrderField(ChSellOrderField value) {
        this.sellOrderField = value;
    }

    /**
     * Gets the value of the timeMax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getTimeMax() {
        return timeMax;
    }

    /**
     * Sets the value of the timeMax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeMax(Date value) {
        this.timeMax = value;
    }

    /**
     * Gets the value of the timeMin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getTimeMin() {
        return timeMin;
    }

    /**
     * Sets the value of the timeMin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeMin(Date value) {
        this.timeMin = value;
    }

}
