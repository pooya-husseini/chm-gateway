
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBeOpenedDeposits complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBeOpenedDeposits">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBeOpenedDepositRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBeOpenedDepositRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBeOpenedDeposits", propOrder = {
    "chBeOpenedDepositRequestBean"
})
public class GetBeOpenedDeposits
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBeOpenedDepositRequestBean chBeOpenedDepositRequestBean;

    /**
     * Gets the value of the chBeOpenedDepositRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBeOpenedDepositRequestBean }
     *     
     */
    public ChBeOpenedDepositRequestBean getChBeOpenedDepositRequestBean() {
        return chBeOpenedDepositRequestBean;
    }

    /**
     * Sets the value of the chBeOpenedDepositRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBeOpenedDepositRequestBean }
     *     
     */
    public void setChBeOpenedDepositRequestBean(ChBeOpenedDepositRequestBean value) {
        this.chBeOpenedDepositRequestBean = value;
    }

}
