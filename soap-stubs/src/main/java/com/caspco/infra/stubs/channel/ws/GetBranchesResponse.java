
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBranchesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBranchesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBranchResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBranchResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBranchesResponse", propOrder = {
    "chBranchResponseBean"
})
public class GetBranchesResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBranchResponseBean chBranchResponseBean;

    /**
     * Gets the value of the chBranchResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBranchResponseBean }
     *     
     */
    public ChBranchResponseBean getChBranchResponseBean() {
        return chBranchResponseBean;
    }

    /**
     * Sets the value of the chBranchResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBranchResponseBean }
     *     
     */
    public void setChBranchResponseBean(ChBranchResponseBean value) {
        this.chBranchResponseBean = value;
    }

}
