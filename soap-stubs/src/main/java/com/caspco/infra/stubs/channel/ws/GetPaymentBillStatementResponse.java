
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPaymentBillStatementResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPaymentBillStatementResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chPaymentBillStatementsResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPaymentBillStatementsResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPaymentBillStatementResponse", propOrder = {
    "chPaymentBillStatementsResponseBean"
})
public class GetPaymentBillStatementResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChPaymentBillStatementsResponseBean chPaymentBillStatementsResponseBean;

    /**
     * Gets the value of the chPaymentBillStatementsResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChPaymentBillStatementsResponseBean }
     *     
     */
    public ChPaymentBillStatementsResponseBean getChPaymentBillStatementsResponseBean() {
        return chPaymentBillStatementsResponseBean;
    }

    /**
     * Sets the value of the chPaymentBillStatementsResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPaymentBillStatementsResponseBean }
     *     
     */
    public void setChPaymentBillStatementsResponseBean(ChPaymentBillStatementsResponseBean value) {
        this.chPaymentBillStatementsResponseBean = value;
    }

}
