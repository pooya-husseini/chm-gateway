package com.caspco.channel.services.channel;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 9/17/17
 * Time: 10:00 AM
 */


public class GenericRequest<T> {
    private T payload;

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
