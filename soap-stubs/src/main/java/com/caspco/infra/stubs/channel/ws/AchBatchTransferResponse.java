
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for achBatchTransferResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="achBatchTransferResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAchBatchTransferResultBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAchBatchTransferResultBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "achBatchTransferResponse", propOrder = {
    "chAchBatchTransferResultBean"
})
public class AchBatchTransferResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAchBatchTransferResultBean chAchBatchTransferResultBean;

    /**
     * Gets the value of the chAchBatchTransferResultBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAchBatchTransferResultBean }
     *     
     */
    public ChAchBatchTransferResultBean getChAchBatchTransferResultBean() {
        return chAchBatchTransferResultBean;
    }

    /**
     * Sets the value of the chAchBatchTransferResultBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAchBatchTransferResultBean }
     *     
     */
    public void setChAchBatchTransferResultBean(ChAchBatchTransferResultBean value) {
        this.chAchBatchTransferResultBean = value;
    }

}
