
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCustomerInfoListByModifyDateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCustomerInfoListByModifyDateResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChFullCustomerInfoResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chFullCustomerInfoResponseBean" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCustomerInfoListByModifyDateResponse", propOrder = {
    "chFullCustomerInfoResponseBean"
})
public class GetCustomerInfoListByModifyDateResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ChFullCustomerInfoResponseBean")
    protected List<ChFullCustomerInfoResponseBean> chFullCustomerInfoResponseBean;

    /**
     * Gets the value of the chFullCustomerInfoResponseBean property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chFullCustomerInfoResponseBean property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChFullCustomerInfoResponseBean().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChFullCustomerInfoResponseBean }
     * 
     * 
     */
    public List<ChFullCustomerInfoResponseBean> getChFullCustomerInfoResponseBean() {
        if (chFullCustomerInfoResponseBean == null) {
            chFullCustomerInfoResponseBean = new ArrayList<ChFullCustomerInfoResponseBean>();
        }
        return this.chFullCustomerInfoResponseBean;
    }

}
