/**
 * ChCardTransactionResponseBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

public class CardTransactionResponseDto  implements java.io.Serializable {

    @ApiModelProperty(value = "cardActivityType")
    private com.caspco.channel.dtos.CardActivityType activityType;

    private java.math.BigDecimal amount;

    private java.lang.String description;

    private java.lang.String issuerName;

    private java.lang.String issuerNumber;

    private java.lang.String opponentDepositNumber;

    private java.lang.String opponentIssuerName;

    private java.lang.String opponentIssuerNumber;

    private java.lang.String opponentPan;

    private java.lang.String pan;

    private java.lang.String referenceNumber;

    private java.lang.String traceNumber;

    private java.util.Date transactionDate;

    private java.util.Date transactionSentDate;

    @ApiModelProperty(value = "cardTransactionType")
    private com.caspco.channel.dtos.CardTransactionType transactionType;

    public CardActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(CardActivityType activityType) {
        this.activityType = activityType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public String getIssuerNumber() {
        return issuerNumber;
    }

    public void setIssuerNumber(String issuerNumber) {
        this.issuerNumber = issuerNumber;
    }

    public String getOpponentDepositNumber() {
        return opponentDepositNumber;
    }

    public void setOpponentDepositNumber(String opponentDepositNumber) {
        this.opponentDepositNumber = opponentDepositNumber;
    }

    public String getOpponentIssuerName() {
        return opponentIssuerName;
    }

    public void setOpponentIssuerName(String opponentIssuerName) {
        this.opponentIssuerName = opponentIssuerName;
    }

    public String getOpponentIssuerNumber() {
        return opponentIssuerNumber;
    }

    public void setOpponentIssuerNumber(String opponentIssuerNumber) {
        this.opponentIssuerNumber = opponentIssuerNumber;
    }

    public String getOpponentPan() {
        return opponentPan;
    }

    public void setOpponentPan(String opponentPan) {
        this.opponentPan = opponentPan;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getTraceNumber() {
        return traceNumber;
    }

    public void setTraceNumber(String traceNumber) {
        this.traceNumber = traceNumber;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getTransactionSentDate() {
        return transactionSentDate;
    }

    public void setTransactionSentDate(Date transactionSentDate) {
        this.transactionSentDate = transactionSentDate;
    }

    public CardTransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(CardTransactionType transactionType) {
        this.transactionType = transactionType;
    }
}
