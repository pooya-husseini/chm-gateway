
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for extendSMSSetting complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="extendSMSSetting">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chUserSMSSettingRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chUserSMSSettingRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "extendSMSSetting", propOrder = {
    "chUserSMSSettingRequestBean"
})
public class ExtendSMSSetting
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChUserSMSSettingRequestBean chUserSMSSettingRequestBean;

    /**
     * Gets the value of the chUserSMSSettingRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChUserSMSSettingRequestBean }
     *     
     */
    public ChUserSMSSettingRequestBean getChUserSMSSettingRequestBean() {
        return chUserSMSSettingRequestBean;
    }

    /**
     * Sets the value of the chUserSMSSettingRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChUserSMSSettingRequestBean }
     *     
     */
    public void setChUserSMSSettingRequestBean(ChUserSMSSettingRequestBean value) {
        this.chUserSMSSettingRequestBean = value;
    }

}
