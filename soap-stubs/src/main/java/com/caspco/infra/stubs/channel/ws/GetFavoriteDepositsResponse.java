
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFavoriteDepositsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFavoriteDepositsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chFavoriteDepositResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chFavoriteDepositResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFavoriteDepositsResponse", propOrder = {
    "chFavoriteDepositResponseBean"
})
public class GetFavoriteDepositsResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChFavoriteDepositResponseBean chFavoriteDepositResponseBean;

    /**
     * Gets the value of the chFavoriteDepositResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChFavoriteDepositResponseBean }
     *     
     */
    public ChFavoriteDepositResponseBean getChFavoriteDepositResponseBean() {
        return chFavoriteDepositResponseBean;
    }

    /**
     * Sets the value of the chFavoriteDepositResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChFavoriteDepositResponseBean }
     *     
     */
    public void setChFavoriteDepositResponseBean(ChFavoriteDepositResponseBean value) {
        this.chFavoriteDepositResponseBean = value;
    }

}
