
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for verifyTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="verifyTransaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chVerifyTransactionRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chVerifyTransactionRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "verifyTransaction", propOrder = {
    "chVerifyTransactionRequestBean"
})
public class VerifyTransaction
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChVerifyTransactionRequestBean chVerifyTransactionRequestBean;

    /**
     * Gets the value of the chVerifyTransactionRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChVerifyTransactionRequestBean }
     *     
     */
    public ChVerifyTransactionRequestBean getChVerifyTransactionRequestBean() {
        return chVerifyTransactionRequestBean;
    }

    /**
     * Sets the value of the chVerifyTransactionRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChVerifyTransactionRequestBean }
     *     
     */
    public void setChVerifyTransactionRequestBean(ChVerifyTransactionRequestBean value) {
        this.chVerifyTransactionRequestBean = value;
    }

}
