
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for changePassword complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="changePassword">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chChangePasswordRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chChangePasswordRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changePassword", propOrder = {
    "chChangePasswordRequestBean"
})
public class ChangePassword
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChChangePasswordRequestBean chChangePasswordRequestBean;

    /**
     * Gets the value of the chChangePasswordRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChChangePasswordRequestBean }
     *     
     */
    public ChChangePasswordRequestBean getChChangePasswordRequestBean() {
        return chChangePasswordRequestBean;
    }

    /**
     * Sets the value of the chChangePasswordRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChChangePasswordRequestBean }
     *     
     */
    public void setChChangePasswordRequestBean(ChChangePasswordRequestBean value) {
        this.chChangePasswordRequestBean = value;
    }

}
