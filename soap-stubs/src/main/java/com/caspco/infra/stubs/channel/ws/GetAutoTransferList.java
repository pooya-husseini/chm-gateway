
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAutoTransferList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAutoTransferList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSearchAutoTransferRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSearchAutoTransferRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAutoTransferList", propOrder = {
    "chSearchAutoTransferRequestBean"
})
public class GetAutoTransferList
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSearchAutoTransferRequestBean chSearchAutoTransferRequestBean;

    /**
     * Gets the value of the chSearchAutoTransferRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChSearchAutoTransferRequestBean }
     *     
     */
    public ChSearchAutoTransferRequestBean getChSearchAutoTransferRequestBean() {
        return chSearchAutoTransferRequestBean;
    }

    /**
     * Sets the value of the chSearchAutoTransferRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSearchAutoTransferRequestBean }
     *     
     */
    public void setChSearchAutoTransferRequestBean(ChSearchAutoTransferRequestBean value) {
        this.chSearchAutoTransferRequestBean = value;
    }

}
