package com.caspco.channel.exceptions;

import com.caspco.spl.model.wrench.Locale;

import java.util.HashMap;
import java.util.Map;

public class ExceptionDescription{
    private Map<Locale, String> map;

    public ExceptionDescription() {
        map=new HashMap<>();
    }

    public ExceptionDescription of(Locale locale, String label){
        map.put(locale, label);
        return this;
    }

    public Map<Locale, String> getMap() {
        return map;
    }

    public static ExceptionDescription start(){
        return new ExceptionDescription();
    }
}
