
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chFundTransferRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chFundTransferRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardTransfer", propOrder = {
    "chFundTransferRequestBean"
})
public class CardTransfer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChFundTransferRequestBean chFundTransferRequestBean;

    /**
     * Gets the value of the chFundTransferRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChFundTransferRequestBean }
     *     
     */
    public ChFundTransferRequestBean getChFundTransferRequestBean() {
        return chFundTransferRequestBean;
    }

    /**
     * Sets the value of the chFundTransferRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChFundTransferRequestBean }
     *     
     */
    public void setChFundTransferRequestBean(ChFundTransferRequestBean value) {
        this.chFundTransferRequestBean = value;
    }

}
