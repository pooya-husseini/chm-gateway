
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chInvestmentDepositBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chInvestmentDepositBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="interestSettlementDay" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="interestToDepositNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chInvestmentDepositBean", propOrder = {
    "interestSettlementDay",
    "interestToDepositNumber"
})
public class ChInvestmentDepositBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected Short interestSettlementDay;
    protected String interestToDepositNumber;

    /**
     * Gets the value of the interestSettlementDay property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getInterestSettlementDay() {
        return interestSettlementDay;
    }

    /**
     * Sets the value of the interestSettlementDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setInterestSettlementDay(Short value) {
        this.interestSettlementDay = value;
    }

    /**
     * Gets the value of the interestToDepositNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterestToDepositNumber() {
        return interestToDepositNumber;
    }

    /**
     * Sets the value of the interestToDepositNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterestToDepositNumber(String value) {
        this.interestToDepositNumber = value;
    }

}
