
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardCorrectLinkedAccountsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardCorrectLinkedAccountsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardCorrectLinkedAccountsResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardCorrectLinkedAccountsResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardCorrectLinkedAccountsResponse", propOrder = {
    "chCardCorrectLinkedAccountsResponseBean"
})
public class CardCorrectLinkedAccountsResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardCorrectLinkedAccountsResponseBean chCardCorrectLinkedAccountsResponseBean;

    /**
     * Gets the value of the chCardCorrectLinkedAccountsResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardCorrectLinkedAccountsResponseBean }
     *     
     */
    public ChCardCorrectLinkedAccountsResponseBean getChCardCorrectLinkedAccountsResponseBean() {
        return chCardCorrectLinkedAccountsResponseBean;
    }

    /**
     * Sets the value of the chCardCorrectLinkedAccountsResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardCorrectLinkedAccountsResponseBean }
     *     
     */
    public void setChCardCorrectLinkedAccountsResponseBean(ChCardCorrectLinkedAccountsResponseBean value) {
        this.chCardCorrectLinkedAccountsResponseBean = value;
    }

}
