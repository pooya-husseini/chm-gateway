
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chCoreBlockDepositAmountRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chCoreBlockDepositAmountRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="blockReason" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBlockReason" minOccurs="0"/>
 *         &lt;element name="dueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ebTrackId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ibanOrAccountNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="narrative" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operatorClientId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="subBlockType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSubBlockType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chCoreBlockDepositAmountRequestBean", propOrder = {
    "amount",
    "blockReason",
    "dueDate",
    "ebTrackId",
    "ibanOrAccountNo",
    "narrative",
    "operatorClientId",
    "subBlockType"
})
public class ChCoreBlockDepositAmountRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigDecimal amount;
    @XmlSchemaType(name = "string")
    protected ChBlockReason blockReason;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date dueDate;
    protected String ebTrackId;
    protected String ibanOrAccountNo;
    protected String narrative;
    protected Long operatorClientId;
    @XmlSchemaType(name = "string")
    protected ChSubBlockType subBlockType;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the blockReason property.
     * 
     * @return
     *     possible object is
     *     {@link ChBlockReason }
     *     
     */
    public ChBlockReason getBlockReason() {
        return blockReason;
    }

    /**
     * Sets the value of the blockReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBlockReason }
     *     
     */
    public void setBlockReason(ChBlockReason value) {
        this.blockReason = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDueDate(Date value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the ebTrackId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEbTrackId() {
        return ebTrackId;
    }

    /**
     * Sets the value of the ebTrackId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEbTrackId(String value) {
        this.ebTrackId = value;
    }

    /**
     * Gets the value of the ibanOrAccountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIbanOrAccountNo() {
        return ibanOrAccountNo;
    }

    /**
     * Sets the value of the ibanOrAccountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbanOrAccountNo(String value) {
        this.ibanOrAccountNo = value;
    }

    /**
     * Gets the value of the narrative property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNarrative() {
        return narrative;
    }

    /**
     * Sets the value of the narrative property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNarrative(String value) {
        this.narrative = value;
    }

    /**
     * Gets the value of the operatorClientId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOperatorClientId() {
        return operatorClientId;
    }

    /**
     * Sets the value of the operatorClientId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOperatorClientId(Long value) {
        this.operatorClientId = value;
    }

    /**
     * Gets the value of the subBlockType property.
     * 
     * @return
     *     possible object is
     *     {@link ChSubBlockType }
     *     
     */
    public ChSubBlockType getSubBlockType() {
        return subBlockType;
    }

    /**
     * Sets the value of the subBlockType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSubBlockType }
     *     
     */
    public void setSubBlockType(ChSubBlockType value) {
        this.subBlockType = value;
    }

}
