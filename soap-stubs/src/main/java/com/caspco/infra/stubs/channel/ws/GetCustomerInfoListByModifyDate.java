
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCustomerInfoListByModifyDate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCustomerInfoListByModifyDate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChFullCustomerInfoRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chFullCustomerInfoRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCustomerInfoListByModifyDate", propOrder = {
    "chFullCustomerInfoRequestBean"
})
public class GetCustomerInfoListByModifyDate
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ChFullCustomerInfoRequestBean")
    protected ChFullCustomerInfoRequestBean chFullCustomerInfoRequestBean;

    /**
     * Gets the value of the chFullCustomerInfoRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChFullCustomerInfoRequestBean }
     *     
     */
    public ChFullCustomerInfoRequestBean getChFullCustomerInfoRequestBean() {
        return chFullCustomerInfoRequestBean;
    }

    /**
     * Sets the value of the chFullCustomerInfoRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChFullCustomerInfoRequestBean }
     *     
     */
    public void setChFullCustomerInfoRequestBean(ChFullCustomerInfoRequestBean value) {
        this.chFullCustomerInfoRequestBean = value;
    }

}
