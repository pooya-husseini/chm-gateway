
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBranchOrderType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chBranchOrderType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BRANCH_CODE"/>
 *     &lt;enumeration value="BRANCH_NAME"/>
 *     &lt;enumeration value="BALANCE"/>
 *     &lt;enumeration value="CURRENCY"/>
 *     &lt;enumeration value="CITY_NAME"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chBranchOrderType")
@XmlEnum
public enum ChBranchOrderType {

    BRANCH_CODE,
    BRANCH_NAME,
    BALANCE,
    CURRENCY,
    CITY_NAME;

    public String value() {
        return name();
    }

    public static ChBranchOrderType fromValue(String v) {
        return valueOf(v);
    }

}
