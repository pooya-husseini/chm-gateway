
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chDossierCycleLength.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chDossierCycleLength">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INFINITE"/>
 *     &lt;enumeration value="BETWEEN_1_79_DAYS"/>
 *     &lt;enumeration value="WEEKLY"/>
 *     &lt;enumeration value="TOW_WEEKLY"/>
 *     &lt;enumeration value="FIFTEEN_DAYS"/>
 *     &lt;enumeration value="MONTHLY"/>
 *     &lt;enumeration value="THREE_MONTHLY"/>
 *     &lt;enumeration value="SIX_MONTHLY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chDossierCycleLength")
@XmlEnum
public enum ChDossierCycleLength {

    INFINITE,
    BETWEEN_1_79_DAYS,
    WEEKLY,
    TOW_WEEKLY,
    FIFTEEN_DAYS,
    MONTHLY,
    THREE_MONTHLY,
    SIX_MONTHLY;

    public String value() {
        return name();
    }

    public static ChDossierCycleLength fromValue(String v) {
        return valueOf(v);
    }

}
