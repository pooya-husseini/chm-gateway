/**
 * ChLoginResponseBean.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.services.channel.model;

import com.caspco.infra.stubs.channel.ws.ChGender;
import io.swagger.annotations.ApiModel;

import java.util.Date;

@ApiModel
public class LoginResponseDto implements java.io.Serializable {

    private java.lang.String code;

    private java.lang.Long customerNo;

    private java.lang.String foreignName;

    private ChGender gender;

    private java.util.Date lastLoginTime;

    private java.lang.String name;

    private java.lang.Boolean requiredChangeSecondPassword;

    private java.lang.String sessionId;

    private java.lang.String title;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(Long customerNo) {
        this.customerNo = customerNo;
    }

    public String getForeignName() {
        return foreignName;
    }

    public void setForeignName(String foreignName) {
        this.foreignName = foreignName;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getRequiredChangeSecondPassword() {
        return requiredChangeSecondPassword;
    }

    public void setRequiredChangeSecondPassword(Boolean requiredChangeSecondPassword) {
        this.requiredChangeSecondPassword = requiredChangeSecondPassword;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ChGender getGender() {
        return gender;
    }

    public void setGender(ChGender gender) {
        this.gender = gender;
    }
}