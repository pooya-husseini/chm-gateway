/**
 * ChStatementResponseBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import java.util.List;

public class StatementResponseDto  implements java.io.Serializable {

    private List<StatementDto> statementBeans;

    private java.lang.Long totalRecord;

    public List<StatementDto> getStatementBeans() {
        return statementBeans;
    }

    public void setStatementBeans(List<StatementDto> statementBeans) {
        this.statementBeans = statementBeans;
    }

    public Long getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(Long totalRecord) {
        this.totalRecord = totalRecord;
    }
}
