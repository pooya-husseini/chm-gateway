
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for doPaymentResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="doPaymentResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chPaymentResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPaymentResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doPaymentResponse", propOrder = {
    "chPaymentResponseBean"
})
public class DoPaymentResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChPaymentResponseBean chPaymentResponseBean;

    /**
     * Gets the value of the chPaymentResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChPaymentResponseBean }
     *     
     */
    public ChPaymentResponseBean getChPaymentResponseBean() {
        return chPaymentResponseBean;
    }

    /**
     * Sets the value of the chPaymentResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPaymentResponseBean }
     *     
     */
    public void setChPaymentResponseBean(ChPaymentResponseBean value) {
        this.chPaymentResponseBean = value;
    }

}
