
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardProducts complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardProducts">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardProductsRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardProductsRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardProducts", propOrder = {
    "chCardProductsRequestBean"
})
public class GetCardProducts
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardProductsRequestBean chCardProductsRequestBean;

    /**
     * Gets the value of the chCardProductsRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardProductsRequestBean }
     *     
     */
    public ChCardProductsRequestBean getChCardProductsRequestBean() {
        return chCardProductsRequestBean;
    }

    /**
     * Sets the value of the chCardProductsRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardProductsRequestBean }
     *     
     */
    public void setChCardProductsRequestBean(ChCardProductsRequestBean value) {
        this.chCardProductsRequestBean = value;
    }

}
