
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chDefaultStatementResponseBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chDefaultStatementResponseBean">
 *   &lt;complexContent>
 *     &lt;extension base="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chStatementResponseBean">
 *       &lt;sequence>
 *         &lt;element name="billFields" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillBeanField" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="billPageSize" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="defaultBillStatementType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDefaultBillStatementBeanType" minOccurs="0"/>
 *         &lt;element name="defaultLastN" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="defaultOrder" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chOrderStatus" minOccurs="0"/>
 *         &lt;element name="fromDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="toDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chDefaultStatementResponseBean", propOrder = {
    "billFields",
    "billPageSize",
    "defaultBillStatementType",
    "defaultLastN",
    "defaultOrder",
    "fromDate",
    "toDate"
})
public class ChDefaultStatementResponseBean
    extends ChStatementResponseBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "string")
    protected List<ChBillBeanField> billFields;
    protected Short billPageSize;
    @XmlSchemaType(name = "string")
    protected ChDefaultBillStatementBeanType defaultBillStatementType;
    protected Short defaultLastN;
    @XmlSchemaType(name = "string")
    protected ChOrderStatus defaultOrder;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date fromDate;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date toDate;

    /**
     * Gets the value of the billFields property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the billFields property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBillFields().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChBillBeanField }
     * 
     * 
     */
    public List<ChBillBeanField> getBillFields() {
        if (billFields == null) {
            billFields = new ArrayList<ChBillBeanField>();
        }
        return this.billFields;
    }

    /**
     * Gets the value of the billPageSize property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getBillPageSize() {
        return billPageSize;
    }

    /**
     * Sets the value of the billPageSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setBillPageSize(Short value) {
        this.billPageSize = value;
    }

    /**
     * Gets the value of the defaultBillStatementType property.
     * 
     * @return
     *     possible object is
     *     {@link ChDefaultBillStatementBeanType }
     *     
     */
    public ChDefaultBillStatementBeanType getDefaultBillStatementType() {
        return defaultBillStatementType;
    }

    /**
     * Sets the value of the defaultBillStatementType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDefaultBillStatementBeanType }
     *     
     */
    public void setDefaultBillStatementType(ChDefaultBillStatementBeanType value) {
        this.defaultBillStatementType = value;
    }

    /**
     * Gets the value of the defaultLastN property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getDefaultLastN() {
        return defaultLastN;
    }

    /**
     * Sets the value of the defaultLastN property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setDefaultLastN(Short value) {
        this.defaultLastN = value;
    }

    /**
     * Gets the value of the defaultOrder property.
     * 
     * @return
     *     possible object is
     *     {@link ChOrderStatus }
     *     
     */
    public ChOrderStatus getDefaultOrder() {
        return defaultOrder;
    }

    /**
     * Sets the value of the defaultOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChOrderStatus }
     *     
     */
    public void setDefaultOrder(ChOrderStatus value) {
        this.defaultOrder = value;
    }

    /**
     * Gets the value of the fromDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * Sets the value of the fromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromDate(Date value) {
        this.fromDate = value;
    }

    /**
     * Gets the value of the toDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * Sets the value of the toDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToDate(Date value) {
        this.toDate = value;
    }

}
