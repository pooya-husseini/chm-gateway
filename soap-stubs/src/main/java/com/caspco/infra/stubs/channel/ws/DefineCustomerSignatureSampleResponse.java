
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for defineCustomerSignatureSampleResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="defineCustomerSignatureSampleResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDefineCustomerSignatureSampleResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDefineCustomerSignatureSampleResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "defineCustomerSignatureSampleResponse", propOrder = {
    "chDefineCustomerSignatureSampleResponseBean"
})
public class DefineCustomerSignatureSampleResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDefineCustomerSignatureSampleResponseBean chDefineCustomerSignatureSampleResponseBean;

    /**
     * Gets the value of the chDefineCustomerSignatureSampleResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDefineCustomerSignatureSampleResponseBean }
     *     
     */
    public ChDefineCustomerSignatureSampleResponseBean getChDefineCustomerSignatureSampleResponseBean() {
        return chDefineCustomerSignatureSampleResponseBean;
    }

    /**
     * Sets the value of the chDefineCustomerSignatureSampleResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDefineCustomerSignatureSampleResponseBean }
     *     
     */
    public void setChDefineCustomerSignatureSampleResponseBean(ChDefineCustomerSignatureSampleResponseBean value) {
        this.chDefineCustomerSignatureSampleResponseBean = value;
    }

}
