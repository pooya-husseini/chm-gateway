/**
 * ChSecondPasswordType.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;

import java.util.HashMap;

@ApiModel
public  enum SecondPasswordType implements java.io.Serializable {
    OTP,
    TRANSFER_SECOND_PASS;

    @JsonCreator
    public static SecondPasswordType fromStringOperator(HashMap<String, String> map) {
        if (map != null) {
            for (SecondPasswordType operator : SecondPasswordType.values()) {
                if (map.get("value").equals(operator.name())) {
                    return operator;
                }
            }
        }
        return null;
    }
}
