
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for payCreditBillResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="payCreditBillResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCreditBillPaymentResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCreditBillPaymentResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "payCreditBillResponse", propOrder = {
    "chCreditBillPaymentResponseBean"
})
public class PayCreditBillResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCreditBillPaymentResponseBean chCreditBillPaymentResponseBean;

    /**
     * Gets the value of the chCreditBillPaymentResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCreditBillPaymentResponseBean }
     *     
     */
    public ChCreditBillPaymentResponseBean getChCreditBillPaymentResponseBean() {
        return chCreditBillPaymentResponseBean;
    }

    /**
     * Sets the value of the chCreditBillPaymentResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCreditBillPaymentResponseBean }
     *     
     */
    public void setChCreditBillPaymentResponseBean(ChCreditBillPaymentResponseBean value) {
        this.chCreditBillPaymentResponseBean = value;
    }

}
