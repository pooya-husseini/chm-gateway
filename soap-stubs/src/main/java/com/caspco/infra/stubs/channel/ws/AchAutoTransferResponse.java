
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for achAutoTransferResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="achAutoTransferResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAchAutoTransferResultBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAchAutoTransferResultBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "achAutoTransferResponse", propOrder = {
    "chAchAutoTransferResultBean"
})
public class AchAutoTransferResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAchAutoTransferResultBean chAchAutoTransferResultBean;

    /**
     * Gets the value of the chAchAutoTransferResultBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAchAutoTransferResultBean }
     *     
     */
    public ChAchAutoTransferResultBean getChAchAutoTransferResultBean() {
        return chAchAutoTransferResultBean;
    }

    /**
     * Sets the value of the chAchAutoTransferResultBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAchAutoTransferResultBean }
     *     
     */
    public void setChAchAutoTransferResultBean(ChAchAutoTransferResultBean value) {
        this.chAchAutoTransferResultBean = value;
    }

}
