
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for switchFundTransferFromVirtualTerminal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="switchFundTransferFromVirtualTerminal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chVirtualCardTransferRequest" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chVirtualCardTransferRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "switchFundTransferFromVirtualTerminal", propOrder = {
    "chVirtualCardTransferRequest"
})
public class SwitchFundTransferFromVirtualTerminal
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChVirtualCardTransferRequest chVirtualCardTransferRequest;

    /**
     * Gets the value of the chVirtualCardTransferRequest property.
     * 
     * @return
     *     possible object is
     *     {@link ChVirtualCardTransferRequest }
     *     
     */
    public ChVirtualCardTransferRequest getChVirtualCardTransferRequest() {
        return chVirtualCardTransferRequest;
    }

    /**
     * Sets the value of the chVirtualCardTransferRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChVirtualCardTransferRequest }
     *     
     */
    public void setChVirtualCardTransferRequest(ChVirtualCardTransferRequest value) {
        this.chVirtualCardTransferRequest = value;
    }

}
