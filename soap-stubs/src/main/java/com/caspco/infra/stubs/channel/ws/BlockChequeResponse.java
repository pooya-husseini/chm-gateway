
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for blockChequeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="blockChequeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBlockChequeResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBlockChequeResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "blockChequeResponse", propOrder = {
    "chBlockChequeResponseBean"
})
public class BlockChequeResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBlockChequeResponseBean chBlockChequeResponseBean;

    /**
     * Gets the value of the chBlockChequeResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBlockChequeResponseBean }
     *     
     */
    public ChBlockChequeResponseBean getChBlockChequeResponseBean() {
        return chBlockChequeResponseBean;
    }

    /**
     * Sets the value of the chBlockChequeResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBlockChequeResponseBean }
     *     
     */
    public void setChBlockChequeResponseBean(ChBlockChequeResponseBean value) {
        this.chBlockChequeResponseBean = value;
    }

}
