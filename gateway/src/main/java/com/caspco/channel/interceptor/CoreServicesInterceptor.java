package com.caspco.channel.interceptor;

import com.caspco.channel.configuration.UrlPattern;
import org.springframework.stereotype.Component;

//import org.apache.axis.client.Stub;
//import org.apache.axis.transport.http.HTTPConstants;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/3/17
 * Time: 4:23 PM
 */

@Component
@UrlPattern("/core")
public class CoreServicesInterceptor extends AbstractServicesInterceptor {
    public CoreServicesInterceptor() {
        super(com.caspco.infra.stubs.channel.ws.CoreBankingService.class);
    }
}