
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chSellOrderField.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chSellOrderField">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TRANSACTION_TYPE"/>
 *     &lt;enumeration value="CARD_PAN"/>
 *     &lt;enumeration value="MERCHANT_ID"/>
 *     &lt;enumeration value="AMOUNT"/>
 *     &lt;enumeration value="TRANSACTION_TIME"/>
 *     &lt;enumeration value="RRN"/>
 *     &lt;enumeration value="DONE"/>
 *     &lt;enumeration value="CHANNEL"/>
 *     &lt;enumeration value="VERIFICATION_COUNT"/>
 *     &lt;enumeration value="MERCHANT_ASSIGNED_CODE"/>
 *     &lt;enumeration value="BILL_TYPE"/>
 *     &lt;enumeration value="BILL_ID"/>
 *     &lt;enumeration value="PAYMENT_ID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chSellOrderField")
@XmlEnum
public enum ChSellOrderField {

    TRANSACTION_TYPE,
    CARD_PAN,
    MERCHANT_ID,
    AMOUNT,
    TRANSACTION_TIME,
    RRN,
    DONE,
    CHANNEL,
    VERIFICATION_COUNT,
    MERCHANT_ASSIGNED_CODE,
    BILL_TYPE,
    BILL_ID,
    PAYMENT_ID;

    public String value() {
        return name();
    }

    public static ChSellOrderField fromValue(String v) {
        return valueOf(v);
    }

}
