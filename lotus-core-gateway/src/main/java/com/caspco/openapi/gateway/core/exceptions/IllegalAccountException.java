package com.caspco.openapi.gateway.core.exceptions;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/23/17
 *         Time: 11:20 AM
 */
public class IllegalAccountException extends RuntimeException{
    public IllegalAccountException() {
        super();
    }

    public IllegalAccountException(String message) {
        super(message);
    }

    public IllegalAccountException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalAccountException(Throwable cause) {
        super(cause);
    }

    protected IllegalAccountException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
