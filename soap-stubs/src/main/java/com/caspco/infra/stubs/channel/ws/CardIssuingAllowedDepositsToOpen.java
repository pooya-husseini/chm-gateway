
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardIssuingAllowedDepositsToOpen complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardIssuingAllowedDepositsToOpen">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardIssuingAllowedDepositsToOpenRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardIssuingAllowedDepositsToOpenRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardIssuingAllowedDepositsToOpen", propOrder = {
    "chCardIssuingAllowedDepositsToOpenRequestBean"
})
public class CardIssuingAllowedDepositsToOpen
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardIssuingAllowedDepositsToOpenRequestBean chCardIssuingAllowedDepositsToOpenRequestBean;

    /**
     * Gets the value of the chCardIssuingAllowedDepositsToOpenRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardIssuingAllowedDepositsToOpenRequestBean }
     *     
     */
    public ChCardIssuingAllowedDepositsToOpenRequestBean getChCardIssuingAllowedDepositsToOpenRequestBean() {
        return chCardIssuingAllowedDepositsToOpenRequestBean;
    }

    /**
     * Sets the value of the chCardIssuingAllowedDepositsToOpenRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardIssuingAllowedDepositsToOpenRequestBean }
     *     
     */
    public void setChCardIssuingAllowedDepositsToOpenRequestBean(ChCardIssuingAllowedDepositsToOpenRequestBean value) {
        this.chCardIssuingAllowedDepositsToOpenRequestBean = value;
    }

}
