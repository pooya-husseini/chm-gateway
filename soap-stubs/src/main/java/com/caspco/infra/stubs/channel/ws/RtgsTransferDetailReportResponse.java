
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rtgsTransferDetailReportResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rtgsTransferDetailReportResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chRtgsTransferDetailResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chRtgsTransferDetailResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rtgsTransferDetailReportResponse", propOrder = {
    "chRtgsTransferDetailResponseBean"
})
public class RtgsTransferDetailReportResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChRtgsTransferDetailResponseBean chRtgsTransferDetailResponseBean;

    /**
     * Gets the value of the chRtgsTransferDetailResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChRtgsTransferDetailResponseBean }
     *     
     */
    public ChRtgsTransferDetailResponseBean getChRtgsTransferDetailResponseBean() {
        return chRtgsTransferDetailResponseBean;
    }

    /**
     * Sets the value of the chRtgsTransferDetailResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChRtgsTransferDetailResponseBean }
     *     
     */
    public void setChRtgsTransferDetailResponseBean(ChRtgsTransferDetailResponseBean value) {
        this.chRtgsTransferDetailResponseBean = value;
    }

}
