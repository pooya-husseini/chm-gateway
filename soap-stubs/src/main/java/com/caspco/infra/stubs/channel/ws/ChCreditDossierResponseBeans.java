
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCreditDossierResponseBeans complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chCreditDossierResponseBeans">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dossiers" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCreditDossierBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="totalRecordCount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chCreditDossierResponseBeans", propOrder = {
    "dossiers",
    "totalRecordCount"
})
public class ChCreditDossierResponseBeans
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(nillable = true)
    protected List<ChCreditDossierBean> dossiers;
    protected Long totalRecordCount;

    /**
     * Gets the value of the dossiers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dossiers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDossiers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChCreditDossierBean }
     * 
     * 
     */
    public List<ChCreditDossierBean> getDossiers() {
        if (dossiers == null) {
            dossiers = new ArrayList<ChCreditDossierBean>();
        }
        return this.dossiers;
    }

    /**
     * Gets the value of the totalRecordCount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTotalRecordCount() {
        return totalRecordCount;
    }

    /**
     * Sets the value of the totalRecordCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTotalRecordCount(Long value) {
        this.totalRecordCount = value;
    }

}
