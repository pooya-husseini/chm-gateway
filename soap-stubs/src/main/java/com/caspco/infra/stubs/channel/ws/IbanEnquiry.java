
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ibanEnquiry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ibanEnquiry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Message02" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chIBANEnquiryRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ibanEnquiry", propOrder = {
    "message02"
})
public class IbanEnquiry
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Message02")
    protected ChIBANEnquiryRequest message02;

    /**
     * Gets the value of the message02 property.
     * 
     * @return
     *     possible object is
     *     {@link ChIBANEnquiryRequest }
     *     
     */
    public ChIBANEnquiryRequest getMessage02() {
        return message02;
    }

    /**
     * Sets the value of the message02 property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChIBANEnquiryRequest }
     *     
     */
    public void setMessage02(ChIBANEnquiryRequest value) {
        this.message02 = value;
    }

}
