
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardPurchase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardPurchase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardPurchaseRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardPurchaseRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardPurchase", propOrder = {
    "chCardPurchaseRequestBean"
})
public class CardPurchase
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardPurchaseRequestBean chCardPurchaseRequestBean;

    /**
     * Gets the value of the chCardPurchaseRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardPurchaseRequestBean }
     *     
     */
    public ChCardPurchaseRequestBean getChCardPurchaseRequestBean() {
        return chCardPurchaseRequestBean;
    }

    /**
     * Sets the value of the chCardPurchaseRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardPurchaseRequestBean }
     *     
     */
    public void setChCardPurchaseRequestBean(ChCardPurchaseRequestBean value) {
        this.chCardPurchaseRequestBean = value;
    }

}
