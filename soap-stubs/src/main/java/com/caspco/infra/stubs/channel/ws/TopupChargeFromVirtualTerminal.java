
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for topupChargeFromVirtualTerminal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="topupChargeFromVirtualTerminal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chTopupChargeRequest" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chTopupChargeRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "topupChargeFromVirtualTerminal", propOrder = {
    "chTopupChargeRequest"
})
public class TopupChargeFromVirtualTerminal
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChTopupChargeRequest chTopupChargeRequest;

    /**
     * Gets the value of the chTopupChargeRequest property.
     * 
     * @return
     *     possible object is
     *     {@link ChTopupChargeRequest }
     *     
     */
    public ChTopupChargeRequest getChTopupChargeRequest() {
        return chTopupChargeRequest;
    }

    /**
     * Sets the value of the chTopupChargeRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChTopupChargeRequest }
     *     
     */
    public void setChTopupChargeRequest(ChTopupChargeRequest value) {
        this.chTopupChargeRequest = value;
    }

}
