package com.caspco.openapi.gateway.core.exceptions;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/23/17
 *         Time: 11:45 AM
 */
public class CoreException extends RuntimeException {
    public CoreException() {
        super();
    }

    public CoreException(String message) {
        super(message);
    }

    public CoreException(String message, Throwable cause) {
        super(message, cause);
    }

    public CoreException(Throwable cause) {
        super(cause);
    }

    protected CoreException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
