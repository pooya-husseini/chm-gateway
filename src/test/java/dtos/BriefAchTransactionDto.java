/**
 * ChBriefAchTransactionBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class BriefAchTransactionDto  implements java.io.Serializable {

    private java.lang.Long count;

    private java.math.BigDecimal totalAmount;

    @ApiModelProperty(value = "transactionStatus")
    private com.caspco.channel.dtos.TransactionStatus transactionStatus;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
}
