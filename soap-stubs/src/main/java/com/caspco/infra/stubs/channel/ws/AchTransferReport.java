
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for achTransferReport complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="achTransferReport">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAchTransferSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAchTransferSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "achTransferReport", propOrder = {
    "chAchTransferSearchRequestBean"
})
public class AchTransferReport
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAchTransferSearchRequestBean chAchTransferSearchRequestBean;

    /**
     * Gets the value of the chAchTransferSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAchTransferSearchRequestBean }
     *     
     */
    public ChAchTransferSearchRequestBean getChAchTransferSearchRequestBean() {
        return chAchTransferSearchRequestBean;
    }

    /**
     * Sets the value of the chAchTransferSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAchTransferSearchRequestBean }
     *     
     */
    public void setChAchTransferSearchRequestBean(ChAchTransferSearchRequestBean value) {
        this.chAchTransferSearchRequestBean = value;
    }

}
