
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for doPayment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="doPayment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chPaymentRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPaymentRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doPayment", propOrder = {
    "chPaymentRequestBean"
})
public class DoPayment
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChPaymentRequestBean chPaymentRequestBean;

    /**
     * Gets the value of the chPaymentRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChPaymentRequestBean }
     *     
     */
    public ChPaymentRequestBean getChPaymentRequestBean() {
        return chPaymentRequestBean;
    }

    /**
     * Sets the value of the chPaymentRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPaymentRequestBean }
     *     
     */
    public void setChPaymentRequestBean(ChPaymentRequestBean value) {
        this.chPaymentRequestBean = value;
    }

}
