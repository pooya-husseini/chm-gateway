
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardInfoRetrievation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardInfoRetrievation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardInfoRetrievationRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardInfoRetrievationRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardInfoRetrievation", propOrder = {
    "chCardInfoRetrievationRequestBean"
})
public class CardInfoRetrievation
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardInfoRetrievationRequestBean chCardInfoRetrievationRequestBean;

    /**
     * Gets the value of the chCardInfoRetrievationRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardInfoRetrievationRequestBean }
     *     
     */
    public ChCardInfoRetrievationRequestBean getChCardInfoRetrievationRequestBean() {
        return chCardInfoRetrievationRequestBean;
    }

    /**
     * Sets the value of the chCardInfoRetrievationRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardInfoRetrievationRequestBean }
     *     
     */
    public void setChCardInfoRetrievationRequestBean(ChCardInfoRetrievationRequestBean value) {
        this.chCardInfoRetrievationRequestBean = value;
    }

}
