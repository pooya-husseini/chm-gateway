
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chPeriodicRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chPeriodicRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="depositNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="durationLength" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="durationType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPeriodicDurationType" minOccurs="0"/>
 *         &lt;element name="registerDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="sendResponses" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chMediaType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chPeriodicRequestBean", propOrder = {
    "depositNumber",
    "durationLength",
    "durationType",
    "registerDate",
    "sendResponses"
})
public class ChPeriodicRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String depositNumber;
    protected short durationLength;
    @XmlSchemaType(name = "string")
    protected ChPeriodicDurationType durationType;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date registerDate;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "string")
    protected List<ChMediaType> sendResponses;

    /**
     * Gets the value of the depositNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepositNumber() {
        return depositNumber;
    }

    /**
     * Sets the value of the depositNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepositNumber(String value) {
        this.depositNumber = value;
    }

    /**
     * Gets the value of the durationLength property.
     * 
     */
    public short getDurationLength() {
        return durationLength;
    }

    /**
     * Sets the value of the durationLength property.
     * 
     */
    public void setDurationLength(short value) {
        this.durationLength = value;
    }

    /**
     * Gets the value of the durationType property.
     * 
     * @return
     *     possible object is
     *     {@link ChPeriodicDurationType }
     *     
     */
    public ChPeriodicDurationType getDurationType() {
        return durationType;
    }

    /**
     * Sets the value of the durationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPeriodicDurationType }
     *     
     */
    public void setDurationType(ChPeriodicDurationType value) {
        this.durationType = value;
    }

    /**
     * Gets the value of the registerDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getRegisterDate() {
        return registerDate;
    }

    /**
     * Sets the value of the registerDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterDate(Date value) {
        this.registerDate = value;
    }

    /**
     * Gets the value of the sendResponses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sendResponses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSendResponses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChMediaType }
     * 
     * 
     */
    public List<ChMediaType> getSendResponses() {
        if (sendResponses == null) {
            sendResponses = new ArrayList<ChMediaType>();
        }
        return this.sendResponses;
    }

}
