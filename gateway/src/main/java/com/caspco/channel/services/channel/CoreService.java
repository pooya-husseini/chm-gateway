package com.caspco.channel.services.channel;

import com.caspco.channel.exceptions.SystemException;
import com.caspco.infra.stubs.channel.ws.CoreBankingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/11/17
 * Time: 10:18 AM
 */

@Service
public class CoreService {
    private com.caspco.infra.stubs.channel.ws.CoreBankingService actualService;
    private URL url;

    @Autowired
    private Environment environment;

    @PostConstruct
    public void init() {

        try {
            String soapUrl = environment.getProperty("core.soap.url");
            System.out.println("CM -> " + environment.getProperty("CASPIAN_CM_HOST"));
            url = new URL(soapUrl);
            System.out.println("Soap Url = " + soapUrl);
        } catch (MalformedURLException e) {
            throw new SystemException(e);
        }
    }

    public CoreBankingService getService(HttpServletRequest request) {

        actualService = new com.caspco.infra.stubs.channel.ws.WebserviceGatewayCoreBankingServiceImplService(url)
                .getWebserviceGatewayCoreBankingServiceImplPort();

        return (CoreBankingService) Enhancer.create(CoreBankingService.class, (MethodInterceptor) (o, method, objects, methodProxy) -> {
            setHeader(request);
            return method.invoke(actualService, objects);
        });
    }

    private void setHeader(HttpServletRequest request) {
        String token = request.getHeader("token");
        if (token == null) {
            token = request.getParameter("token");
        }
        Map<String, List<String>> maps = new Hashtable<>();
        maps.put("token", Collections.singletonList(token));
        Map<String, Object> requestContext = ((BindingProvider) (actualService)).getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, maps);
    }
}