package com.caspco.openapi.gateway.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/23/17
 *         Time: 10:19 AM
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerAccount implements Serializable {
    private static final long serialVersionUID = 9185629364067679770L;
    private String accountId;
    private String externalRef;
    private String iban;
    private String ownerName;
    private String residentCountryCode;
    private Boolean hasCard;
    private String currencyCode;
    private String description;
    private String originCountryCode;


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getExternalRef() {
        return externalRef;
    }

    public void setExternalRef(String externalRef) {
        this.externalRef = externalRef;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getResidentCountryCode() {
        return residentCountryCode;
    }

    public void setResidentCountryCode(String residentCountryCode) {
        this.residentCountryCode = residentCountryCode;
    }

    public Boolean getHasCard() {
        return hasCard;
    }

    public void setHasCard(Boolean hasCard) {
        this.hasCard = hasCard;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOriginCountryCode() {
        return originCountryCode;
    }

    public void setOriginCountryCode(String originCountryCode) {
        this.originCountryCode = originCountryCode;
    }
}
