
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hotCard complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hotCard">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chPanRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPanRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hotCard", propOrder = {
    "chPanRequestBean"
})
public class HotCard
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChPanRequestBean chPanRequestBean;

    /**
     * Gets the value of the chPanRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChPanRequestBean }
     *     
     */
    public ChPanRequestBean getChPanRequestBean() {
        return chPanRequestBean;
    }

    /**
     * Sets the value of the chPanRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPanRequestBean }
     *     
     */
    public void setChPanRequestBean(ChPanRequestBean value) {
        this.chPanRequestBean = value;
    }

}
