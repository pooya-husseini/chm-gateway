
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for registerChequeBookRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="registerChequeBookRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chChequeBookRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chChequeBookRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "registerChequeBookRequest", propOrder = {
    "chChequeBookRequestBean"
})
public class RegisterChequeBookRequest
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChChequeBookRequestBean chChequeBookRequestBean;

    /**
     * Gets the value of the chChequeBookRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChChequeBookRequestBean }
     *     
     */
    public ChChequeBookRequestBean getChChequeBookRequestBean() {
        return chChequeBookRequestBean;
    }

    /**
     * Sets the value of the chChequeBookRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChChequeBookRequestBean }
     *     
     */
    public void setChChequeBookRequestBean(ChChequeBookRequestBean value) {
        this.chChequeBookRequestBean = value;
    }

}
