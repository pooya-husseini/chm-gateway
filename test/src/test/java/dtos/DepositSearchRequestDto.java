/**
 * ChDepositSearchRequestBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

import java.util.List;

public class DepositSearchRequestDto  implements java.io.Serializable {

    private String cif;

    private List<String> depositNumbers;

    private DepositStatus depositStatus;

    private List<String> excludeCurrency;

    private List<DepositGroupType> excludeType;

    private Boolean forSearch;

    private Boolean includeCreditAccount;

    private List<String> includeCurrency;

    private Boolean includeSupportAccount;

    private List<DepositGroupType> includeType;

    private Long length;

    private Long offset;

    private List<PersonalityType> personality;

    private ServicesCode serviceCode;

    private List<SignatureOwnerStatus> signatureStatus;

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public List<String> getDepositNumbers() {
        return depositNumbers;
    }

    public void setDepositNumbers(List<String> depositNumbers) {
        this.depositNumbers = depositNumbers;
    }

    public DepositStatus getDepositStatus() {
        return depositStatus;
    }

    public void setDepositStatus(DepositStatus depositStatus) {
        this.depositStatus = depositStatus;
    }

    public List<String> getExcludeCurrency() {
        return excludeCurrency;
    }

    public void setExcludeCurrency(List<String> excludeCurrency) {
        this.excludeCurrency = excludeCurrency;
    }

    public List<DepositGroupType> getExcludeType() {
        return excludeType;
    }

    public void setExcludeType(List<DepositGroupType> excludeType) {
        this.excludeType = excludeType;
    }

    public Boolean getForSearch() {
        return forSearch;
    }

    public void setForSearch(Boolean forSearch) {
        this.forSearch = forSearch;
    }

    public Boolean getIncludeCreditAccount() {
        return includeCreditAccount;
    }

    public void setIncludeCreditAccount(Boolean includeCreditAccount) {
        this.includeCreditAccount = includeCreditAccount;
    }

    public List<String> getIncludeCurrency() {
        return includeCurrency;
    }

    public void setIncludeCurrency(List<String> includeCurrency) {
        this.includeCurrency = includeCurrency;
    }

    public Boolean getIncludeSupportAccount() {
        return includeSupportAccount;
    }

    public void setIncludeSupportAccount(Boolean includeSupportAccount) {
        this.includeSupportAccount = includeSupportAccount;
    }

    public List<DepositGroupType> getIncludeType() {
        return includeType;
    }

    public void setIncludeType(List<DepositGroupType> includeType) {
        this.includeType = includeType;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public List<PersonalityType> getPersonality() {
        return personality;
    }

    public void setPersonality(List<PersonalityType> personality) {
        this.personality = personality;
    }

    public ServicesCode getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(ServicesCode serviceCode) {
        this.serviceCode = serviceCode;
    }

    public List<SignatureOwnerStatus> getSignatureStatus() {
        return signatureStatus;
    }

    public void setSignatureStatus(List<SignatureOwnerStatus> signatureStatus) {
        this.signatureStatus = signatureStatus;
    }
}
