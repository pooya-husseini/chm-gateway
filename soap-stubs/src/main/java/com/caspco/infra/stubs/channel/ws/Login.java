
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for login complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="login">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chUserInfoRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chUserInfoRequestBean" minOccurs="0"/>
 *         &lt;element name="channelServiceType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}channelServiceType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "login", propOrder = {
    "chUserInfoRequestBean",
    "channelServiceType"
})
public class Login
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChUserInfoRequestBean chUserInfoRequestBean;
    @XmlSchemaType(name = "string")
    protected ChannelServiceType channelServiceType;

    /**
     * Gets the value of the chUserInfoRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChUserInfoRequestBean }
     *     
     */
    public ChUserInfoRequestBean getChUserInfoRequestBean() {
        return chUserInfoRequestBean;
    }

    /**
     * Sets the value of the chUserInfoRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChUserInfoRequestBean }
     *     
     */
    public void setChUserInfoRequestBean(ChUserInfoRequestBean value) {
        this.chUserInfoRequestBean = value;
    }

    /**
     * Gets the value of the channelServiceType property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelServiceType }
     *     
     */
    public ChannelServiceType getChannelServiceType() {
        return channelServiceType;
    }

    /**
     * Sets the value of the channelServiceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelServiceType }
     *     
     */
    public void setChannelServiceType(ChannelServiceType value) {
        this.channelServiceType = value;
    }

}
