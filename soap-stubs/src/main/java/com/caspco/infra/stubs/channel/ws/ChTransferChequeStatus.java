
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chTransferChequeStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chTransferChequeStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="IN_WAY"/>
 *     &lt;enumeration value="CASH"/>
 *     &lt;enumeration value="REJECT"/>
 *     &lt;enumeration value="SEND_TO_CLER"/>
 *     &lt;enumeration value="RETURN"/>
 *     &lt;enumeration value="RETURN_REJECT"/>
 *     &lt;enumeration value="WITHOUT_ACTION"/>
 *     &lt;enumeration value="RETURN_WITHOUT_ACTION"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chTransferChequeStatus")
@XmlEnum
public enum ChTransferChequeStatus {

    IN_WAY,
    CASH,
    REJECT,
    SEND_TO_CLER,
    RETURN,
    RETURN_REJECT,
    WITHOUT_ACTION,
    RETURN_WITHOUT_ACTION;

    public String value() {
        return name();
    }

    public static ChTransferChequeStatus fromValue(String v) {
        return valueOf(v);
    }

}
