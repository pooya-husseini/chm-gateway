
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBanksResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBanksResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBankResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBankResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBanksResponse", propOrder = {
    "chBankResponseBean"
})
public class GetBanksResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBankResponseBean chBankResponseBean;

    /**
     * Gets the value of the chBankResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBankResponseBean }
     *     
     */
    public ChBankResponseBean getChBankResponseBean() {
        return chBankResponseBean;
    }

    /**
     * Sets the value of the chBankResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBankResponseBean }
     *     
     */
    public void setChBankResponseBean(ChBankResponseBean value) {
        this.chBankResponseBean = value;
    }

}
