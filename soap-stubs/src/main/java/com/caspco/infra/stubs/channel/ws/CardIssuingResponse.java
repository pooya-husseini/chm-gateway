
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardIssuingResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardIssuingResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardIssuingResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardIssuingResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardIssuingResponse", propOrder = {
    "chCardIssuingResponseBean"
})
public class CardIssuingResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardIssuingResponseBean chCardIssuingResponseBean;

    /**
     * Gets the value of the chCardIssuingResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardIssuingResponseBean }
     *     
     */
    public ChCardIssuingResponseBean getChCardIssuingResponseBean() {
        return chCardIssuingResponseBean;
    }

    /**
     * Sets the value of the chCardIssuingResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardIssuingResponseBean }
     *     
     */
    public void setChCardIssuingResponseBean(ChCardIssuingResponseBean value) {
        this.chCardIssuingResponseBean = value;
    }

}
