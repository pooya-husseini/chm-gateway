
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getUserSMSSetting complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getUserSMSSetting">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSMSSettingRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSMSSettingRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUserSMSSetting", propOrder = {
    "chSMSSettingRequestBean"
})
public class GetUserSMSSetting
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSMSSettingRequestBean chSMSSettingRequestBean;

    /**
     * Gets the value of the chSMSSettingRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChSMSSettingRequestBean }
     *     
     */
    public ChSMSSettingRequestBean getChSMSSettingRequestBean() {
        return chSMSSettingRequestBean;
    }

    /**
     * Sets the value of the chSMSSettingRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSMSSettingRequestBean }
     *     
     */
    public void setChSMSSettingRequestBean(ChSMSSettingRequestBean value) {
        this.chSMSSettingRequestBean = value;
    }

}
