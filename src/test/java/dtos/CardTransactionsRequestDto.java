/**
 * ChCardTransactionsRequestBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class CardTransactionsRequestDto  implements java.io.Serializable {

    @ApiModelProperty(value = "cardActivityType",allowableValues = "Test1,Test2")
    private List<CardActivityType> activityTypes;

    private java.lang.String description;

    private java.math.BigDecimal fromAmount;

    private java.util.Date fromDate;

    private java.lang.String issuerNumber;

    private java.lang.Long length;

    private java.lang.Long offset;

    private java.lang.String opponentIssuerNumber;

    private java.lang.String opponentPan;

    private java.lang.String pan;

    private java.lang.String referralNumber;

    private boolean showTotalRecord;

    private java.math.BigDecimal toAmount;

    private java.util.Date toDate;

    @ApiModelProperty(value = "cardTransactionType")
    private List<com.caspco.channel.dtos.CardTransactionType> types;


    public List<CardActivityType> getActivityTypes() {
        return activityTypes;
    }

    public void setActivityTypes(List<CardActivityType> activityTypes) {
        this.activityTypes = activityTypes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(BigDecimal fromAmount) {
        this.fromAmount = fromAmount;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public String getIssuerNumber() {
        return issuerNumber;
    }

    public void setIssuerNumber(String issuerNumber) {
        this.issuerNumber = issuerNumber;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public String getOpponentIssuerNumber() {
        return opponentIssuerNumber;
    }

    public void setOpponentIssuerNumber(String opponentIssuerNumber) {
        this.opponentIssuerNumber = opponentIssuerNumber;
    }

    public String getOpponentPan() {
        return opponentPan;
    }

    public void setOpponentPan(String opponentPan) {
        this.opponentPan = opponentPan;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getReferralNumber() {
        return referralNumber;
    }

    public void setReferralNumber(String referralNumber) {
        this.referralNumber = referralNumber;
    }

    public boolean isShowTotalRecord() {
        return showTotalRecord;
    }

    public void setShowTotalRecord(boolean showTotalRecord) {
        this.showTotalRecord = showTotalRecord;
    }

    public BigDecimal getToAmount() {
        return toAmount;
    }

    public void setToAmount(BigDecimal toAmount) {
        this.toAmount = toAmount;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public List<CardTransactionType> getTypes() {
        return types;
    }

    public void setTypes(List<CardTransactionType> types) {
        this.types = types;
    }
}
