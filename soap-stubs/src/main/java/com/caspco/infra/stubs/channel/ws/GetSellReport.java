
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getSellReport complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getSellReport">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSellReportRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSellReportRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSellReport", propOrder = {
    "chSellReportRequestBean"
})
public class GetSellReport
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSellReportRequestBean chSellReportRequestBean;

    /**
     * Gets the value of the chSellReportRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChSellReportRequestBean }
     *     
     */
    public ChSellReportRequestBean getChSellReportRequestBean() {
        return chSellReportRequestBean;
    }

    /**
     * Sets the value of the chSellReportRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSellReportRequestBean }
     *     
     */
    public void setChSellReportRequestBean(ChSellReportRequestBean value) {
        this.chSellReportRequestBean = value;
    }

}
