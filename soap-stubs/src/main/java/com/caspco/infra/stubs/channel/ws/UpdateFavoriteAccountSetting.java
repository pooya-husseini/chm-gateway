
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateFavoriteAccountSetting complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateFavoriteAccountSetting">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chFavoriteDepositNumberRequestBeans" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chFavoriteDepositNumberRequestBeans" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateFavoriteAccountSetting", propOrder = {
    "chFavoriteDepositNumberRequestBeans"
})
public class UpdateFavoriteAccountSetting
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChFavoriteDepositNumberRequestBeans chFavoriteDepositNumberRequestBeans;

    /**
     * Gets the value of the chFavoriteDepositNumberRequestBeans property.
     * 
     * @return
     *     possible object is
     *     {@link ChFavoriteDepositNumberRequestBeans }
     *     
     */
    public ChFavoriteDepositNumberRequestBeans getChFavoriteDepositNumberRequestBeans() {
        return chFavoriteDepositNumberRequestBeans;
    }

    /**
     * Sets the value of the chFavoriteDepositNumberRequestBeans property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChFavoriteDepositNumberRequestBeans }
     *     
     */
    public void setChFavoriteDepositNumberRequestBeans(ChFavoriteDepositNumberRequestBeans value) {
        this.chFavoriteDepositNumberRequestBeans = value;
    }

}
