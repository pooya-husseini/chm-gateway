
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rtgsActionTypeWS.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="rtgsActionTypeWS">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DASTUR_SABT_SHODE"/>
 *     &lt;enumeration value="VIRAYESH_SHODE"/>
 *     &lt;enumeration value="TALIGH_DAEM"/>
 *     &lt;enumeration value="TAEED_SHODE"/>
 *     &lt;enumeration value="ADAM_TAEED_SHOBE"/>
 *     &lt;enumeration value="TAEED_SHOBE_SATNA"/>
 *     &lt;enumeration value="ADAM_TAEED_SHOBE_SATNA"/>
 *     &lt;enumeration value="ERSAL_SHODE_BE_BANK_MAGHSAD"/>
 *     &lt;enumeration value="VARIZ_SHODE_BE_BANK_MAGHSAD"/>
 *     &lt;enumeration value="VARIZ_NASHODE_BE_BANK_MAGHSAD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "rtgsActionTypeWS")
@XmlEnum
public enum RtgsActionTypeWS {

    DASTUR_SABT_SHODE,
    VIRAYESH_SHODE,
    TALIGH_DAEM,
    TAEED_SHODE,
    ADAM_TAEED_SHOBE,
    TAEED_SHOBE_SATNA,
    ADAM_TAEED_SHOBE_SATNA,
    ERSAL_SHODE_BE_BANK_MAGHSAD,
    VARIZ_SHODE_BE_BANK_MAGHSAD,
    VARIZ_NASHODE_BE_BANK_MAGHSAD;

    public String value() {
        return name();
    }

    public static RtgsActionTypeWS fromValue(String v) {
        return valueOf(v);
    }

}
