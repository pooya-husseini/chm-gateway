
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCreditBillTransactions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCreditBillTransactions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBillTransactionSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillTransactionSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCreditBillTransactions", propOrder = {
    "chBillTransactionSearchRequestBean"
})
public class GetCreditBillTransactions
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBillTransactionSearchRequestBean chBillTransactionSearchRequestBean;

    /**
     * Gets the value of the chBillTransactionSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillTransactionSearchRequestBean }
     *     
     */
    public ChBillTransactionSearchRequestBean getChBillTransactionSearchRequestBean() {
        return chBillTransactionSearchRequestBean;
    }

    /**
     * Sets the value of the chBillTransactionSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillTransactionSearchRequestBean }
     *     
     */
    public void setChBillTransactionSearchRequestBean(ChBillTransactionSearchRequestBean value) {
        this.chBillTransactionSearchRequestBean = value;
    }

}
