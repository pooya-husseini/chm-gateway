/**
 * ChRtgTransferSearchRequestBean.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public class RtgTransferSearchRequestDto extends SearchBeanDto implements java.io.Serializable {
    private String cif;
    private RtgsTransferStatus status;

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public RtgsTransferStatus getStatus() {
        return status;
    }

    public void setStatus(RtgsTransferStatus status) {
        this.status = status;
    }
}
