
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardTransactions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardTransactions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardTransactionsRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardTransactionsRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardTransactions", propOrder = {
    "chCardTransactionsRequestBean"
})
public class GetCardTransactions
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardTransactionsRequestBean chCardTransactionsRequestBean;

    /**
     * Gets the value of the chCardTransactionsRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardTransactionsRequestBean }
     *     
     */
    public ChCardTransactionsRequestBean getChCardTransactionsRequestBean() {
        return chCardTransactionsRequestBean;
    }

    /**
     * Sets the value of the chCardTransactionsRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardTransactionsRequestBean }
     *     
     */
    public void setChCardTransactionsRequestBean(ChCardTransactionsRequestBean value) {
        this.chCardTransactionsRequestBean = value;
    }

}
