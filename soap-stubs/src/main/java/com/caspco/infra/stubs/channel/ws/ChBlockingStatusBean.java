
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBlockingStatusBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chBlockingStatusBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="blockingStatus" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBlockingStatus" minOccurs="0"/>
 *         &lt;element name="chequeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chBlockingStatusBean", propOrder = {
    "blockingStatus",
    "chequeNumber"
})
public class ChBlockingStatusBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlSchemaType(name = "string")
    protected ChBlockingStatus blockingStatus;
    protected String chequeNumber;

    /**
     * Gets the value of the blockingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ChBlockingStatus }
     *     
     */
    public ChBlockingStatus getBlockingStatus() {
        return blockingStatus;
    }

    /**
     * Sets the value of the blockingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBlockingStatus }
     *     
     */
    public void setBlockingStatus(ChBlockingStatus value) {
        this.blockingStatus = value;
    }

    /**
     * Gets the value of the chequeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChequeNumber() {
        return chequeNumber;
    }

    /**
     * Sets the value of the chequeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChequeNumber(String value) {
        this.chequeNumber = value;
    }

}
