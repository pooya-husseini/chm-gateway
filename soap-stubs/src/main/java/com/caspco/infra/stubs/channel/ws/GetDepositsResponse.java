
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDepositsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDepositsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDepositResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDepositsResponse", propOrder = {
    "chDepositResponseBean"
})
public class GetDepositsResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDepositResponseBean chDepositResponseBean;

    /**
     * Gets the value of the chDepositResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositResponseBean }
     *     
     */
    public ChDepositResponseBean getChDepositResponseBean() {
        return chDepositResponseBean;
    }

    /**
     * Sets the value of the chDepositResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositResponseBean }
     *     
     */
    public void setChDepositResponseBean(ChDepositResponseBean value) {
        this.chDepositResponseBean = value;
    }

}
