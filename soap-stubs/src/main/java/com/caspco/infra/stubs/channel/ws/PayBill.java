
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for payBill complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="payBill">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBillPaymentRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillPaymentRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "payBill", propOrder = {
    "chBillPaymentRequestBean"
})
public class PayBill
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBillPaymentRequestBean chBillPaymentRequestBean;

    /**
     * Gets the value of the chBillPaymentRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillPaymentRequestBean }
     *     
     */
    public ChBillPaymentRequestBean getChBillPaymentRequestBean() {
        return chBillPaymentRequestBean;
    }

    /**
     * Sets the value of the chBillPaymentRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillPaymentRequestBean }
     *     
     */
    public void setChBillPaymentRequestBean(ChBillPaymentRequestBean value) {
        this.chBillPaymentRequestBean = value;
    }

}
