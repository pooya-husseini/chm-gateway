
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBranchBalanceSearchBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chBranchBalanceSearchBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="branchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="branchOrder" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBranchOrderType" minOccurs="0"/>
 *         &lt;element name="cityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fromAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="fromValuableItemTypeCount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="offset" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="orderType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chOrderDirection" minOccurs="0"/>
 *         &lt;element name="toAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="toValuableItemTypeCount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="valuableItem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valuableItemType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chBranchBalanceSearchBean", propOrder = {
    "branchCode",
    "branchOrder",
    "cityCode",
    "currency",
    "fromAmount",
    "fromValuableItemTypeCount",
    "length",
    "offset",
    "orderType",
    "toAmount",
    "toValuableItemTypeCount",
    "valuableItem",
    "valuableItemType"
})
public class ChBranchBalanceSearchBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String branchCode;
    @XmlSchemaType(name = "string")
    protected ChBranchOrderType branchOrder;
    protected String cityCode;
    protected String currency;
    protected BigDecimal fromAmount;
    protected Long fromValuableItemTypeCount;
    protected Long length;
    protected Long offset;
    @XmlSchemaType(name = "string")
    protected ChOrderDirection orderType;
    protected BigDecimal toAmount;
    protected Long toValuableItemTypeCount;
    protected String valuableItem;
    protected String valuableItemType;

    /**
     * Gets the value of the branchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * Sets the value of the branchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchCode(String value) {
        this.branchCode = value;
    }

    /**
     * Gets the value of the branchOrder property.
     * 
     * @return
     *     possible object is
     *     {@link ChBranchOrderType }
     *     
     */
    public ChBranchOrderType getBranchOrder() {
        return branchOrder;
    }

    /**
     * Sets the value of the branchOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBranchOrderType }
     *     
     */
    public void setBranchOrder(ChBranchOrderType value) {
        this.branchOrder = value;
    }

    /**
     * Gets the value of the cityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityCode() {
        return cityCode;
    }

    /**
     * Sets the value of the cityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityCode(String value) {
        this.cityCode = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the fromAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFromAmount() {
        return fromAmount;
    }

    /**
     * Sets the value of the fromAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFromAmount(BigDecimal value) {
        this.fromAmount = value;
    }

    /**
     * Gets the value of the fromValuableItemTypeCount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFromValuableItemTypeCount() {
        return fromValuableItemTypeCount;
    }

    /**
     * Sets the value of the fromValuableItemTypeCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFromValuableItemTypeCount(Long value) {
        this.fromValuableItemTypeCount = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLength(Long value) {
        this.length = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOffset(Long value) {
        this.offset = value;
    }

    /**
     * Gets the value of the orderType property.
     * 
     * @return
     *     possible object is
     *     {@link ChOrderDirection }
     *     
     */
    public ChOrderDirection getOrderType() {
        return orderType;
    }

    /**
     * Sets the value of the orderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChOrderDirection }
     *     
     */
    public void setOrderType(ChOrderDirection value) {
        this.orderType = value;
    }

    /**
     * Gets the value of the toAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getToAmount() {
        return toAmount;
    }

    /**
     * Sets the value of the toAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setToAmount(BigDecimal value) {
        this.toAmount = value;
    }

    /**
     * Gets the value of the toValuableItemTypeCount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getToValuableItemTypeCount() {
        return toValuableItemTypeCount;
    }

    /**
     * Sets the value of the toValuableItemTypeCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setToValuableItemTypeCount(Long value) {
        this.toValuableItemTypeCount = value;
    }

    /**
     * Gets the value of the valuableItem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValuableItem() {
        return valuableItem;
    }

    /**
     * Sets the value of the valuableItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValuableItem(String value) {
        this.valuableItem = value;
    }

    /**
     * Gets the value of the valuableItemType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValuableItemType() {
        return valuableItemType;
    }

    /**
     * Sets the value of the valuableItemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValuableItemType(String value) {
        this.valuableItemType = value;
    }

}
