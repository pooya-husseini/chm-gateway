
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chBillBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chBillBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="billId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clearingStatus" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillClearingStatus" minOccurs="0"/>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cycleStatus" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillCycleStatus" minOccurs="0"/>
 *         &lt;element name="endCycleDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fees" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chFeeBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="loanNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="preDebitAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="proposalLoan" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chLoanInfoBean" minOccurs="0"/>
 *         &lt;element name="respiteDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="startCycleDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="withdrawableAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chBillBean", propOrder = {
    "amount",
    "billId",
    "clearingStatus",
    "currency",
    "cycleStatus",
    "endCycleDate",
    "fees",
    "loanNumber",
    "payedAmount",
    "preDebitAmount",
    "proposalLoan",
    "respiteDate",
    "startCycleDate",
    "withdrawableAmount"
})
public class ChBillBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigDecimal amount;
    protected String billId;
    @XmlSchemaType(name = "string")
    protected ChBillClearingStatus clearingStatus;
    protected String currency;
    @XmlSchemaType(name = "string")
    protected ChBillCycleStatus cycleStatus;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date endCycleDate;
    @XmlElement(nillable = true)
    protected List<ChFeeBean> fees;
    protected String loanNumber;
    protected BigDecimal payedAmount;
    protected BigDecimal preDebitAmount;
    protected ChLoanInfoBean proposalLoan;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date respiteDate;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date startCycleDate;
    protected BigDecimal withdrawableAmount;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the billId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillId() {
        return billId;
    }

    /**
     * Sets the value of the billId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillId(String value) {
        this.billId = value;
    }

    /**
     * Gets the value of the clearingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillClearingStatus }
     *     
     */
    public ChBillClearingStatus getClearingStatus() {
        return clearingStatus;
    }

    /**
     * Sets the value of the clearingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillClearingStatus }
     *     
     */
    public void setClearingStatus(ChBillClearingStatus value) {
        this.clearingStatus = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the cycleStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillCycleStatus }
     *     
     */
    public ChBillCycleStatus getCycleStatus() {
        return cycleStatus;
    }

    /**
     * Sets the value of the cycleStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillCycleStatus }
     *     
     */
    public void setCycleStatus(ChBillCycleStatus value) {
        this.cycleStatus = value;
    }

    /**
     * Gets the value of the endCycleDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getEndCycleDate() {
        return endCycleDate;
    }

    /**
     * Sets the value of the endCycleDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndCycleDate(Date value) {
        this.endCycleDate = value;
    }

    /**
     * Gets the value of the fees property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fees property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFees().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChFeeBean }
     * 
     * 
     */
    public List<ChFeeBean> getFees() {
        if (fees == null) {
            fees = new ArrayList<ChFeeBean>();
        }
        return this.fees;
    }

    /**
     * Gets the value of the loanNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanNumber() {
        return loanNumber;
    }

    /**
     * Sets the value of the loanNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanNumber(String value) {
        this.loanNumber = value;
    }

    /**
     * Gets the value of the payedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayedAmount() {
        return payedAmount;
    }

    /**
     * Sets the value of the payedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayedAmount(BigDecimal value) {
        this.payedAmount = value;
    }

    /**
     * Gets the value of the preDebitAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPreDebitAmount() {
        return preDebitAmount;
    }

    /**
     * Sets the value of the preDebitAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPreDebitAmount(BigDecimal value) {
        this.preDebitAmount = value;
    }

    /**
     * Gets the value of the proposalLoan property.
     * 
     * @return
     *     possible object is
     *     {@link ChLoanInfoBean }
     *     
     */
    public ChLoanInfoBean getProposalLoan() {
        return proposalLoan;
    }

    /**
     * Sets the value of the proposalLoan property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChLoanInfoBean }
     *     
     */
    public void setProposalLoan(ChLoanInfoBean value) {
        this.proposalLoan = value;
    }

    /**
     * Gets the value of the respiteDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getRespiteDate() {
        return respiteDate;
    }

    /**
     * Sets the value of the respiteDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRespiteDate(Date value) {
        this.respiteDate = value;
    }

    /**
     * Gets the value of the startCycleDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getStartCycleDate() {
        return startCycleDate;
    }

    /**
     * Sets the value of the startCycleDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartCycleDate(Date value) {
        this.startCycleDate = value;
    }

    /**
     * Gets the value of the withdrawableAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWithdrawableAmount() {
        return withdrawableAmount;
    }

    /**
     * Sets the value of the withdrawableAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWithdrawableAmount(BigDecimal value) {
        this.withdrawableAmount = value;
    }

}
