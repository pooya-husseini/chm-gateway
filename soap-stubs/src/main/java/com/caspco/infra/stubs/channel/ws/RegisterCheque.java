
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for registerCheque complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="registerCheque">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chRegisterChequeRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chRegisterChequeRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "registerCheque", propOrder = {
    "chRegisterChequeRequestBean"
})
public class RegisterCheque
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChRegisterChequeRequestBean chRegisterChequeRequestBean;

    /**
     * Gets the value of the chRegisterChequeRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChRegisterChequeRequestBean }
     *     
     */
    public ChRegisterChequeRequestBean getChRegisterChequeRequestBean() {
        return chRegisterChequeRequestBean;
    }

    /**
     * Sets the value of the chRegisterChequeRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChRegisterChequeRequestBean }
     *     
     */
    public void setChRegisterChequeRequestBean(ChRegisterChequeRequestBean value) {
        this.chRegisterChequeRequestBean = value;
    }

}
