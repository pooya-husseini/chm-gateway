
package com.caspco.infra.stubs.channel.ws;

import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter1
    extends XmlAdapter<String, Date>
{


    public Date unmarshal(String value) {
        return (com.caspco.infra.stubs.DateTypeConverter.unmarshal(value));
    }

    public String marshal(Date value) {
        return (com.caspco.infra.stubs.DateTypeConverter.marshal(value));
    }

}
