package com.caspco.channel.interceptor;

import com.caspco.channel.exceptions.BaseExceptionHandler;
import com.caspco.channel.mapper.SecuredMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/11/17
 * Time: 4:02 PM
 */
public abstract class DynamicFilter implements Filter {
    public static final String SHOULD_NEXT = "SHOULD_NEXT";
    private static final Logger LOGGER = Logger.getLogger(DynamicFilter.class);

    @Autowired
    private ObjectMapper objectMapper;
    private ObjectMapper securedObjectMapper = new SecuredMapper();

    @Autowired
    private BaseExceptionHandler exceptionHandler;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        try {
            Object o = createResponse(request);
            LOGGER.debug("Response is " + securedObjectMapper.writeValueAsString(o));

            if (SHOULD_NEXT.equals(o)) {
                chain.doFilter(req, res);
                return;
            }

            response.setCharacterEncoding("utf-8");
            response.setContentType("application/json");
            response.getWriter().write(objectMapper.writeValueAsString(o));
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().flush();
        } catch (IllegalAccessException | InvocationTargetException e) {
            exceptionHandler.logError(e, response);
//            Throwable rootCause = Throwables.getRootCause(e);
//            String message = rootCause.getMessage() == null ? e.getClass().getName() : rootCause.getMessage().replace("com.caspian.moderngateway.core.webservicegateway.exception.WebserviceGatewayFaultException:", "").trim();
//            ErrorResponse errorResponse = new ErrorResponse();
//            errorResponse.setCode("-1");
//            errorResponse.setDescription(message);
//            errorResponse.setMessage(ServiceException.class.getName());
//            errorResponse.setTimestamp(new Date());
//            LOGGER.error("An error occurred in http filter!", e);
//            String exceptionJson = objectMapper.writeValueAsString(errorResponse);
//            response.setContentType("application/json");
//            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            response.getWriter().print(exceptionJson);
//            response.getWriter().flush();
        }
    }

    public abstract Object createResponse(HttpServletRequest request) throws IOException, ServletException, InvocationTargetException, IllegalAccessException;

    @Override
    public void destroy() {

    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }
}
