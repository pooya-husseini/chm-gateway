
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBillField.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chBillField">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DEPOSIT_NUMBER"/>
 *     &lt;enumeration value="BALANCE"/>
 *     &lt;enumeration value="DOCUMENT_NUMBER"/>
 *     &lt;enumeration value="CHEQUE_NUMBER"/>
 *     &lt;enumeration value="DATE"/>
 *     &lt;enumeration value="NOTE"/>
 *     &lt;enumeration value="DEPOSIT_TITLE"/>
 *     &lt;enumeration value="DEBIT"/>
 *     &lt;enumeration value="CREDIT"/>
 *     &lt;enumeration value="TIME"/>
 *     &lt;enumeration value="AGENTBRANCH"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chBillField")
@XmlEnum
public enum ChBillField {

    DEPOSIT_NUMBER,
    BALANCE,
    DOCUMENT_NUMBER,
    CHEQUE_NUMBER,
    DATE,
    NOTE,
    DEPOSIT_TITLE,
    DEBIT,
    CREDIT,
    TIME,
    AGENTBRANCH;

    public String value() {
        return name();
    }

    public static ChBillField fromValue(String v) {
        return valueOf(v);
    }

}
