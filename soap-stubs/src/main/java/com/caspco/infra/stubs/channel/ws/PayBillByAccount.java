
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for payBillByAccount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="payBillByAccount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBillPaymentAccountRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillPaymentAccountRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "payBillByAccount", propOrder = {
    "chBillPaymentAccountRequestBean"
})
public class PayBillByAccount
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBillPaymentAccountRequestBean chBillPaymentAccountRequestBean;

    /**
     * Gets the value of the chBillPaymentAccountRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillPaymentAccountRequestBean }
     *     
     */
    public ChBillPaymentAccountRequestBean getChBillPaymentAccountRequestBean() {
        return chBillPaymentAccountRequestBean;
    }

    /**
     * Sets the value of the chBillPaymentAccountRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillPaymentAccountRequestBean }
     *     
     */
    public void setChBillPaymentAccountRequestBean(ChBillPaymentAccountRequestBean value) {
        this.chBillPaymentAccountRequestBean = value;
    }

}
