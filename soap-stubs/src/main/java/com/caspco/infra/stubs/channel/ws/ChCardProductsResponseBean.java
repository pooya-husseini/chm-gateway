
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCardProductsResponseBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chCardProductsResponseBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardProductCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardProductId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="cardProductTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chCardProductsResponseBean", propOrder = {
    "cardProductCode",
    "cardProductId",
    "cardProductTitle"
})
public class ChCardProductsResponseBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String cardProductCode;
    protected Long cardProductId;
    protected String cardProductTitle;

    /**
     * Gets the value of the cardProductCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardProductCode() {
        return cardProductCode;
    }

    /**
     * Sets the value of the cardProductCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardProductCode(String value) {
        this.cardProductCode = value;
    }

    /**
     * Gets the value of the cardProductId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCardProductId() {
        return cardProductId;
    }

    /**
     * Sets the value of the cardProductId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCardProductId(Long value) {
        this.cardProductId = value;
    }

    /**
     * Gets the value of the cardProductTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardProductTitle() {
        return cardProductTitle;
    }

    /**
     * Sets the value of the cardProductTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardProductTitle(String value) {
        this.cardProductTitle = value;
    }

}
