
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rtgsTransferReport complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rtgsTransferReport">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chRtgTransferSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chRtgTransferSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rtgsTransferReport", propOrder = {
    "chRtgTransferSearchRequestBean"
})
public class RtgsTransferReport
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChRtgTransferSearchRequestBean chRtgTransferSearchRequestBean;

    /**
     * Gets the value of the chRtgTransferSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChRtgTransferSearchRequestBean }
     *     
     */
    public ChRtgTransferSearchRequestBean getChRtgTransferSearchRequestBean() {
        return chRtgTransferSearchRequestBean;
    }

    /**
     * Sets the value of the chRtgTransferSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChRtgTransferSearchRequestBean }
     *     
     */
    public void setChRtgTransferSearchRequestBean(ChRtgTransferSearchRequestBean value) {
        this.chRtgTransferSearchRequestBean = value;
    }

}
