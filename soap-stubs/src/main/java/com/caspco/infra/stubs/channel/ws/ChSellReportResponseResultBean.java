
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chSellReportResponseResultBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chSellReportResponseResultBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="billId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="billType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillType" minOccurs="0"/>
 *         &lt;element name="customerRefNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="mainTransactionId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="paymentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="time" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="txState" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSellTransactionState" minOccurs="0"/>
 *         &lt;element name="txType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSellTransactionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chSellReportResponseResultBean", propOrder = {
    "amount",
    "billId",
    "billType",
    "customerRefNum",
    "id",
    "mainTransactionId",
    "paymentId",
    "refNum",
    "resNum",
    "time",
    "txState",
    "txType"
})
public class ChSellReportResponseResultBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigDecimal amount;
    protected String billId;
    @XmlSchemaType(name = "string")
    protected ChBillType billType;
    protected String customerRefNum;
    protected Long id;
    protected Long mainTransactionId;
    protected String paymentId;
    protected String refNum;
    protected String resNum;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date time;
    @XmlSchemaType(name = "string")
    protected ChSellTransactionState txState;
    @XmlSchemaType(name = "string")
    protected ChSellTransactionType txType;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the billId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillId() {
        return billId;
    }

    /**
     * Sets the value of the billId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillId(String value) {
        this.billId = value;
    }

    /**
     * Gets the value of the billType property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillType }
     *     
     */
    public ChBillType getBillType() {
        return billType;
    }

    /**
     * Sets the value of the billType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillType }
     *     
     */
    public void setBillType(ChBillType value) {
        this.billType = value;
    }

    /**
     * Gets the value of the customerRefNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRefNum() {
        return customerRefNum;
    }

    /**
     * Sets the value of the customerRefNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRefNum(String value) {
        this.customerRefNum = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Gets the value of the mainTransactionId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMainTransactionId() {
        return mainTransactionId;
    }

    /**
     * Sets the value of the mainTransactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMainTransactionId(Long value) {
        this.mainTransactionId = value;
    }

    /**
     * Gets the value of the paymentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * Sets the value of the paymentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentId(String value) {
        this.paymentId = value;
    }

    /**
     * Gets the value of the refNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefNum() {
        return refNum;
    }

    /**
     * Sets the value of the refNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefNum(String value) {
        this.refNum = value;
    }

    /**
     * Gets the value of the resNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResNum() {
        return resNum;
    }

    /**
     * Sets the value of the resNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResNum(String value) {
        this.resNum = value;
    }

    /**
     * Gets the value of the time property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getTime() {
        return time;
    }

    /**
     * Sets the value of the time property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTime(Date value) {
        this.time = value;
    }

    /**
     * Gets the value of the txState property.
     * 
     * @return
     *     possible object is
     *     {@link ChSellTransactionState }
     *     
     */
    public ChSellTransactionState getTxState() {
        return txState;
    }

    /**
     * Sets the value of the txState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSellTransactionState }
     *     
     */
    public void setTxState(ChSellTransactionState value) {
        this.txState = value;
    }

    /**
     * Gets the value of the txType property.
     * 
     * @return
     *     possible object is
     *     {@link ChSellTransactionType }
     *     
     */
    public ChSellTransactionType getTxType() {
        return txType;
    }

    /**
     * Sets the value of the txType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSellTransactionType }
     *     
     */
    public void setTxType(ChSellTransactionType value) {
        this.txType = value;
    }

}
