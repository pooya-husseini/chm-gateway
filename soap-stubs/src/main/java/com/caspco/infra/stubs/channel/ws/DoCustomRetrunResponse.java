
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for doCustomRetrunResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="doCustomRetrunResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCustomReturnResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCustomReturnResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doCustomRetrunResponse", propOrder = {
    "chCustomReturnResponseBean"
})
public class DoCustomRetrunResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCustomReturnResponseBean chCustomReturnResponseBean;

    /**
     * Gets the value of the chCustomReturnResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCustomReturnResponseBean }
     *     
     */
    public ChCustomReturnResponseBean getChCustomReturnResponseBean() {
        return chCustomReturnResponseBean;
    }

    /**
     * Sets the value of the chCustomReturnResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCustomReturnResponseBean }
     *     
     */
    public void setChCustomReturnResponseBean(ChCustomReturnResponseBean value) {
        this.chCustomReturnResponseBean = value;
    }

}
