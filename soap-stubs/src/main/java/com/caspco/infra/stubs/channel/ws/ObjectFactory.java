
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.caspco.infra.stubs.channel.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InquiryNaturalPersonOpenSingleDepositResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "inquiryNaturalPersonOpenSingleDepositResponse");
    private final static QName _RtgsTransferReportResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "rtgsTransferReportResponse");
    private final static QName _GetLoanDetailResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getLoanDetailResponse");
    private final static QName _ChTopupChargeRequest_QNAME = new QName("bean", "chTopupChargeRequest");
    private final static QName _ChVerifyTransactionRequestBean_QNAME = new QName("bean", "chVerifyTransactionRequestBean");
    private final static QName _CardCorrectLinkedAccounts_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardCorrectLinkedAccounts");
    private final static QName _CustomerExistenceInquiry_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "customerExistenceInquiry");
    private final static QName _AutoTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "autoTransferResponse");
    private final static QName _GetBillInfoResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBillInfoResponse");
    private final static QName _AddCardDepositResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "addCardDepositResponse");
    private final static QName _ChOpenDepositRequestBean_QNAME = new QName("bean", "chOpenDepositRequestBean");
    private final static QName _GetCurrencyRate_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCurrencyRate");
    private final static QName _GetChequeBookList_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getChequeBookList");
    private final static QName _ConvertDepositNumberToIban_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "convertDepositNumberToIban");
    private final static QName _PayBill_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "payBill");
    private final static QName _ChBillPaymentRequestBean_QNAME = new QName("bean", "chBillPaymentRequestBean");
    private final static QName _BatchTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "batchTransfer");
    private final static QName _ChCardDepositRequestBean_QNAME = new QName("bean", "chCardDepositRequestBean");
    private final static QName _LogoutResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "logoutResponse");
    private final static QName _ChangePasswordResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "changePasswordResponse");
    private final static QName _GetCardLimitations_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardLimitations");
    private final static QName _BlockCheque_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "blockCheque");
    private final static QName _ChSearchTransferChequeRequestBean_QNAME = new QName("bean", "chSearchTransferChequeRequestBean");
    private final static QName _ChTransferConstraintRequestBean_QNAME = new QName("bean", "chTransferConstraintRequestBean");
    private final static QName _DefineCustomerSignatureSampleResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "defineCustomerSignatureSampleResponse");
    private final static QName _PayBillResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "payBillResponse");
    private final static QName _AchTransferReport_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "achTransferReport");
    private final static QName _GetCardProducts_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardProducts");
    private final static QName _AchAutoTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "achAutoTransferResponse");
    private final static QName _ChChequeBookRequestBean_QNAME = new QName("bean", "chChequeBookRequestBean");
    private final static QName _AddPeriodicBalanceRequest_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "addPeriodicBalanceRequest");
    private final static QName _RtgsTransferDetailReportResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "rtgsTransferDetailReportResponse");
    private final static QName _RegisterCheque_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "registerCheque");
    private final static QName _GetCardStatementInquiry_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardStatementInquiry");
    private final static QName _GetFavoriteDepositsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getFavoriteDepositsResponse");
    private final static QName _CardBalanceInquiry_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardBalanceInquiry");
    private final static QName _ChAutoTransferRequestBean_QNAME = new QName("bean", "chAutoTransferRequestBean");
    private final static QName _ChSignRequestBean_QNAME = new QName("bean", "chSignRequestBean");
    private final static QName _BatchTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "batchTransferResponse");
    private final static QName _CardIssuingAllowedDepositsToOpenResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardIssuingAllowedDepositsToOpenResponse");
    private final static QName _GetTransferConstraintInfoResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getTransferConstraintInfoResponse");
    private final static QName _LoadCoreParameters_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "loadCoreParameters");
    private final static QName _GetEcho_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getEcho");
    private final static QName _GetOwnerDepositNameResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getOwnerDepositNameResponse");
    private final static QName _GetTransferChequeList_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getTransferChequeList");
    private final static QName _ChangePassword_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "changePassword");
    private final static QName _AchTransactionReportResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "achTransactionReportResponse");
    private final static QName _GetAtmStatus_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getAtmStatus");
    private final static QName _GetCardDeposits_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardDeposits");
    private final static QName _GetSellReportResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getSellReportResponse");
    private final static QName _AchBatchTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "achBatchTransferResponse");
    private final static QName _AddCardDeposit_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "addCardDeposit");
    private final static QName _BlockChequeResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "blockChequeResponse");
    private final static QName _GetServiceConstraintListResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getServiceConstraintListResponse");
    private final static QName _GetCards_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCards");
    private final static QName _GetDepositRates_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getDepositRates");
    private final static QName _TopupChargeFromVirtualTerminalResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "topupChargeFromVirtualTerminalResponse");
    private final static QName _ChSignedDepositRequestBean_QNAME = new QName("bean", "chSignedDepositRequestBean");
    private final static QName _GetOwnerDepositName_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getOwnerDepositName");
    private final static QName _GetTransferChequeListResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getTransferChequeListResponse");
    private final static QName _GetPaymentPreview_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getPaymentPreview");
    private final static QName _GetRequestStatusResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getRequestStatusResponse");
    private final static QName _SendReversal_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "sendReversal");
    private final static QName _GetStatement_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getStatement");
    private final static QName _ChAchTransferSummeryFilterBean_QNAME = new QName("bean", "chAchTransferSummeryFilterBean");
    private final static QName _ChargeCard_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "chargeCard");
    private final static QName _IbanEnquiryResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "ibanEnquiryResponse");
    private final static QName _ChangeUsername_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "changeUsername");
    private final static QName _ChDepositNumberToIbanRequestBean_QNAME = new QName("bean", "chDepositNumberToIbanRequestBean");
    private final static QName _ChAutoAchTransferRequestBean_QNAME = new QName("bean", "chAutoAchTransferRequestBean");
    private final static QName _GetChequeBookListResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getChequeBookListResponse");
    private final static QName _ResumeAchTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "resumeAchTransferResponse");
    private final static QName _ChSMSSettingRequestBean_QNAME = new QName("bean", "chSMSSettingRequestBean");
    private final static QName _UpdateUserBillSettingResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "updateUserBillSettingResponse");
    private final static QName _AchAutoTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "achAutoTransfer");
    private final static QName _ChDisableTransferRequestBean_QNAME = new QName("bean", "chDisableTransferRequestBean");
    private final static QName _HotCard_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "hotCard");
    private final static QName _PayCreditBill_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "payCreditBill");
    private final static QName _ChBaseSearchChequeBookRequestBean_QNAME = new QName("bean", "chBaseSearchChequeBookRequestBean");
    private final static QName _DefineCustomerSignatureSample_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "defineCustomerSignatureSample");
    private final static QName _GetDepositCustomerResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getDepositCustomerResponse");
    private final static QName _GetCreditBillTransactionsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCreditBillTransactionsResponse");
    private final static QName _PayLoanResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "payLoanResponse");
    private final static QName _SaveOrUpdateSecondPassword_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "saveOrUpdateSecondPassword");
    private final static QName _GetCreditDossiers_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCreditDossiers");
    private final static QName _CardPurchase_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardPurchase");
    private final static QName _CardSetPasswordResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardSetPasswordResponse");
    private final static QName _GetCurrencyRateResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCurrencyRateResponse");
    private final static QName _ChBillTransactionSearchRequestBean_QNAME = new QName("bean", "chBillTransactionSearchRequestBean");
    private final static QName _ChDepositSearchRequestBean_QNAME = new QName("bean", "chDepositSearchRequestBean");
    private final static QName _ChRtgsNormalTransferRequestBean_QNAME = new QName("bean", "chRtgsNormalTransferRequestBean");
    private final static QName _GetBeOpenedDepositsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBeOpenedDepositsResponse");
    private final static QName _GetPaymentServicesResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getPaymentServicesResponse");
    private final static QName _ChCustomerAddressRequestBean_QNAME = new QName("bean", "chCustomerAddressRequestBean");
    private final static QName _BlockDepositAmount_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "blockDepositAmount");
    private final static QName _GetDepositCustomer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getDepositCustomer");
    private final static QName _RtgsNormalTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "rtgsNormalTransferResponse");
    private final static QName _ChAchTransferSearchRequestBean_QNAME = new QName("bean", "chAchTransferSearchRequestBean");
    private final static QName _GetUserInfoResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getUserInfoResponse");
    private final static QName _CancelAchTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cancelAchTransfer");
    private final static QName _ResumeAchTransaction_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "resumeAchTransaction");
    private final static QName _GetAutoTransferList_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getAutoTransferList");
    private final static QName _GetBatchBillInfoResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBatchBillInfoResponse");
    private final static QName _GetFavoriteAccountSetting_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getFavoriteAccountSetting");
    private final static QName _GetRequestStatus_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getRequestStatus");
    private final static QName _CardExtend_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardExtend");
    private final static QName _ResumeAchTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "resumeAchTransfer");
    private final static QName _AccountOwner_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "AccountOwner");
    private final static QName _GetTransferConstraintInfoPerService_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getTransferConstraintInfoPerService");
    private final static QName _SwitchFundTransferFromPhysicalTerminal_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "switchFundTransferFromPhysicalTerminal");
    private final static QName _ChLoanPaymentRequestBean_QNAME = new QName("bean", "chLoanPaymentRequestBean");
    private final static QName _GetPaymentServicesByTypeResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getPaymentServicesByTypeResponse");
    private final static QName _ChCreditBillPaymentRequestBean_QNAME = new QName("bean", "chCreditBillPaymentRequestBean");
    private final static QName _GetSellDetailReport_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getSellDetailReport");
    private final static QName _ChVirtualCardTransferRequest_QNAME = new QName("bean", "chVirtualCardTransferRequest");
    private final static QName _LoginWithMobileNumber_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "loginWithMobileNumber");
    private final static QName _AutoTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "autoTransfer");
    private final static QName _ChBatchBillPaymentRequestBean_QNAME = new QName("bean", "chBatchBillPaymentRequestBean");
    private final static QName _ResumeAchTransactionResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "resumeAchTransactionResponse");
    private final static QName _Login_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "login");
    private final static QName _RegisterChequeBookRequest_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "registerChequeBookRequest");
    private final static QName _DoPayment_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "doPayment");
    private final static QName _ChSearchCreditDossierRequestBean_QNAME = new QName("bean", "chSearchCreditDossierRequestBean");
    private final static QName _CashChequeResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cashChequeResponse");
    private final static QName _CashCheque_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cashCheque");
    private final static QName _GetCardProductsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardProductsResponse");
    private final static QName _ChBillSettingRequestBean_QNAME = new QName("bean", "chBillSettingRequestBean");
    private final static QName _ChUserSMSSettingRequestBean_QNAME = new QName("bean", "chUserSMSSettingRequestBean");
    private final static QName _ReportAchTransferSummaryResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "reportAchTransferSummaryResponse");
    private final static QName _ChFavoriteDepositNumberRequestBeans_QNAME = new QName("bean", "chFavoriteDepositNumberRequestBeans");
    private final static QName _ChMerchantInfoSearchRequestBean_QNAME = new QName("bean", "chMerchantInfoSearchRequestBean");
    private final static QName _DoPaymentResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "doPaymentResponse");
    private final static QName _ConvertIbanToDepositNumber_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "convertIbanToDepositNumber");
    private final static QName _GetUserInfo_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getUserInfo");
    private final static QName _ChDepositCustomerRequestBean_QNAME = new QName("bean", "chDepositCustomerRequestBean");
    private final static QName _ChRemoveCardDepositRequestBean_QNAME = new QName("bean", "chRemoveCardDepositRequestBean");
    private final static QName _ChChangePasswordRequestBean_QNAME = new QName("bean", "chChangePasswordRequestBean");
    private final static QName _ChAchTransactionKeysRequestBean_QNAME = new QName("bean", "chAchTransactionKeysRequestBean");
    private final static QName _GetUserSMSSetting_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getUserSMSSetting");
    private final static QName _ChStatementSearchRequestBean_QNAME = new QName("bean", "chStatementSearchRequestBean");
    private final static QName _ChSellDetailRequestBean_QNAME = new QName("bean", "chSellDetailRequestBean");
    private final static QName _GetGuarantyListResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getGuarantyListResponse");
    private final static QName _CancelAchTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cancelAchTransferResponse");
    private final static QName _AcceptAchTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "acceptAchTransferResponse");
    private final static QName _ChChargeCardRequestBean_QNAME = new QName("bean", "chChargeCardRequestBean");
    private final static QName _GetCustomerSignaturesResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCustomerSignaturesResponse");
    private final static QName _GetSellDetailReportResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getSellDetailReportResponse");
    private final static QName _ChDepositRateInquiryRequestBean_QNAME = new QName("bean", "chDepositRateInquiryRequestBean");
    private final static QName _ChMobileLoginRequestBean_QNAME = new QName("bean", "chMobileLoginRequestBean");
    private final static QName _GetSellReport_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getSellReport");
    private final static QName _Logout_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "logout");
    private final static QName _GetBranches_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBranches");
    private final static QName _AchTransactionReport_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "achTransactionReport");
    private final static QName _CardIssuingAllowedDepositsToOpen_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardIssuingAllowedDepositsToOpen");
    private final static QName _GetDeposits_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getDeposits");
    private final static QName _SuspendAchTransactionResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "suspendAchTransactionResponse");
    private final static QName _HotCardByPin_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "hotCardByPin");
    private final static QName _ChBaseCreditBillSearchRequestBean_QNAME = new QName("bean", "chBaseCreditBillSearchRequestBean");
    private final static QName _RegisterChequeResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "registerChequeResponse");
    private final static QName _GetSignedDepositsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getSignedDepositsResponse");
    private final static QName _ChHotCardByPinRequestBean_QNAME = new QName("bean", "chHotCardByPinRequestBean");
    private final static QName _ChAddCardDepositRequestBean_QNAME = new QName("bean", "chAddCardDepositRequestBean");
    private final static QName _ChLoanDetailSearchRequestBean_QNAME = new QName("bean", "chLoanDetailSearchRequestBean");
    private final static QName _CardIssuing_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardIssuing");
    private final static QName _ChFundTransferRequestBean_QNAME = new QName("bean", "chFundTransferRequestBean");
    private final static QName _RemoveCardDepositResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "removeCardDepositResponse");
    private final static QName _GetLoans_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getLoans");
    private final static QName _GetCreditBillsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCreditBillsResponse");
    private final static QName _RemoveCardDeposit_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "removeCardDeposit");
    private final static QName _GetPaymentBillStatementResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getPaymentBillStatementResponse");
    private final static QName _SendMail_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "sendMail");
    private final static QName _ChSecondPasswordRequestBean_QNAME = new QName("bean", "chSecondPasswordRequestBean");
    private final static QName _GetCardOwner_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardOwner");
    private final static QName _GetPaymentPreviewResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getPaymentPreviewResponse");
    private final static QName _ChNormalTransferRequestBean_QNAME = new QName("bean", "chNormalTransferRequestBean");
    private final static QName _HotCardByPinResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "hotCardByPinResponse");
    private final static QName _GetBranchesResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBranchesResponse");
    private final static QName _CardChangePin_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardChangePin");
    private final static QName _GetBranchAllowedDepositsToOpenResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBranchAllowedDepositsToOpenResponse");
    private final static QName _ChBankSearchRequestBean_QNAME = new QName("bean", "chBankSearchRequestBean");
    private final static QName _ChRegisterChequeRequestBean_QNAME = new QName("bean", "chRegisterChequeRequestBean");
    private final static QName _ChPaymentBillStatementRequestBean_QNAME = new QName("bean", "chPaymentBillStatementRequestBean");
    private final static QName _UpdateDefaultContactInfoResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "updateDefaultContactInfoResponse");
    private final static QName _GetServiceConstraintList_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getServiceConstraintList");
    private final static QName _GetDestinationCardOwnerResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getDestinationCardOwnerResponse");
    private final static QName _ChCardPurchaseRequestBean_QNAME = new QName("bean", "chCardPurchaseRequestBean");
    private final static QName _ConvertIbanToDepositNumberResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "convertIbanToDepositNumberResponse");
    private final static QName _SearchThirdPartyResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "searchThirdPartyResponse");
    private final static QName _ChCardsSearchRequestBean_QNAME = new QName("bean", "chCardsSearchRequestBean");
    private final static QName _PayBillByAccount_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "payBillByAccount");
    private final static QName _ChangeUsernameResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "changeUsernameResponse");
    private final static QName _ConvertDepositNumberToIbanResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "convertDepositNumberToIbanResponse");
    private final static QName _GetTransferConstraintInfo_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getTransferConstraintInfo");
    private final static QName _ChBatchBillInfoSearchRequestBean_QNAME = new QName("bean", "chBatchBillInfoSearchRequestBean");
    private final static QName _ChAchNormalTransferRequestBean_QNAME = new QName("bean", "chAchNormalTransferRequestBean");
    private final static QName _LoadCoreParametersResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "loadCoreParametersResponse");
    private final static QName _ChBillPaymentAccountRequestBean_QNAME = new QName("bean", "chBillPaymentAccountRequestBean");
    private final static QName _GetDepositOwnerName_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getDepositOwnerName");
    private final static QName _GetInstitutions_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getInstitutions");
    private final static QName _ChSearchAutoTransferRequestBean_QNAME = new QName("bean", "chSearchAutoTransferRequestBean");
    private final static QName _CardSetPassword_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardSetPassword");
    private final static QName _CardInfoRetrievation_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardInfoRetrievation");
    private final static QName _AddPeriodicBillRequestResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "addPeriodicBillRequestResponse");
    private final static QName _ChCurrencyRateRequestBean_QNAME = new QName("bean", "chCurrencyRateRequestBean");
    private final static QName _ChIBANEnquiryRequest_QNAME = new QName("bean", "chIBANEnquiryRequest");
    private final static QName _ChInstitutionRequestBean_QNAME = new QName("bean", "chInstitutionRequestBean");
    private final static QName _ChAchTransactionSearchRequestBean_QNAME = new QName("bean", "chAchTransactionSearchRequestBean");
    private final static QName _WebserviceGatewayFaultException_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "WebserviceGatewayFaultException");
    private final static QName _GetCardTransactionsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardTransactionsResponse");
    private final static QName _ReportAchTransferSummary_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "reportAchTransferSummary");
    private final static QName _ChLoansSearchRequestBean_QNAME = new QName("bean", "chLoansSearchRequestBean");
    private final static QName _ChChangeUsernameRequestBean_QNAME = new QName("bean", "chChangeUsernameRequestBean");
    private final static QName _PayCreditBillResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "payCreditBillResponse");
    private final static QName _AchNormalTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "achNormalTransferResponse");
    private final static QName _AddPeriodicBalanceRequestResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "addPeriodicBalanceRequestResponse");
    private final static QName _GetAtmStatusResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getAtmStatusResponse");
    private final static QName _CardIssuingResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardIssuingResponse");
    private final static QName _DeactivateModernGatewayUserResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "deactivateModernGatewayUserResponse");
    private final static QName _SendMailResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "sendMailResponse");
    private final static QName _ChPhysicalCardTransferRequest_QNAME = new QName("bean", "chPhysicalCardTransferRequest");
    private final static QName _GetCheque_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCheque");
    private final static QName _CardTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardTransfer");
    private final static QName _ChCardBalanceRequestBean_QNAME = new QName("bean", "chCardBalanceRequestBean");
    private final static QName _ChThirdPartySearchRequestBean_QNAME = new QName("bean", "chThirdPartySearchRequestBean");
    private final static QName _GetMerchantInfo_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getMerchantInfo");
    private final static QName _ChResumeAchTransferRequestBean_QNAME = new QName("bean", "chResumeAchTransferRequestBean");
    private final static QName _GetCardOwnerResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardOwnerResponse");
    private final static QName _SuspendAchTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "suspendAchTransfer");
    private final static QName _GetCardLimitationsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardLimitationsResponse");
    private final static QName _GetCustomerInfoListByModifyDateResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCustomerInfoListByModifyDateResponse");
    private final static QName _ChCardLimitRequestBean_QNAME = new QName("bean", "chCardLimitRequestBean");
    private final static QName _GetUserBillSetting_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getUserBillSetting");
    private final static QName _GetCustomerSignatures_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCustomerSignatures");
    private final static QName _GetStatementResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getStatementResponse");
    private final static QName _GetBranchBalance_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBranchBalance");
    private final static QName _DeactivateModernGatewayUser_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "deactivateModernGatewayUser");
    private final static QName _UpdateUserBillSetting_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "updateUserBillSetting");
    private final static QName _DoCustomRetrun_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "doCustomRetrun");
    private final static QName _CardExtendResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardExtendResponse");
    private final static QName _SearchThirdParty_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "searchThirdParty");
    private final static QName _GetBeOpenedDeposits_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBeOpenedDeposits");
    private final static QName _BlockDepositAmountResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "blockDepositAmountResponse");
    private final static QName _GetPaymentServices_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getPaymentServices");
    private final static QName _ChChequeSearchRequestBean_QNAME = new QName("bean", "chChequeSearchRequestBean");
    private final static QName _ChPeriodicRequestBean_QNAME = new QName("bean", "chPeriodicRequestBean");
    private final static QName _GetBranchAllowedDepositsToOpen_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBranchAllowedDepositsToOpen");
    private final static QName _GetCreditBillTransactions_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCreditBillTransactions");
    private final static QName _TopupChargeFromVirtualTerminal_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "topupChargeFromVirtualTerminal");
    private final static QName _GetDepositRatesResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getDepositRatesResponse");
    private final static QName _HotCardResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "hotCardResponse");
    private final static QName _InquiryCardIssuing_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "inquiryCardIssuing");
    private final static QName _NormalTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "normalTransfer");
    private final static QName _ChUserRequestBean_QNAME = new QName("bean", "chUserRequestBean");
    private final static QName _InstitutionalTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "institutionalTransfer");
    private final static QName _VerifyTransactionResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "verifyTransactionResponse");
    private final static QName _ChAtmStatusRequestBean_QNAME = new QName("bean", "ChAtmStatusRequestBean");
    private final static QName _CardPurchaseResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardPurchaseResponse");
    private final static QName _AchNormalTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "achNormalTransfer");
    private final static QName _GetCustomerInfo_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCustomerInfo");
    private final static QName _PayLoan_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "payLoan");
    private final static QName _RtgsNormalTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "rtgsNormalTransfer");
    private final static QName _PayBatchBill_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "payBatchBill");
    private final static QName _DisableTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "disableTransferResponse");
    private final static QName _UpdateDefaultContactInfo_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "updateDefaultContactInfo");
    private final static QName _GetUserBillSettingResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getUserBillSettingResponse");
    private final static QName _CancelAchTransactionResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cancelAchTransactionResponse");
    private final static QName _CreateNaturalCustomer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "createNaturalCustomer");
    private final static QName _NaturalPersonOpenSingleDeposit_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "naturalPersonOpenSingleDeposit");
    private final static QName _ChBillInfoSearchRequestBean_QNAME = new QName("bean", "chBillInfoSearchRequestBean");
    private final static QName _GetChequeResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getChequeResponse");
    private final static QName _GetCreditBills_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCreditBills");
    private final static QName _RtgsTransferDetailReport_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "rtgsTransferDetailReport");
    private final static QName _ChOwnerDepositRequestBean_QNAME = new QName("bean", "chOwnerDepositRequestBean");
    private final static QName _CustomerExistenceInquiryResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "customerExistenceInquiryResponse");
    private final static QName _CardCorrectLinkedAccountsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardCorrectLinkedAccountsResponse");
    private final static QName _InquiryCardIssuingResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "inquiryCardIssuingResponse");
    private final static QName _ChCardStatementRequestBean_QNAME = new QName("bean", "chCardStatementRequestBean");
    private final static QName _ChAcceptAchTransferRequestBean_QNAME = new QName("bean", "chAcceptAchTransferRequestBean");
    private final static QName _LoginResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "loginResponse");
    private final static QName _OpenDepositResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "openDepositResponse");
    private final static QName _ChBranchSearchRequestBean_QNAME = new QName("bean", "chBranchSearchRequestBean");
    private final static QName _GetLoanDetail_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getLoanDetail");
    private final static QName _SwitchFundTransferFromPhysicalTerminalResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "switchFundTransferFromPhysicalTerminalResponse");
    private final static QName _SwitchFundTransferFromVirtualTerminal_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "switchFundTransferFromVirtualTerminal");
    private final static QName _ChCardTransactionsRequestBean_QNAME = new QName("bean", "chCardTransactionsRequestBean");
    private final static QName _GetFavoriteDeposits_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getFavoriteDeposits");
    private final static QName _GetInstitutionsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getInstitutionsResponse");
    private final static QName _PayBillByAccountResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "payBillByAccountResponse");
    private final static QName _ChCardChangePinRequestBean_QNAME = new QName("bean", "chCardChangePinRequestBean");
    private final static QName _SignRequestDetailsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "signRequestDetailsResponse");
    private final static QName _ChIbanToDepositRequestBean_QNAME = new QName("bean", "chIbanToDepositRequestBean");
    private final static QName _ChMailRequestBean_QNAME = new QName("bean", "chMailRequestBean");
    private final static QName _ChCashChequeRequestBean_QNAME = new QName("bean", "chCashChequeRequestBean");
    private final static QName _ExtendSMSSettingResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "extendSMSSettingResponse");
    private final static QName _GetSignedDeposits_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getSignedDeposits");
    private final static QName _GetDestinationCardOwner_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getDestinationCardOwner");
    private final static QName _CancelAchTransaction_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cancelAchTransaction");
    private final static QName _SuspendAchTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "suspendAchTransferResponse");
    private final static QName _GetTransferConstraintInfoPerServiceResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getTransferConstraintInfoPerServiceResponse");
    private final static QName _ChInstitutionalTransferRequestBean_QNAME = new QName("bean", "chInstitutionalTransferRequestBean");
    private final static QName _ChPanRequestBean_QNAME = new QName("bean", "chPanRequestBean");
    private final static QName _SuspendAchTransaction_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "suspendAchTransaction");
    private final static QName _AchTransferReportResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "achTransferReportResponse");
    private final static QName _ChRtgTransferDetailSearchRequestBean_QNAME = new QName("bean", "chRtgTransferDetailSearchRequestBean");
    private final static QName _GetBatchBillInfo_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBatchBillInfo");
    private final static QName _GetEchoResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getEchoResponse");
    private final static QName _GetCustomerInfoListByModifyDate_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCustomerInfoListByModifyDate");
    private final static QName _GetMerchantInfoResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getMerchantInfoResponse");
    private final static QName _RegisterChequeBookRequestResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "registerChequeBookRequestResponse");
    private final static QName _ChAchTransferKeyRequestBean_QNAME = new QName("bean", "chAchTransferKeyRequestBean");
    private final static QName _ChCustomReturnRequestBean_QNAME = new QName("bean", "chCustomReturnRequestBean");
    private final static QName _GetBanksResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBanksResponse");
    private final static QName _NormalTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "normalTransferResponse");
    private final static QName _InquiryNaturalPersonOpenSingleDeposit_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "inquiryNaturalPersonOpenSingleDeposit");
    private final static QName _UpdateFavoriteAccountSetting_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "updateFavoriteAccountSetting");
    private final static QName _RtgsTransferReport_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "rtgsTransferReport");
    private final static QName _SignRequestDetails_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "signRequestDetails");
    private final static QName _ChSellReportRequestBean_QNAME = new QName("bean", "chSellReportRequestBean");
    private final static QName _GetCustomerAddress_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCustomerAddress");
    private final static QName _GetGuarantyList_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getGuarantyList");
    private final static QName _ChBatchTransferRequestBean_QNAME = new QName("bean", "chBatchTransferRequestBean");
    private final static QName _ChReqStatusRequestBean_QNAME = new QName("bean", "chReqStatusRequestBean");
    private final static QName _GetDefaultBillStatement_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getDefaultBillStatement");
    private final static QName _InstitutionalTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "institutionalTransferResponse");
    private final static QName _GetDefaultBillStatementResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getDefaultBillStatementResponse");
    private final static QName _CreateNaturalCustomerResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "createNaturalCustomerResponse");
    private final static QName _DisableTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "disableTransfer");
    private final static QName _GetCardTransactions_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardTransactions");
    private final static QName _GetBranchBalanceResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBranchBalanceResponse");
    private final static QName _SaveOrUpdateSecondPasswordResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "saveOrUpdateSecondPasswordResponse");
    private final static QName _CardInfoRetrievationResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardInfoRetrievationResponse");
    private final static QName _ChBeOpenedDepositRequestBean_QNAME = new QName("bean", "chBeOpenedDepositRequestBean");
    private final static QName _AddPeriodicBillRequest_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "addPeriodicBillRequest");
    private final static QName _ChDefaultStatementRequestBean_QNAME = new QName("bean", "chDefaultStatementRequestBean");
    private final static QName _ChPaymentPreviewRequestBean_QNAME = new QName("bean", "chPaymentPreviewRequestBean");
    private final static QName _IbanEnquiry_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "ibanEnquiry");
    private final static QName _UpdateFavoriteAccountSettingResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "updateFavoriteAccountSettingResponse");
    private final static QName _ChRtgTransferSearchRequestBean_QNAME = new QName("bean", "chRtgTransferSearchRequestBean");
    private final static QName _GetBillInfo_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBillInfo");
    private final static QName _LoginWithMobileNumberResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "loginWithMobileNumberResponse");
    private final static QName _NaturalPersonOpenSingleDepositResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "naturalPersonOpenSingleDepositResponse");
    private final static QName _OpenDeposit_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "openDeposit");
    private final static QName _SendReversalResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "sendReversalResponse");
    private final static QName _ChBlockChequeRequestBean_QNAME = new QName("bean", "chBlockChequeRequestBean");
    private final static QName _GetLoansResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getLoansResponse");
    private final static QName _SwitchFundTransferFromVirtualTerminalResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "switchFundTransferFromVirtualTerminalResponse");
    private final static QName _GetCustomerAddressResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCustomerAddressResponse");
    private final static QName _VerifyTransaction_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "verifyTransaction");
    private final static QName _AchBatchTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "achBatchTransfer");
    private final static QName _GetCardDepositsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardDepositsResponse");
    private final static QName _ChGuarantiesRequestBean_QNAME = new QName("bean", "chGuarantiesRequestBean");
    private final static QName _AcceptAchTransfer_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "acceptAchTransfer");
    private final static QName _GetCardStatementInquiryResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardStatementInquiryResponse");
    private final static QName _GetFavoriteAccountSettingResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getFavoriteAccountSettingResponse");
    private final static QName _GetDepositsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getDepositsResponse");
    private final static QName _GetDepositOwnerNameResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getDepositOwnerNameResponse");
    private final static QName _GetPaymentServicesByType_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getPaymentServicesByType");
    private final static QName _ChCardOwnerRequestBean_QNAME = new QName("bean", "chCardOwnerRequestBean");
    private final static QName _GetCustomerInfoResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCustomerInfoResponse");
    private final static QName _CardBalanceInquiryResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardBalanceInquiryResponse");
    private final static QName _DoCustomRetrunResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "doCustomRetrunResponse");
    private final static QName _ChDepositOwnerRequestBean_QNAME = new QName("bean", "chDepositOwnerRequestBean");
    private final static QName _CardTransferResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardTransferResponse");
    private final static QName _ExtendSMSSetting_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "extendSMSSetting");
    private final static QName _GetBanks_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getBanks");
    private final static QName _GetPaymentBillStatement_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getPaymentBillStatement");
    private final static QName _ChargeCardResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "chargeCardResponse");
    private final static QName _MenuDto_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "MenuDto");
    private final static QName _GetCardsResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCardsResponse");
    private final static QName _GetUserSMSSettingResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getUserSMSSettingResponse");
    private final static QName _ChBranchBalanceSearchBean_QNAME = new QName("bean", "chBranchBalanceSearchBean");
    private final static QName _MenuEntryDto_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "MenuEntryDto");
    private final static QName _GetAutoTransferListResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getAutoTransferListResponse");
    private final static QName _ChPaymentRequestBean_QNAME = new QName("bean", "chPaymentRequestBean");
    private final static QName _CardChangePinResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "cardChangePinResponse");
    private final static QName _GetCreditDossiersResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "getCreditDossiersResponse");
    private final static QName _ChDestinationCardOwnerRequest_QNAME = new QName("bean", "chDestinationCardOwnerRequest");
    private final static QName _PayBatchBillResponse_QNAME = new QName("http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", "payBatchBillResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.caspco.infra.stubs.channel.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetDepositOwnerNameResponse }
     * 
     */
    public GetDepositOwnerNameResponse createGetDepositOwnerNameResponse() {
        return new GetDepositOwnerNameResponse();
    }

    /**
     * Create an instance of {@link GetDepositsResponse }
     * 
     */
    public GetDepositsResponse createGetDepositsResponse() {
        return new GetDepositsResponse();
    }

    /**
     * Create an instance of {@link AcceptAchTransfer }
     * 
     */
    public AcceptAchTransfer createAcceptAchTransfer() {
        return new AcceptAchTransfer();
    }

    /**
     * Create an instance of {@link GetCardStatementInquiryResponse }
     * 
     */
    public GetCardStatementInquiryResponse createGetCardStatementInquiryResponse() {
        return new GetCardStatementInquiryResponse();
    }

    /**
     * Create an instance of {@link GetFavoriteAccountSettingResponse }
     * 
     */
    public GetFavoriteAccountSettingResponse createGetFavoriteAccountSettingResponse() {
        return new GetFavoriteAccountSettingResponse();
    }

    /**
     * Create an instance of {@link AchBatchTransfer }
     * 
     */
    public AchBatchTransfer createAchBatchTransfer() {
        return new AchBatchTransfer();
    }

    /**
     * Create an instance of {@link GetCardDepositsResponse }
     * 
     */
    public GetCardDepositsResponse createGetCardDepositsResponse() {
        return new GetCardDepositsResponse();
    }

    /**
     * Create an instance of {@link GetPaymentServicesByType }
     * 
     */
    public GetPaymentServicesByType createGetPaymentServicesByType() {
        return new GetPaymentServicesByType();
    }

    /**
     * Create an instance of {@link GetBillInfo }
     * 
     */
    public GetBillInfo createGetBillInfo() {
        return new GetBillInfo();
    }

    /**
     * Create an instance of {@link LoginWithMobileNumberResponse }
     * 
     */
    public LoginWithMobileNumberResponse createLoginWithMobileNumberResponse() {
        return new LoginWithMobileNumberResponse();
    }

    /**
     * Create an instance of {@link IbanEnquiry }
     * 
     */
    public IbanEnquiry createIbanEnquiry() {
        return new IbanEnquiry();
    }

    /**
     * Create an instance of {@link UpdateFavoriteAccountSettingResponse }
     * 
     */
    public UpdateFavoriteAccountSettingResponse createUpdateFavoriteAccountSettingResponse() {
        return new UpdateFavoriteAccountSettingResponse();
    }

    /**
     * Create an instance of {@link AddPeriodicBillRequest }
     * 
     */
    public AddPeriodicBillRequest createAddPeriodicBillRequest() {
        return new AddPeriodicBillRequest();
    }

    /**
     * Create an instance of {@link VerifyTransaction }
     * 
     */
    public VerifyTransaction createVerifyTransaction() {
        return new VerifyTransaction();
    }

    /**
     * Create an instance of {@link GetCustomerAddressResponse }
     * 
     */
    public GetCustomerAddressResponse createGetCustomerAddressResponse() {
        return new GetCustomerAddressResponse();
    }

    /**
     * Create an instance of {@link GetLoansResponse }
     * 
     */
    public GetLoansResponse createGetLoansResponse() {
        return new GetLoansResponse();
    }

    /**
     * Create an instance of {@link SwitchFundTransferFromVirtualTerminalResponse }
     * 
     */
    public SwitchFundTransferFromVirtualTerminalResponse createSwitchFundTransferFromVirtualTerminalResponse() {
        return new SwitchFundTransferFromVirtualTerminalResponse();
    }

    /**
     * Create an instance of {@link NaturalPersonOpenSingleDepositResponse }
     * 
     */
    public NaturalPersonOpenSingleDepositResponse createNaturalPersonOpenSingleDepositResponse() {
        return new NaturalPersonOpenSingleDepositResponse();
    }

    /**
     * Create an instance of {@link OpenDeposit }
     * 
     */
    public OpenDeposit createOpenDeposit() {
        return new OpenDeposit();
    }

    /**
     * Create an instance of {@link SendReversalResponse }
     * 
     */
    public SendReversalResponse createSendReversalResponse() {
        return new SendReversalResponse();
    }

    /**
     * Create an instance of {@link MenuEntryDto }
     * 
     */
    public MenuEntryDto createMenuEntryDto() {
        return new MenuEntryDto();
    }

    /**
     * Create an instance of {@link PayBatchBillResponse }
     * 
     */
    public PayBatchBillResponse createPayBatchBillResponse() {
        return new PayBatchBillResponse();
    }

    /**
     * Create an instance of {@link GetCreditDossiersResponse }
     * 
     */
    public GetCreditDossiersResponse createGetCreditDossiersResponse() {
        return new GetCreditDossiersResponse();
    }

    /**
     * Create an instance of {@link CardChangePinResponse }
     * 
     */
    public CardChangePinResponse createCardChangePinResponse() {
        return new CardChangePinResponse();
    }

    /**
     * Create an instance of {@link GetAutoTransferListResponse }
     * 
     */
    public GetAutoTransferListResponse createGetAutoTransferListResponse() {
        return new GetAutoTransferListResponse();
    }

    /**
     * Create an instance of {@link GetPaymentBillStatement }
     * 
     */
    public GetPaymentBillStatement createGetPaymentBillStatement() {
        return new GetPaymentBillStatement();
    }

    /**
     * Create an instance of {@link ExtendSMSSetting }
     * 
     */
    public ExtendSMSSetting createExtendSMSSetting() {
        return new ExtendSMSSetting();
    }

    /**
     * Create an instance of {@link GetBanks }
     * 
     */
    public GetBanks createGetBanks() {
        return new GetBanks();
    }

    /**
     * Create an instance of {@link CardTransferResponse }
     * 
     */
    public CardTransferResponse createCardTransferResponse() {
        return new CardTransferResponse();
    }

    /**
     * Create an instance of {@link DoCustomRetrunResponse }
     * 
     */
    public DoCustomRetrunResponse createDoCustomRetrunResponse() {
        return new DoCustomRetrunResponse();
    }

    /**
     * Create an instance of {@link CardBalanceInquiryResponse }
     * 
     */
    public CardBalanceInquiryResponse createCardBalanceInquiryResponse() {
        return new CardBalanceInquiryResponse();
    }

    /**
     * Create an instance of {@link GetCustomerInfoResponse }
     * 
     */
    public GetCustomerInfoResponse createGetCustomerInfoResponse() {
        return new GetCustomerInfoResponse();
    }

    /**
     * Create an instance of {@link GetUserSMSSettingResponse }
     * 
     */
    public GetUserSMSSettingResponse createGetUserSMSSettingResponse() {
        return new GetUserSMSSettingResponse();
    }

    /**
     * Create an instance of {@link GetCardsResponse }
     * 
     */
    public GetCardsResponse createGetCardsResponse() {
        return new GetCardsResponse();
    }

    /**
     * Create an instance of {@link MenuDto }
     * 
     */
    public MenuDto createMenuDto() {
        return new MenuDto();
    }

    /**
     * Create an instance of {@link ChargeCardResponse }
     * 
     */
    public ChargeCardResponse createChargeCardResponse() {
        return new ChargeCardResponse();
    }

    /**
     * Create an instance of {@link SuspendAchTransferResponse }
     * 
     */
    public SuspendAchTransferResponse createSuspendAchTransferResponse() {
        return new SuspendAchTransferResponse();
    }

    /**
     * Create an instance of {@link CancelAchTransaction }
     * 
     */
    public CancelAchTransaction createCancelAchTransaction() {
        return new CancelAchTransaction();
    }

    /**
     * Create an instance of {@link GetDestinationCardOwner }
     * 
     */
    public GetDestinationCardOwner createGetDestinationCardOwner() {
        return new GetDestinationCardOwner();
    }

    /**
     * Create an instance of {@link GetSignedDeposits }
     * 
     */
    public GetSignedDeposits createGetSignedDeposits() {
        return new GetSignedDeposits();
    }

    /**
     * Create an instance of {@link GetTransferConstraintInfoPerServiceResponse }
     * 
     */
    public GetTransferConstraintInfoPerServiceResponse createGetTransferConstraintInfoPerServiceResponse() {
        return new GetTransferConstraintInfoPerServiceResponse();
    }

    /**
     * Create an instance of {@link SignRequestDetailsResponse }
     * 
     */
    public SignRequestDetailsResponse createSignRequestDetailsResponse() {
        return new SignRequestDetailsResponse();
    }

    /**
     * Create an instance of {@link PayBillByAccountResponse }
     * 
     */
    public PayBillByAccountResponse createPayBillByAccountResponse() {
        return new PayBillByAccountResponse();
    }

    /**
     * Create an instance of {@link GetInstitutionsResponse }
     * 
     */
    public GetInstitutionsResponse createGetInstitutionsResponse() {
        return new GetInstitutionsResponse();
    }

    /**
     * Create an instance of {@link ExtendSMSSettingResponse }
     * 
     */
    public ExtendSMSSettingResponse createExtendSMSSettingResponse() {
        return new ExtendSMSSettingResponse();
    }

    /**
     * Create an instance of {@link DisableTransfer }
     * 
     */
    public DisableTransfer createDisableTransfer() {
        return new DisableTransfer();
    }

    /**
     * Create an instance of {@link GetCardTransactions }
     * 
     */
    public GetCardTransactions createGetCardTransactions() {
        return new GetCardTransactions();
    }

    /**
     * Create an instance of {@link CreateNaturalCustomerResponse }
     * 
     */
    public CreateNaturalCustomerResponse createCreateNaturalCustomerResponse() {
        return new CreateNaturalCustomerResponse();
    }

    /**
     * Create an instance of {@link GetDefaultBillStatementResponse }
     * 
     */
    public GetDefaultBillStatementResponse createGetDefaultBillStatementResponse() {
        return new GetDefaultBillStatementResponse();
    }

    /**
     * Create an instance of {@link GetDefaultBillStatement }
     * 
     */
    public GetDefaultBillStatement createGetDefaultBillStatement() {
        return new GetDefaultBillStatement();
    }

    /**
     * Create an instance of {@link InstitutionalTransferResponse }
     * 
     */
    public InstitutionalTransferResponse createInstitutionalTransferResponse() {
        return new InstitutionalTransferResponse();
    }

    /**
     * Create an instance of {@link CardInfoRetrievationResponse }
     * 
     */
    public CardInfoRetrievationResponse createCardInfoRetrievationResponse() {
        return new CardInfoRetrievationResponse();
    }

    /**
     * Create an instance of {@link SaveOrUpdateSecondPasswordResponse }
     * 
     */
    public SaveOrUpdateSecondPasswordResponse createSaveOrUpdateSecondPasswordResponse() {
        return new SaveOrUpdateSecondPasswordResponse();
    }

    /**
     * Create an instance of {@link GetBranchBalanceResponse }
     * 
     */
    public GetBranchBalanceResponse createGetBranchBalanceResponse() {
        return new GetBranchBalanceResponse();
    }

    /**
     * Create an instance of {@link GetBatchBillInfo }
     * 
     */
    public GetBatchBillInfo createGetBatchBillInfo() {
        return new GetBatchBillInfo();
    }

    /**
     * Create an instance of {@link GetEchoResponse }
     * 
     */
    public GetEchoResponse createGetEchoResponse() {
        return new GetEchoResponse();
    }

    /**
     * Create an instance of {@link AchTransferReportResponse }
     * 
     */
    public AchTransferReportResponse createAchTransferReportResponse() {
        return new AchTransferReportResponse();
    }

    /**
     * Create an instance of {@link SuspendAchTransaction }
     * 
     */
    public SuspendAchTransaction createSuspendAchTransaction() {
        return new SuspendAchTransaction();
    }

    /**
     * Create an instance of {@link GetCustomerAddress }
     * 
     */
    public GetCustomerAddress createGetCustomerAddress() {
        return new GetCustomerAddress();
    }

    /**
     * Create an instance of {@link GetGuarantyList }
     * 
     */
    public GetGuarantyList createGetGuarantyList() {
        return new GetGuarantyList();
    }

    /**
     * Create an instance of {@link RtgsTransferReport }
     * 
     */
    public RtgsTransferReport createRtgsTransferReport() {
        return new RtgsTransferReport();
    }

    /**
     * Create an instance of {@link SignRequestDetails }
     * 
     */
    public SignRequestDetails createSignRequestDetails() {
        return new SignRequestDetails();
    }

    /**
     * Create an instance of {@link InquiryNaturalPersonOpenSingleDeposit }
     * 
     */
    public InquiryNaturalPersonOpenSingleDeposit createInquiryNaturalPersonOpenSingleDeposit() {
        return new InquiryNaturalPersonOpenSingleDeposit();
    }

    /**
     * Create an instance of {@link UpdateFavoriteAccountSetting }
     * 
     */
    public UpdateFavoriteAccountSetting createUpdateFavoriteAccountSetting() {
        return new UpdateFavoriteAccountSetting();
    }

    /**
     * Create an instance of {@link GetBanksResponse }
     * 
     */
    public GetBanksResponse createGetBanksResponse() {
        return new GetBanksResponse();
    }

    /**
     * Create an instance of {@link NormalTransferResponse }
     * 
     */
    public NormalTransferResponse createNormalTransferResponse() {
        return new NormalTransferResponse();
    }

    /**
     * Create an instance of {@link GetCustomerInfoListByModifyDate }
     * 
     */
    public GetCustomerInfoListByModifyDate createGetCustomerInfoListByModifyDate() {
        return new GetCustomerInfoListByModifyDate();
    }

    /**
     * Create an instance of {@link GetMerchantInfoResponse }
     * 
     */
    public GetMerchantInfoResponse createGetMerchantInfoResponse() {
        return new GetMerchantInfoResponse();
    }

    /**
     * Create an instance of {@link RegisterChequeBookRequestResponse }
     * 
     */
    public RegisterChequeBookRequestResponse createRegisterChequeBookRequestResponse() {
        return new RegisterChequeBookRequestResponse();
    }

    /**
     * Create an instance of {@link GetDepositRatesResponse }
     * 
     */
    public GetDepositRatesResponse createGetDepositRatesResponse() {
        return new GetDepositRatesResponse();
    }

    /**
     * Create an instance of {@link TopupChargeFromVirtualTerminal }
     * 
     */
    public TopupChargeFromVirtualTerminal createTopupChargeFromVirtualTerminal() {
        return new TopupChargeFromVirtualTerminal();
    }

    /**
     * Create an instance of {@link GetBranchAllowedDepositsToOpen }
     * 
     */
    public GetBranchAllowedDepositsToOpen createGetBranchAllowedDepositsToOpen() {
        return new GetBranchAllowedDepositsToOpen();
    }

    /**
     * Create an instance of {@link GetCreditBillTransactions }
     * 
     */
    public GetCreditBillTransactions createGetCreditBillTransactions() {
        return new GetCreditBillTransactions();
    }

    /**
     * Create an instance of {@link InstitutionalTransfer }
     * 
     */
    public InstitutionalTransfer createInstitutionalTransfer() {
        return new InstitutionalTransfer();
    }

    /**
     * Create an instance of {@link VerifyTransactionResponse }
     * 
     */
    public VerifyTransactionResponse createVerifyTransactionResponse() {
        return new VerifyTransactionResponse();
    }

    /**
     * Create an instance of {@link NormalTransfer }
     * 
     */
    public NormalTransfer createNormalTransfer() {
        return new NormalTransfer();
    }

    /**
     * Create an instance of {@link HotCardResponse }
     * 
     */
    public HotCardResponse createHotCardResponse() {
        return new HotCardResponse();
    }

    /**
     * Create an instance of {@link InquiryCardIssuing }
     * 
     */
    public InquiryCardIssuing createInquiryCardIssuing() {
        return new InquiryCardIssuing();
    }

    /**
     * Create an instance of {@link SearchThirdParty }
     * 
     */
    public SearchThirdParty createSearchThirdParty() {
        return new SearchThirdParty();
    }

    /**
     * Create an instance of {@link CardExtendResponse }
     * 
     */
    public CardExtendResponse createCardExtendResponse() {
        return new CardExtendResponse();
    }

    /**
     * Create an instance of {@link DoCustomRetrun }
     * 
     */
    public DoCustomRetrun createDoCustomRetrun() {
        return new DoCustomRetrun();
    }

    /**
     * Create an instance of {@link UpdateUserBillSetting }
     * 
     */
    public UpdateUserBillSetting createUpdateUserBillSetting() {
        return new UpdateUserBillSetting();
    }

    /**
     * Create an instance of {@link DeactivateModernGatewayUser }
     * 
     */
    public DeactivateModernGatewayUser createDeactivateModernGatewayUser() {
        return new DeactivateModernGatewayUser();
    }

    /**
     * Create an instance of {@link GetPaymentServices }
     * 
     */
    public GetPaymentServices createGetPaymentServices() {
        return new GetPaymentServices();
    }

    /**
     * Create an instance of {@link BlockDepositAmountResponse }
     * 
     */
    public BlockDepositAmountResponse createBlockDepositAmountResponse() {
        return new BlockDepositAmountResponse();
    }

    /**
     * Create an instance of {@link GetBeOpenedDeposits }
     * 
     */
    public GetBeOpenedDeposits createGetBeOpenedDeposits() {
        return new GetBeOpenedDeposits();
    }

    /**
     * Create an instance of {@link GetLoanDetail }
     * 
     */
    public GetLoanDetail createGetLoanDetail() {
        return new GetLoanDetail();
    }

    /**
     * Create an instance of {@link SwitchFundTransferFromPhysicalTerminalResponse }
     * 
     */
    public SwitchFundTransferFromPhysicalTerminalResponse createSwitchFundTransferFromPhysicalTerminalResponse() {
        return new SwitchFundTransferFromPhysicalTerminalResponse();
    }

    /**
     * Create an instance of {@link SwitchFundTransferFromVirtualTerminal }
     * 
     */
    public SwitchFundTransferFromVirtualTerminal createSwitchFundTransferFromVirtualTerminal() {
        return new SwitchFundTransferFromVirtualTerminal();
    }

    /**
     * Create an instance of {@link OpenDepositResponse }
     * 
     */
    public OpenDepositResponse createOpenDepositResponse() {
        return new OpenDepositResponse();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link CardCorrectLinkedAccountsResponse }
     * 
     */
    public CardCorrectLinkedAccountsResponse createCardCorrectLinkedAccountsResponse() {
        return new CardCorrectLinkedAccountsResponse();
    }

    /**
     * Create an instance of {@link InquiryCardIssuingResponse }
     * 
     */
    public InquiryCardIssuingResponse createInquiryCardIssuingResponse() {
        return new InquiryCardIssuingResponse();
    }

    /**
     * Create an instance of {@link GetFavoriteDeposits }
     * 
     */
    public GetFavoriteDeposits createGetFavoriteDeposits() {
        return new GetFavoriteDeposits();
    }

    /**
     * Create an instance of {@link DisableTransferResponse }
     * 
     */
    public DisableTransferResponse createDisableTransferResponse() {
        return new DisableTransferResponse();
    }

    /**
     * Create an instance of {@link UpdateDefaultContactInfo }
     * 
     */
    public UpdateDefaultContactInfo createUpdateDefaultContactInfo() {
        return new UpdateDefaultContactInfo();
    }

    /**
     * Create an instance of {@link PayBatchBill }
     * 
     */
    public PayBatchBill createPayBatchBill() {
        return new PayBatchBill();
    }

    /**
     * Create an instance of {@link AchNormalTransfer }
     * 
     */
    public AchNormalTransfer createAchNormalTransfer() {
        return new AchNormalTransfer();
    }

    /**
     * Create an instance of {@link GetCustomerInfo }
     * 
     */
    public GetCustomerInfo createGetCustomerInfo() {
        return new GetCustomerInfo();
    }

    /**
     * Create an instance of {@link PayLoan }
     * 
     */
    public PayLoan createPayLoan() {
        return new PayLoan();
    }

    /**
     * Create an instance of {@link RtgsNormalTransfer }
     * 
     */
    public RtgsNormalTransfer createRtgsNormalTransfer() {
        return new RtgsNormalTransfer();
    }

    /**
     * Create an instance of {@link CardPurchaseResponse }
     * 
     */
    public CardPurchaseResponse createCardPurchaseResponse() {
        return new CardPurchaseResponse();
    }

    /**
     * Create an instance of {@link CustomerExistenceInquiryResponse }
     * 
     */
    public CustomerExistenceInquiryResponse createCustomerExistenceInquiryResponse() {
        return new CustomerExistenceInquiryResponse();
    }

    /**
     * Create an instance of {@link GetCreditBills }
     * 
     */
    public GetCreditBills createGetCreditBills() {
        return new GetCreditBills();
    }

    /**
     * Create an instance of {@link RtgsTransferDetailReport }
     * 
     */
    public RtgsTransferDetailReport createRtgsTransferDetailReport() {
        return new RtgsTransferDetailReport();
    }

    /**
     * Create an instance of {@link GetChequeResponse }
     * 
     */
    public GetChequeResponse createGetChequeResponse() {
        return new GetChequeResponse();
    }

    /**
     * Create an instance of {@link CreateNaturalCustomer }
     * 
     */
    public CreateNaturalCustomer createCreateNaturalCustomer() {
        return new CreateNaturalCustomer();
    }

    /**
     * Create an instance of {@link NaturalPersonOpenSingleDeposit }
     * 
     */
    public NaturalPersonOpenSingleDeposit createNaturalPersonOpenSingleDeposit() {
        return new NaturalPersonOpenSingleDeposit();
    }

    /**
     * Create an instance of {@link CancelAchTransactionResponse }
     * 
     */
    public CancelAchTransactionResponse createCancelAchTransactionResponse() {
        return new CancelAchTransactionResponse();
    }

    /**
     * Create an instance of {@link GetUserBillSettingResponse }
     * 
     */
    public GetUserBillSettingResponse createGetUserBillSettingResponse() {
        return new GetUserBillSettingResponse();
    }

    /**
     * Create an instance of {@link ConvertDepositNumberToIbanResponse }
     * 
     */
    public ConvertDepositNumberToIbanResponse createConvertDepositNumberToIbanResponse() {
        return new ConvertDepositNumberToIbanResponse();
    }

    /**
     * Create an instance of {@link GetTransferConstraintInfo }
     * 
     */
    public GetTransferConstraintInfo createGetTransferConstraintInfo() {
        return new GetTransferConstraintInfo();
    }

    /**
     * Create an instance of {@link ChangeUsernameResponse }
     * 
     */
    public ChangeUsernameResponse createChangeUsernameResponse() {
        return new ChangeUsernameResponse();
    }

    /**
     * Create an instance of {@link PayBillByAccount }
     * 
     */
    public PayBillByAccount createPayBillByAccount() {
        return new PayBillByAccount();
    }

    /**
     * Create an instance of {@link AddPeriodicBillRequestResponse }
     * 
     */
    public AddPeriodicBillRequestResponse createAddPeriodicBillRequestResponse() {
        return new AddPeriodicBillRequestResponse();
    }

    /**
     * Create an instance of {@link CardInfoRetrievation }
     * 
     */
    public CardInfoRetrievation createCardInfoRetrievation() {
        return new CardInfoRetrievation();
    }

    /**
     * Create an instance of {@link CardSetPassword }
     * 
     */
    public CardSetPassword createCardSetPassword() {
        return new CardSetPassword();
    }

    /**
     * Create an instance of {@link GetDepositOwnerName }
     * 
     */
    public GetDepositOwnerName createGetDepositOwnerName() {
        return new GetDepositOwnerName();
    }

    /**
     * Create an instance of {@link GetInstitutions }
     * 
     */
    public GetInstitutions createGetInstitutions() {
        return new GetInstitutions();
    }

    /**
     * Create an instance of {@link LoadCoreParametersResponse }
     * 
     */
    public LoadCoreParametersResponse createLoadCoreParametersResponse() {
        return new LoadCoreParametersResponse();
    }

    /**
     * Create an instance of {@link GetDestinationCardOwnerResponse }
     * 
     */
    public GetDestinationCardOwnerResponse createGetDestinationCardOwnerResponse() {
        return new GetDestinationCardOwnerResponse();
    }

    /**
     * Create an instance of {@link GetServiceConstraintList }
     * 
     */
    public GetServiceConstraintList createGetServiceConstraintList() {
        return new GetServiceConstraintList();
    }

    /**
     * Create an instance of {@link UpdateDefaultContactInfoResponse }
     * 
     */
    public UpdateDefaultContactInfoResponse createUpdateDefaultContactInfoResponse() {
        return new UpdateDefaultContactInfoResponse();
    }

    /**
     * Create an instance of {@link SearchThirdPartyResponse }
     * 
     */
    public SearchThirdPartyResponse createSearchThirdPartyResponse() {
        return new SearchThirdPartyResponse();
    }

    /**
     * Create an instance of {@link ConvertIbanToDepositNumberResponse }
     * 
     */
    public ConvertIbanToDepositNumberResponse createConvertIbanToDepositNumberResponse() {
        return new ConvertIbanToDepositNumberResponse();
    }

    /**
     * Create an instance of {@link GetCardOwnerResponse }
     * 
     */
    public GetCardOwnerResponse createGetCardOwnerResponse() {
        return new GetCardOwnerResponse();
    }

    /**
     * Create an instance of {@link GetMerchantInfo }
     * 
     */
    public GetMerchantInfo createGetMerchantInfo() {
        return new GetMerchantInfo();
    }

    /**
     * Create an instance of {@link CardTransfer }
     * 
     */
    public CardTransfer createCardTransfer() {
        return new CardTransfer();
    }

    /**
     * Create an instance of {@link GetCheque }
     * 
     */
    public GetCheque createGetCheque() {
        return new GetCheque();
    }

    /**
     * Create an instance of {@link GetBranchBalance }
     * 
     */
    public GetBranchBalance createGetBranchBalance() {
        return new GetBranchBalance();
    }

    /**
     * Create an instance of {@link GetCustomerSignatures }
     * 
     */
    public GetCustomerSignatures createGetCustomerSignatures() {
        return new GetCustomerSignatures();
    }

    /**
     * Create an instance of {@link GetStatementResponse }
     * 
     */
    public GetStatementResponse createGetStatementResponse() {
        return new GetStatementResponse();
    }

    /**
     * Create an instance of {@link GetUserBillSetting }
     * 
     */
    public GetUserBillSetting createGetUserBillSetting() {
        return new GetUserBillSetting();
    }

    /**
     * Create an instance of {@link GetCustomerInfoListByModifyDateResponse }
     * 
     */
    public GetCustomerInfoListByModifyDateResponse createGetCustomerInfoListByModifyDateResponse() {
        return new GetCustomerInfoListByModifyDateResponse();
    }

    /**
     * Create an instance of {@link GetCardLimitationsResponse }
     * 
     */
    public GetCardLimitationsResponse createGetCardLimitationsResponse() {
        return new GetCardLimitationsResponse();
    }

    /**
     * Create an instance of {@link SuspendAchTransfer }
     * 
     */
    public SuspendAchTransfer createSuspendAchTransfer() {
        return new SuspendAchTransfer();
    }

    /**
     * Create an instance of {@link GetCardTransactionsResponse }
     * 
     */
    public GetCardTransactionsResponse createGetCardTransactionsResponse() {
        return new GetCardTransactionsResponse();
    }

    /**
     * Create an instance of {@link ReportAchTransferSummary }
     * 
     */
    public ReportAchTransferSummary createReportAchTransferSummary() {
        return new ReportAchTransferSummary();
    }

    /**
     * Create an instance of {@link WebserviceGatewayFaultException }
     * 
     */
    public WebserviceGatewayFaultException createWebserviceGatewayFaultException() {
        return new WebserviceGatewayFaultException();
    }

    /**
     * Create an instance of {@link CardIssuingResponse }
     * 
     */
    public CardIssuingResponse createCardIssuingResponse() {
        return new CardIssuingResponse();
    }

    /**
     * Create an instance of {@link DeactivateModernGatewayUserResponse }
     * 
     */
    public DeactivateModernGatewayUserResponse createDeactivateModernGatewayUserResponse() {
        return new DeactivateModernGatewayUserResponse();
    }

    /**
     * Create an instance of {@link SendMailResponse }
     * 
     */
    public SendMailResponse createSendMailResponse() {
        return new SendMailResponse();
    }

    /**
     * Create an instance of {@link GetAtmStatusResponse }
     * 
     */
    public GetAtmStatusResponse createGetAtmStatusResponse() {
        return new GetAtmStatusResponse();
    }

    /**
     * Create an instance of {@link AchNormalTransferResponse }
     * 
     */
    public AchNormalTransferResponse createAchNormalTransferResponse() {
        return new AchNormalTransferResponse();
    }

    /**
     * Create an instance of {@link AddPeriodicBalanceRequestResponse }
     * 
     */
    public AddPeriodicBalanceRequestResponse createAddPeriodicBalanceRequestResponse() {
        return new AddPeriodicBalanceRequestResponse();
    }

    /**
     * Create an instance of {@link PayCreditBillResponse }
     * 
     */
    public PayCreditBillResponse createPayCreditBillResponse() {
        return new PayCreditBillResponse();
    }

    /**
     * Create an instance of {@link CardIssuing }
     * 
     */
    public CardIssuing createCardIssuing() {
        return new CardIssuing();
    }

    /**
     * Create an instance of {@link GetPaymentBillStatementResponse }
     * 
     */
    public GetPaymentBillStatementResponse createGetPaymentBillStatementResponse() {
        return new GetPaymentBillStatementResponse();
    }

    /**
     * Create an instance of {@link SendMail }
     * 
     */
    public SendMail createSendMail() {
        return new SendMail();
    }

    /**
     * Create an instance of {@link RemoveCardDeposit }
     * 
     */
    public RemoveCardDeposit createRemoveCardDeposit() {
        return new RemoveCardDeposit();
    }

    /**
     * Create an instance of {@link GetCreditBillsResponse }
     * 
     */
    public GetCreditBillsResponse createGetCreditBillsResponse() {
        return new GetCreditBillsResponse();
    }

    /**
     * Create an instance of {@link GetLoans }
     * 
     */
    public GetLoans createGetLoans() {
        return new GetLoans();
    }

    /**
     * Create an instance of {@link RemoveCardDepositResponse }
     * 
     */
    public RemoveCardDepositResponse createRemoveCardDepositResponse() {
        return new RemoveCardDepositResponse();
    }

    /**
     * Create an instance of {@link GetBranches }
     * 
     */
    public GetBranches createGetBranches() {
        return new GetBranches();
    }

    /**
     * Create an instance of {@link Logout }
     * 
     */
    public Logout createLogout() {
        return new Logout();
    }

    /**
     * Create an instance of {@link GetSellReport }
     * 
     */
    public GetSellReport createGetSellReport() {
        return new GetSellReport();
    }

    /**
     * Create an instance of {@link GetSellDetailReportResponse }
     * 
     */
    public GetSellDetailReportResponse createGetSellDetailReportResponse() {
        return new GetSellDetailReportResponse();
    }

    /**
     * Create an instance of {@link GetSignedDepositsResponse }
     * 
     */
    public GetSignedDepositsResponse createGetSignedDepositsResponse() {
        return new GetSignedDepositsResponse();
    }

    /**
     * Create an instance of {@link RegisterChequeResponse }
     * 
     */
    public RegisterChequeResponse createRegisterChequeResponse() {
        return new RegisterChequeResponse();
    }

    /**
     * Create an instance of {@link HotCardByPin }
     * 
     */
    public HotCardByPin createHotCardByPin() {
        return new HotCardByPin();
    }

    /**
     * Create an instance of {@link AchTransactionReport }
     * 
     */
    public AchTransactionReport createAchTransactionReport() {
        return new AchTransactionReport();
    }

    /**
     * Create an instance of {@link CardIssuingAllowedDepositsToOpen }
     * 
     */
    public CardIssuingAllowedDepositsToOpen createCardIssuingAllowedDepositsToOpen() {
        return new CardIssuingAllowedDepositsToOpen();
    }

    /**
     * Create an instance of {@link GetDeposits }
     * 
     */
    public GetDeposits createGetDeposits() {
        return new GetDeposits();
    }

    /**
     * Create an instance of {@link SuspendAchTransactionResponse }
     * 
     */
    public SuspendAchTransactionResponse createSuspendAchTransactionResponse() {
        return new SuspendAchTransactionResponse();
    }

    /**
     * Create an instance of {@link GetBranchAllowedDepositsToOpenResponse }
     * 
     */
    public GetBranchAllowedDepositsToOpenResponse createGetBranchAllowedDepositsToOpenResponse() {
        return new GetBranchAllowedDepositsToOpenResponse();
    }

    /**
     * Create an instance of {@link CardChangePin }
     * 
     */
    public CardChangePin createCardChangePin() {
        return new CardChangePin();
    }

    /**
     * Create an instance of {@link GetBranchesResponse }
     * 
     */
    public GetBranchesResponse createGetBranchesResponse() {
        return new GetBranchesResponse();
    }

    /**
     * Create an instance of {@link HotCardByPinResponse }
     * 
     */
    public HotCardByPinResponse createHotCardByPinResponse() {
        return new HotCardByPinResponse();
    }

    /**
     * Create an instance of {@link GetPaymentPreviewResponse }
     * 
     */
    public GetPaymentPreviewResponse createGetPaymentPreviewResponse() {
        return new GetPaymentPreviewResponse();
    }

    /**
     * Create an instance of {@link GetCardOwner }
     * 
     */
    public GetCardOwner createGetCardOwner() {
        return new GetCardOwner();
    }

    /**
     * Create an instance of {@link ResumeAchTransactionResponse }
     * 
     */
    public ResumeAchTransactionResponse createResumeAchTransactionResponse() {
        return new ResumeAchTransactionResponse();
    }

    /**
     * Create an instance of {@link AutoTransfer }
     * 
     */
    public AutoTransfer createAutoTransfer() {
        return new AutoTransfer();
    }

    /**
     * Create an instance of {@link LoginWithMobileNumber }
     * 
     */
    public LoginWithMobileNumber createLoginWithMobileNumber() {
        return new LoginWithMobileNumber();
    }

    /**
     * Create an instance of {@link DoPayment }
     * 
     */
    public DoPayment createDoPayment() {
        return new DoPayment();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link RegisterChequeBookRequest }
     * 
     */
    public RegisterChequeBookRequest createRegisterChequeBookRequest() {
        return new RegisterChequeBookRequest();
    }

    /**
     * Create an instance of {@link CardExtend }
     * 
     */
    public CardExtend createCardExtend() {
        return new CardExtend();
    }

    /**
     * Create an instance of {@link ResumeAchTransfer }
     * 
     */
    public ResumeAchTransfer createResumeAchTransfer() {
        return new ResumeAchTransfer();
    }

    /**
     * Create an instance of {@link GetFavoriteAccountSetting }
     * 
     */
    public GetFavoriteAccountSetting createGetFavoriteAccountSetting() {
        return new GetFavoriteAccountSetting();
    }

    /**
     * Create an instance of {@link GetRequestStatus }
     * 
     */
    public GetRequestStatus createGetRequestStatus() {
        return new GetRequestStatus();
    }

    /**
     * Create an instance of {@link GetAutoTransferList }
     * 
     */
    public GetAutoTransferList createGetAutoTransferList() {
        return new GetAutoTransferList();
    }

    /**
     * Create an instance of {@link GetBatchBillInfoResponse }
     * 
     */
    public GetBatchBillInfoResponse createGetBatchBillInfoResponse() {
        return new GetBatchBillInfoResponse();
    }

    /**
     * Create an instance of {@link ResumeAchTransaction }
     * 
     */
    public ResumeAchTransaction createResumeAchTransaction() {
        return new ResumeAchTransaction();
    }

    /**
     * Create an instance of {@link GetSellDetailReport }
     * 
     */
    public GetSellDetailReport createGetSellDetailReport() {
        return new GetSellDetailReport();
    }

    /**
     * Create an instance of {@link GetPaymentServicesByTypeResponse }
     * 
     */
    public GetPaymentServicesByTypeResponse createGetPaymentServicesByTypeResponse() {
        return new GetPaymentServicesByTypeResponse();
    }

    /**
     * Create an instance of {@link SwitchFundTransferFromPhysicalTerminal }
     * 
     */
    public SwitchFundTransferFromPhysicalTerminal createSwitchFundTransferFromPhysicalTerminal() {
        return new SwitchFundTransferFromPhysicalTerminal();
    }

    /**
     * Create an instance of {@link GetTransferConstraintInfoPerService }
     * 
     */
    public GetTransferConstraintInfoPerService createGetTransferConstraintInfoPerService() {
        return new GetTransferConstraintInfoPerService();
    }

    /**
     * Create an instance of {@link ChAccountOwner }
     * 
     */
    public ChAccountOwner createChAccountOwner() {
        return new ChAccountOwner();
    }

    /**
     * Create an instance of {@link GetUserSMSSetting }
     * 
     */
    public GetUserSMSSetting createGetUserSMSSetting() {
        return new GetUserSMSSetting();
    }

    /**
     * Create an instance of {@link GetCustomerSignaturesResponse }
     * 
     */
    public GetCustomerSignaturesResponse createGetCustomerSignaturesResponse() {
        return new GetCustomerSignaturesResponse();
    }

    /**
     * Create an instance of {@link AcceptAchTransferResponse }
     * 
     */
    public AcceptAchTransferResponse createAcceptAchTransferResponse() {
        return new AcceptAchTransferResponse();
    }

    /**
     * Create an instance of {@link CancelAchTransferResponse }
     * 
     */
    public CancelAchTransferResponse createCancelAchTransferResponse() {
        return new CancelAchTransferResponse();
    }

    /**
     * Create an instance of {@link GetGuarantyListResponse }
     * 
     */
    public GetGuarantyListResponse createGetGuarantyListResponse() {
        return new GetGuarantyListResponse();
    }

    /**
     * Create an instance of {@link ReportAchTransferSummaryResponse }
     * 
     */
    public ReportAchTransferSummaryResponse createReportAchTransferSummaryResponse() {
        return new ReportAchTransferSummaryResponse();
    }

    /**
     * Create an instance of {@link CashCheque }
     * 
     */
    public CashCheque createCashCheque() {
        return new CashCheque();
    }

    /**
     * Create an instance of {@link GetCardProductsResponse }
     * 
     */
    public GetCardProductsResponse createGetCardProductsResponse() {
        return new GetCardProductsResponse();
    }

    /**
     * Create an instance of {@link CashChequeResponse }
     * 
     */
    public CashChequeResponse createCashChequeResponse() {
        return new CashChequeResponse();
    }

    /**
     * Create an instance of {@link ConvertIbanToDepositNumber }
     * 
     */
    public ConvertIbanToDepositNumber createConvertIbanToDepositNumber() {
        return new ConvertIbanToDepositNumber();
    }

    /**
     * Create an instance of {@link GetUserInfo }
     * 
     */
    public GetUserInfo createGetUserInfo() {
        return new GetUserInfo();
    }

    /**
     * Create an instance of {@link DoPaymentResponse }
     * 
     */
    public DoPaymentResponse createDoPaymentResponse() {
        return new DoPaymentResponse();
    }

    /**
     * Create an instance of {@link ChangeUsername }
     * 
     */
    public ChangeUsername createChangeUsername() {
        return new ChangeUsername();
    }

    /**
     * Create an instance of {@link HotCard }
     * 
     */
    public HotCard createHotCard() {
        return new HotCard();
    }

    /**
     * Create an instance of {@link PayCreditBill }
     * 
     */
    public PayCreditBill createPayCreditBill() {
        return new PayCreditBill();
    }

    /**
     * Create an instance of {@link AchAutoTransfer }
     * 
     */
    public AchAutoTransfer createAchAutoTransfer() {
        return new AchAutoTransfer();
    }

    /**
     * Create an instance of {@link UpdateUserBillSettingResponse }
     * 
     */
    public UpdateUserBillSettingResponse createUpdateUserBillSettingResponse() {
        return new UpdateUserBillSettingResponse();
    }

    /**
     * Create an instance of {@link ResumeAchTransferResponse }
     * 
     */
    public ResumeAchTransferResponse createResumeAchTransferResponse() {
        return new ResumeAchTransferResponse();
    }

    /**
     * Create an instance of {@link GetChequeBookListResponse }
     * 
     */
    public GetChequeBookListResponse createGetChequeBookListResponse() {
        return new GetChequeBookListResponse();
    }

    /**
     * Create an instance of {@link GetOwnerDepositName }
     * 
     */
    public GetOwnerDepositName createGetOwnerDepositName() {
        return new GetOwnerDepositName();
    }

    /**
     * Create an instance of {@link GetTransferChequeListResponse }
     * 
     */
    public GetTransferChequeListResponse createGetTransferChequeListResponse() {
        return new GetTransferChequeListResponse();
    }

    /**
     * Create an instance of {@link GetCards }
     * 
     */
    public GetCards createGetCards() {
        return new GetCards();
    }

    /**
     * Create an instance of {@link GetDepositRates }
     * 
     */
    public GetDepositRates createGetDepositRates() {
        return new GetDepositRates();
    }

    /**
     * Create an instance of {@link TopupChargeFromVirtualTerminalResponse }
     * 
     */
    public TopupChargeFromVirtualTerminalResponse createTopupChargeFromVirtualTerminalResponse() {
        return new TopupChargeFromVirtualTerminalResponse();
    }

    /**
     * Create an instance of {@link AddCardDeposit }
     * 
     */
    public AddCardDeposit createAddCardDeposit() {
        return new AddCardDeposit();
    }

    /**
     * Create an instance of {@link BlockChequeResponse }
     * 
     */
    public BlockChequeResponse createBlockChequeResponse() {
        return new BlockChequeResponse();
    }

    /**
     * Create an instance of {@link GetServiceConstraintListResponse }
     * 
     */
    public GetServiceConstraintListResponse createGetServiceConstraintListResponse() {
        return new GetServiceConstraintListResponse();
    }

    /**
     * Create an instance of {@link AchBatchTransferResponse }
     * 
     */
    public AchBatchTransferResponse createAchBatchTransferResponse() {
        return new AchBatchTransferResponse();
    }

    /**
     * Create an instance of {@link GetSellReportResponse }
     * 
     */
    public GetSellReportResponse createGetSellReportResponse() {
        return new GetSellReportResponse();
    }

    /**
     * Create an instance of {@link IbanEnquiryResponse }
     * 
     */
    public IbanEnquiryResponse createIbanEnquiryResponse() {
        return new IbanEnquiryResponse();
    }

    /**
     * Create an instance of {@link ChargeCard }
     * 
     */
    public ChargeCard createChargeCard() {
        return new ChargeCard();
    }

    /**
     * Create an instance of {@link GetStatement }
     * 
     */
    public GetStatement createGetStatement() {
        return new GetStatement();
    }

    /**
     * Create an instance of {@link SendReversal }
     * 
     */
    public SendReversal createSendReversal() {
        return new SendReversal();
    }

    /**
     * Create an instance of {@link GetPaymentPreview }
     * 
     */
    public GetPaymentPreview createGetPaymentPreview() {
        return new GetPaymentPreview();
    }

    /**
     * Create an instance of {@link GetRequestStatusResponse }
     * 
     */
    public GetRequestStatusResponse createGetRequestStatusResponse() {
        return new GetRequestStatusResponse();
    }

    /**
     * Create an instance of {@link GetBeOpenedDepositsResponse }
     * 
     */
    public GetBeOpenedDepositsResponse createGetBeOpenedDepositsResponse() {
        return new GetBeOpenedDepositsResponse();
    }

    /**
     * Create an instance of {@link GetPaymentServicesResponse }
     * 
     */
    public GetPaymentServicesResponse createGetPaymentServicesResponse() {
        return new GetPaymentServicesResponse();
    }

    /**
     * Create an instance of {@link GetCurrencyRateResponse }
     * 
     */
    public GetCurrencyRateResponse createGetCurrencyRateResponse() {
        return new GetCurrencyRateResponse();
    }

    /**
     * Create an instance of {@link CardSetPasswordResponse }
     * 
     */
    public CardSetPasswordResponse createCardSetPasswordResponse() {
        return new CardSetPasswordResponse();
    }

    /**
     * Create an instance of {@link CancelAchTransfer }
     * 
     */
    public CancelAchTransfer createCancelAchTransfer() {
        return new CancelAchTransfer();
    }

    /**
     * Create an instance of {@link GetUserInfoResponse }
     * 
     */
    public GetUserInfoResponse createGetUserInfoResponse() {
        return new GetUserInfoResponse();
    }

    /**
     * Create an instance of {@link RtgsNormalTransferResponse }
     * 
     */
    public RtgsNormalTransferResponse createRtgsNormalTransferResponse() {
        return new RtgsNormalTransferResponse();
    }

    /**
     * Create an instance of {@link BlockDepositAmount }
     * 
     */
    public BlockDepositAmount createBlockDepositAmount() {
        return new BlockDepositAmount();
    }

    /**
     * Create an instance of {@link GetDepositCustomer }
     * 
     */
    public GetDepositCustomer createGetDepositCustomer() {
        return new GetDepositCustomer();
    }

    /**
     * Create an instance of {@link PayLoanResponse }
     * 
     */
    public PayLoanResponse createPayLoanResponse() {
        return new PayLoanResponse();
    }

    /**
     * Create an instance of {@link GetCreditBillTransactionsResponse }
     * 
     */
    public GetCreditBillTransactionsResponse createGetCreditBillTransactionsResponse() {
        return new GetCreditBillTransactionsResponse();
    }

    /**
     * Create an instance of {@link GetDepositCustomerResponse }
     * 
     */
    public GetDepositCustomerResponse createGetDepositCustomerResponse() {
        return new GetDepositCustomerResponse();
    }

    /**
     * Create an instance of {@link DefineCustomerSignatureSample }
     * 
     */
    public DefineCustomerSignatureSample createDefineCustomerSignatureSample() {
        return new DefineCustomerSignatureSample();
    }

    /**
     * Create an instance of {@link CardPurchase }
     * 
     */
    public CardPurchase createCardPurchase() {
        return new CardPurchase();
    }

    /**
     * Create an instance of {@link GetCreditDossiers }
     * 
     */
    public GetCreditDossiers createGetCreditDossiers() {
        return new GetCreditDossiers();
    }

    /**
     * Create an instance of {@link SaveOrUpdateSecondPassword }
     * 
     */
    public SaveOrUpdateSecondPassword createSaveOrUpdateSecondPassword() {
        return new SaveOrUpdateSecondPassword();
    }

    /**
     * Create an instance of {@link GetCardLimitations }
     * 
     */
    public GetCardLimitations createGetCardLimitations() {
        return new GetCardLimitations();
    }

    /**
     * Create an instance of {@link ChangePasswordResponse }
     * 
     */
    public ChangePasswordResponse createChangePasswordResponse() {
        return new ChangePasswordResponse();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     * 
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    /**
     * Create an instance of {@link BatchTransfer }
     * 
     */
    public BatchTransfer createBatchTransfer() {
        return new BatchTransfer();
    }

    /**
     * Create an instance of {@link ConvertDepositNumberToIban }
     * 
     */
    public ConvertDepositNumberToIban createConvertDepositNumberToIban() {
        return new ConvertDepositNumberToIban();
    }

    /**
     * Create an instance of {@link PayBill }
     * 
     */
    public PayBill createPayBill() {
        return new PayBill();
    }

    /**
     * Create an instance of {@link AchTransferReport }
     * 
     */
    public AchTransferReport createAchTransferReport() {
        return new AchTransferReport();
    }

    /**
     * Create an instance of {@link GetCardProducts }
     * 
     */
    public GetCardProducts createGetCardProducts() {
        return new GetCardProducts();
    }

    /**
     * Create an instance of {@link DefineCustomerSignatureSampleResponse }
     * 
     */
    public DefineCustomerSignatureSampleResponse createDefineCustomerSignatureSampleResponse() {
        return new DefineCustomerSignatureSampleResponse();
    }

    /**
     * Create an instance of {@link PayBillResponse }
     * 
     */
    public PayBillResponse createPayBillResponse() {
        return new PayBillResponse();
    }

    /**
     * Create an instance of {@link BlockCheque }
     * 
     */
    public BlockCheque createBlockCheque() {
        return new BlockCheque();
    }

    /**
     * Create an instance of {@link AutoTransferResponse }
     * 
     */
    public AutoTransferResponse createAutoTransferResponse() {
        return new AutoTransferResponse();
    }

    /**
     * Create an instance of {@link CustomerExistenceInquiry }
     * 
     */
    public CustomerExistenceInquiry createCustomerExistenceInquiry() {
        return new CustomerExistenceInquiry();
    }

    /**
     * Create an instance of {@link CardCorrectLinkedAccounts }
     * 
     */
    public CardCorrectLinkedAccounts createCardCorrectLinkedAccounts() {
        return new CardCorrectLinkedAccounts();
    }

    /**
     * Create an instance of {@link GetLoanDetailResponse }
     * 
     */
    public GetLoanDetailResponse createGetLoanDetailResponse() {
        return new GetLoanDetailResponse();
    }

    /**
     * Create an instance of {@link RtgsTransferReportResponse }
     * 
     */
    public RtgsTransferReportResponse createRtgsTransferReportResponse() {
        return new RtgsTransferReportResponse();
    }

    /**
     * Create an instance of {@link InquiryNaturalPersonOpenSingleDepositResponse }
     * 
     */
    public InquiryNaturalPersonOpenSingleDepositResponse createInquiryNaturalPersonOpenSingleDepositResponse() {
        return new InquiryNaturalPersonOpenSingleDepositResponse();
    }

    /**
     * Create an instance of {@link GetChequeBookList }
     * 
     */
    public GetChequeBookList createGetChequeBookList() {
        return new GetChequeBookList();
    }

    /**
     * Create an instance of {@link GetCurrencyRate }
     * 
     */
    public GetCurrencyRate createGetCurrencyRate() {
        return new GetCurrencyRate();
    }

    /**
     * Create an instance of {@link AddCardDepositResponse }
     * 
     */
    public AddCardDepositResponse createAddCardDepositResponse() {
        return new AddCardDepositResponse();
    }

    /**
     * Create an instance of {@link GetBillInfoResponse }
     * 
     */
    public GetBillInfoResponse createGetBillInfoResponse() {
        return new GetBillInfoResponse();
    }

    /**
     * Create an instance of {@link GetOwnerDepositNameResponse }
     * 
     */
    public GetOwnerDepositNameResponse createGetOwnerDepositNameResponse() {
        return new GetOwnerDepositNameResponse();
    }

    /**
     * Create an instance of {@link GetTransferChequeList }
     * 
     */
    public GetTransferChequeList createGetTransferChequeList() {
        return new GetTransferChequeList();
    }

    /**
     * Create an instance of {@link GetEcho }
     * 
     */
    public GetEcho createGetEcho() {
        return new GetEcho();
    }

    /**
     * Create an instance of {@link GetTransferConstraintInfoResponse }
     * 
     */
    public GetTransferConstraintInfoResponse createGetTransferConstraintInfoResponse() {
        return new GetTransferConstraintInfoResponse();
    }

    /**
     * Create an instance of {@link LoadCoreParameters }
     * 
     */
    public LoadCoreParameters createLoadCoreParameters() {
        return new LoadCoreParameters();
    }

    /**
     * Create an instance of {@link GetCardDeposits }
     * 
     */
    public GetCardDeposits createGetCardDeposits() {
        return new GetCardDeposits();
    }

    /**
     * Create an instance of {@link GetAtmStatus }
     * 
     */
    public GetAtmStatus createGetAtmStatus() {
        return new GetAtmStatus();
    }

    /**
     * Create an instance of {@link AchTransactionReportResponse }
     * 
     */
    public AchTransactionReportResponse createAchTransactionReportResponse() {
        return new AchTransactionReportResponse();
    }

    /**
     * Create an instance of {@link ChangePassword }
     * 
     */
    public ChangePassword createChangePassword() {
        return new ChangePassword();
    }

    /**
     * Create an instance of {@link GetFavoriteDepositsResponse }
     * 
     */
    public GetFavoriteDepositsResponse createGetFavoriteDepositsResponse() {
        return new GetFavoriteDepositsResponse();
    }

    /**
     * Create an instance of {@link GetCardStatementInquiry }
     * 
     */
    public GetCardStatementInquiry createGetCardStatementInquiry() {
        return new GetCardStatementInquiry();
    }

    /**
     * Create an instance of {@link RegisterCheque }
     * 
     */
    public RegisterCheque createRegisterCheque() {
        return new RegisterCheque();
    }

    /**
     * Create an instance of {@link RtgsTransferDetailReportResponse }
     * 
     */
    public RtgsTransferDetailReportResponse createRtgsTransferDetailReportResponse() {
        return new RtgsTransferDetailReportResponse();
    }

    /**
     * Create an instance of {@link AddPeriodicBalanceRequest }
     * 
     */
    public AddPeriodicBalanceRequest createAddPeriodicBalanceRequest() {
        return new AddPeriodicBalanceRequest();
    }

    /**
     * Create an instance of {@link AchAutoTransferResponse }
     * 
     */
    public AchAutoTransferResponse createAchAutoTransferResponse() {
        return new AchAutoTransferResponse();
    }

    /**
     * Create an instance of {@link BatchTransferResponse }
     * 
     */
    public BatchTransferResponse createBatchTransferResponse() {
        return new BatchTransferResponse();
    }

    /**
     * Create an instance of {@link CardIssuingAllowedDepositsToOpenResponse }
     * 
     */
    public CardIssuingAllowedDepositsToOpenResponse createCardIssuingAllowedDepositsToOpenResponse() {
        return new CardIssuingAllowedDepositsToOpenResponse();
    }

    /**
     * Create an instance of {@link CardBalanceInquiry }
     * 
     */
    public CardBalanceInquiry createCardBalanceInquiry() {
        return new CardBalanceInquiry();
    }

    /**
     * Create an instance of {@link ChBranchBalanceInfoDetail }
     * 
     */
    public ChBranchBalanceInfoDetail createChBranchBalanceInfoDetail() {
        return new ChBranchBalanceInfoDetail();
    }

    /**
     * Create an instance of {@link ChDepositResponseBean }
     * 
     */
    public ChDepositResponseBean createChDepositResponseBean() {
        return new ChDepositResponseBean();
    }

    /**
     * Create an instance of {@link ChAchTransactionKeyRequestBean }
     * 
     */
    public ChAchTransactionKeyRequestBean createChAchTransactionKeyRequestBean() {
        return new ChAchTransactionKeyRequestBean();
    }

    /**
     * Create an instance of {@link ChCashChequeRequestBean }
     * 
     */
    public ChCashChequeRequestBean createChCashChequeRequestBean() {
        return new ChCashChequeRequestBean();
    }

    /**
     * Create an instance of {@link ChIbanToDepositRequestBean }
     * 
     */
    public ChIbanToDepositRequestBean createChIbanToDepositRequestBean() {
        return new ChIbanToDepositRequestBean();
    }

    /**
     * Create an instance of {@link ChCardAuthorizeParamsBean }
     * 
     */
    public ChCardAuthorizeParamsBean createChCardAuthorizeParamsBean() {
        return new ChCardAuthorizeParamsBean();
    }

    /**
     * Create an instance of {@link ChPaymentBillStatementBean }
     * 
     */
    public ChPaymentBillStatementBean createChPaymentBillStatementBean() {
        return new ChPaymentBillStatementBean();
    }

    /**
     * Create an instance of {@link ChCardCorrectLinkedAccountsResponseBean }
     * 
     */
    public ChCardCorrectLinkedAccountsResponseBean createChCardCorrectLinkedAccountsResponseBean() {
        return new ChCardCorrectLinkedAccountsResponseBean();
    }

    /**
     * Create an instance of {@link ChBeOpenedDepositRequestBean }
     * 
     */
    public ChBeOpenedDepositRequestBean createChBeOpenedDepositRequestBean() {
        return new ChBeOpenedDepositRequestBean();
    }

    /**
     * Create an instance of {@link ChCardCreatePasswordRequestBean }
     * 
     */
    public ChCardCreatePasswordRequestBean createChCardCreatePasswordRequestBean() {
        return new ChCardCreatePasswordRequestBean();
    }

    /**
     * Create an instance of {@link ChChequeSheetsResponseBean }
     * 
     */
    public ChChequeSheetsResponseBean createChChequeSheetsResponseBean() {
        return new ChChequeSheetsResponseBean();
    }

    /**
     * Create an instance of {@link ChCustomerSignaturesResponseBean }
     * 
     */
    public ChCustomerSignaturesResponseBean createChCustomerSignaturesResponseBean() {
        return new ChCustomerSignaturesResponseBean();
    }

    /**
     * Create an instance of {@link ChReqStatusRequestBean }
     * 
     */
    public ChReqStatusRequestBean createChReqStatusRequestBean() {
        return new ChReqStatusRequestBean();
    }

    /**
     * Create an instance of {@link ChSellReportRequestBean }
     * 
     */
    public ChSellReportRequestBean createChSellReportRequestBean() {
        return new ChSellReportRequestBean();
    }

    /**
     * Create an instance of {@link ChBatchTransferRequestBean }
     * 
     */
    public ChBatchTransferRequestBean createChBatchTransferRequestBean() {
        return new ChBatchTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChAchTransferKeyRequestBean }
     * 
     */
    public ChAchTransferKeyRequestBean createChAchTransferKeyRequestBean() {
        return new ChAchTransferKeyRequestBean();
    }

    /**
     * Create an instance of {@link ChCustomReturnRequestBean }
     * 
     */
    public ChCustomReturnRequestBean createChCustomReturnRequestBean() {
        return new ChCustomReturnRequestBean();
    }

    /**
     * Create an instance of {@link ChIBANEnquiryResponse }
     * 
     */
    public ChIBANEnquiryResponse createChIBANEnquiryResponse() {
        return new ChIBANEnquiryResponse();
    }

    /**
     * Create an instance of {@link ChBranchAllowedDepositsToOpenRequestBean }
     * 
     */
    public ChBranchAllowedDepositsToOpenRequestBean createChBranchAllowedDepositsToOpenRequestBean() {
        return new ChBranchAllowedDepositsToOpenRequestBean();
    }

    /**
     * Create an instance of {@link ChCardOwnerRequestBean }
     * 
     */
    public ChCardOwnerRequestBean createChCardOwnerRequestBean() {
        return new ChCardOwnerRequestBean();
    }

    /**
     * Create an instance of {@link ChInquiryNaturalPersonOpenSingleDepositResponseBean }
     * 
     */
    public ChInquiryNaturalPersonOpenSingleDepositResponseBean createChInquiryNaturalPersonOpenSingleDepositResponseBean() {
        return new ChInquiryNaturalPersonOpenSingleDepositResponseBean();
    }

    /**
     * Create an instance of {@link ChFinancialServiceResponseBean }
     * 
     */
    public ChFinancialServiceResponseBean createChFinancialServiceResponseBean() {
        return new ChFinancialServiceResponseBean();
    }

    /**
     * Create an instance of {@link ChBranchInfoBean }
     * 
     */
    public ChBranchInfoBean createChBranchInfoBean() {
        return new ChBranchInfoBean();
    }

    /**
     * Create an instance of {@link ChUserSMSSettingResponseBean }
     * 
     */
    public ChUserSMSSettingResponseBean createChUserSMSSettingResponseBean() {
        return new ChUserSMSSettingResponseBean();
    }

    /**
     * Create an instance of {@link ChPaymentServiceInputParameterBean }
     * 
     */
    public ChPaymentServiceInputParameterBean createChPaymentServiceInputParameterBean() {
        return new ChPaymentServiceInputParameterBean();
    }

    /**
     * Create an instance of {@link ChRtgTransferSearchRequestBean }
     * 
     */
    public ChRtgTransferSearchRequestBean createChRtgTransferSearchRequestBean() {
        return new ChRtgTransferSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChLoginResponseBean }
     * 
     */
    public ChLoginResponseBean createChLoginResponseBean() {
        return new ChLoginResponseBean();
    }

    /**
     * Create an instance of {@link ChDefaultStatementRequestBean }
     * 
     */
    public ChDefaultStatementRequestBean createChDefaultStatementRequestBean() {
        return new ChDefaultStatementRequestBean();
    }

    /**
     * Create an instance of {@link ChBriefTransferConstraintInfoResponseBean }
     * 
     */
    public ChBriefTransferConstraintInfoResponseBean createChBriefTransferConstraintInfoResponseBean() {
        return new ChBriefTransferConstraintInfoResponseBean();
    }

    /**
     * Create an instance of {@link ChUserInfoRequestBean }
     * 
     */
    public ChUserInfoRequestBean createChUserInfoRequestBean() {
        return new ChUserInfoRequestBean();
    }

    /**
     * Create an instance of {@link ChDestinationCardOwnerRequest }
     * 
     */
    public ChDestinationCardOwnerRequest createChDestinationCardOwnerRequest() {
        return new ChDestinationCardOwnerRequest();
    }

    /**
     * Create an instance of {@link ChLoansResponseBean }
     * 
     */
    public ChLoansResponseBean createChLoansResponseBean() {
        return new ChLoansResponseBean();
    }

    /**
     * Create an instance of {@link ChFullCustomerInfoResponseBean }
     * 
     */
    public ChFullCustomerInfoResponseBean createChFullCustomerInfoResponseBean() {
        return new ChFullCustomerInfoResponseBean();
    }

    /**
     * Create an instance of {@link ChDepositOwnerRequestBean }
     * 
     */
    public ChDepositOwnerRequestBean createChDepositOwnerRequestBean() {
        return new ChDepositOwnerRequestBean();
    }

    /**
     * Create an instance of {@link ChCardBalanceResponseBean }
     * 
     */
    public ChCardBalanceResponseBean createChCardBalanceResponseBean() {
        return new ChCardBalanceResponseBean();
    }

    /**
     * Create an instance of {@link ChCurrencyRateResponseBean }
     * 
     */
    public ChCurrencyRateResponseBean createChCurrencyRateResponseBean() {
        return new ChCurrencyRateResponseBean();
    }

    /**
     * Create an instance of {@link ChBillPaymentAccountRequestBean }
     * 
     */
    public ChBillPaymentAccountRequestBean createChBillPaymentAccountRequestBean() {
        return new ChBillPaymentAccountRequestBean();
    }

    /**
     * Create an instance of {@link ChAchNormalTransferRequestBean }
     * 
     */
    public ChAchNormalTransferRequestBean createChAchNormalTransferRequestBean() {
        return new ChAchNormalTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChPaymentResponseBean }
     * 
     */
    public ChPaymentResponseBean createChPaymentResponseBean() {
        return new ChPaymentResponseBean();
    }

    /**
     * Create an instance of {@link ChAutoTransferDetailsResponseBean }
     * 
     */
    public ChAutoTransferDetailsResponseBean createChAutoTransferDetailsResponseBean() {
        return new ChAutoTransferDetailsResponseBean();
    }

    /**
     * Create an instance of {@link ChCardsSearchRequestBean }
     * 
     */
    public ChCardsSearchRequestBean createChCardsSearchRequestBean() {
        return new ChCardsSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChStatementBean }
     * 
     */
    public ChStatementBean createChStatementBean() {
        return new ChStatementBean();
    }

    /**
     * Create an instance of {@link ChTransferDetailBean }
     * 
     */
    public ChTransferDetailBean createChTransferDetailBean() {
        return new ChTransferDetailBean();
    }

    /**
     * Create an instance of {@link ChPaymentBillStatementRequestBean }
     * 
     */
    public ChPaymentBillStatementRequestBean createChPaymentBillStatementRequestBean() {
        return new ChPaymentBillStatementRequestBean();
    }

    /**
     * Create an instance of {@link ChBatchBillInfoBean }
     * 
     */
    public ChBatchBillInfoBean createChBatchBillInfoBean() {
        return new ChBatchBillInfoBean();
    }

    /**
     * Create an instance of {@link ChInquiryNaturalPersonOpenSingleDepositRequestBean }
     * 
     */
    public ChInquiryNaturalPersonOpenSingleDepositRequestBean createChInquiryNaturalPersonOpenSingleDepositRequestBean() {
        return new ChInquiryNaturalPersonOpenSingleDepositRequestBean();
    }

    /**
     * Create an instance of {@link ChThirdPartySearchRequestBean }
     * 
     */
    public ChThirdPartySearchRequestBean createChThirdPartySearchRequestBean() {
        return new ChThirdPartySearchRequestBean();
    }

    /**
     * Create an instance of {@link ChSearchBean }
     * 
     */
    public ChSearchBean createChSearchBean() {
        return new ChSearchBean();
    }

    /**
     * Create an instance of {@link ChBankResponseBean }
     * 
     */
    public ChBankResponseBean createChBankResponseBean() {
        return new ChBankResponseBean();
    }

    /**
     * Create an instance of {@link ChLoanPaymentResponseBean }
     * 
     */
    public ChLoanPaymentResponseBean createChLoanPaymentResponseBean() {
        return new ChLoanPaymentResponseBean();
    }

    /**
     * Create an instance of {@link ChSearchCardBean }
     * 
     */
    public ChSearchCardBean createChSearchCardBean() {
        return new ChSearchCardBean();
    }

    /**
     * Create an instance of {@link ChBranchBean }
     * 
     */
    public ChBranchBean createChBranchBean() {
        return new ChBranchBean();
    }

    /**
     * Create an instance of {@link ChLoansSearchRequestBean }
     * 
     */
    public ChLoansSearchRequestBean createChLoansSearchRequestBean() {
        return new ChLoansSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChCardExtendRequestBean }
     * 
     */
    public ChCardExtendRequestBean createChCardExtendRequestBean() {
        return new ChCardExtendRequestBean();
    }

    /**
     * Create an instance of {@link ChCardCorrectLinkedAccountsRequestBean }
     * 
     */
    public ChCardCorrectLinkedAccountsRequestBean createChCardCorrectLinkedAccountsRequestBean() {
        return new ChCardCorrectLinkedAccountsRequestBean();
    }

    /**
     * Create an instance of {@link ChAchTransactionSearchRequestBean }
     * 
     */
    public ChAchTransactionSearchRequestBean createChAchTransactionSearchRequestBean() {
        return new ChAchTransactionSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChCurrencyRateRequestBean }
     * 
     */
    public ChCurrencyRateRequestBean createChCurrencyRateRequestBean() {
        return new ChCurrencyRateRequestBean();
    }

    /**
     * Create an instance of {@link ChDefaultStatementResponseBean }
     * 
     */
    public ChDefaultStatementResponseBean createChDefaultStatementResponseBean() {
        return new ChDefaultStatementResponseBean();
    }

    /**
     * Create an instance of {@link ChIBANEnquiryRequest }
     * 
     */
    public ChIBANEnquiryRequest createChIBANEnquiryRequest() {
        return new ChIBANEnquiryRequest();
    }

    /**
     * Create an instance of {@link ChChargeCardResponseBean }
     * 
     */
    public ChChargeCardResponseBean createChChargeCardResponseBean() {
        return new ChChargeCardResponseBean();
    }

    /**
     * Create an instance of {@link ChInstitutionsResponseBean }
     * 
     */
    public ChInstitutionsResponseBean createChInstitutionsResponseBean() {
        return new ChInstitutionsResponseBean();
    }

    /**
     * Create an instance of {@link ChNaturalPersonOpenSingleDepositRequestBean }
     * 
     */
    public ChNaturalPersonOpenSingleDepositRequestBean createChNaturalPersonOpenSingleDepositRequestBean() {
        return new ChNaturalPersonOpenSingleDepositRequestBean();
    }

    /**
     * Create an instance of {@link ChCreditDossierResponseBeans }
     * 
     */
    public ChCreditDossierResponseBeans createChCreditDossierResponseBeans() {
        return new ChCreditDossierResponseBeans();
    }

    /**
     * Create an instance of {@link ChNormalAchTransferResultBean }
     * 
     */
    public ChNormalAchTransferResultBean createChNormalAchTransferResultBean() {
        return new ChNormalAchTransferResultBean();
    }

    /**
     * Create an instance of {@link ChSavingDepositBean }
     * 
     */
    public ChSavingDepositBean createChSavingDepositBean() {
        return new ChSavingDepositBean();
    }

    /**
     * Create an instance of {@link ChBranchSearchRequestBean }
     * 
     */
    public ChBranchSearchRequestBean createChBranchSearchRequestBean() {
        return new ChBranchSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChAcceptAchTransferRequestBean }
     * 
     */
    public ChAcceptAchTransferRequestBean createChAcceptAchTransferRequestBean() {
        return new ChAcceptAchTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChLoanRowBean }
     * 
     */
    public ChLoanRowBean createChLoanRowBean() {
        return new ChLoanRowBean();
    }

    /**
     * Create an instance of {@link ActivityDto }
     * 
     */
    public ActivityDto createActivityDto() {
        return new ActivityDto();
    }

    /**
     * Create an instance of {@link ChTransferChequesResponseBean }
     * 
     */
    public ChTransferChequesResponseBean createChTransferChequesResponseBean() {
        return new ChTransferChequesResponseBean();
    }

    /**
     * Create an instance of {@link ChBeOpenedDepositResponseBean }
     * 
     */
    public ChBeOpenedDepositResponseBean createChBeOpenedDepositResponseBean() {
        return new ChBeOpenedDepositResponseBean();
    }

    /**
     * Create an instance of {@link ChBatchBillPaymentRequestBean }
     * 
     */
    public ChBatchBillPaymentRequestBean createChBatchBillPaymentRequestBean() {
        return new ChBatchBillPaymentRequestBean();
    }

    /**
     * Create an instance of {@link ChCustomerExistenceInquiryRequestBean }
     * 
     */
    public ChCustomerExistenceInquiryRequestBean createChCustomerExistenceInquiryRequestBean() {
        return new ChCustomerExistenceInquiryRequestBean();
    }

    /**
     * Create an instance of {@link ChChequeBooksResponseBean }
     * 
     */
    public ChChequeBooksResponseBean createChChequeBooksResponseBean() {
        return new ChChequeBooksResponseBean();
    }

    /**
     * Create an instance of {@link ChBatchTransferResponseBean }
     * 
     */
    public ChBatchTransferResponseBean createChBatchTransferResponseBean() {
        return new ChBatchTransferResponseBean();
    }

    /**
     * Create an instance of {@link ChAchTransactionBean }
     * 
     */
    public ChAchTransactionBean createChAchTransactionBean() {
        return new ChAchTransactionBean();
    }

    /**
     * Create an instance of {@link ChLoanPaymentRequestBean }
     * 
     */
    public ChLoanPaymentRequestBean createChLoanPaymentRequestBean() {
        return new ChLoanPaymentRequestBean();
    }

    /**
     * Create an instance of {@link ChServiceConstraintInformationBean }
     * 
     */
    public ChServiceConstraintInformationBean createChServiceConstraintInformationBean() {
        return new ChServiceConstraintInformationBean();
    }

    /**
     * Create an instance of {@link ChDestinationBatchTransferInfoBean }
     * 
     */
    public ChDestinationBatchTransferInfoBean createChDestinationBatchTransferInfoBean() {
        return new ChDestinationBatchTransferInfoBean();
    }

    /**
     * Create an instance of {@link ChSendReversalRequest }
     * 
     */
    public ChSendReversalRequest createChSendReversalRequest() {
        return new ChSendReversalRequest();
    }

    /**
     * Create an instance of {@link ChDestinationAchAutoTransactionBean }
     * 
     */
    public ChDestinationAchAutoTransactionBean createChDestinationAchAutoTransactionBean() {
        return new ChDestinationAchAutoTransactionBean();
    }

    /**
     * Create an instance of {@link ChSellDetailRequestBean }
     * 
     */
    public ChSellDetailRequestBean createChSellDetailRequestBean() {
        return new ChSellDetailRequestBean();
    }

    /**
     * Create an instance of {@link ChStatusBean }
     * 
     */
    public ChStatusBean createChStatusBean() {
        return new ChStatusBean();
    }

    /**
     * Create an instance of {@link ChStatementSearchRequestBean }
     * 
     */
    public ChStatementSearchRequestBean createChStatementSearchRequestBean() {
        return new ChStatementSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChAchTransactionKeysRequestBean }
     * 
     */
    public ChAchTransactionKeysRequestBean createChAchTransactionKeysRequestBean() {
        return new ChAchTransactionKeysRequestBean();
    }

    /**
     * Create an instance of {@link ChChangePasswordRequestBean }
     * 
     */
    public ChChangePasswordRequestBean createChChangePasswordRequestBean() {
        return new ChChangePasswordRequestBean();
    }

    /**
     * Create an instance of {@link ChCurrencyDetailBean }
     * 
     */
    public ChCurrencyDetailBean createChCurrencyDetailBean() {
        return new ChCurrencyDetailBean();
    }

    /**
     * Create an instance of {@link ChDepositCustomerRequestBean }
     * 
     */
    public ChDepositCustomerRequestBean createChDepositCustomerRequestBean() {
        return new ChDepositCustomerRequestBean();
    }

    /**
     * Create an instance of {@link ChCardExtendResponseBean }
     * 
     */
    public ChCardExtendResponseBean createChCardExtendResponseBean() {
        return new ChCardExtendResponseBean();
    }

    /**
     * Create an instance of {@link ChFavoriteDepositNumberRequestBeans }
     * 
     */
    public ChFavoriteDepositNumberRequestBeans createChFavoriteDepositNumberRequestBeans() {
        return new ChFavoriteDepositNumberRequestBeans();
    }

    /**
     * Create an instance of {@link ChBatchBillPaymentBean }
     * 
     */
    public ChBatchBillPaymentBean createChBatchBillPaymentBean() {
        return new ChBatchBillPaymentBean();
    }

    /**
     * Create an instance of {@link ChCardIssuingResponseBean }
     * 
     */
    public ChCardIssuingResponseBean createChCardIssuingResponseBean() {
        return new ChCardIssuingResponseBean();
    }

    /**
     * Create an instance of {@link ChFundTransferRequestBean }
     * 
     */
    public ChFundTransferRequestBean createChFundTransferRequestBean() {
        return new ChFundTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChAchAutoTransferResultBean }
     * 
     */
    public ChAchAutoTransferResultBean createChAchAutoTransferResultBean() {
        return new ChAchAutoTransferResultBean();
    }

    /**
     * Create an instance of {@link ChCardCreatePasswordResponseBean }
     * 
     */
    public ChCardCreatePasswordResponseBean createChCardCreatePasswordResponseBean() {
        return new ChCardCreatePasswordResponseBean();
    }

    /**
     * Create an instance of {@link ChLoanDetailSearchRequestBean }
     * 
     */
    public ChLoanDetailSearchRequestBean createChLoanDetailSearchRequestBean() {
        return new ChLoanDetailSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChAddCardDepositRequestBean }
     * 
     */
    public ChAddCardDepositRequestBean createChAddCardDepositRequestBean() {
        return new ChAddCardDepositRequestBean();
    }

    /**
     * Create an instance of {@link ChAtmStatusCassetteResponseBean }
     * 
     */
    public ChAtmStatusCassetteResponseBean createChAtmStatusCassetteResponseBean() {
        return new ChAtmStatusCassetteResponseBean();
    }

    /**
     * Create an instance of {@link ChHotCardByPinRequestBean }
     * 
     */
    public ChHotCardByPinRequestBean createChHotCardByPinRequestBean() {
        return new ChHotCardByPinRequestBean();
    }

    /**
     * Create an instance of {@link ChBaseCreditBillSearchRequestBean }
     * 
     */
    public ChBaseCreditBillSearchRequestBean createChBaseCreditBillSearchRequestBean() {
        return new ChBaseCreditBillSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChCustomerAddressBean }
     * 
     */
    public ChCustomerAddressBean createChCustomerAddressBean() {
        return new ChCustomerAddressBean();
    }

    /**
     * Create an instance of {@link ChDepositCustomerBean }
     * 
     */
    public ChDepositCustomerBean createChDepositCustomerBean() {
        return new ChDepositCustomerBean();
    }

    /**
     * Create an instance of {@link ChRegisterChequeRequestBean }
     * 
     */
    public ChRegisterChequeRequestBean createChRegisterChequeRequestBean() {
        return new ChRegisterChequeRequestBean();
    }

    /**
     * Create an instance of {@link ChSellReportResponseBean }
     * 
     */
    public ChSellReportResponseBean createChSellReportResponseBean() {
        return new ChSellReportResponseBean();
    }

    /**
     * Create an instance of {@link ChRtgsTransferDetailResponseBean }
     * 
     */
    public ChRtgsTransferDetailResponseBean createChRtgsTransferDetailResponseBean() {
        return new ChRtgsTransferDetailResponseBean();
    }

    /**
     * Create an instance of {@link ChCustomerSignatureBean }
     * 
     */
    public ChCustomerSignatureBean createChCustomerSignatureBean() {
        return new ChCustomerSignatureBean();
    }

    /**
     * Create an instance of {@link ChNormalTransferResponseBean }
     * 
     */
    public ChNormalTransferResponseBean createChNormalTransferResponseBean() {
        return new ChNormalTransferResponseBean();
    }

    /**
     * Create an instance of {@link ChCardPurchaseResponseBean }
     * 
     */
    public ChCardPurchaseResponseBean createChCardPurchaseResponseBean() {
        return new ChCardPurchaseResponseBean();
    }

    /**
     * Create an instance of {@link ChBranchBalanceBean }
     * 
     */
    public ChBranchBalanceBean createChBranchBalanceBean() {
        return new ChBranchBalanceBean();
    }

    /**
     * Create an instance of {@link ChTransferChequeBean }
     * 
     */
    public ChTransferChequeBean createChTransferChequeBean() {
        return new ChTransferChequeBean();
    }

    /**
     * Create an instance of {@link ChNormalTransferRequestBean }
     * 
     */
    public ChNormalTransferRequestBean createChNormalTransferRequestBean() {
        return new ChNormalTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChPaymentServiceGroupBean }
     * 
     */
    public ChPaymentServiceGroupBean createChPaymentServiceGroupBean() {
        return new ChPaymentServiceGroupBean();
    }

    /**
     * Create an instance of {@link ChSecondPasswordRequestBean }
     * 
     */
    public ChSecondPasswordRequestBean createChSecondPasswordRequestBean() {
        return new ChSecondPasswordRequestBean();
    }

    /**
     * Create an instance of {@link ChInvestmentDepositBean }
     * 
     */
    public ChInvestmentDepositBean createChInvestmentDepositBean() {
        return new ChInvestmentDepositBean();
    }

    /**
     * Create an instance of {@link ChTransferConstraintRequestBean }
     * 
     */
    public ChTransferConstraintRequestBean createChTransferConstraintRequestBean() {
        return new ChTransferConstraintRequestBean();
    }

    /**
     * Create an instance of {@link ChDepositCustomerResponseBean }
     * 
     */
    public ChDepositCustomerResponseBean createChDepositCustomerResponseBean() {
        return new ChDepositCustomerResponseBean();
    }

    /**
     * Create an instance of {@link ChUpdateDefaultContactInfoRequestBean }
     * 
     */
    public ChUpdateDefaultContactInfoRequestBean createChUpdateDefaultContactInfoRequestBean() {
        return new ChUpdateDefaultContactInfoRequestBean();
    }

    /**
     * Create an instance of {@link ChCardDepositRequestBean }
     * 
     */
    public ChCardDepositRequestBean createChCardDepositRequestBean() {
        return new ChCardDepositRequestBean();
    }

    /**
     * Create an instance of {@link ChBillPaymentRequestBean }
     * 
     */
    public ChBillPaymentRequestBean createChBillPaymentRequestBean() {
        return new ChBillPaymentRequestBean();
    }

    /**
     * Create an instance of {@link ChBlockingStatusBean }
     * 
     */
    public ChBlockingStatusBean createChBlockingStatusBean() {
        return new ChBlockingStatusBean();
    }

    /**
     * Create an instance of {@link ChCardInfoRetrievationRequestBean }
     * 
     */
    public ChCardInfoRetrievationRequestBean createChCardInfoRetrievationRequestBean() {
        return new ChCardInfoRetrievationRequestBean();
    }

    /**
     * Create an instance of {@link ChTopupChargeRequest }
     * 
     */
    public ChTopupChargeRequest createChTopupChargeRequest() {
        return new ChTopupChargeRequest();
    }

    /**
     * Create an instance of {@link ChChargeCardResponseBeans }
     * 
     */
    public ChChargeCardResponseBeans createChChargeCardResponseBeans() {
        return new ChChargeCardResponseBeans();
    }

    /**
     * Create an instance of {@link ChCardStatementBean }
     * 
     */
    public ChCardStatementBean createChCardStatementBean() {
        return new ChCardStatementBean();
    }

    /**
     * Create an instance of {@link ChCardIssuingAllowedDepositsToOpenRequestBean }
     * 
     */
    public ChCardIssuingAllowedDepositsToOpenRequestBean createChCardIssuingAllowedDepositsToOpenRequestBean() {
        return new ChCardIssuingAllowedDepositsToOpenRequestBean();
    }

    /**
     * Create an instance of {@link ChSMSSettingResponseBean }
     * 
     */
    public ChSMSSettingResponseBean createChSMSSettingResponseBean() {
        return new ChSMSSettingResponseBean();
    }

    /**
     * Create an instance of {@link ChSignRequestBean }
     * 
     */
    public ChSignRequestBean createChSignRequestBean() {
        return new ChSignRequestBean();
    }

    /**
     * Create an instance of {@link ChDestinationBatchTransferBean }
     * 
     */
    public ChDestinationBatchTransferBean createChDestinationBatchTransferBean() {
        return new ChDestinationBatchTransferBean();
    }

    /**
     * Create an instance of {@link ChRelatedDepositOfCardBean }
     * 
     */
    public ChRelatedDepositOfCardBean createChRelatedDepositOfCardBean() {
        return new ChRelatedDepositOfCardBean();
    }

    /**
     * Create an instance of {@link ChDestinationAchAutoTransactionResultBean }
     * 
     */
    public ChDestinationAchAutoTransactionResultBean createChDestinationAchAutoTransactionResultBean() {
        return new ChDestinationAchAutoTransactionResultBean();
    }

    /**
     * Create an instance of {@link ChChequeBookRequestBean }
     * 
     */
    public ChChequeBookRequestBean createChChequeBookRequestBean() {
        return new ChChequeBookRequestBean();
    }

    /**
     * Create an instance of {@link ChDisableTransferRequestBean }
     * 
     */
    public ChDisableTransferRequestBean createChDisableTransferRequestBean() {
        return new ChDisableTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChPaymentPreviewResponseBean }
     * 
     */
    public ChPaymentPreviewResponseBean createChPaymentPreviewResponseBean() {
        return new ChPaymentPreviewResponseBean();
    }

    /**
     * Create an instance of {@link ChAutoAchTransferRequestBean }
     * 
     */
    public ChAutoAchTransferRequestBean createChAutoAchTransferRequestBean() {
        return new ChAutoAchTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChDepositNumberToIbanRequestBean }
     * 
     */
    public ChDepositNumberToIbanRequestBean createChDepositNumberToIbanRequestBean() {
        return new ChDepositNumberToIbanRequestBean();
    }

    /**
     * Create an instance of {@link ChCardOwnerResponseBean }
     * 
     */
    public ChCardOwnerResponseBean createChCardOwnerResponseBean() {
        return new ChCardOwnerResponseBean();
    }

    /**
     * Create an instance of {@link ChRtgsTransferResponseBean }
     * 
     */
    public ChRtgsTransferResponseBean createChRtgsTransferResponseBean() {
        return new ChRtgsTransferResponseBean();
    }

    /**
     * Create an instance of {@link ChInquiryCardIssuingRequestBean }
     * 
     */
    public ChInquiryCardIssuingRequestBean createChInquiryCardIssuingRequestBean() {
        return new ChInquiryCardIssuingRequestBean();
    }

    /**
     * Create an instance of {@link ChAchTransferSummeryFilterBean }
     * 
     */
    public ChAchTransferSummeryFilterBean createChAchTransferSummeryFilterBean() {
        return new ChAchTransferSummeryFilterBean();
    }

    /**
     * Create an instance of {@link ChCustomerExistenceInquiryResponseBean }
     * 
     */
    public ChCustomerExistenceInquiryResponseBean createChCustomerExistenceInquiryResponseBean() {
        return new ChCustomerExistenceInquiryResponseBean();
    }

    /**
     * Create an instance of {@link ChAchTransactionsResponseBean }
     * 
     */
    public ChAchTransactionsResponseBean createChAchTransactionsResponseBean() {
        return new ChAchTransactionsResponseBean();
    }

    /**
     * Create an instance of {@link ChSignedDepositRequestBean }
     * 
     */
    public ChSignedDepositRequestBean createChSignedDepositRequestBean() {
        return new ChSignedDepositRequestBean();
    }

    /**
     * Create an instance of {@link ChCustomerSignaturesRequestBean }
     * 
     */
    public ChCustomerSignaturesRequestBean createChCustomerSignaturesRequestBean() {
        return new ChCustomerSignaturesRequestBean();
    }

    /**
     * Create an instance of {@link ChAchTransferSearchRequestBean }
     * 
     */
    public ChAchTransferSearchRequestBean createChAchTransferSearchRequestBean() {
        return new ChAchTransferSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChRtgsNormalTransferRequestBean }
     * 
     */
    public ChRtgsNormalTransferRequestBean createChRtgsNormalTransferRequestBean() {
        return new ChRtgsNormalTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChCustomerAddressRequestBean }
     * 
     */
    public ChCustomerAddressRequestBean createChCustomerAddressRequestBean() {
        return new ChCustomerAddressRequestBean();
    }

    /**
     * Create an instance of {@link ChBillTransactionSearchRequestBean }
     * 
     */
    public ChBillTransactionSearchRequestBean createChBillTransactionSearchRequestBean() {
        return new ChBillTransactionSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChDepositSearchRequestBean }
     * 
     */
    public ChDepositSearchRequestBean createChDepositSearchRequestBean() {
        return new ChDepositSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChCardBean }
     * 
     */
    public ChCardBean createChCardBean() {
        return new ChCardBean();
    }

    /**
     * Create an instance of {@link ChCardTransactionBean }
     * 
     */
    public ChCardTransactionBean createChCardTransactionBean() {
        return new ChCardTransactionBean();
    }

    /**
     * Create an instance of {@link ChDepositBean }
     * 
     */
    public ChDepositBean createChDepositBean() {
        return new ChDepositBean();
    }

    /**
     * Create an instance of {@link ChBaseSearchChequeBookRequestBean }
     * 
     */
    public ChBaseSearchChequeBookRequestBean createChBaseSearchChequeBookRequestBean() {
        return new ChBaseSearchChequeBookRequestBean();
    }

    /**
     * Create an instance of {@link ChAutoTransactionPeriodBean }
     * 
     */
    public ChAutoTransactionPeriodBean createChAutoTransactionPeriodBean() {
        return new ChAutoTransactionPeriodBean();
    }

    /**
     * Create an instance of {@link ChAchTransfersResponseBean }
     * 
     */
    public ChAchTransfersResponseBean createChAchTransfersResponseBean() {
        return new ChAchTransfersResponseBean();
    }

    /**
     * Create an instance of {@link ChInstitutionalTransferRequestBean }
     * 
     */
    public ChInstitutionalTransferRequestBean createChInstitutionalTransferRequestBean() {
        return new ChInstitutionalTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChPanRequestBean }
     * 
     */
    public ChPanRequestBean createChPanRequestBean() {
        return new ChPanRequestBean();
    }

    /**
     * Create an instance of {@link ChDestinationCardOwnerResponse }
     * 
     */
    public ChDestinationCardOwnerResponse createChDestinationCardOwnerResponse() {
        return new ChDestinationCardOwnerResponse();
    }

    /**
     * Create an instance of {@link ChChequeSheetBean }
     * 
     */
    public ChChequeSheetBean createChChequeSheetBean() {
        return new ChChequeSheetBean();
    }

    /**
     * Create an instance of {@link ChDepositRateInquiryResponseBean }
     * 
     */
    public ChDepositRateInquiryResponseBean createChDepositRateInquiryResponseBean() {
        return new ChDepositRateInquiryResponseBean();
    }

    /**
     * Create an instance of {@link ChDefineCustomerSignatureSampleResponseBean }
     * 
     */
    public ChDefineCustomerSignatureSampleResponseBean createChDefineCustomerSignatureSampleResponseBean() {
        return new ChDefineCustomerSignatureSampleResponseBean();
    }

    /**
     * Create an instance of {@link ChSignedDepositResponseBean }
     * 
     */
    public ChSignedDepositResponseBean createChSignedDepositResponseBean() {
        return new ChSignedDepositResponseBean();
    }

    /**
     * Create an instance of {@link ChNaturalPersonOpenSingleDepositResponseBean }
     * 
     */
    public ChNaturalPersonOpenSingleDepositResponseBean createChNaturalPersonOpenSingleDepositResponseBean() {
        return new ChNaturalPersonOpenSingleDepositResponseBean();
    }

    /**
     * Create an instance of {@link ChCardPaginationBean }
     * 
     */
    public ChCardPaginationBean createChCardPaginationBean() {
        return new ChCardPaginationBean();
    }

    /**
     * Create an instance of {@link ChAmountBean }
     * 
     */
    public ChAmountBean createChAmountBean() {
        return new ChAmountBean();
    }

    /**
     * Create an instance of {@link ChMailRequestBean }
     * 
     */
    public ChMailRequestBean createChMailRequestBean() {
        return new ChMailRequestBean();
    }

    /**
     * Create an instance of {@link ChDestinationAchTransactionBean }
     * 
     */
    public ChDestinationAchTransactionBean createChDestinationAchTransactionBean() {
        return new ChDestinationAchTransactionBean();
    }

    /**
     * Create an instance of {@link ChReqStatusBean }
     * 
     */
    public ChReqStatusBean createChReqStatusBean() {
        return new ChReqStatusBean();
    }

    /**
     * Create an instance of {@link ChLoanInfoBean }
     * 
     */
    public ChLoanInfoBean createChLoanInfoBean() {
        return new ChLoanInfoBean();
    }

    /**
     * Create an instance of {@link ChCardChangePinRequestBean }
     * 
     */
    public ChCardChangePinRequestBean createChCardChangePinRequestBean() {
        return new ChCardChangePinRequestBean();
    }

    /**
     * Create an instance of {@link ChCardTransactionsResponseBean }
     * 
     */
    public ChCardTransactionsResponseBean createChCardTransactionsResponseBean() {
        return new ChCardTransactionsResponseBean();
    }

    /**
     * Create an instance of {@link ChCreditBillPaymentResponseBean }
     * 
     */
    public ChCreditBillPaymentResponseBean createChCreditBillPaymentResponseBean() {
        return new ChCreditBillPaymentResponseBean();
    }

    /**
     * Create an instance of {@link ChCustomReturnResponseBean }
     * 
     */
    public ChCustomReturnResponseBean createChCustomReturnResponseBean() {
        return new ChCustomReturnResponseBean();
    }

    /**
     * Create an instance of {@link ChFullCustomerInfoRequestBean }
     * 
     */
    public ChFullCustomerInfoRequestBean createChFullCustomerInfoRequestBean() {
        return new ChFullCustomerInfoRequestBean();
    }

    /**
     * Create an instance of {@link ChCardIssuingAllowedDepositsToOpenResponseBean }
     * 
     */
    public ChCardIssuingAllowedDepositsToOpenResponseBean createChCardIssuingAllowedDepositsToOpenResponseBean() {
        return new ChCardIssuingAllowedDepositsToOpenResponseBean();
    }

    /**
     * Create an instance of {@link ChAchBatchTransferResultBean }
     * 
     */
    public ChAchBatchTransferResultBean createChAchBatchTransferResultBean() {
        return new ChAchBatchTransferResultBean();
    }

    /**
     * Create an instance of {@link ChCardStatementResponseBean }
     * 
     */
    public ChCardStatementResponseBean createChCardStatementResponseBean() {
        return new ChCardStatementResponseBean();
    }

    /**
     * Create an instance of {@link ChRtgTransferDetailSearchRequestBean }
     * 
     */
    public ChRtgTransferDetailSearchRequestBean createChRtgTransferDetailSearchRequestBean() {
        return new ChRtgTransferDetailSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChGuarantiesRequestBean }
     * 
     */
    public ChGuarantiesRequestBean createChGuarantiesRequestBean() {
        return new ChGuarantiesRequestBean();
    }

    /**
     * Create an instance of {@link ChBlockChequeResponseBean }
     * 
     */
    public ChBlockChequeResponseBean createChBlockChequeResponseBean() {
        return new ChBlockChequeResponseBean();
    }

    /**
     * Create an instance of {@link ChTransactionResponseBean }
     * 
     */
    public ChTransactionResponseBean createChTransactionResponseBean() {
        return new ChTransactionResponseBean();
    }

    /**
     * Create an instance of {@link ChBlockChequeRequestBean }
     * 
     */
    public ChBlockChequeRequestBean createChBlockChequeRequestBean() {
        return new ChBlockChequeRequestBean();
    }

    /**
     * Create an instance of {@link ChNormalRtgsTransferResponseBean }
     * 
     */
    public ChNormalRtgsTransferResponseBean createChNormalRtgsTransferResponseBean() {
        return new ChNormalRtgsTransferResponseBean();
    }

    /**
     * Create an instance of {@link ChPaymentPreviewRequestBean }
     * 
     */
    public ChPaymentPreviewRequestBean createChPaymentPreviewRequestBean() {
        return new ChPaymentPreviewRequestBean();
    }

    /**
     * Create an instance of {@link ChFavoriteDepositResponseBean }
     * 
     */
    public ChFavoriteDepositResponseBean createChFavoriteDepositResponseBean() {
        return new ChFavoriteDepositResponseBean();
    }

    /**
     * Create an instance of {@link ChPaymentRequestBean }
     * 
     */
    public ChPaymentRequestBean createChPaymentRequestBean() {
        return new ChPaymentRequestBean();
    }

    /**
     * Create an instance of {@link ChCardInfoRetrievationResponseBean }
     * 
     */
    public ChCardInfoRetrievationResponseBean createChCardInfoRetrievationResponseBean() {
        return new ChCardInfoRetrievationResponseBean();
    }

    /**
     * Create an instance of {@link ChCardProductsResponseBean }
     * 
     */
    public ChCardProductsResponseBean createChCardProductsResponseBean() {
        return new ChCardProductsResponseBean();
    }

    /**
     * Create an instance of {@link ChBranchBalanceSearchBean }
     * 
     */
    public ChBranchBalanceSearchBean createChBranchBalanceSearchBean() {
        return new ChBranchBalanceSearchBean();
    }

    /**
     * Create an instance of {@link ChReqStatusResponseBeans }
     * 
     */
    public ChReqStatusResponseBeans createChReqStatusResponseBeans() {
        return new ChReqStatusResponseBeans();
    }

    /**
     * Create an instance of {@link ChBillPaymentResponseBean }
     * 
     */
    public ChBillPaymentResponseBean createChBillPaymentResponseBean() {
        return new ChBillPaymentResponseBean();
    }

    /**
     * Create an instance of {@link ChPaymentBillStatementsResponseBean }
     * 
     */
    public ChPaymentBillStatementsResponseBean createChPaymentBillStatementsResponseBean() {
        return new ChPaymentBillStatementsResponseBean();
    }

    /**
     * Create an instance of {@link ChBranchBalanceInfo }
     * 
     */
    public ChBranchBalanceInfo createChBranchBalanceInfo() {
        return new ChBranchBalanceInfo();
    }

    /**
     * Create an instance of {@link ChCashChequeResponseBean }
     * 
     */
    public ChCashChequeResponseBean createChCashChequeResponseBean() {
        return new ChCashChequeResponseBean();
    }

    /**
     * Create an instance of {@link ChCreateNaturalCustomerResponseBean }
     * 
     */
    public ChCreateNaturalCustomerResponseBean createChCreateNaturalCustomerResponseBean() {
        return new ChCreateNaturalCustomerResponseBean();
    }

    /**
     * Create an instance of {@link ChPaymentServiceOutputParameterBean }
     * 
     */
    public ChPaymentServiceOutputParameterBean createChPaymentServiceOutputParameterBean() {
        return new ChPaymentServiceOutputParameterBean();
    }

    /**
     * Create an instance of {@link ChSearchAutoTransferRequestBean }
     * 
     */
    public ChSearchAutoTransferRequestBean createChSearchAutoTransferRequestBean() {
        return new ChSearchAutoTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChCustomerAddressResponseBean }
     * 
     */
    public ChCustomerAddressResponseBean createChCustomerAddressResponseBean() {
        return new ChCustomerAddressResponseBean();
    }

    /**
     * Create an instance of {@link ChBatchBillInfoSearchRequestBean }
     * 
     */
    public ChBatchBillInfoSearchRequestBean createChBatchBillInfoSearchRequestBean() {
        return new ChBatchBillInfoSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChAtmStatusResponseBean }
     * 
     */
    public ChAtmStatusResponseBean createChAtmStatusResponseBean() {
        return new ChAtmStatusResponseBean();
    }

    /**
     * Create an instance of {@link ChFavoriteDepositNumberBean }
     * 
     */
    public ChFavoriteDepositNumberBean createChFavoriteDepositNumberBean() {
        return new ChFavoriteDepositNumberBean();
    }

    /**
     * Create an instance of {@link ChCardDepositBean }
     * 
     */
    public ChCardDepositBean createChCardDepositBean() {
        return new ChCardDepositBean();
    }

    /**
     * Create an instance of {@link ChCardPurchaseRequestBean }
     * 
     */
    public ChCardPurchaseRequestBean createChCardPurchaseRequestBean() {
        return new ChCardPurchaseRequestBean();
    }

    /**
     * Create an instance of {@link ChDepositDetailBean }
     * 
     */
    public ChDepositDetailBean createChDepositDetailBean() {
        return new ChDepositDetailBean();
    }

    /**
     * Create an instance of {@link ChCardLimitRequestBean }
     * 
     */
    public ChCardLimitRequestBean createChCardLimitRequestBean() {
        return new ChCardLimitRequestBean();
    }

    /**
     * Create an instance of {@link ChBankBean }
     * 
     */
    public ChBankBean createChBankBean() {
        return new ChBankBean();
    }

    /**
     * Create an instance of {@link ChResumeAchTransferRequestBean }
     * 
     */
    public ChResumeAchTransferRequestBean createChResumeAchTransferRequestBean() {
        return new ChResumeAchTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChCardBalanceRequestBean }
     * 
     */
    public ChCardBalanceRequestBean createChCardBalanceRequestBean() {
        return new ChCardBalanceRequestBean();
    }

    /**
     * Create an instance of {@link ChPhysicalCardTransferRequest }
     * 
     */
    public ChPhysicalCardTransferRequest createChPhysicalCardTransferRequest() {
        return new ChPhysicalCardTransferRequest();
    }

    /**
     * Create an instance of {@link ChChangeUsernameRequestBean }
     * 
     */
    public ChChangeUsernameRequestBean createChChangeUsernameRequestBean() {
        return new ChChangeUsernameRequestBean();
    }

    /**
     * Create an instance of {@link ChInstitutionRequestBean }
     * 
     */
    public ChInstitutionRequestBean createChInstitutionRequestBean() {
        return new ChInstitutionRequestBean();
    }

    /**
     * Create an instance of {@link ChUserRequestBean }
     * 
     */
    public ChUserRequestBean createChUserRequestBean() {
        return new ChUserRequestBean();
    }

    /**
     * Create an instance of {@link ChAutoTransferResponseBean }
     * 
     */
    public ChAutoTransferResponseBean createChAutoTransferResponseBean() {
        return new ChAutoTransferResponseBean();
    }

    /**
     * Create an instance of {@link ChAchTransferSummeryBean }
     * 
     */
    public ChAchTransferSummeryBean createChAchTransferSummeryBean() {
        return new ChAchTransferSummeryBean();
    }

    /**
     * Create an instance of {@link ChChequeSearchRequestBean }
     * 
     */
    public ChChequeSearchRequestBean createChChequeSearchRequestBean() {
        return new ChChequeSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChBillInfoResponseBean }
     * 
     */
    public ChBillInfoResponseBean createChBillInfoResponseBean() {
        return new ChBillInfoResponseBean();
    }

    /**
     * Create an instance of {@link ChPeriodicRequestBean }
     * 
     */
    public ChPeriodicRequestBean createChPeriodicRequestBean() {
        return new ChPeriodicRequestBean();
    }

    /**
     * Create an instance of {@link ChCardProductsRequestBean }
     * 
     */
    public ChCardProductsRequestBean createChCardProductsRequestBean() {
        return new ChCardProductsRequestBean();
    }

    /**
     * Create an instance of {@link ChBillBean }
     * 
     */
    public ChBillBean createChBillBean() {
        return new ChBillBean();
    }

    /**
     * Create an instance of {@link ChCreditDossierBean }
     * 
     */
    public ChCreditDossierBean createChCreditDossierBean() {
        return new ChCreditDossierBean();
    }

    /**
     * Create an instance of {@link ChLoanBean }
     * 
     */
    public ChLoanBean createChLoanBean() {
        return new ChLoanBean();
    }

    /**
     * Create an instance of {@link ChBriefAchTransactionBean }
     * 
     */
    public ChBriefAchTransactionBean createChBriefAchTransactionBean() {
        return new ChBriefAchTransactionBean();
    }

    /**
     * Create an instance of {@link ChCoreBlockDepositAmountRequestBean }
     * 
     */
    public ChCoreBlockDepositAmountRequestBean createChCoreBlockDepositAmountRequestBean() {
        return new ChCoreBlockDepositAmountRequestBean();
    }

    /**
     * Create an instance of {@link ChCardTransactionResponseBean }
     * 
     */
    public ChCardTransactionResponseBean createChCardTransactionResponseBean() {
        return new ChCardTransactionResponseBean();
    }

    /**
     * Create an instance of {@link ChFeeBean }
     * 
     */
    public ChFeeBean createChFeeBean() {
        return new ChFeeBean();
    }

    /**
     * Create an instance of {@link ChBatchAchTransferRequestBean }
     * 
     */
    public ChBatchAchTransferRequestBean createChBatchAchTransferRequestBean() {
        return new ChBatchAchTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChSignRequestDetailBean }
     * 
     */
    public ChSignRequestDetailBean createChSignRequestDetailBean() {
        return new ChSignRequestDetailBean();
    }

    /**
     * Create an instance of {@link ChDepositRateInfoBean }
     * 
     */
    public ChDepositRateInfoBean createChDepositRateInfoBean() {
        return new ChDepositRateInfoBean();
    }

    /**
     * Create an instance of {@link ChPayedThirdPartyResponseBean }
     * 
     */
    public ChPayedThirdPartyResponseBean createChPayedThirdPartyResponseBean() {
        return new ChPayedThirdPartyResponseBean();
    }

    /**
     * Create an instance of {@link ChCardTransactionsRequestBean }
     * 
     */
    public ChCardTransactionsRequestBean createChCardTransactionsRequestBean() {
        return new ChCardTransactionsRequestBean();
    }

    /**
     * Create an instance of {@link ChCardStatementRequestBean }
     * 
     */
    public ChCardStatementRequestBean createChCardStatementRequestBean() {
        return new ChCardStatementRequestBean();
    }

    /**
     * Create an instance of {@link ChOwnerDepositRequestBean }
     * 
     */
    public ChOwnerDepositRequestBean createChOwnerDepositRequestBean() {
        return new ChOwnerDepositRequestBean();
    }

    /**
     * Create an instance of {@link ChSellReportResponseResultBean }
     * 
     */
    public ChSellReportResponseResultBean createChSellReportResponseResultBean() {
        return new ChSellReportResponseResultBean();
    }

    /**
     * Create an instance of {@link ChBillInfoSearchRequestBean }
     * 
     */
    public ChBillInfoSearchRequestBean createChBillInfoSearchRequestBean() {
        return new ChBillInfoSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChInquiryCardIssuingResponseBean }
     * 
     */
    public ChInquiryCardIssuingResponseBean createChInquiryCardIssuingResponseBean() {
        return new ChInquiryCardIssuingResponseBean();
    }

    /**
     * Create an instance of {@link ChGuarantyBean }
     * 
     */
    public ChGuarantyBean createChGuarantyBean() {
        return new ChGuarantyBean();
    }

    /**
     * Create an instance of {@link ChSearchCreditDossierRequestBean }
     * 
     */
    public ChSearchCreditDossierRequestBean createChSearchCreditDossierRequestBean() {
        return new ChSearchCreditDossierRequestBean();
    }

    /**
     * Create an instance of {@link ChCompleteDepositBean }
     * 
     */
    public ChCompleteDepositBean createChCompleteDepositBean() {
        return new ChCompleteDepositBean();
    }

    /**
     * Create an instance of {@link ChPaymentParameterBean }
     * 
     */
    public ChPaymentParameterBean createChPaymentParameterBean() {
        return new ChPaymentParameterBean();
    }

    /**
     * Create an instance of {@link ChVirtualCardTransferRequest }
     * 
     */
    public ChVirtualCardTransferRequest createChVirtualCardTransferRequest() {
        return new ChVirtualCardTransferRequest();
    }

    /**
     * Create an instance of {@link ChCreditBillPaymentRequestBean }
     * 
     */
    public ChCreditBillPaymentRequestBean createChCreditBillPaymentRequestBean() {
        return new ChCreditBillPaymentRequestBean();
    }

    /**
     * Create an instance of {@link ChAchTransferBean }
     * 
     */
    public ChAchTransferBean createChAchTransferBean() {
        return new ChAchTransferBean();
    }

    /**
     * Create an instance of {@link ChConstraintInformationBean }
     * 
     */
    public ChConstraintInformationBean createChConstraintInformationBean() {
        return new ChConstraintInformationBean();
    }

    /**
     * Create an instance of {@link ChChargeCardRequestBean }
     * 
     */
    public ChChargeCardRequestBean createChChargeCardRequestBean() {
        return new ChChargeCardRequestBean();
    }

    /**
     * Create an instance of {@link ChFavoriteDepositNumberResponseBeans }
     * 
     */
    public ChFavoriteDepositNumberResponseBeans createChFavoriteDepositNumberResponseBeans() {
        return new ChFavoriteDepositNumberResponseBeans();
    }

    /**
     * Create an instance of {@link ChCardIssuingRequestBean }
     * 
     */
    public ChCardIssuingRequestBean createChCardIssuingRequestBean() {
        return new ChCardIssuingRequestBean();
    }

    /**
     * Create an instance of {@link ChCreateNaturalCustomerRequestBean }
     * 
     */
    public ChCreateNaturalCustomerRequestBean createChCreateNaturalCustomerRequestBean() {
        return new ChCreateNaturalCustomerRequestBean();
    }

    /**
     * Create an instance of {@link ChDefineCustomerSignatureSampleRequestBean }
     * 
     */
    public ChDefineCustomerSignatureSampleRequestBean createChDefineCustomerSignatureSampleRequestBean() {
        return new ChDefineCustomerSignatureSampleRequestBean();
    }

    /**
     * Create an instance of {@link ChRemoveCardDepositRequestBean }
     * 
     */
    public ChRemoveCardDepositRequestBean createChRemoveCardDepositRequestBean() {
        return new ChRemoveCardDepositRequestBean();
    }

    /**
     * Create an instance of {@link ChMerchantInfoSearchRequestBean }
     * 
     */
    public ChMerchantInfoSearchRequestBean createChMerchantInfoSearchRequestBean() {
        return new ChMerchantInfoSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChMerchantInfoResponseBean }
     * 
     */
    public ChMerchantInfoResponseBean createChMerchantInfoResponseBean() {
        return new ChMerchantInfoResponseBean();
    }

    /**
     * Create an instance of {@link ChStatementResponseBean }
     * 
     */
    public ChStatementResponseBean createChStatementResponseBean() {
        return new ChStatementResponseBean();
    }

    /**
     * Create an instance of {@link ChBillSettingRequestBean }
     * 
     */
    public ChBillSettingRequestBean createChBillSettingRequestBean() {
        return new ChBillSettingRequestBean();
    }

    /**
     * Create an instance of {@link ChUserSMSSettingRequestBean }
     * 
     */
    public ChUserSMSSettingRequestBean createChUserSMSSettingRequestBean() {
        return new ChUserSMSSettingRequestBean();
    }

    /**
     * Create an instance of {@link ChSignRequestDetailResponseBean }
     * 
     */
    public ChSignRequestDetailResponseBean createChSignRequestDetailResponseBean() {
        return new ChSignRequestDetailResponseBean();
    }

    /**
     * Create an instance of {@link ChSearchCustomerBean }
     * 
     */
    public ChSearchCustomerBean createChSearchCustomerBean() {
        return new ChSearchCustomerBean();
    }

    /**
     * Create an instance of {@link ChPaymentServicesResponseBean }
     * 
     */
    public ChPaymentServicesResponseBean createChPaymentServicesResponseBean() {
        return new ChPaymentServicesResponseBean();
    }

    /**
     * Create an instance of {@link ChLoanDetailResponseBean }
     * 
     */
    public ChLoanDetailResponseBean createChLoanDetailResponseBean() {
        return new ChLoanDetailResponseBean();
    }

    /**
     * Create an instance of {@link ChUserResponseBean }
     * 
     */
    public ChUserResponseBean createChUserResponseBean() {
        return new ChUserResponseBean();
    }

    /**
     * Create an instance of {@link ChDepositRateInquiryRequestBean }
     * 
     */
    public ChDepositRateInquiryRequestBean createChDepositRateInquiryRequestBean() {
        return new ChDepositRateInquiryRequestBean();
    }

    /**
     * Create an instance of {@link ChMobileLoginRequestBean }
     * 
     */
    public ChMobileLoginRequestBean createChMobileLoginRequestBean() {
        return new ChMobileLoginRequestBean();
    }

    /**
     * Create an instance of {@link ChBatchBillPaymentResponseBean }
     * 
     */
    public ChBatchBillPaymentResponseBean createChBatchBillPaymentResponseBean() {
        return new ChBatchBillPaymentResponseBean();
    }

    /**
     * Create an instance of {@link ChBeOpenedDepositBean }
     * 
     */
    public ChBeOpenedDepositBean createChBeOpenedDepositBean() {
        return new ChBeOpenedDepositBean();
    }

    /**
     * Create an instance of {@link ChBankSearchRequestBean }
     * 
     */
    public ChBankSearchRequestBean createChBankSearchRequestBean() {
        return new ChBankSearchRequestBean();
    }

    /**
     * Create an instance of {@link ChAutoTransferDetailBean }
     * 
     */
    public ChAutoTransferDetailBean createChAutoTransferDetailBean() {
        return new ChAutoTransferDetailBean();
    }

    /**
     * Create an instance of {@link ChCardTransferResponse }
     * 
     */
    public ChCardTransferResponse createChCardTransferResponse() {
        return new ChCardTransferResponse();
    }

    /**
     * Create an instance of {@link ChServiceConstraintInformationResponseBeans }
     * 
     */
    public ChServiceConstraintInformationResponseBeans createChServiceConstraintInformationResponseBeans() {
        return new ChServiceConstraintInformationResponseBeans();
    }

    /**
     * Create an instance of {@link ChSwitchAmount }
     * 
     */
    public ChSwitchAmount createChSwitchAmount() {
        return new ChSwitchAmount();
    }

    /**
     * Create an instance of {@link ChBillSettingResponseBean }
     * 
     */
    public ChBillSettingResponseBean createChBillSettingResponseBean() {
        return new ChBillSettingResponseBean();
    }

    /**
     * Create an instance of {@link ChCardResponseBean }
     * 
     */
    public ChCardResponseBean createChCardResponseBean() {
        return new ChCardResponseBean();
    }

    /**
     * Create an instance of {@link ChCardTypeItemPrintFormat }
     * 
     */
    public ChCardTypeItemPrintFormat createChCardTypeItemPrintFormat() {
        return new ChCardTypeItemPrintFormat();
    }

    /**
     * Create an instance of {@link ChCustomerInformationBean }
     * 
     */
    public ChCustomerInformationBean createChCustomerInformationBean() {
        return new ChCustomerInformationBean();
    }

    /**
     * Create an instance of {@link ChChequeBookBean }
     * 
     */
    public ChChequeBookBean createChChequeBookBean() {
        return new ChChequeBookBean();
    }

    /**
     * Create an instance of {@link ChSearchTransferChequeRequestBean }
     * 
     */
    public ChSearchTransferChequeRequestBean createChSearchTransferChequeRequestBean() {
        return new ChSearchTransferChequeRequestBean();
    }

    /**
     * Create an instance of {@link ChBranchAllowedDepositsToOpenResponseBean }
     * 
     */
    public ChBranchAllowedDepositsToOpenResponseBean createChBranchAllowedDepositsToOpenResponseBean() {
        return new ChBranchAllowedDepositsToOpenResponseBean();
    }

    /**
     * Create an instance of {@link ChSellDetailReportResponseResult }
     * 
     */
    public ChSellDetailReportResponseResult createChSellDetailReportResponseResult() {
        return new ChSellDetailReportResponseResult();
    }

    /**
     * Create an instance of {@link ChCardLimitationResponseBean }
     * 
     */
    public ChCardLimitationResponseBean createChCardLimitationResponseBean() {
        return new ChCardLimitationResponseBean();
    }

    /**
     * Create an instance of {@link ChOpenDepositRequestBean }
     * 
     */
    public ChOpenDepositRequestBean createChOpenDepositRequestBean() {
        return new ChOpenDepositRequestBean();
    }

    /**
     * Create an instance of {@link ChBatchBillInfoResponseBean }
     * 
     */
    public ChBatchBillInfoResponseBean createChBatchBillInfoResponseBean() {
        return new ChBatchBillInfoResponseBean();
    }

    /**
     * Create an instance of {@link ChSellDetailReportResponse }
     * 
     */
    public ChSellDetailReportResponse createChSellDetailReportResponse() {
        return new ChSellDetailReportResponse();
    }

    /**
     * Create an instance of {@link ChVerifyTransactionRequestBean }
     * 
     */
    public ChVerifyTransactionRequestBean createChVerifyTransactionRequestBean() {
        return new ChVerifyTransactionRequestBean();
    }

    /**
     * Create an instance of {@link ChChargeCardBean }
     * 
     */
    public ChChargeCardBean createChChargeCardBean() {
        return new ChChargeCardBean();
    }

    /**
     * Create an instance of {@link ChAutoTransferRequestBean }
     * 
     */
    public ChAutoTransferRequestBean createChAutoTransferRequestBean() {
        return new ChAutoTransferRequestBean();
    }

    /**
     * Create an instance of {@link ChSendReversalResponse }
     * 
     */
    public ChSendReversalResponse createChSendReversalResponse() {
        return new ChSendReversalResponse();
    }

    /**
     * Create an instance of {@link ChAtmStatusRequestBean }
     * 
     */
    public ChAtmStatusRequestBean createChAtmStatusRequestBean() {
        return new ChAtmStatusRequestBean();
    }

    /**
     * Create an instance of {@link ChPaymentServiceBean }
     * 
     */
    public ChPaymentServiceBean createChPaymentServiceBean() {
        return new ChPaymentServiceBean();
    }

    /**
     * Create an instance of {@link ChGeneralParameterBean }
     * 
     */
    public ChGeneralParameterBean createChGeneralParameterBean() {
        return new ChGeneralParameterBean();
    }

    /**
     * Create an instance of {@link ChDepositProblemBean }
     * 
     */
    public ChDepositProblemBean createChDepositProblemBean() {
        return new ChDepositProblemBean();
    }

    /**
     * Create an instance of {@link ChBranchResponseBean }
     * 
     */
    public ChBranchResponseBean createChBranchResponseBean() {
        return new ChBranchResponseBean();
    }

    /**
     * Create an instance of {@link ChCoreBlockDepositAmountResponseBean }
     * 
     */
    public ChCoreBlockDepositAmountResponseBean createChCoreBlockDepositAmountResponseBean() {
        return new ChCoreBlockDepositAmountResponseBean();
    }

    /**
     * Create an instance of {@link ChCustomerInfo }
     * 
     */
    public ChCustomerInfo createChCustomerInfo() {
        return new ChCustomerInfo();
    }

    /**
     * Create an instance of {@link ChSMSSettingRequestBean }
     * 
     */
    public ChSMSSettingRequestBean createChSMSSettingRequestBean() {
        return new ChSMSSettingRequestBean();
    }

    /**
     * Create an instance of {@link ChGuarantiesResponseBean }
     * 
     */
    public ChGuarantiesResponseBean createChGuarantiesResponseBean() {
        return new ChGuarantiesResponseBean();
    }

    /**
     * Create an instance of {@link Attachment }
     * 
     */
    public Attachment createAttachment() {
        return new Attachment();
    }

    /**
     * Create an instance of {@link ChBankInfoBean }
     * 
     */
    public ChBankInfoBean createChBankInfoBean() {
        return new ChBankInfoBean();
    }

    /**
     * Create an instance of {@link ChBillResponseBean }
     * 
     */
    public ChBillResponseBean createChBillResponseBean() {
        return new ChBillResponseBean();
    }

    /**
     * Create an instance of {@link ChInstitutionalTransferResponseBean }
     * 
     */
    public ChInstitutionalTransferResponseBean createChInstitutionalTransferResponseBean() {
        return new ChInstitutionalTransferResponseBean();
    }

    /**
     * Create an instance of {@link ChDestinationAchTransactionResultBean }
     * 
     */
    public ChDestinationAchTransactionResultBean createChDestinationAchTransactionResultBean() {
        return new ChDestinationAchTransactionResultBean();
    }

    /**
     * Create an instance of {@link ChPaymentInputParameterBean }
     * 
     */
    public ChPaymentInputParameterBean createChPaymentInputParameterBean() {
        return new ChPaymentInputParameterBean();
    }

    /**
     * Create an instance of {@link ChCardDepositResponseBean }
     * 
     */
    public ChCardDepositResponseBean createChCardDepositResponseBean() {
        return new ChCardDepositResponseBean();
    }

    /**
     * Create an instance of {@link ChVerifyTransactionResponseBean }
     * 
     */
    public ChVerifyTransactionResponseBean createChVerifyTransactionResponseBean() {
        return new ChVerifyTransactionResponseBean();
    }

    /**
     * Create an instance of {@link ChInstitutionBean }
     * 
     */
    public ChInstitutionBean createChInstitutionBean() {
        return new ChInstitutionBean();
    }

    /**
     * Create an instance of {@link ChCompleteCardBean }
     * 
     */
    public ChCompleteCardBean createChCompleteCardBean() {
        return new ChCompleteCardBean();
    }

    /**
     * Create an instance of {@link ChSignerBean }
     * 
     */
    public ChSignerBean createChSignerBean() {
        return new ChSignerBean();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InquiryNaturalPersonOpenSingleDepositResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "inquiryNaturalPersonOpenSingleDepositResponse")
    public JAXBElement<InquiryNaturalPersonOpenSingleDepositResponse> createInquiryNaturalPersonOpenSingleDepositResponse(InquiryNaturalPersonOpenSingleDepositResponse value) {
        return new JAXBElement<InquiryNaturalPersonOpenSingleDepositResponse>(_InquiryNaturalPersonOpenSingleDepositResponse_QNAME, InquiryNaturalPersonOpenSingleDepositResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RtgsTransferReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "rtgsTransferReportResponse")
    public JAXBElement<RtgsTransferReportResponse> createRtgsTransferReportResponse(RtgsTransferReportResponse value) {
        return new JAXBElement<RtgsTransferReportResponse>(_RtgsTransferReportResponse_QNAME, RtgsTransferReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLoanDetailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getLoanDetailResponse")
    public JAXBElement<GetLoanDetailResponse> createGetLoanDetailResponse(GetLoanDetailResponse value) {
        return new JAXBElement<GetLoanDetailResponse>(_GetLoanDetailResponse_QNAME, GetLoanDetailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChTopupChargeRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chTopupChargeRequest")
    public JAXBElement<ChTopupChargeRequest> createChTopupChargeRequest(ChTopupChargeRequest value) {
        return new JAXBElement<ChTopupChargeRequest>(_ChTopupChargeRequest_QNAME, ChTopupChargeRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChVerifyTransactionRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chVerifyTransactionRequestBean")
    public JAXBElement<ChVerifyTransactionRequestBean> createChVerifyTransactionRequestBean(ChVerifyTransactionRequestBean value) {
        return new JAXBElement<ChVerifyTransactionRequestBean>(_ChVerifyTransactionRequestBean_QNAME, ChVerifyTransactionRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardCorrectLinkedAccounts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardCorrectLinkedAccounts")
    public JAXBElement<CardCorrectLinkedAccounts> createCardCorrectLinkedAccounts(CardCorrectLinkedAccounts value) {
        return new JAXBElement<CardCorrectLinkedAccounts>(_CardCorrectLinkedAccounts_QNAME, CardCorrectLinkedAccounts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerExistenceInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "customerExistenceInquiry")
    public JAXBElement<CustomerExistenceInquiry> createCustomerExistenceInquiry(CustomerExistenceInquiry value) {
        return new JAXBElement<CustomerExistenceInquiry>(_CustomerExistenceInquiry_QNAME, CustomerExistenceInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AutoTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "autoTransferResponse")
    public JAXBElement<AutoTransferResponse> createAutoTransferResponse(AutoTransferResponse value) {
        return new JAXBElement<AutoTransferResponse>(_AutoTransferResponse_QNAME, AutoTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBillInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBillInfoResponse")
    public JAXBElement<GetBillInfoResponse> createGetBillInfoResponse(GetBillInfoResponse value) {
        return new JAXBElement<GetBillInfoResponse>(_GetBillInfoResponse_QNAME, GetBillInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCardDepositResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "addCardDepositResponse")
    public JAXBElement<AddCardDepositResponse> createAddCardDepositResponse(AddCardDepositResponse value) {
        return new JAXBElement<AddCardDepositResponse>(_AddCardDepositResponse_QNAME, AddCardDepositResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChOpenDepositRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chOpenDepositRequestBean")
    public JAXBElement<ChOpenDepositRequestBean> createChOpenDepositRequestBean(ChOpenDepositRequestBean value) {
        return new JAXBElement<ChOpenDepositRequestBean>(_ChOpenDepositRequestBean_QNAME, ChOpenDepositRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrencyRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCurrencyRate")
    public JAXBElement<GetCurrencyRate> createGetCurrencyRate(GetCurrencyRate value) {
        return new JAXBElement<GetCurrencyRate>(_GetCurrencyRate_QNAME, GetCurrencyRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChequeBookList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getChequeBookList")
    public JAXBElement<GetChequeBookList> createGetChequeBookList(GetChequeBookList value) {
        return new JAXBElement<GetChequeBookList>(_GetChequeBookList_QNAME, GetChequeBookList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConvertDepositNumberToIban }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "convertDepositNumberToIban")
    public JAXBElement<ConvertDepositNumberToIban> createConvertDepositNumberToIban(ConvertDepositNumberToIban value) {
        return new JAXBElement<ConvertDepositNumberToIban>(_ConvertDepositNumberToIban_QNAME, ConvertDepositNumberToIban.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayBill }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "payBill")
    public JAXBElement<PayBill> createPayBill(PayBill value) {
        return new JAXBElement<PayBill>(_PayBill_QNAME, PayBill.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBillPaymentRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBillPaymentRequestBean")
    public JAXBElement<ChBillPaymentRequestBean> createChBillPaymentRequestBean(ChBillPaymentRequestBean value) {
        return new JAXBElement<ChBillPaymentRequestBean>(_ChBillPaymentRequestBean_QNAME, ChBillPaymentRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BatchTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "batchTransfer")
    public JAXBElement<BatchTransfer> createBatchTransfer(BatchTransfer value) {
        return new JAXBElement<BatchTransfer>(_BatchTransfer_QNAME, BatchTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCardDepositRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCardDepositRequestBean")
    public JAXBElement<ChCardDepositRequestBean> createChCardDepositRequestBean(ChCardDepositRequestBean value) {
        return new JAXBElement<ChCardDepositRequestBean>(_ChCardDepositRequestBean_QNAME, ChCardDepositRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogoutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "logoutResponse")
    public JAXBElement<LogoutResponse> createLogoutResponse(LogoutResponse value) {
        return new JAXBElement<LogoutResponse>(_LogoutResponse_QNAME, LogoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "changePasswordResponse")
    public JAXBElement<ChangePasswordResponse> createChangePasswordResponse(ChangePasswordResponse value) {
        return new JAXBElement<ChangePasswordResponse>(_ChangePasswordResponse_QNAME, ChangePasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardLimitations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardLimitations")
    public JAXBElement<GetCardLimitations> createGetCardLimitations(GetCardLimitations value) {
        return new JAXBElement<GetCardLimitations>(_GetCardLimitations_QNAME, GetCardLimitations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BlockCheque }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "blockCheque")
    public JAXBElement<BlockCheque> createBlockCheque(BlockCheque value) {
        return new JAXBElement<BlockCheque>(_BlockCheque_QNAME, BlockCheque.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChSearchTransferChequeRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chSearchTransferChequeRequestBean")
    public JAXBElement<ChSearchTransferChequeRequestBean> createChSearchTransferChequeRequestBean(ChSearchTransferChequeRequestBean value) {
        return new JAXBElement<ChSearchTransferChequeRequestBean>(_ChSearchTransferChequeRequestBean_QNAME, ChSearchTransferChequeRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChTransferConstraintRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chTransferConstraintRequestBean")
    public JAXBElement<ChTransferConstraintRequestBean> createChTransferConstraintRequestBean(ChTransferConstraintRequestBean value) {
        return new JAXBElement<ChTransferConstraintRequestBean>(_ChTransferConstraintRequestBean_QNAME, ChTransferConstraintRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DefineCustomerSignatureSampleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "defineCustomerSignatureSampleResponse")
    public JAXBElement<DefineCustomerSignatureSampleResponse> createDefineCustomerSignatureSampleResponse(DefineCustomerSignatureSampleResponse value) {
        return new JAXBElement<DefineCustomerSignatureSampleResponse>(_DefineCustomerSignatureSampleResponse_QNAME, DefineCustomerSignatureSampleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayBillResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "payBillResponse")
    public JAXBElement<PayBillResponse> createPayBillResponse(PayBillResponse value) {
        return new JAXBElement<PayBillResponse>(_PayBillResponse_QNAME, PayBillResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AchTransferReport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "achTransferReport")
    public JAXBElement<AchTransferReport> createAchTransferReport(AchTransferReport value) {
        return new JAXBElement<AchTransferReport>(_AchTransferReport_QNAME, AchTransferReport.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardProducts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardProducts")
    public JAXBElement<GetCardProducts> createGetCardProducts(GetCardProducts value) {
        return new JAXBElement<GetCardProducts>(_GetCardProducts_QNAME, GetCardProducts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AchAutoTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "achAutoTransferResponse")
    public JAXBElement<AchAutoTransferResponse> createAchAutoTransferResponse(AchAutoTransferResponse value) {
        return new JAXBElement<AchAutoTransferResponse>(_AchAutoTransferResponse_QNAME, AchAutoTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChChequeBookRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chChequeBookRequestBean")
    public JAXBElement<ChChequeBookRequestBean> createChChequeBookRequestBean(ChChequeBookRequestBean value) {
        return new JAXBElement<ChChequeBookRequestBean>(_ChChequeBookRequestBean_QNAME, ChChequeBookRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPeriodicBalanceRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "addPeriodicBalanceRequest")
    public JAXBElement<AddPeriodicBalanceRequest> createAddPeriodicBalanceRequest(AddPeriodicBalanceRequest value) {
        return new JAXBElement<AddPeriodicBalanceRequest>(_AddPeriodicBalanceRequest_QNAME, AddPeriodicBalanceRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RtgsTransferDetailReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "rtgsTransferDetailReportResponse")
    public JAXBElement<RtgsTransferDetailReportResponse> createRtgsTransferDetailReportResponse(RtgsTransferDetailReportResponse value) {
        return new JAXBElement<RtgsTransferDetailReportResponse>(_RtgsTransferDetailReportResponse_QNAME, RtgsTransferDetailReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterCheque }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "registerCheque")
    public JAXBElement<RegisterCheque> createRegisterCheque(RegisterCheque value) {
        return new JAXBElement<RegisterCheque>(_RegisterCheque_QNAME, RegisterCheque.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardStatementInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardStatementInquiry")
    public JAXBElement<GetCardStatementInquiry> createGetCardStatementInquiry(GetCardStatementInquiry value) {
        return new JAXBElement<GetCardStatementInquiry>(_GetCardStatementInquiry_QNAME, GetCardStatementInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFavoriteDepositsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getFavoriteDepositsResponse")
    public JAXBElement<GetFavoriteDepositsResponse> createGetFavoriteDepositsResponse(GetFavoriteDepositsResponse value) {
        return new JAXBElement<GetFavoriteDepositsResponse>(_GetFavoriteDepositsResponse_QNAME, GetFavoriteDepositsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardBalanceInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardBalanceInquiry")
    public JAXBElement<CardBalanceInquiry> createCardBalanceInquiry(CardBalanceInquiry value) {
        return new JAXBElement<CardBalanceInquiry>(_CardBalanceInquiry_QNAME, CardBalanceInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChAutoTransferRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chAutoTransferRequestBean")
    public JAXBElement<ChAutoTransferRequestBean> createChAutoTransferRequestBean(ChAutoTransferRequestBean value) {
        return new JAXBElement<ChAutoTransferRequestBean>(_ChAutoTransferRequestBean_QNAME, ChAutoTransferRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChSignRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chSignRequestBean")
    public JAXBElement<ChSignRequestBean> createChSignRequestBean(ChSignRequestBean value) {
        return new JAXBElement<ChSignRequestBean>(_ChSignRequestBean_QNAME, ChSignRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BatchTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "batchTransferResponse")
    public JAXBElement<BatchTransferResponse> createBatchTransferResponse(BatchTransferResponse value) {
        return new JAXBElement<BatchTransferResponse>(_BatchTransferResponse_QNAME, BatchTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIssuingAllowedDepositsToOpenResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardIssuingAllowedDepositsToOpenResponse")
    public JAXBElement<CardIssuingAllowedDepositsToOpenResponse> createCardIssuingAllowedDepositsToOpenResponse(CardIssuingAllowedDepositsToOpenResponse value) {
        return new JAXBElement<CardIssuingAllowedDepositsToOpenResponse>(_CardIssuingAllowedDepositsToOpenResponse_QNAME, CardIssuingAllowedDepositsToOpenResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransferConstraintInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getTransferConstraintInfoResponse")
    public JAXBElement<GetTransferConstraintInfoResponse> createGetTransferConstraintInfoResponse(GetTransferConstraintInfoResponse value) {
        return new JAXBElement<GetTransferConstraintInfoResponse>(_GetTransferConstraintInfoResponse_QNAME, GetTransferConstraintInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadCoreParameters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "loadCoreParameters")
    public JAXBElement<LoadCoreParameters> createLoadCoreParameters(LoadCoreParameters value) {
        return new JAXBElement<LoadCoreParameters>(_LoadCoreParameters_QNAME, LoadCoreParameters.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEcho }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getEcho")
    public JAXBElement<GetEcho> createGetEcho(GetEcho value) {
        return new JAXBElement<GetEcho>(_GetEcho_QNAME, GetEcho.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOwnerDepositNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getOwnerDepositNameResponse")
    public JAXBElement<GetOwnerDepositNameResponse> createGetOwnerDepositNameResponse(GetOwnerDepositNameResponse value) {
        return new JAXBElement<GetOwnerDepositNameResponse>(_GetOwnerDepositNameResponse_QNAME, GetOwnerDepositNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransferChequeList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getTransferChequeList")
    public JAXBElement<GetTransferChequeList> createGetTransferChequeList(GetTransferChequeList value) {
        return new JAXBElement<GetTransferChequeList>(_GetTransferChequeList_QNAME, GetTransferChequeList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "changePassword")
    public JAXBElement<ChangePassword> createChangePassword(ChangePassword value) {
        return new JAXBElement<ChangePassword>(_ChangePassword_QNAME, ChangePassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AchTransactionReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "achTransactionReportResponse")
    public JAXBElement<AchTransactionReportResponse> createAchTransactionReportResponse(AchTransactionReportResponse value) {
        return new JAXBElement<AchTransactionReportResponse>(_AchTransactionReportResponse_QNAME, AchTransactionReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAtmStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getAtmStatus")
    public JAXBElement<GetAtmStatus> createGetAtmStatus(GetAtmStatus value) {
        return new JAXBElement<GetAtmStatus>(_GetAtmStatus_QNAME, GetAtmStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardDeposits }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardDeposits")
    public JAXBElement<GetCardDeposits> createGetCardDeposits(GetCardDeposits value) {
        return new JAXBElement<GetCardDeposits>(_GetCardDeposits_QNAME, GetCardDeposits.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSellReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getSellReportResponse")
    public JAXBElement<GetSellReportResponse> createGetSellReportResponse(GetSellReportResponse value) {
        return new JAXBElement<GetSellReportResponse>(_GetSellReportResponse_QNAME, GetSellReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AchBatchTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "achBatchTransferResponse")
    public JAXBElement<AchBatchTransferResponse> createAchBatchTransferResponse(AchBatchTransferResponse value) {
        return new JAXBElement<AchBatchTransferResponse>(_AchBatchTransferResponse_QNAME, AchBatchTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCardDeposit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "addCardDeposit")
    public JAXBElement<AddCardDeposit> createAddCardDeposit(AddCardDeposit value) {
        return new JAXBElement<AddCardDeposit>(_AddCardDeposit_QNAME, AddCardDeposit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BlockChequeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "blockChequeResponse")
    public JAXBElement<BlockChequeResponse> createBlockChequeResponse(BlockChequeResponse value) {
        return new JAXBElement<BlockChequeResponse>(_BlockChequeResponse_QNAME, BlockChequeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServiceConstraintListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getServiceConstraintListResponse")
    public JAXBElement<GetServiceConstraintListResponse> createGetServiceConstraintListResponse(GetServiceConstraintListResponse value) {
        return new JAXBElement<GetServiceConstraintListResponse>(_GetServiceConstraintListResponse_QNAME, GetServiceConstraintListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCards }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCards")
    public JAXBElement<GetCards> createGetCards(GetCards value) {
        return new JAXBElement<GetCards>(_GetCards_QNAME, GetCards.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositRates }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getDepositRates")
    public JAXBElement<GetDepositRates> createGetDepositRates(GetDepositRates value) {
        return new JAXBElement<GetDepositRates>(_GetDepositRates_QNAME, GetDepositRates.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TopupChargeFromVirtualTerminalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "topupChargeFromVirtualTerminalResponse")
    public JAXBElement<TopupChargeFromVirtualTerminalResponse> createTopupChargeFromVirtualTerminalResponse(TopupChargeFromVirtualTerminalResponse value) {
        return new JAXBElement<TopupChargeFromVirtualTerminalResponse>(_TopupChargeFromVirtualTerminalResponse_QNAME, TopupChargeFromVirtualTerminalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChSignedDepositRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chSignedDepositRequestBean")
    public JAXBElement<ChSignedDepositRequestBean> createChSignedDepositRequestBean(ChSignedDepositRequestBean value) {
        return new JAXBElement<ChSignedDepositRequestBean>(_ChSignedDepositRequestBean_QNAME, ChSignedDepositRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOwnerDepositName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getOwnerDepositName")
    public JAXBElement<GetOwnerDepositName> createGetOwnerDepositName(GetOwnerDepositName value) {
        return new JAXBElement<GetOwnerDepositName>(_GetOwnerDepositName_QNAME, GetOwnerDepositName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransferChequeListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getTransferChequeListResponse")
    public JAXBElement<GetTransferChequeListResponse> createGetTransferChequeListResponse(GetTransferChequeListResponse value) {
        return new JAXBElement<GetTransferChequeListResponse>(_GetTransferChequeListResponse_QNAME, GetTransferChequeListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPaymentPreview }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getPaymentPreview")
    public JAXBElement<GetPaymentPreview> createGetPaymentPreview(GetPaymentPreview value) {
        return new JAXBElement<GetPaymentPreview>(_GetPaymentPreview_QNAME, GetPaymentPreview.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRequestStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getRequestStatusResponse")
    public JAXBElement<GetRequestStatusResponse> createGetRequestStatusResponse(GetRequestStatusResponse value) {
        return new JAXBElement<GetRequestStatusResponse>(_GetRequestStatusResponse_QNAME, GetRequestStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendReversal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "sendReversal")
    public JAXBElement<SendReversal> createSendReversal(SendReversal value) {
        return new JAXBElement<SendReversal>(_SendReversal_QNAME, SendReversal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStatement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getStatement")
    public JAXBElement<GetStatement> createGetStatement(GetStatement value) {
        return new JAXBElement<GetStatement>(_GetStatement_QNAME, GetStatement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChAchTransferSummeryFilterBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chAchTransferSummeryFilterBean")
    public JAXBElement<ChAchTransferSummeryFilterBean> createChAchTransferSummeryFilterBean(ChAchTransferSummeryFilterBean value) {
        return new JAXBElement<ChAchTransferSummeryFilterBean>(_ChAchTransferSummeryFilterBean_QNAME, ChAchTransferSummeryFilterBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChargeCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "chargeCard")
    public JAXBElement<ChargeCard> createChargeCard(ChargeCard value) {
        return new JAXBElement<ChargeCard>(_ChargeCard_QNAME, ChargeCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IbanEnquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "ibanEnquiryResponse")
    public JAXBElement<IbanEnquiryResponse> createIbanEnquiryResponse(IbanEnquiryResponse value) {
        return new JAXBElement<IbanEnquiryResponse>(_IbanEnquiryResponse_QNAME, IbanEnquiryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeUsername }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "changeUsername")
    public JAXBElement<ChangeUsername> createChangeUsername(ChangeUsername value) {
        return new JAXBElement<ChangeUsername>(_ChangeUsername_QNAME, ChangeUsername.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChDepositNumberToIbanRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chDepositNumberToIbanRequestBean")
    public JAXBElement<ChDepositNumberToIbanRequestBean> createChDepositNumberToIbanRequestBean(ChDepositNumberToIbanRequestBean value) {
        return new JAXBElement<ChDepositNumberToIbanRequestBean>(_ChDepositNumberToIbanRequestBean_QNAME, ChDepositNumberToIbanRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChAutoAchTransferRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chAutoAchTransferRequestBean")
    public JAXBElement<ChAutoAchTransferRequestBean> createChAutoAchTransferRequestBean(ChAutoAchTransferRequestBean value) {
        return new JAXBElement<ChAutoAchTransferRequestBean>(_ChAutoAchTransferRequestBean_QNAME, ChAutoAchTransferRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChequeBookListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getChequeBookListResponse")
    public JAXBElement<GetChequeBookListResponse> createGetChequeBookListResponse(GetChequeBookListResponse value) {
        return new JAXBElement<GetChequeBookListResponse>(_GetChequeBookListResponse_QNAME, GetChequeBookListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResumeAchTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "resumeAchTransferResponse")
    public JAXBElement<ResumeAchTransferResponse> createResumeAchTransferResponse(ResumeAchTransferResponse value) {
        return new JAXBElement<ResumeAchTransferResponse>(_ResumeAchTransferResponse_QNAME, ResumeAchTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChSMSSettingRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chSMSSettingRequestBean")
    public JAXBElement<ChSMSSettingRequestBean> createChSMSSettingRequestBean(ChSMSSettingRequestBean value) {
        return new JAXBElement<ChSMSSettingRequestBean>(_ChSMSSettingRequestBean_QNAME, ChSMSSettingRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserBillSettingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "updateUserBillSettingResponse")
    public JAXBElement<UpdateUserBillSettingResponse> createUpdateUserBillSettingResponse(UpdateUserBillSettingResponse value) {
        return new JAXBElement<UpdateUserBillSettingResponse>(_UpdateUserBillSettingResponse_QNAME, UpdateUserBillSettingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AchAutoTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "achAutoTransfer")
    public JAXBElement<AchAutoTransfer> createAchAutoTransfer(AchAutoTransfer value) {
        return new JAXBElement<AchAutoTransfer>(_AchAutoTransfer_QNAME, AchAutoTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChDisableTransferRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chDisableTransferRequestBean")
    public JAXBElement<ChDisableTransferRequestBean> createChDisableTransferRequestBean(ChDisableTransferRequestBean value) {
        return new JAXBElement<ChDisableTransferRequestBean>(_ChDisableTransferRequestBean_QNAME, ChDisableTransferRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HotCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "hotCard")
    public JAXBElement<HotCard> createHotCard(HotCard value) {
        return new JAXBElement<HotCard>(_HotCard_QNAME, HotCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayCreditBill }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "payCreditBill")
    public JAXBElement<PayCreditBill> createPayCreditBill(PayCreditBill value) {
        return new JAXBElement<PayCreditBill>(_PayCreditBill_QNAME, PayCreditBill.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBaseSearchChequeBookRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBaseSearchChequeBookRequestBean")
    public JAXBElement<ChBaseSearchChequeBookRequestBean> createChBaseSearchChequeBookRequestBean(ChBaseSearchChequeBookRequestBean value) {
        return new JAXBElement<ChBaseSearchChequeBookRequestBean>(_ChBaseSearchChequeBookRequestBean_QNAME, ChBaseSearchChequeBookRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DefineCustomerSignatureSample }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "defineCustomerSignatureSample")
    public JAXBElement<DefineCustomerSignatureSample> createDefineCustomerSignatureSample(DefineCustomerSignatureSample value) {
        return new JAXBElement<DefineCustomerSignatureSample>(_DefineCustomerSignatureSample_QNAME, DefineCustomerSignatureSample.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getDepositCustomerResponse")
    public JAXBElement<GetDepositCustomerResponse> createGetDepositCustomerResponse(GetDepositCustomerResponse value) {
        return new JAXBElement<GetDepositCustomerResponse>(_GetDepositCustomerResponse_QNAME, GetDepositCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCreditBillTransactionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCreditBillTransactionsResponse")
    public JAXBElement<GetCreditBillTransactionsResponse> createGetCreditBillTransactionsResponse(GetCreditBillTransactionsResponse value) {
        return new JAXBElement<GetCreditBillTransactionsResponse>(_GetCreditBillTransactionsResponse_QNAME, GetCreditBillTransactionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayLoanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "payLoanResponse")
    public JAXBElement<PayLoanResponse> createPayLoanResponse(PayLoanResponse value) {
        return new JAXBElement<PayLoanResponse>(_PayLoanResponse_QNAME, PayLoanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveOrUpdateSecondPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "saveOrUpdateSecondPassword")
    public JAXBElement<SaveOrUpdateSecondPassword> createSaveOrUpdateSecondPassword(SaveOrUpdateSecondPassword value) {
        return new JAXBElement<SaveOrUpdateSecondPassword>(_SaveOrUpdateSecondPassword_QNAME, SaveOrUpdateSecondPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCreditDossiers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCreditDossiers")
    public JAXBElement<GetCreditDossiers> createGetCreditDossiers(GetCreditDossiers value) {
        return new JAXBElement<GetCreditDossiers>(_GetCreditDossiers_QNAME, GetCreditDossiers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardPurchase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardPurchase")
    public JAXBElement<CardPurchase> createCardPurchase(CardPurchase value) {
        return new JAXBElement<CardPurchase>(_CardPurchase_QNAME, CardPurchase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardSetPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardSetPasswordResponse")
    public JAXBElement<CardSetPasswordResponse> createCardSetPasswordResponse(CardSetPasswordResponse value) {
        return new JAXBElement<CardSetPasswordResponse>(_CardSetPasswordResponse_QNAME, CardSetPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrencyRateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCurrencyRateResponse")
    public JAXBElement<GetCurrencyRateResponse> createGetCurrencyRateResponse(GetCurrencyRateResponse value) {
        return new JAXBElement<GetCurrencyRateResponse>(_GetCurrencyRateResponse_QNAME, GetCurrencyRateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBillTransactionSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBillTransactionSearchRequestBean")
    public JAXBElement<ChBillTransactionSearchRequestBean> createChBillTransactionSearchRequestBean(ChBillTransactionSearchRequestBean value) {
        return new JAXBElement<ChBillTransactionSearchRequestBean>(_ChBillTransactionSearchRequestBean_QNAME, ChBillTransactionSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChDepositSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chDepositSearchRequestBean")
    public JAXBElement<ChDepositSearchRequestBean> createChDepositSearchRequestBean(ChDepositSearchRequestBean value) {
        return new JAXBElement<ChDepositSearchRequestBean>(_ChDepositSearchRequestBean_QNAME, ChDepositSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChRtgsNormalTransferRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chRtgsNormalTransferRequestBean")
    public JAXBElement<ChRtgsNormalTransferRequestBean> createChRtgsNormalTransferRequestBean(ChRtgsNormalTransferRequestBean value) {
        return new JAXBElement<ChRtgsNormalTransferRequestBean>(_ChRtgsNormalTransferRequestBean_QNAME, ChRtgsNormalTransferRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBeOpenedDepositsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBeOpenedDepositsResponse")
    public JAXBElement<GetBeOpenedDepositsResponse> createGetBeOpenedDepositsResponse(GetBeOpenedDepositsResponse value) {
        return new JAXBElement<GetBeOpenedDepositsResponse>(_GetBeOpenedDepositsResponse_QNAME, GetBeOpenedDepositsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPaymentServicesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getPaymentServicesResponse")
    public JAXBElement<GetPaymentServicesResponse> createGetPaymentServicesResponse(GetPaymentServicesResponse value) {
        return new JAXBElement<GetPaymentServicesResponse>(_GetPaymentServicesResponse_QNAME, GetPaymentServicesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCustomerAddressRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCustomerAddressRequestBean")
    public JAXBElement<ChCustomerAddressRequestBean> createChCustomerAddressRequestBean(ChCustomerAddressRequestBean value) {
        return new JAXBElement<ChCustomerAddressRequestBean>(_ChCustomerAddressRequestBean_QNAME, ChCustomerAddressRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BlockDepositAmount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "blockDepositAmount")
    public JAXBElement<BlockDepositAmount> createBlockDepositAmount(BlockDepositAmount value) {
        return new JAXBElement<BlockDepositAmount>(_BlockDepositAmount_QNAME, BlockDepositAmount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositCustomer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getDepositCustomer")
    public JAXBElement<GetDepositCustomer> createGetDepositCustomer(GetDepositCustomer value) {
        return new JAXBElement<GetDepositCustomer>(_GetDepositCustomer_QNAME, GetDepositCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RtgsNormalTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "rtgsNormalTransferResponse")
    public JAXBElement<RtgsNormalTransferResponse> createRtgsNormalTransferResponse(RtgsNormalTransferResponse value) {
        return new JAXBElement<RtgsNormalTransferResponse>(_RtgsNormalTransferResponse_QNAME, RtgsNormalTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChAchTransferSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chAchTransferSearchRequestBean")
    public JAXBElement<ChAchTransferSearchRequestBean> createChAchTransferSearchRequestBean(ChAchTransferSearchRequestBean value) {
        return new JAXBElement<ChAchTransferSearchRequestBean>(_ChAchTransferSearchRequestBean_QNAME, ChAchTransferSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getUserInfoResponse")
    public JAXBElement<GetUserInfoResponse> createGetUserInfoResponse(GetUserInfoResponse value) {
        return new JAXBElement<GetUserInfoResponse>(_GetUserInfoResponse_QNAME, GetUserInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelAchTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cancelAchTransfer")
    public JAXBElement<CancelAchTransfer> createCancelAchTransfer(CancelAchTransfer value) {
        return new JAXBElement<CancelAchTransfer>(_CancelAchTransfer_QNAME, CancelAchTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResumeAchTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "resumeAchTransaction")
    public JAXBElement<ResumeAchTransaction> createResumeAchTransaction(ResumeAchTransaction value) {
        return new JAXBElement<ResumeAchTransaction>(_ResumeAchTransaction_QNAME, ResumeAchTransaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAutoTransferList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getAutoTransferList")
    public JAXBElement<GetAutoTransferList> createGetAutoTransferList(GetAutoTransferList value) {
        return new JAXBElement<GetAutoTransferList>(_GetAutoTransferList_QNAME, GetAutoTransferList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBatchBillInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBatchBillInfoResponse")
    public JAXBElement<GetBatchBillInfoResponse> createGetBatchBillInfoResponse(GetBatchBillInfoResponse value) {
        return new JAXBElement<GetBatchBillInfoResponse>(_GetBatchBillInfoResponse_QNAME, GetBatchBillInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFavoriteAccountSetting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getFavoriteAccountSetting")
    public JAXBElement<GetFavoriteAccountSetting> createGetFavoriteAccountSetting(GetFavoriteAccountSetting value) {
        return new JAXBElement<GetFavoriteAccountSetting>(_GetFavoriteAccountSetting_QNAME, GetFavoriteAccountSetting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRequestStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getRequestStatus")
    public JAXBElement<GetRequestStatus> createGetRequestStatus(GetRequestStatus value) {
        return new JAXBElement<GetRequestStatus>(_GetRequestStatus_QNAME, GetRequestStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardExtend }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardExtend")
    public JAXBElement<CardExtend> createCardExtend(CardExtend value) {
        return new JAXBElement<CardExtend>(_CardExtend_QNAME, CardExtend.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResumeAchTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "resumeAchTransfer")
    public JAXBElement<ResumeAchTransfer> createResumeAchTransfer(ResumeAchTransfer value) {
        return new JAXBElement<ResumeAchTransfer>(_ResumeAchTransfer_QNAME, ResumeAchTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChAccountOwner }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "AccountOwner")
    public JAXBElement<ChAccountOwner> createAccountOwner(ChAccountOwner value) {
        return new JAXBElement<ChAccountOwner>(_AccountOwner_QNAME, ChAccountOwner.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransferConstraintInfoPerService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getTransferConstraintInfoPerService")
    public JAXBElement<GetTransferConstraintInfoPerService> createGetTransferConstraintInfoPerService(GetTransferConstraintInfoPerService value) {
        return new JAXBElement<GetTransferConstraintInfoPerService>(_GetTransferConstraintInfoPerService_QNAME, GetTransferConstraintInfoPerService.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SwitchFundTransferFromPhysicalTerminal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "switchFundTransferFromPhysicalTerminal")
    public JAXBElement<SwitchFundTransferFromPhysicalTerminal> createSwitchFundTransferFromPhysicalTerminal(SwitchFundTransferFromPhysicalTerminal value) {
        return new JAXBElement<SwitchFundTransferFromPhysicalTerminal>(_SwitchFundTransferFromPhysicalTerminal_QNAME, SwitchFundTransferFromPhysicalTerminal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChLoanPaymentRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chLoanPaymentRequestBean")
    public JAXBElement<ChLoanPaymentRequestBean> createChLoanPaymentRequestBean(ChLoanPaymentRequestBean value) {
        return new JAXBElement<ChLoanPaymentRequestBean>(_ChLoanPaymentRequestBean_QNAME, ChLoanPaymentRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPaymentServicesByTypeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getPaymentServicesByTypeResponse")
    public JAXBElement<GetPaymentServicesByTypeResponse> createGetPaymentServicesByTypeResponse(GetPaymentServicesByTypeResponse value) {
        return new JAXBElement<GetPaymentServicesByTypeResponse>(_GetPaymentServicesByTypeResponse_QNAME, GetPaymentServicesByTypeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCreditBillPaymentRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCreditBillPaymentRequestBean")
    public JAXBElement<ChCreditBillPaymentRequestBean> createChCreditBillPaymentRequestBean(ChCreditBillPaymentRequestBean value) {
        return new JAXBElement<ChCreditBillPaymentRequestBean>(_ChCreditBillPaymentRequestBean_QNAME, ChCreditBillPaymentRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSellDetailReport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getSellDetailReport")
    public JAXBElement<GetSellDetailReport> createGetSellDetailReport(GetSellDetailReport value) {
        return new JAXBElement<GetSellDetailReport>(_GetSellDetailReport_QNAME, GetSellDetailReport.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChVirtualCardTransferRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chVirtualCardTransferRequest")
    public JAXBElement<ChVirtualCardTransferRequest> createChVirtualCardTransferRequest(ChVirtualCardTransferRequest value) {
        return new JAXBElement<ChVirtualCardTransferRequest>(_ChVirtualCardTransferRequest_QNAME, ChVirtualCardTransferRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginWithMobileNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "loginWithMobileNumber")
    public JAXBElement<LoginWithMobileNumber> createLoginWithMobileNumber(LoginWithMobileNumber value) {
        return new JAXBElement<LoginWithMobileNumber>(_LoginWithMobileNumber_QNAME, LoginWithMobileNumber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AutoTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "autoTransfer")
    public JAXBElement<AutoTransfer> createAutoTransfer(AutoTransfer value) {
        return new JAXBElement<AutoTransfer>(_AutoTransfer_QNAME, AutoTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBatchBillPaymentRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBatchBillPaymentRequestBean")
    public JAXBElement<ChBatchBillPaymentRequestBean> createChBatchBillPaymentRequestBean(ChBatchBillPaymentRequestBean value) {
        return new JAXBElement<ChBatchBillPaymentRequestBean>(_ChBatchBillPaymentRequestBean_QNAME, ChBatchBillPaymentRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResumeAchTransactionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "resumeAchTransactionResponse")
    public JAXBElement<ResumeAchTransactionResponse> createResumeAchTransactionResponse(ResumeAchTransactionResponse value) {
        return new JAXBElement<ResumeAchTransactionResponse>(_ResumeAchTransactionResponse_QNAME, ResumeAchTransactionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Login }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "login")
    public JAXBElement<Login> createLogin(Login value) {
        return new JAXBElement<Login>(_Login_QNAME, Login.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterChequeBookRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "registerChequeBookRequest")
    public JAXBElement<RegisterChequeBookRequest> createRegisterChequeBookRequest(RegisterChequeBookRequest value) {
        return new JAXBElement<RegisterChequeBookRequest>(_RegisterChequeBookRequest_QNAME, RegisterChequeBookRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoPayment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "doPayment")
    public JAXBElement<DoPayment> createDoPayment(DoPayment value) {
        return new JAXBElement<DoPayment>(_DoPayment_QNAME, DoPayment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChSearchCreditDossierRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chSearchCreditDossierRequestBean")
    public JAXBElement<ChSearchCreditDossierRequestBean> createChSearchCreditDossierRequestBean(ChSearchCreditDossierRequestBean value) {
        return new JAXBElement<ChSearchCreditDossierRequestBean>(_ChSearchCreditDossierRequestBean_QNAME, ChSearchCreditDossierRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CashChequeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cashChequeResponse")
    public JAXBElement<CashChequeResponse> createCashChequeResponse(CashChequeResponse value) {
        return new JAXBElement<CashChequeResponse>(_CashChequeResponse_QNAME, CashChequeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CashCheque }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cashCheque")
    public JAXBElement<CashCheque> createCashCheque(CashCheque value) {
        return new JAXBElement<CashCheque>(_CashCheque_QNAME, CashCheque.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardProductsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardProductsResponse")
    public JAXBElement<GetCardProductsResponse> createGetCardProductsResponse(GetCardProductsResponse value) {
        return new JAXBElement<GetCardProductsResponse>(_GetCardProductsResponse_QNAME, GetCardProductsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBillSettingRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBillSettingRequestBean")
    public JAXBElement<ChBillSettingRequestBean> createChBillSettingRequestBean(ChBillSettingRequestBean value) {
        return new JAXBElement<ChBillSettingRequestBean>(_ChBillSettingRequestBean_QNAME, ChBillSettingRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChUserSMSSettingRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chUserSMSSettingRequestBean")
    public JAXBElement<ChUserSMSSettingRequestBean> createChUserSMSSettingRequestBean(ChUserSMSSettingRequestBean value) {
        return new JAXBElement<ChUserSMSSettingRequestBean>(_ChUserSMSSettingRequestBean_QNAME, ChUserSMSSettingRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportAchTransferSummaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "reportAchTransferSummaryResponse")
    public JAXBElement<ReportAchTransferSummaryResponse> createReportAchTransferSummaryResponse(ReportAchTransferSummaryResponse value) {
        return new JAXBElement<ReportAchTransferSummaryResponse>(_ReportAchTransferSummaryResponse_QNAME, ReportAchTransferSummaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChFavoriteDepositNumberRequestBeans }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chFavoriteDepositNumberRequestBeans")
    public JAXBElement<ChFavoriteDepositNumberRequestBeans> createChFavoriteDepositNumberRequestBeans(ChFavoriteDepositNumberRequestBeans value) {
        return new JAXBElement<ChFavoriteDepositNumberRequestBeans>(_ChFavoriteDepositNumberRequestBeans_QNAME, ChFavoriteDepositNumberRequestBeans.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChMerchantInfoSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chMerchantInfoSearchRequestBean")
    public JAXBElement<ChMerchantInfoSearchRequestBean> createChMerchantInfoSearchRequestBean(ChMerchantInfoSearchRequestBean value) {
        return new JAXBElement<ChMerchantInfoSearchRequestBean>(_ChMerchantInfoSearchRequestBean_QNAME, ChMerchantInfoSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoPaymentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "doPaymentResponse")
    public JAXBElement<DoPaymentResponse> createDoPaymentResponse(DoPaymentResponse value) {
        return new JAXBElement<DoPaymentResponse>(_DoPaymentResponse_QNAME, DoPaymentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConvertIbanToDepositNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "convertIbanToDepositNumber")
    public JAXBElement<ConvertIbanToDepositNumber> createConvertIbanToDepositNumber(ConvertIbanToDepositNumber value) {
        return new JAXBElement<ConvertIbanToDepositNumber>(_ConvertIbanToDepositNumber_QNAME, ConvertIbanToDepositNumber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getUserInfo")
    public JAXBElement<GetUserInfo> createGetUserInfo(GetUserInfo value) {
        return new JAXBElement<GetUserInfo>(_GetUserInfo_QNAME, GetUserInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChDepositCustomerRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chDepositCustomerRequestBean")
    public JAXBElement<ChDepositCustomerRequestBean> createChDepositCustomerRequestBean(ChDepositCustomerRequestBean value) {
        return new JAXBElement<ChDepositCustomerRequestBean>(_ChDepositCustomerRequestBean_QNAME, ChDepositCustomerRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChRemoveCardDepositRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chRemoveCardDepositRequestBean")
    public JAXBElement<ChRemoveCardDepositRequestBean> createChRemoveCardDepositRequestBean(ChRemoveCardDepositRequestBean value) {
        return new JAXBElement<ChRemoveCardDepositRequestBean>(_ChRemoveCardDepositRequestBean_QNAME, ChRemoveCardDepositRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChChangePasswordRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chChangePasswordRequestBean")
    public JAXBElement<ChChangePasswordRequestBean> createChChangePasswordRequestBean(ChChangePasswordRequestBean value) {
        return new JAXBElement<ChChangePasswordRequestBean>(_ChChangePasswordRequestBean_QNAME, ChChangePasswordRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChAchTransactionKeysRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chAchTransactionKeysRequestBean")
    public JAXBElement<ChAchTransactionKeysRequestBean> createChAchTransactionKeysRequestBean(ChAchTransactionKeysRequestBean value) {
        return new JAXBElement<ChAchTransactionKeysRequestBean>(_ChAchTransactionKeysRequestBean_QNAME, ChAchTransactionKeysRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserSMSSetting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getUserSMSSetting")
    public JAXBElement<GetUserSMSSetting> createGetUserSMSSetting(GetUserSMSSetting value) {
        return new JAXBElement<GetUserSMSSetting>(_GetUserSMSSetting_QNAME, GetUserSMSSetting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChStatementSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chStatementSearchRequestBean")
    public JAXBElement<ChStatementSearchRequestBean> createChStatementSearchRequestBean(ChStatementSearchRequestBean value) {
        return new JAXBElement<ChStatementSearchRequestBean>(_ChStatementSearchRequestBean_QNAME, ChStatementSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChSellDetailRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chSellDetailRequestBean")
    public JAXBElement<ChSellDetailRequestBean> createChSellDetailRequestBean(ChSellDetailRequestBean value) {
        return new JAXBElement<ChSellDetailRequestBean>(_ChSellDetailRequestBean_QNAME, ChSellDetailRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGuarantyListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getGuarantyListResponse")
    public JAXBElement<GetGuarantyListResponse> createGetGuarantyListResponse(GetGuarantyListResponse value) {
        return new JAXBElement<GetGuarantyListResponse>(_GetGuarantyListResponse_QNAME, GetGuarantyListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelAchTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cancelAchTransferResponse")
    public JAXBElement<CancelAchTransferResponse> createCancelAchTransferResponse(CancelAchTransferResponse value) {
        return new JAXBElement<CancelAchTransferResponse>(_CancelAchTransferResponse_QNAME, CancelAchTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcceptAchTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "acceptAchTransferResponse")
    public JAXBElement<AcceptAchTransferResponse> createAcceptAchTransferResponse(AcceptAchTransferResponse value) {
        return new JAXBElement<AcceptAchTransferResponse>(_AcceptAchTransferResponse_QNAME, AcceptAchTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChChargeCardRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chChargeCardRequestBean")
    public JAXBElement<ChChargeCardRequestBean> createChChargeCardRequestBean(ChChargeCardRequestBean value) {
        return new JAXBElement<ChChargeCardRequestBean>(_ChChargeCardRequestBean_QNAME, ChChargeCardRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerSignaturesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCustomerSignaturesResponse")
    public JAXBElement<GetCustomerSignaturesResponse> createGetCustomerSignaturesResponse(GetCustomerSignaturesResponse value) {
        return new JAXBElement<GetCustomerSignaturesResponse>(_GetCustomerSignaturesResponse_QNAME, GetCustomerSignaturesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSellDetailReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getSellDetailReportResponse")
    public JAXBElement<GetSellDetailReportResponse> createGetSellDetailReportResponse(GetSellDetailReportResponse value) {
        return new JAXBElement<GetSellDetailReportResponse>(_GetSellDetailReportResponse_QNAME, GetSellDetailReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChDepositRateInquiryRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chDepositRateInquiryRequestBean")
    public JAXBElement<ChDepositRateInquiryRequestBean> createChDepositRateInquiryRequestBean(ChDepositRateInquiryRequestBean value) {
        return new JAXBElement<ChDepositRateInquiryRequestBean>(_ChDepositRateInquiryRequestBean_QNAME, ChDepositRateInquiryRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChMobileLoginRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chMobileLoginRequestBean")
    public JAXBElement<ChMobileLoginRequestBean> createChMobileLoginRequestBean(ChMobileLoginRequestBean value) {
        return new JAXBElement<ChMobileLoginRequestBean>(_ChMobileLoginRequestBean_QNAME, ChMobileLoginRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSellReport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getSellReport")
    public JAXBElement<GetSellReport> createGetSellReport(GetSellReport value) {
        return new JAXBElement<GetSellReport>(_GetSellReport_QNAME, GetSellReport.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Logout }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "logout")
    public JAXBElement<Logout> createLogout(Logout value) {
        return new JAXBElement<Logout>(_Logout_QNAME, Logout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBranches }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBranches")
    public JAXBElement<GetBranches> createGetBranches(GetBranches value) {
        return new JAXBElement<GetBranches>(_GetBranches_QNAME, GetBranches.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AchTransactionReport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "achTransactionReport")
    public JAXBElement<AchTransactionReport> createAchTransactionReport(AchTransactionReport value) {
        return new JAXBElement<AchTransactionReport>(_AchTransactionReport_QNAME, AchTransactionReport.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIssuingAllowedDepositsToOpen }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardIssuingAllowedDepositsToOpen")
    public JAXBElement<CardIssuingAllowedDepositsToOpen> createCardIssuingAllowedDepositsToOpen(CardIssuingAllowedDepositsToOpen value) {
        return new JAXBElement<CardIssuingAllowedDepositsToOpen>(_CardIssuingAllowedDepositsToOpen_QNAME, CardIssuingAllowedDepositsToOpen.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeposits }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getDeposits")
    public JAXBElement<GetDeposits> createGetDeposits(GetDeposits value) {
        return new JAXBElement<GetDeposits>(_GetDeposits_QNAME, GetDeposits.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SuspendAchTransactionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "suspendAchTransactionResponse")
    public JAXBElement<SuspendAchTransactionResponse> createSuspendAchTransactionResponse(SuspendAchTransactionResponse value) {
        return new JAXBElement<SuspendAchTransactionResponse>(_SuspendAchTransactionResponse_QNAME, SuspendAchTransactionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HotCardByPin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "hotCardByPin")
    public JAXBElement<HotCardByPin> createHotCardByPin(HotCardByPin value) {
        return new JAXBElement<HotCardByPin>(_HotCardByPin_QNAME, HotCardByPin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBaseCreditBillSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBaseCreditBillSearchRequestBean")
    public JAXBElement<ChBaseCreditBillSearchRequestBean> createChBaseCreditBillSearchRequestBean(ChBaseCreditBillSearchRequestBean value) {
        return new JAXBElement<ChBaseCreditBillSearchRequestBean>(_ChBaseCreditBillSearchRequestBean_QNAME, ChBaseCreditBillSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterChequeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "registerChequeResponse")
    public JAXBElement<RegisterChequeResponse> createRegisterChequeResponse(RegisterChequeResponse value) {
        return new JAXBElement<RegisterChequeResponse>(_RegisterChequeResponse_QNAME, RegisterChequeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSignedDepositsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getSignedDepositsResponse")
    public JAXBElement<GetSignedDepositsResponse> createGetSignedDepositsResponse(GetSignedDepositsResponse value) {
        return new JAXBElement<GetSignedDepositsResponse>(_GetSignedDepositsResponse_QNAME, GetSignedDepositsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChHotCardByPinRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chHotCardByPinRequestBean")
    public JAXBElement<ChHotCardByPinRequestBean> createChHotCardByPinRequestBean(ChHotCardByPinRequestBean value) {
        return new JAXBElement<ChHotCardByPinRequestBean>(_ChHotCardByPinRequestBean_QNAME, ChHotCardByPinRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChAddCardDepositRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chAddCardDepositRequestBean")
    public JAXBElement<ChAddCardDepositRequestBean> createChAddCardDepositRequestBean(ChAddCardDepositRequestBean value) {
        return new JAXBElement<ChAddCardDepositRequestBean>(_ChAddCardDepositRequestBean_QNAME, ChAddCardDepositRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChLoanDetailSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chLoanDetailSearchRequestBean")
    public JAXBElement<ChLoanDetailSearchRequestBean> createChLoanDetailSearchRequestBean(ChLoanDetailSearchRequestBean value) {
        return new JAXBElement<ChLoanDetailSearchRequestBean>(_ChLoanDetailSearchRequestBean_QNAME, ChLoanDetailSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIssuing }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardIssuing")
    public JAXBElement<CardIssuing> createCardIssuing(CardIssuing value) {
        return new JAXBElement<CardIssuing>(_CardIssuing_QNAME, CardIssuing.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChFundTransferRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chFundTransferRequestBean")
    public JAXBElement<ChFundTransferRequestBean> createChFundTransferRequestBean(ChFundTransferRequestBean value) {
        return new JAXBElement<ChFundTransferRequestBean>(_ChFundTransferRequestBean_QNAME, ChFundTransferRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveCardDepositResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "removeCardDepositResponse")
    public JAXBElement<RemoveCardDepositResponse> createRemoveCardDepositResponse(RemoveCardDepositResponse value) {
        return new JAXBElement<RemoveCardDepositResponse>(_RemoveCardDepositResponse_QNAME, RemoveCardDepositResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLoans }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getLoans")
    public JAXBElement<GetLoans> createGetLoans(GetLoans value) {
        return new JAXBElement<GetLoans>(_GetLoans_QNAME, GetLoans.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCreditBillsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCreditBillsResponse")
    public JAXBElement<GetCreditBillsResponse> createGetCreditBillsResponse(GetCreditBillsResponse value) {
        return new JAXBElement<GetCreditBillsResponse>(_GetCreditBillsResponse_QNAME, GetCreditBillsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveCardDeposit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "removeCardDeposit")
    public JAXBElement<RemoveCardDeposit> createRemoveCardDeposit(RemoveCardDeposit value) {
        return new JAXBElement<RemoveCardDeposit>(_RemoveCardDeposit_QNAME, RemoveCardDeposit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPaymentBillStatementResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getPaymentBillStatementResponse")
    public JAXBElement<GetPaymentBillStatementResponse> createGetPaymentBillStatementResponse(GetPaymentBillStatementResponse value) {
        return new JAXBElement<GetPaymentBillStatementResponse>(_GetPaymentBillStatementResponse_QNAME, GetPaymentBillStatementResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "sendMail")
    public JAXBElement<SendMail> createSendMail(SendMail value) {
        return new JAXBElement<SendMail>(_SendMail_QNAME, SendMail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChSecondPasswordRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chSecondPasswordRequestBean")
    public JAXBElement<ChSecondPasswordRequestBean> createChSecondPasswordRequestBean(ChSecondPasswordRequestBean value) {
        return new JAXBElement<ChSecondPasswordRequestBean>(_ChSecondPasswordRequestBean_QNAME, ChSecondPasswordRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardOwner }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardOwner")
    public JAXBElement<GetCardOwner> createGetCardOwner(GetCardOwner value) {
        return new JAXBElement<GetCardOwner>(_GetCardOwner_QNAME, GetCardOwner.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPaymentPreviewResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getPaymentPreviewResponse")
    public JAXBElement<GetPaymentPreviewResponse> createGetPaymentPreviewResponse(GetPaymentPreviewResponse value) {
        return new JAXBElement<GetPaymentPreviewResponse>(_GetPaymentPreviewResponse_QNAME, GetPaymentPreviewResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChNormalTransferRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chNormalTransferRequestBean")
    public JAXBElement<ChNormalTransferRequestBean> createChNormalTransferRequestBean(ChNormalTransferRequestBean value) {
        return new JAXBElement<ChNormalTransferRequestBean>(_ChNormalTransferRequestBean_QNAME, ChNormalTransferRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HotCardByPinResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "hotCardByPinResponse")
    public JAXBElement<HotCardByPinResponse> createHotCardByPinResponse(HotCardByPinResponse value) {
        return new JAXBElement<HotCardByPinResponse>(_HotCardByPinResponse_QNAME, HotCardByPinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBranchesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBranchesResponse")
    public JAXBElement<GetBranchesResponse> createGetBranchesResponse(GetBranchesResponse value) {
        return new JAXBElement<GetBranchesResponse>(_GetBranchesResponse_QNAME, GetBranchesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardChangePin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardChangePin")
    public JAXBElement<CardChangePin> createCardChangePin(CardChangePin value) {
        return new JAXBElement<CardChangePin>(_CardChangePin_QNAME, CardChangePin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBranchAllowedDepositsToOpenResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBranchAllowedDepositsToOpenResponse")
    public JAXBElement<GetBranchAllowedDepositsToOpenResponse> createGetBranchAllowedDepositsToOpenResponse(GetBranchAllowedDepositsToOpenResponse value) {
        return new JAXBElement<GetBranchAllowedDepositsToOpenResponse>(_GetBranchAllowedDepositsToOpenResponse_QNAME, GetBranchAllowedDepositsToOpenResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBankSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBankSearchRequestBean")
    public JAXBElement<ChBankSearchRequestBean> createChBankSearchRequestBean(ChBankSearchRequestBean value) {
        return new JAXBElement<ChBankSearchRequestBean>(_ChBankSearchRequestBean_QNAME, ChBankSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChRegisterChequeRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chRegisterChequeRequestBean")
    public JAXBElement<ChRegisterChequeRequestBean> createChRegisterChequeRequestBean(ChRegisterChequeRequestBean value) {
        return new JAXBElement<ChRegisterChequeRequestBean>(_ChRegisterChequeRequestBean_QNAME, ChRegisterChequeRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChPaymentBillStatementRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chPaymentBillStatementRequestBean")
    public JAXBElement<ChPaymentBillStatementRequestBean> createChPaymentBillStatementRequestBean(ChPaymentBillStatementRequestBean value) {
        return new JAXBElement<ChPaymentBillStatementRequestBean>(_ChPaymentBillStatementRequestBean_QNAME, ChPaymentBillStatementRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateDefaultContactInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "updateDefaultContactInfoResponse")
    public JAXBElement<UpdateDefaultContactInfoResponse> createUpdateDefaultContactInfoResponse(UpdateDefaultContactInfoResponse value) {
        return new JAXBElement<UpdateDefaultContactInfoResponse>(_UpdateDefaultContactInfoResponse_QNAME, UpdateDefaultContactInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServiceConstraintList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getServiceConstraintList")
    public JAXBElement<GetServiceConstraintList> createGetServiceConstraintList(GetServiceConstraintList value) {
        return new JAXBElement<GetServiceConstraintList>(_GetServiceConstraintList_QNAME, GetServiceConstraintList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDestinationCardOwnerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getDestinationCardOwnerResponse")
    public JAXBElement<GetDestinationCardOwnerResponse> createGetDestinationCardOwnerResponse(GetDestinationCardOwnerResponse value) {
        return new JAXBElement<GetDestinationCardOwnerResponse>(_GetDestinationCardOwnerResponse_QNAME, GetDestinationCardOwnerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCardPurchaseRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCardPurchaseRequestBean")
    public JAXBElement<ChCardPurchaseRequestBean> createChCardPurchaseRequestBean(ChCardPurchaseRequestBean value) {
        return new JAXBElement<ChCardPurchaseRequestBean>(_ChCardPurchaseRequestBean_QNAME, ChCardPurchaseRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConvertIbanToDepositNumberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "convertIbanToDepositNumberResponse")
    public JAXBElement<ConvertIbanToDepositNumberResponse> createConvertIbanToDepositNumberResponse(ConvertIbanToDepositNumberResponse value) {
        return new JAXBElement<ConvertIbanToDepositNumberResponse>(_ConvertIbanToDepositNumberResponse_QNAME, ConvertIbanToDepositNumberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchThirdPartyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "searchThirdPartyResponse")
    public JAXBElement<SearchThirdPartyResponse> createSearchThirdPartyResponse(SearchThirdPartyResponse value) {
        return new JAXBElement<SearchThirdPartyResponse>(_SearchThirdPartyResponse_QNAME, SearchThirdPartyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCardsSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCardsSearchRequestBean")
    public JAXBElement<ChCardsSearchRequestBean> createChCardsSearchRequestBean(ChCardsSearchRequestBean value) {
        return new JAXBElement<ChCardsSearchRequestBean>(_ChCardsSearchRequestBean_QNAME, ChCardsSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayBillByAccount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "payBillByAccount")
    public JAXBElement<PayBillByAccount> createPayBillByAccount(PayBillByAccount value) {
        return new JAXBElement<PayBillByAccount>(_PayBillByAccount_QNAME, PayBillByAccount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeUsernameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "changeUsernameResponse")
    public JAXBElement<ChangeUsernameResponse> createChangeUsernameResponse(ChangeUsernameResponse value) {
        return new JAXBElement<ChangeUsernameResponse>(_ChangeUsernameResponse_QNAME, ChangeUsernameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConvertDepositNumberToIbanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "convertDepositNumberToIbanResponse")
    public JAXBElement<ConvertDepositNumberToIbanResponse> createConvertDepositNumberToIbanResponse(ConvertDepositNumberToIbanResponse value) {
        return new JAXBElement<ConvertDepositNumberToIbanResponse>(_ConvertDepositNumberToIbanResponse_QNAME, ConvertDepositNumberToIbanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransferConstraintInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getTransferConstraintInfo")
    public JAXBElement<GetTransferConstraintInfo> createGetTransferConstraintInfo(GetTransferConstraintInfo value) {
        return new JAXBElement<GetTransferConstraintInfo>(_GetTransferConstraintInfo_QNAME, GetTransferConstraintInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBatchBillInfoSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBatchBillInfoSearchRequestBean")
    public JAXBElement<ChBatchBillInfoSearchRequestBean> createChBatchBillInfoSearchRequestBean(ChBatchBillInfoSearchRequestBean value) {
        return new JAXBElement<ChBatchBillInfoSearchRequestBean>(_ChBatchBillInfoSearchRequestBean_QNAME, ChBatchBillInfoSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChAchNormalTransferRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chAchNormalTransferRequestBean")
    public JAXBElement<ChAchNormalTransferRequestBean> createChAchNormalTransferRequestBean(ChAchNormalTransferRequestBean value) {
        return new JAXBElement<ChAchNormalTransferRequestBean>(_ChAchNormalTransferRequestBean_QNAME, ChAchNormalTransferRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadCoreParametersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "loadCoreParametersResponse")
    public JAXBElement<LoadCoreParametersResponse> createLoadCoreParametersResponse(LoadCoreParametersResponse value) {
        return new JAXBElement<LoadCoreParametersResponse>(_LoadCoreParametersResponse_QNAME, LoadCoreParametersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBillPaymentAccountRequestBean")
    public JAXBElement<Object> createChBillPaymentAccountRequestBean(Object value) {
        return new JAXBElement<Object>(_ChBillPaymentAccountRequestBean_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositOwnerName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getDepositOwnerName")
    public JAXBElement<GetDepositOwnerName> createGetDepositOwnerName(GetDepositOwnerName value) {
        return new JAXBElement<GetDepositOwnerName>(_GetDepositOwnerName_QNAME, GetDepositOwnerName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetInstitutions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getInstitutions")
    public JAXBElement<GetInstitutions> createGetInstitutions(GetInstitutions value) {
        return new JAXBElement<GetInstitutions>(_GetInstitutions_QNAME, GetInstitutions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChSearchAutoTransferRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chSearchAutoTransferRequestBean")
    public JAXBElement<ChSearchAutoTransferRequestBean> createChSearchAutoTransferRequestBean(ChSearchAutoTransferRequestBean value) {
        return new JAXBElement<ChSearchAutoTransferRequestBean>(_ChSearchAutoTransferRequestBean_QNAME, ChSearchAutoTransferRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardSetPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardSetPassword")
    public JAXBElement<CardSetPassword> createCardSetPassword(CardSetPassword value) {
        return new JAXBElement<CardSetPassword>(_CardSetPassword_QNAME, CardSetPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardInfoRetrievation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardInfoRetrievation")
    public JAXBElement<CardInfoRetrievation> createCardInfoRetrievation(CardInfoRetrievation value) {
        return new JAXBElement<CardInfoRetrievation>(_CardInfoRetrievation_QNAME, CardInfoRetrievation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPeriodicBillRequestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "addPeriodicBillRequestResponse")
    public JAXBElement<AddPeriodicBillRequestResponse> createAddPeriodicBillRequestResponse(AddPeriodicBillRequestResponse value) {
        return new JAXBElement<AddPeriodicBillRequestResponse>(_AddPeriodicBillRequestResponse_QNAME, AddPeriodicBillRequestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCurrencyRateRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCurrencyRateRequestBean")
    public JAXBElement<ChCurrencyRateRequestBean> createChCurrencyRateRequestBean(ChCurrencyRateRequestBean value) {
        return new JAXBElement<ChCurrencyRateRequestBean>(_ChCurrencyRateRequestBean_QNAME, ChCurrencyRateRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChIBANEnquiryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chIBANEnquiryRequest")
    public JAXBElement<ChIBANEnquiryRequest> createChIBANEnquiryRequest(ChIBANEnquiryRequest value) {
        return new JAXBElement<ChIBANEnquiryRequest>(_ChIBANEnquiryRequest_QNAME, ChIBANEnquiryRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChInstitutionRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chInstitutionRequestBean")
    public JAXBElement<ChInstitutionRequestBean> createChInstitutionRequestBean(ChInstitutionRequestBean value) {
        return new JAXBElement<ChInstitutionRequestBean>(_ChInstitutionRequestBean_QNAME, ChInstitutionRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChAchTransactionSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chAchTransactionSearchRequestBean")
    public JAXBElement<ChAchTransactionSearchRequestBean> createChAchTransactionSearchRequestBean(ChAchTransactionSearchRequestBean value) {
        return new JAXBElement<ChAchTransactionSearchRequestBean>(_ChAchTransactionSearchRequestBean_QNAME, ChAchTransactionSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebserviceGatewayFaultException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "WebserviceGatewayFaultException")
    public JAXBElement<WebserviceGatewayFaultException> createWebserviceGatewayFaultException(WebserviceGatewayFaultException value) {
        return new JAXBElement<WebserviceGatewayFaultException>(_WebserviceGatewayFaultException_QNAME, WebserviceGatewayFaultException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardTransactionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardTransactionsResponse")
    public JAXBElement<GetCardTransactionsResponse> createGetCardTransactionsResponse(GetCardTransactionsResponse value) {
        return new JAXBElement<GetCardTransactionsResponse>(_GetCardTransactionsResponse_QNAME, GetCardTransactionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportAchTransferSummary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "reportAchTransferSummary")
    public JAXBElement<ReportAchTransferSummary> createReportAchTransferSummary(ReportAchTransferSummary value) {
        return new JAXBElement<ReportAchTransferSummary>(_ReportAchTransferSummary_QNAME, ReportAchTransferSummary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChLoansSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chLoansSearchRequestBean")
    public JAXBElement<ChLoansSearchRequestBean> createChLoansSearchRequestBean(ChLoansSearchRequestBean value) {
        return new JAXBElement<ChLoansSearchRequestBean>(_ChLoansSearchRequestBean_QNAME, ChLoansSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChChangeUsernameRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chChangeUsernameRequestBean")
    public JAXBElement<ChChangeUsernameRequestBean> createChChangeUsernameRequestBean(ChChangeUsernameRequestBean value) {
        return new JAXBElement<ChChangeUsernameRequestBean>(_ChChangeUsernameRequestBean_QNAME, ChChangeUsernameRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayCreditBillResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "payCreditBillResponse")
    public JAXBElement<PayCreditBillResponse> createPayCreditBillResponse(PayCreditBillResponse value) {
        return new JAXBElement<PayCreditBillResponse>(_PayCreditBillResponse_QNAME, PayCreditBillResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AchNormalTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "achNormalTransferResponse")
    public JAXBElement<AchNormalTransferResponse> createAchNormalTransferResponse(AchNormalTransferResponse value) {
        return new JAXBElement<AchNormalTransferResponse>(_AchNormalTransferResponse_QNAME, AchNormalTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPeriodicBalanceRequestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "addPeriodicBalanceRequestResponse")
    public JAXBElement<AddPeriodicBalanceRequestResponse> createAddPeriodicBalanceRequestResponse(AddPeriodicBalanceRequestResponse value) {
        return new JAXBElement<AddPeriodicBalanceRequestResponse>(_AddPeriodicBalanceRequestResponse_QNAME, AddPeriodicBalanceRequestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAtmStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getAtmStatusResponse")
    public JAXBElement<GetAtmStatusResponse> createGetAtmStatusResponse(GetAtmStatusResponse value) {
        return new JAXBElement<GetAtmStatusResponse>(_GetAtmStatusResponse_QNAME, GetAtmStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIssuingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardIssuingResponse")
    public JAXBElement<CardIssuingResponse> createCardIssuingResponse(CardIssuingResponse value) {
        return new JAXBElement<CardIssuingResponse>(_CardIssuingResponse_QNAME, CardIssuingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeactivateModernGatewayUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "deactivateModernGatewayUserResponse")
    public JAXBElement<DeactivateModernGatewayUserResponse> createDeactivateModernGatewayUserResponse(DeactivateModernGatewayUserResponse value) {
        return new JAXBElement<DeactivateModernGatewayUserResponse>(_DeactivateModernGatewayUserResponse_QNAME, DeactivateModernGatewayUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "sendMailResponse")
    public JAXBElement<SendMailResponse> createSendMailResponse(SendMailResponse value) {
        return new JAXBElement<SendMailResponse>(_SendMailResponse_QNAME, SendMailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChPhysicalCardTransferRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chPhysicalCardTransferRequest")
    public JAXBElement<ChPhysicalCardTransferRequest> createChPhysicalCardTransferRequest(ChPhysicalCardTransferRequest value) {
        return new JAXBElement<ChPhysicalCardTransferRequest>(_ChPhysicalCardTransferRequest_QNAME, ChPhysicalCardTransferRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCheque }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCheque")
    public JAXBElement<GetCheque> createGetCheque(GetCheque value) {
        return new JAXBElement<GetCheque>(_GetCheque_QNAME, GetCheque.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardTransfer")
    public JAXBElement<CardTransfer> createCardTransfer(CardTransfer value) {
        return new JAXBElement<CardTransfer>(_CardTransfer_QNAME, CardTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCardBalanceRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCardBalanceRequestBean")
    public JAXBElement<ChCardBalanceRequestBean> createChCardBalanceRequestBean(ChCardBalanceRequestBean value) {
        return new JAXBElement<ChCardBalanceRequestBean>(_ChCardBalanceRequestBean_QNAME, ChCardBalanceRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChThirdPartySearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chThirdPartySearchRequestBean")
    public JAXBElement<ChThirdPartySearchRequestBean> createChThirdPartySearchRequestBean(ChThirdPartySearchRequestBean value) {
        return new JAXBElement<ChThirdPartySearchRequestBean>(_ChThirdPartySearchRequestBean_QNAME, ChThirdPartySearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMerchantInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getMerchantInfo")
    public JAXBElement<GetMerchantInfo> createGetMerchantInfo(GetMerchantInfo value) {
        return new JAXBElement<GetMerchantInfo>(_GetMerchantInfo_QNAME, GetMerchantInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChResumeAchTransferRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chResumeAchTransferRequestBean")
    public JAXBElement<ChResumeAchTransferRequestBean> createChResumeAchTransferRequestBean(ChResumeAchTransferRequestBean value) {
        return new JAXBElement<ChResumeAchTransferRequestBean>(_ChResumeAchTransferRequestBean_QNAME, ChResumeAchTransferRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardOwnerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardOwnerResponse")
    public JAXBElement<GetCardOwnerResponse> createGetCardOwnerResponse(GetCardOwnerResponse value) {
        return new JAXBElement<GetCardOwnerResponse>(_GetCardOwnerResponse_QNAME, GetCardOwnerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SuspendAchTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "suspendAchTransfer")
    public JAXBElement<SuspendAchTransfer> createSuspendAchTransfer(SuspendAchTransfer value) {
        return new JAXBElement<SuspendAchTransfer>(_SuspendAchTransfer_QNAME, SuspendAchTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardLimitationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardLimitationsResponse")
    public JAXBElement<GetCardLimitationsResponse> createGetCardLimitationsResponse(GetCardLimitationsResponse value) {
        return new JAXBElement<GetCardLimitationsResponse>(_GetCardLimitationsResponse_QNAME, GetCardLimitationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerInfoListByModifyDateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCustomerInfoListByModifyDateResponse")
    public JAXBElement<GetCustomerInfoListByModifyDateResponse> createGetCustomerInfoListByModifyDateResponse(GetCustomerInfoListByModifyDateResponse value) {
        return new JAXBElement<GetCustomerInfoListByModifyDateResponse>(_GetCustomerInfoListByModifyDateResponse_QNAME, GetCustomerInfoListByModifyDateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCardLimitRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCardLimitRequestBean")
    public JAXBElement<ChCardLimitRequestBean> createChCardLimitRequestBean(ChCardLimitRequestBean value) {
        return new JAXBElement<ChCardLimitRequestBean>(_ChCardLimitRequestBean_QNAME, ChCardLimitRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserBillSetting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getUserBillSetting")
    public JAXBElement<GetUserBillSetting> createGetUserBillSetting(GetUserBillSetting value) {
        return new JAXBElement<GetUserBillSetting>(_GetUserBillSetting_QNAME, GetUserBillSetting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerSignatures }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCustomerSignatures")
    public JAXBElement<GetCustomerSignatures> createGetCustomerSignatures(GetCustomerSignatures value) {
        return new JAXBElement<GetCustomerSignatures>(_GetCustomerSignatures_QNAME, GetCustomerSignatures.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStatementResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getStatementResponse")
    public JAXBElement<GetStatementResponse> createGetStatementResponse(GetStatementResponse value) {
        return new JAXBElement<GetStatementResponse>(_GetStatementResponse_QNAME, GetStatementResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBranchBalance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBranchBalance")
    public JAXBElement<GetBranchBalance> createGetBranchBalance(GetBranchBalance value) {
        return new JAXBElement<GetBranchBalance>(_GetBranchBalance_QNAME, GetBranchBalance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeactivateModernGatewayUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "deactivateModernGatewayUser")
    public JAXBElement<DeactivateModernGatewayUser> createDeactivateModernGatewayUser(DeactivateModernGatewayUser value) {
        return new JAXBElement<DeactivateModernGatewayUser>(_DeactivateModernGatewayUser_QNAME, DeactivateModernGatewayUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserBillSetting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "updateUserBillSetting")
    public JAXBElement<UpdateUserBillSetting> createUpdateUserBillSetting(UpdateUserBillSetting value) {
        return new JAXBElement<UpdateUserBillSetting>(_UpdateUserBillSetting_QNAME, UpdateUserBillSetting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoCustomRetrun }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "doCustomRetrun")
    public JAXBElement<DoCustomRetrun> createDoCustomRetrun(DoCustomRetrun value) {
        return new JAXBElement<DoCustomRetrun>(_DoCustomRetrun_QNAME, DoCustomRetrun.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardExtendResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardExtendResponse")
    public JAXBElement<CardExtendResponse> createCardExtendResponse(CardExtendResponse value) {
        return new JAXBElement<CardExtendResponse>(_CardExtendResponse_QNAME, CardExtendResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchThirdParty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "searchThirdParty")
    public JAXBElement<SearchThirdParty> createSearchThirdParty(SearchThirdParty value) {
        return new JAXBElement<SearchThirdParty>(_SearchThirdParty_QNAME, SearchThirdParty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBeOpenedDeposits }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBeOpenedDeposits")
    public JAXBElement<GetBeOpenedDeposits> createGetBeOpenedDeposits(GetBeOpenedDeposits value) {
        return new JAXBElement<GetBeOpenedDeposits>(_GetBeOpenedDeposits_QNAME, GetBeOpenedDeposits.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BlockDepositAmountResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "blockDepositAmountResponse")
    public JAXBElement<BlockDepositAmountResponse> createBlockDepositAmountResponse(BlockDepositAmountResponse value) {
        return new JAXBElement<BlockDepositAmountResponse>(_BlockDepositAmountResponse_QNAME, BlockDepositAmountResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPaymentServices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getPaymentServices")
    public JAXBElement<GetPaymentServices> createGetPaymentServices(GetPaymentServices value) {
        return new JAXBElement<GetPaymentServices>(_GetPaymentServices_QNAME, GetPaymentServices.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChChequeSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chChequeSearchRequestBean")
    public JAXBElement<ChChequeSearchRequestBean> createChChequeSearchRequestBean(ChChequeSearchRequestBean value) {
        return new JAXBElement<ChChequeSearchRequestBean>(_ChChequeSearchRequestBean_QNAME, ChChequeSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChPeriodicRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chPeriodicRequestBean")
    public JAXBElement<ChPeriodicRequestBean> createChPeriodicRequestBean(ChPeriodicRequestBean value) {
        return new JAXBElement<ChPeriodicRequestBean>(_ChPeriodicRequestBean_QNAME, ChPeriodicRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBranchAllowedDepositsToOpen }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBranchAllowedDepositsToOpen")
    public JAXBElement<GetBranchAllowedDepositsToOpen> createGetBranchAllowedDepositsToOpen(GetBranchAllowedDepositsToOpen value) {
        return new JAXBElement<GetBranchAllowedDepositsToOpen>(_GetBranchAllowedDepositsToOpen_QNAME, GetBranchAllowedDepositsToOpen.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCreditBillTransactions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCreditBillTransactions")
    public JAXBElement<GetCreditBillTransactions> createGetCreditBillTransactions(GetCreditBillTransactions value) {
        return new JAXBElement<GetCreditBillTransactions>(_GetCreditBillTransactions_QNAME, GetCreditBillTransactions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TopupChargeFromVirtualTerminal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "topupChargeFromVirtualTerminal")
    public JAXBElement<TopupChargeFromVirtualTerminal> createTopupChargeFromVirtualTerminal(TopupChargeFromVirtualTerminal value) {
        return new JAXBElement<TopupChargeFromVirtualTerminal>(_TopupChargeFromVirtualTerminal_QNAME, TopupChargeFromVirtualTerminal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositRatesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getDepositRatesResponse")
    public JAXBElement<GetDepositRatesResponse> createGetDepositRatesResponse(GetDepositRatesResponse value) {
        return new JAXBElement<GetDepositRatesResponse>(_GetDepositRatesResponse_QNAME, GetDepositRatesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HotCardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "hotCardResponse")
    public JAXBElement<HotCardResponse> createHotCardResponse(HotCardResponse value) {
        return new JAXBElement<HotCardResponse>(_HotCardResponse_QNAME, HotCardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InquiryCardIssuing }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "inquiryCardIssuing")
    public JAXBElement<InquiryCardIssuing> createInquiryCardIssuing(InquiryCardIssuing value) {
        return new JAXBElement<InquiryCardIssuing>(_InquiryCardIssuing_QNAME, InquiryCardIssuing.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NormalTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "normalTransfer")
    public JAXBElement<NormalTransfer> createNormalTransfer(NormalTransfer value) {
        return new JAXBElement<NormalTransfer>(_NormalTransfer_QNAME, NormalTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChUserRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chUserRequestBean")
    public JAXBElement<ChUserRequestBean> createChUserRequestBean(ChUserRequestBean value) {
        return new JAXBElement<ChUserRequestBean>(_ChUserRequestBean_QNAME, ChUserRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstitutionalTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "institutionalTransfer")
    public JAXBElement<InstitutionalTransfer> createInstitutionalTransfer(InstitutionalTransfer value) {
        return new JAXBElement<InstitutionalTransfer>(_InstitutionalTransfer_QNAME, InstitutionalTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyTransactionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "verifyTransactionResponse")
    public JAXBElement<VerifyTransactionResponse> createVerifyTransactionResponse(VerifyTransactionResponse value) {
        return new JAXBElement<VerifyTransactionResponse>(_VerifyTransactionResponse_QNAME, VerifyTransactionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChAtmStatusRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "ChAtmStatusRequestBean")
    public JAXBElement<ChAtmStatusRequestBean> createChAtmStatusRequestBean(ChAtmStatusRequestBean value) {
        return new JAXBElement<ChAtmStatusRequestBean>(_ChAtmStatusRequestBean_QNAME, ChAtmStatusRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardPurchaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardPurchaseResponse")
    public JAXBElement<CardPurchaseResponse> createCardPurchaseResponse(CardPurchaseResponse value) {
        return new JAXBElement<CardPurchaseResponse>(_CardPurchaseResponse_QNAME, CardPurchaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AchNormalTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "achNormalTransfer")
    public JAXBElement<AchNormalTransfer> createAchNormalTransfer(AchNormalTransfer value) {
        return new JAXBElement<AchNormalTransfer>(_AchNormalTransfer_QNAME, AchNormalTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCustomerInfo")
    public JAXBElement<GetCustomerInfo> createGetCustomerInfo(GetCustomerInfo value) {
        return new JAXBElement<GetCustomerInfo>(_GetCustomerInfo_QNAME, GetCustomerInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayLoan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "payLoan")
    public JAXBElement<PayLoan> createPayLoan(PayLoan value) {
        return new JAXBElement<PayLoan>(_PayLoan_QNAME, PayLoan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RtgsNormalTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "rtgsNormalTransfer")
    public JAXBElement<RtgsNormalTransfer> createRtgsNormalTransfer(RtgsNormalTransfer value) {
        return new JAXBElement<RtgsNormalTransfer>(_RtgsNormalTransfer_QNAME, RtgsNormalTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayBatchBill }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "payBatchBill")
    public JAXBElement<PayBatchBill> createPayBatchBill(PayBatchBill value) {
        return new JAXBElement<PayBatchBill>(_PayBatchBill_QNAME, PayBatchBill.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisableTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "disableTransferResponse")
    public JAXBElement<DisableTransferResponse> createDisableTransferResponse(DisableTransferResponse value) {
        return new JAXBElement<DisableTransferResponse>(_DisableTransferResponse_QNAME, DisableTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateDefaultContactInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "updateDefaultContactInfo")
    public JAXBElement<UpdateDefaultContactInfo> createUpdateDefaultContactInfo(UpdateDefaultContactInfo value) {
        return new JAXBElement<UpdateDefaultContactInfo>(_UpdateDefaultContactInfo_QNAME, UpdateDefaultContactInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserBillSettingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getUserBillSettingResponse")
    public JAXBElement<GetUserBillSettingResponse> createGetUserBillSettingResponse(GetUserBillSettingResponse value) {
        return new JAXBElement<GetUserBillSettingResponse>(_GetUserBillSettingResponse_QNAME, GetUserBillSettingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelAchTransactionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cancelAchTransactionResponse")
    public JAXBElement<CancelAchTransactionResponse> createCancelAchTransactionResponse(CancelAchTransactionResponse value) {
        return new JAXBElement<CancelAchTransactionResponse>(_CancelAchTransactionResponse_QNAME, CancelAchTransactionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateNaturalCustomer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "createNaturalCustomer")
    public JAXBElement<CreateNaturalCustomer> createCreateNaturalCustomer(CreateNaturalCustomer value) {
        return new JAXBElement<CreateNaturalCustomer>(_CreateNaturalCustomer_QNAME, CreateNaturalCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NaturalPersonOpenSingleDeposit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "naturalPersonOpenSingleDeposit")
    public JAXBElement<NaturalPersonOpenSingleDeposit> createNaturalPersonOpenSingleDeposit(NaturalPersonOpenSingleDeposit value) {
        return new JAXBElement<NaturalPersonOpenSingleDeposit>(_NaturalPersonOpenSingleDeposit_QNAME, NaturalPersonOpenSingleDeposit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBillInfoSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBillInfoSearchRequestBean")
    public JAXBElement<ChBillInfoSearchRequestBean> createChBillInfoSearchRequestBean(ChBillInfoSearchRequestBean value) {
        return new JAXBElement<ChBillInfoSearchRequestBean>(_ChBillInfoSearchRequestBean_QNAME, ChBillInfoSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChequeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getChequeResponse")
    public JAXBElement<GetChequeResponse> createGetChequeResponse(GetChequeResponse value) {
        return new JAXBElement<GetChequeResponse>(_GetChequeResponse_QNAME, GetChequeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCreditBills }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCreditBills")
    public JAXBElement<GetCreditBills> createGetCreditBills(GetCreditBills value) {
        return new JAXBElement<GetCreditBills>(_GetCreditBills_QNAME, GetCreditBills.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RtgsTransferDetailReport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "rtgsTransferDetailReport")
    public JAXBElement<RtgsTransferDetailReport> createRtgsTransferDetailReport(RtgsTransferDetailReport value) {
        return new JAXBElement<RtgsTransferDetailReport>(_RtgsTransferDetailReport_QNAME, RtgsTransferDetailReport.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChOwnerDepositRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chOwnerDepositRequestBean")
    public JAXBElement<ChOwnerDepositRequestBean> createChOwnerDepositRequestBean(ChOwnerDepositRequestBean value) {
        return new JAXBElement<ChOwnerDepositRequestBean>(_ChOwnerDepositRequestBean_QNAME, ChOwnerDepositRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerExistenceInquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "customerExistenceInquiryResponse")
    public JAXBElement<CustomerExistenceInquiryResponse> createCustomerExistenceInquiryResponse(CustomerExistenceInquiryResponse value) {
        return new JAXBElement<CustomerExistenceInquiryResponse>(_CustomerExistenceInquiryResponse_QNAME, CustomerExistenceInquiryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardCorrectLinkedAccountsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardCorrectLinkedAccountsResponse")
    public JAXBElement<CardCorrectLinkedAccountsResponse> createCardCorrectLinkedAccountsResponse(CardCorrectLinkedAccountsResponse value) {
        return new JAXBElement<CardCorrectLinkedAccountsResponse>(_CardCorrectLinkedAccountsResponse_QNAME, CardCorrectLinkedAccountsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InquiryCardIssuingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "inquiryCardIssuingResponse")
    public JAXBElement<InquiryCardIssuingResponse> createInquiryCardIssuingResponse(InquiryCardIssuingResponse value) {
        return new JAXBElement<InquiryCardIssuingResponse>(_InquiryCardIssuingResponse_QNAME, InquiryCardIssuingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCardStatementRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCardStatementRequestBean")
    public JAXBElement<ChCardStatementRequestBean> createChCardStatementRequestBean(ChCardStatementRequestBean value) {
        return new JAXBElement<ChCardStatementRequestBean>(_ChCardStatementRequestBean_QNAME, ChCardStatementRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChAcceptAchTransferRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chAcceptAchTransferRequestBean")
    public JAXBElement<ChAcceptAchTransferRequestBean> createChAcceptAchTransferRequestBean(ChAcceptAchTransferRequestBean value) {
        return new JAXBElement<ChAcceptAchTransferRequestBean>(_ChAcceptAchTransferRequestBean_QNAME, ChAcceptAchTransferRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenDepositResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "openDepositResponse")
    public JAXBElement<OpenDepositResponse> createOpenDepositResponse(OpenDepositResponse value) {
        return new JAXBElement<OpenDepositResponse>(_OpenDepositResponse_QNAME, OpenDepositResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBranchSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBranchSearchRequestBean")
    public JAXBElement<ChBranchSearchRequestBean> createChBranchSearchRequestBean(ChBranchSearchRequestBean value) {
        return new JAXBElement<ChBranchSearchRequestBean>(_ChBranchSearchRequestBean_QNAME, ChBranchSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLoanDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getLoanDetail")
    public JAXBElement<GetLoanDetail> createGetLoanDetail(GetLoanDetail value) {
        return new JAXBElement<GetLoanDetail>(_GetLoanDetail_QNAME, GetLoanDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SwitchFundTransferFromPhysicalTerminalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "switchFundTransferFromPhysicalTerminalResponse")
    public JAXBElement<SwitchFundTransferFromPhysicalTerminalResponse> createSwitchFundTransferFromPhysicalTerminalResponse(SwitchFundTransferFromPhysicalTerminalResponse value) {
        return new JAXBElement<SwitchFundTransferFromPhysicalTerminalResponse>(_SwitchFundTransferFromPhysicalTerminalResponse_QNAME, SwitchFundTransferFromPhysicalTerminalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SwitchFundTransferFromVirtualTerminal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "switchFundTransferFromVirtualTerminal")
    public JAXBElement<SwitchFundTransferFromVirtualTerminal> createSwitchFundTransferFromVirtualTerminal(SwitchFundTransferFromVirtualTerminal value) {
        return new JAXBElement<SwitchFundTransferFromVirtualTerminal>(_SwitchFundTransferFromVirtualTerminal_QNAME, SwitchFundTransferFromVirtualTerminal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCardTransactionsRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCardTransactionsRequestBean")
    public JAXBElement<ChCardTransactionsRequestBean> createChCardTransactionsRequestBean(ChCardTransactionsRequestBean value) {
        return new JAXBElement<ChCardTransactionsRequestBean>(_ChCardTransactionsRequestBean_QNAME, ChCardTransactionsRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFavoriteDeposits }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getFavoriteDeposits")
    public JAXBElement<GetFavoriteDeposits> createGetFavoriteDeposits(GetFavoriteDeposits value) {
        return new JAXBElement<GetFavoriteDeposits>(_GetFavoriteDeposits_QNAME, GetFavoriteDeposits.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetInstitutionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getInstitutionsResponse")
    public JAXBElement<GetInstitutionsResponse> createGetInstitutionsResponse(GetInstitutionsResponse value) {
        return new JAXBElement<GetInstitutionsResponse>(_GetInstitutionsResponse_QNAME, GetInstitutionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayBillByAccountResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "payBillByAccountResponse")
    public JAXBElement<PayBillByAccountResponse> createPayBillByAccountResponse(PayBillByAccountResponse value) {
        return new JAXBElement<PayBillByAccountResponse>(_PayBillByAccountResponse_QNAME, PayBillByAccountResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCardChangePinRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCardChangePinRequestBean")
    public JAXBElement<ChCardChangePinRequestBean> createChCardChangePinRequestBean(ChCardChangePinRequestBean value) {
        return new JAXBElement<ChCardChangePinRequestBean>(_ChCardChangePinRequestBean_QNAME, ChCardChangePinRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignRequestDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "signRequestDetailsResponse")
    public JAXBElement<SignRequestDetailsResponse> createSignRequestDetailsResponse(SignRequestDetailsResponse value) {
        return new JAXBElement<SignRequestDetailsResponse>(_SignRequestDetailsResponse_QNAME, SignRequestDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChIbanToDepositRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chIbanToDepositRequestBean")
    public JAXBElement<ChIbanToDepositRequestBean> createChIbanToDepositRequestBean(ChIbanToDepositRequestBean value) {
        return new JAXBElement<ChIbanToDepositRequestBean>(_ChIbanToDepositRequestBean_QNAME, ChIbanToDepositRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChMailRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chMailRequestBean")
    public JAXBElement<ChMailRequestBean> createChMailRequestBean(ChMailRequestBean value) {
        return new JAXBElement<ChMailRequestBean>(_ChMailRequestBean_QNAME, ChMailRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCashChequeRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCashChequeRequestBean")
    public JAXBElement<ChCashChequeRequestBean> createChCashChequeRequestBean(ChCashChequeRequestBean value) {
        return new JAXBElement<ChCashChequeRequestBean>(_ChCashChequeRequestBean_QNAME, ChCashChequeRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExtendSMSSettingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "extendSMSSettingResponse")
    public JAXBElement<ExtendSMSSettingResponse> createExtendSMSSettingResponse(ExtendSMSSettingResponse value) {
        return new JAXBElement<ExtendSMSSettingResponse>(_ExtendSMSSettingResponse_QNAME, ExtendSMSSettingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSignedDeposits }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getSignedDeposits")
    public JAXBElement<GetSignedDeposits> createGetSignedDeposits(GetSignedDeposits value) {
        return new JAXBElement<GetSignedDeposits>(_GetSignedDeposits_QNAME, GetSignedDeposits.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDestinationCardOwner }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getDestinationCardOwner")
    public JAXBElement<GetDestinationCardOwner> createGetDestinationCardOwner(GetDestinationCardOwner value) {
        return new JAXBElement<GetDestinationCardOwner>(_GetDestinationCardOwner_QNAME, GetDestinationCardOwner.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelAchTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cancelAchTransaction")
    public JAXBElement<CancelAchTransaction> createCancelAchTransaction(CancelAchTransaction value) {
        return new JAXBElement<CancelAchTransaction>(_CancelAchTransaction_QNAME, CancelAchTransaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SuspendAchTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "suspendAchTransferResponse")
    public JAXBElement<SuspendAchTransferResponse> createSuspendAchTransferResponse(SuspendAchTransferResponse value) {
        return new JAXBElement<SuspendAchTransferResponse>(_SuspendAchTransferResponse_QNAME, SuspendAchTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransferConstraintInfoPerServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getTransferConstraintInfoPerServiceResponse")
    public JAXBElement<GetTransferConstraintInfoPerServiceResponse> createGetTransferConstraintInfoPerServiceResponse(GetTransferConstraintInfoPerServiceResponse value) {
        return new JAXBElement<GetTransferConstraintInfoPerServiceResponse>(_GetTransferConstraintInfoPerServiceResponse_QNAME, GetTransferConstraintInfoPerServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChInstitutionalTransferRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chInstitutionalTransferRequestBean")
    public JAXBElement<ChInstitutionalTransferRequestBean> createChInstitutionalTransferRequestBean(ChInstitutionalTransferRequestBean value) {
        return new JAXBElement<ChInstitutionalTransferRequestBean>(_ChInstitutionalTransferRequestBean_QNAME, ChInstitutionalTransferRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChPanRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chPanRequestBean")
    public JAXBElement<ChPanRequestBean> createChPanRequestBean(ChPanRequestBean value) {
        return new JAXBElement<ChPanRequestBean>(_ChPanRequestBean_QNAME, ChPanRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SuspendAchTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "suspendAchTransaction")
    public JAXBElement<SuspendAchTransaction> createSuspendAchTransaction(SuspendAchTransaction value) {
        return new JAXBElement<SuspendAchTransaction>(_SuspendAchTransaction_QNAME, SuspendAchTransaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AchTransferReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "achTransferReportResponse")
    public JAXBElement<AchTransferReportResponse> createAchTransferReportResponse(AchTransferReportResponse value) {
        return new JAXBElement<AchTransferReportResponse>(_AchTransferReportResponse_QNAME, AchTransferReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChRtgTransferDetailSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chRtgTransferDetailSearchRequestBean")
    public JAXBElement<ChRtgTransferDetailSearchRequestBean> createChRtgTransferDetailSearchRequestBean(ChRtgTransferDetailSearchRequestBean value) {
        return new JAXBElement<ChRtgTransferDetailSearchRequestBean>(_ChRtgTransferDetailSearchRequestBean_QNAME, ChRtgTransferDetailSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBatchBillInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBatchBillInfo")
    public JAXBElement<GetBatchBillInfo> createGetBatchBillInfo(GetBatchBillInfo value) {
        return new JAXBElement<GetBatchBillInfo>(_GetBatchBillInfo_QNAME, GetBatchBillInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEchoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getEchoResponse")
    public JAXBElement<GetEchoResponse> createGetEchoResponse(GetEchoResponse value) {
        return new JAXBElement<GetEchoResponse>(_GetEchoResponse_QNAME, GetEchoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerInfoListByModifyDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCustomerInfoListByModifyDate")
    public JAXBElement<GetCustomerInfoListByModifyDate> createGetCustomerInfoListByModifyDate(GetCustomerInfoListByModifyDate value) {
        return new JAXBElement<GetCustomerInfoListByModifyDate>(_GetCustomerInfoListByModifyDate_QNAME, GetCustomerInfoListByModifyDate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMerchantInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getMerchantInfoResponse")
    public JAXBElement<GetMerchantInfoResponse> createGetMerchantInfoResponse(GetMerchantInfoResponse value) {
        return new JAXBElement<GetMerchantInfoResponse>(_GetMerchantInfoResponse_QNAME, GetMerchantInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterChequeBookRequestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "registerChequeBookRequestResponse")
    public JAXBElement<RegisterChequeBookRequestResponse> createRegisterChequeBookRequestResponse(RegisterChequeBookRequestResponse value) {
        return new JAXBElement<RegisterChequeBookRequestResponse>(_RegisterChequeBookRequestResponse_QNAME, RegisterChequeBookRequestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChAchTransferKeyRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chAchTransferKeyRequestBean")
    public JAXBElement<ChAchTransferKeyRequestBean> createChAchTransferKeyRequestBean(ChAchTransferKeyRequestBean value) {
        return new JAXBElement<ChAchTransferKeyRequestBean>(_ChAchTransferKeyRequestBean_QNAME, ChAchTransferKeyRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCustomReturnRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCustomReturnRequestBean")
    public JAXBElement<ChCustomReturnRequestBean> createChCustomReturnRequestBean(ChCustomReturnRequestBean value) {
        return new JAXBElement<ChCustomReturnRequestBean>(_ChCustomReturnRequestBean_QNAME, ChCustomReturnRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBanksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBanksResponse")
    public JAXBElement<GetBanksResponse> createGetBanksResponse(GetBanksResponse value) {
        return new JAXBElement<GetBanksResponse>(_GetBanksResponse_QNAME, GetBanksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NormalTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "normalTransferResponse")
    public JAXBElement<NormalTransferResponse> createNormalTransferResponse(NormalTransferResponse value) {
        return new JAXBElement<NormalTransferResponse>(_NormalTransferResponse_QNAME, NormalTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InquiryNaturalPersonOpenSingleDeposit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "inquiryNaturalPersonOpenSingleDeposit")
    public JAXBElement<InquiryNaturalPersonOpenSingleDeposit> createInquiryNaturalPersonOpenSingleDeposit(InquiryNaturalPersonOpenSingleDeposit value) {
        return new JAXBElement<InquiryNaturalPersonOpenSingleDeposit>(_InquiryNaturalPersonOpenSingleDeposit_QNAME, InquiryNaturalPersonOpenSingleDeposit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateFavoriteAccountSetting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "updateFavoriteAccountSetting")
    public JAXBElement<UpdateFavoriteAccountSetting> createUpdateFavoriteAccountSetting(UpdateFavoriteAccountSetting value) {
        return new JAXBElement<UpdateFavoriteAccountSetting>(_UpdateFavoriteAccountSetting_QNAME, UpdateFavoriteAccountSetting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RtgsTransferReport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "rtgsTransferReport")
    public JAXBElement<RtgsTransferReport> createRtgsTransferReport(RtgsTransferReport value) {
        return new JAXBElement<RtgsTransferReport>(_RtgsTransferReport_QNAME, RtgsTransferReport.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignRequestDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "signRequestDetails")
    public JAXBElement<SignRequestDetails> createSignRequestDetails(SignRequestDetails value) {
        return new JAXBElement<SignRequestDetails>(_SignRequestDetails_QNAME, SignRequestDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChSellReportRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chSellReportRequestBean")
    public JAXBElement<ChSellReportRequestBean> createChSellReportRequestBean(ChSellReportRequestBean value) {
        return new JAXBElement<ChSellReportRequestBean>(_ChSellReportRequestBean_QNAME, ChSellReportRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCustomerAddress")
    public JAXBElement<GetCustomerAddress> createGetCustomerAddress(GetCustomerAddress value) {
        return new JAXBElement<GetCustomerAddress>(_GetCustomerAddress_QNAME, GetCustomerAddress.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGuarantyList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getGuarantyList")
    public JAXBElement<GetGuarantyList> createGetGuarantyList(GetGuarantyList value) {
        return new JAXBElement<GetGuarantyList>(_GetGuarantyList_QNAME, GetGuarantyList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBatchTransferRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBatchTransferRequestBean")
    public JAXBElement<ChBatchTransferRequestBean> createChBatchTransferRequestBean(ChBatchTransferRequestBean value) {
        return new JAXBElement<ChBatchTransferRequestBean>(_ChBatchTransferRequestBean_QNAME, ChBatchTransferRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChReqStatusRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chReqStatusRequestBean")
    public JAXBElement<ChReqStatusRequestBean> createChReqStatusRequestBean(ChReqStatusRequestBean value) {
        return new JAXBElement<ChReqStatusRequestBean>(_ChReqStatusRequestBean_QNAME, ChReqStatusRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDefaultBillStatement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getDefaultBillStatement")
    public JAXBElement<GetDefaultBillStatement> createGetDefaultBillStatement(GetDefaultBillStatement value) {
        return new JAXBElement<GetDefaultBillStatement>(_GetDefaultBillStatement_QNAME, GetDefaultBillStatement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstitutionalTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "institutionalTransferResponse")
    public JAXBElement<InstitutionalTransferResponse> createInstitutionalTransferResponse(InstitutionalTransferResponse value) {
        return new JAXBElement<InstitutionalTransferResponse>(_InstitutionalTransferResponse_QNAME, InstitutionalTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDefaultBillStatementResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getDefaultBillStatementResponse")
    public JAXBElement<GetDefaultBillStatementResponse> createGetDefaultBillStatementResponse(GetDefaultBillStatementResponse value) {
        return new JAXBElement<GetDefaultBillStatementResponse>(_GetDefaultBillStatementResponse_QNAME, GetDefaultBillStatementResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateNaturalCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "createNaturalCustomerResponse")
    public JAXBElement<CreateNaturalCustomerResponse> createCreateNaturalCustomerResponse(CreateNaturalCustomerResponse value) {
        return new JAXBElement<CreateNaturalCustomerResponse>(_CreateNaturalCustomerResponse_QNAME, CreateNaturalCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisableTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "disableTransfer")
    public JAXBElement<DisableTransfer> createDisableTransfer(DisableTransfer value) {
        return new JAXBElement<DisableTransfer>(_DisableTransfer_QNAME, DisableTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardTransactions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardTransactions")
    public JAXBElement<GetCardTransactions> createGetCardTransactions(GetCardTransactions value) {
        return new JAXBElement<GetCardTransactions>(_GetCardTransactions_QNAME, GetCardTransactions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBranchBalanceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBranchBalanceResponse")
    public JAXBElement<GetBranchBalanceResponse> createGetBranchBalanceResponse(GetBranchBalanceResponse value) {
        return new JAXBElement<GetBranchBalanceResponse>(_GetBranchBalanceResponse_QNAME, GetBranchBalanceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveOrUpdateSecondPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "saveOrUpdateSecondPasswordResponse")
    public JAXBElement<SaveOrUpdateSecondPasswordResponse> createSaveOrUpdateSecondPasswordResponse(SaveOrUpdateSecondPasswordResponse value) {
        return new JAXBElement<SaveOrUpdateSecondPasswordResponse>(_SaveOrUpdateSecondPasswordResponse_QNAME, SaveOrUpdateSecondPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardInfoRetrievationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardInfoRetrievationResponse")
    public JAXBElement<CardInfoRetrievationResponse> createCardInfoRetrievationResponse(CardInfoRetrievationResponse value) {
        return new JAXBElement<CardInfoRetrievationResponse>(_CardInfoRetrievationResponse_QNAME, CardInfoRetrievationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBeOpenedDepositRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBeOpenedDepositRequestBean")
    public JAXBElement<ChBeOpenedDepositRequestBean> createChBeOpenedDepositRequestBean(ChBeOpenedDepositRequestBean value) {
        return new JAXBElement<ChBeOpenedDepositRequestBean>(_ChBeOpenedDepositRequestBean_QNAME, ChBeOpenedDepositRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPeriodicBillRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "addPeriodicBillRequest")
    public JAXBElement<AddPeriodicBillRequest> createAddPeriodicBillRequest(AddPeriodicBillRequest value) {
        return new JAXBElement<AddPeriodicBillRequest>(_AddPeriodicBillRequest_QNAME, AddPeriodicBillRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChDefaultStatementRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chDefaultStatementRequestBean")
    public JAXBElement<ChDefaultStatementRequestBean> createChDefaultStatementRequestBean(ChDefaultStatementRequestBean value) {
        return new JAXBElement<ChDefaultStatementRequestBean>(_ChDefaultStatementRequestBean_QNAME, ChDefaultStatementRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChPaymentPreviewRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chPaymentPreviewRequestBean")
    public JAXBElement<ChPaymentPreviewRequestBean> createChPaymentPreviewRequestBean(ChPaymentPreviewRequestBean value) {
        return new JAXBElement<ChPaymentPreviewRequestBean>(_ChPaymentPreviewRequestBean_QNAME, ChPaymentPreviewRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IbanEnquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "ibanEnquiry")
    public JAXBElement<IbanEnquiry> createIbanEnquiry(IbanEnquiry value) {
        return new JAXBElement<IbanEnquiry>(_IbanEnquiry_QNAME, IbanEnquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateFavoriteAccountSettingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "updateFavoriteAccountSettingResponse")
    public JAXBElement<UpdateFavoriteAccountSettingResponse> createUpdateFavoriteAccountSettingResponse(UpdateFavoriteAccountSettingResponse value) {
        return new JAXBElement<UpdateFavoriteAccountSettingResponse>(_UpdateFavoriteAccountSettingResponse_QNAME, UpdateFavoriteAccountSettingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChRtgTransferSearchRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chRtgTransferSearchRequestBean")
    public JAXBElement<ChRtgTransferSearchRequestBean> createChRtgTransferSearchRequestBean(ChRtgTransferSearchRequestBean value) {
        return new JAXBElement<ChRtgTransferSearchRequestBean>(_ChRtgTransferSearchRequestBean_QNAME, ChRtgTransferSearchRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBillInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBillInfo")
    public JAXBElement<GetBillInfo> createGetBillInfo(GetBillInfo value) {
        return new JAXBElement<GetBillInfo>(_GetBillInfo_QNAME, GetBillInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginWithMobileNumberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "loginWithMobileNumberResponse")
    public JAXBElement<LoginWithMobileNumberResponse> createLoginWithMobileNumberResponse(LoginWithMobileNumberResponse value) {
        return new JAXBElement<LoginWithMobileNumberResponse>(_LoginWithMobileNumberResponse_QNAME, LoginWithMobileNumberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NaturalPersonOpenSingleDepositResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "naturalPersonOpenSingleDepositResponse")
    public JAXBElement<NaturalPersonOpenSingleDepositResponse> createNaturalPersonOpenSingleDepositResponse(NaturalPersonOpenSingleDepositResponse value) {
        return new JAXBElement<NaturalPersonOpenSingleDepositResponse>(_NaturalPersonOpenSingleDepositResponse_QNAME, NaturalPersonOpenSingleDepositResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenDeposit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "openDeposit")
    public JAXBElement<OpenDeposit> createOpenDeposit(OpenDeposit value) {
        return new JAXBElement<OpenDeposit>(_OpenDeposit_QNAME, OpenDeposit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendReversalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "sendReversalResponse")
    public JAXBElement<SendReversalResponse> createSendReversalResponse(SendReversalResponse value) {
        return new JAXBElement<SendReversalResponse>(_SendReversalResponse_QNAME, SendReversalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBlockChequeRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBlockChequeRequestBean")
    public JAXBElement<ChBlockChequeRequestBean> createChBlockChequeRequestBean(ChBlockChequeRequestBean value) {
        return new JAXBElement<ChBlockChequeRequestBean>(_ChBlockChequeRequestBean_QNAME, ChBlockChequeRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLoansResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getLoansResponse")
    public JAXBElement<GetLoansResponse> createGetLoansResponse(GetLoansResponse value) {
        return new JAXBElement<GetLoansResponse>(_GetLoansResponse_QNAME, GetLoansResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SwitchFundTransferFromVirtualTerminalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "switchFundTransferFromVirtualTerminalResponse")
    public JAXBElement<SwitchFundTransferFromVirtualTerminalResponse> createSwitchFundTransferFromVirtualTerminalResponse(SwitchFundTransferFromVirtualTerminalResponse value) {
        return new JAXBElement<SwitchFundTransferFromVirtualTerminalResponse>(_SwitchFundTransferFromVirtualTerminalResponse_QNAME, SwitchFundTransferFromVirtualTerminalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerAddressResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCustomerAddressResponse")
    public JAXBElement<GetCustomerAddressResponse> createGetCustomerAddressResponse(GetCustomerAddressResponse value) {
        return new JAXBElement<GetCustomerAddressResponse>(_GetCustomerAddressResponse_QNAME, GetCustomerAddressResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "verifyTransaction")
    public JAXBElement<VerifyTransaction> createVerifyTransaction(VerifyTransaction value) {
        return new JAXBElement<VerifyTransaction>(_VerifyTransaction_QNAME, VerifyTransaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AchBatchTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "achBatchTransfer")
    public JAXBElement<AchBatchTransfer> createAchBatchTransfer(AchBatchTransfer value) {
        return new JAXBElement<AchBatchTransfer>(_AchBatchTransfer_QNAME, AchBatchTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardDepositsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardDepositsResponse")
    public JAXBElement<GetCardDepositsResponse> createGetCardDepositsResponse(GetCardDepositsResponse value) {
        return new JAXBElement<GetCardDepositsResponse>(_GetCardDepositsResponse_QNAME, GetCardDepositsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChGuarantiesRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chGuarantiesRequestBean")
    public JAXBElement<ChGuarantiesRequestBean> createChGuarantiesRequestBean(ChGuarantiesRequestBean value) {
        return new JAXBElement<ChGuarantiesRequestBean>(_ChGuarantiesRequestBean_QNAME, ChGuarantiesRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcceptAchTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "acceptAchTransfer")
    public JAXBElement<AcceptAchTransfer> createAcceptAchTransfer(AcceptAchTransfer value) {
        return new JAXBElement<AcceptAchTransfer>(_AcceptAchTransfer_QNAME, AcceptAchTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardStatementInquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardStatementInquiryResponse")
    public JAXBElement<GetCardStatementInquiryResponse> createGetCardStatementInquiryResponse(GetCardStatementInquiryResponse value) {
        return new JAXBElement<GetCardStatementInquiryResponse>(_GetCardStatementInquiryResponse_QNAME, GetCardStatementInquiryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFavoriteAccountSettingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getFavoriteAccountSettingResponse")
    public JAXBElement<GetFavoriteAccountSettingResponse> createGetFavoriteAccountSettingResponse(GetFavoriteAccountSettingResponse value) {
        return new JAXBElement<GetFavoriteAccountSettingResponse>(_GetFavoriteAccountSettingResponse_QNAME, GetFavoriteAccountSettingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getDepositsResponse")
    public JAXBElement<GetDepositsResponse> createGetDepositsResponse(GetDepositsResponse value) {
        return new JAXBElement<GetDepositsResponse>(_GetDepositsResponse_QNAME, GetDepositsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositOwnerNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getDepositOwnerNameResponse")
    public JAXBElement<GetDepositOwnerNameResponse> createGetDepositOwnerNameResponse(GetDepositOwnerNameResponse value) {
        return new JAXBElement<GetDepositOwnerNameResponse>(_GetDepositOwnerNameResponse_QNAME, GetDepositOwnerNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPaymentServicesByType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getPaymentServicesByType")
    public JAXBElement<GetPaymentServicesByType> createGetPaymentServicesByType(GetPaymentServicesByType value) {
        return new JAXBElement<GetPaymentServicesByType>(_GetPaymentServicesByType_QNAME, GetPaymentServicesByType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChCardOwnerRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chCardOwnerRequestBean")
    public JAXBElement<ChCardOwnerRequestBean> createChCardOwnerRequestBean(ChCardOwnerRequestBean value) {
        return new JAXBElement<ChCardOwnerRequestBean>(_ChCardOwnerRequestBean_QNAME, ChCardOwnerRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCustomerInfoResponse")
    public JAXBElement<GetCustomerInfoResponse> createGetCustomerInfoResponse(GetCustomerInfoResponse value) {
        return new JAXBElement<GetCustomerInfoResponse>(_GetCustomerInfoResponse_QNAME, GetCustomerInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardBalanceInquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardBalanceInquiryResponse")
    public JAXBElement<CardBalanceInquiryResponse> createCardBalanceInquiryResponse(CardBalanceInquiryResponse value) {
        return new JAXBElement<CardBalanceInquiryResponse>(_CardBalanceInquiryResponse_QNAME, CardBalanceInquiryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoCustomRetrunResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "doCustomRetrunResponse")
    public JAXBElement<DoCustomRetrunResponse> createDoCustomRetrunResponse(DoCustomRetrunResponse value) {
        return new JAXBElement<DoCustomRetrunResponse>(_DoCustomRetrunResponse_QNAME, DoCustomRetrunResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChDepositOwnerRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chDepositOwnerRequestBean")
    public JAXBElement<ChDepositOwnerRequestBean> createChDepositOwnerRequestBean(ChDepositOwnerRequestBean value) {
        return new JAXBElement<ChDepositOwnerRequestBean>(_ChDepositOwnerRequestBean_QNAME, ChDepositOwnerRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardTransferResponse")
    public JAXBElement<CardTransferResponse> createCardTransferResponse(CardTransferResponse value) {
        return new JAXBElement<CardTransferResponse>(_CardTransferResponse_QNAME, CardTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExtendSMSSetting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "extendSMSSetting")
    public JAXBElement<ExtendSMSSetting> createExtendSMSSetting(ExtendSMSSetting value) {
        return new JAXBElement<ExtendSMSSetting>(_ExtendSMSSetting_QNAME, ExtendSMSSetting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBanks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getBanks")
    public JAXBElement<GetBanks> createGetBanks(GetBanks value) {
        return new JAXBElement<GetBanks>(_GetBanks_QNAME, GetBanks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPaymentBillStatement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getPaymentBillStatement")
    public JAXBElement<GetPaymentBillStatement> createGetPaymentBillStatement(GetPaymentBillStatement value) {
        return new JAXBElement<GetPaymentBillStatement>(_GetPaymentBillStatement_QNAME, GetPaymentBillStatement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChargeCardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "chargeCardResponse")
    public JAXBElement<ChargeCardResponse> createChargeCardResponse(ChargeCardResponse value) {
        return new JAXBElement<ChargeCardResponse>(_ChargeCardResponse_QNAME, ChargeCardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MenuDto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "MenuDto")
    public JAXBElement<MenuDto> createMenuDto(MenuDto value) {
        return new JAXBElement<MenuDto>(_MenuDto_QNAME, MenuDto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCardsResponse")
    public JAXBElement<GetCardsResponse> createGetCardsResponse(GetCardsResponse value) {
        return new JAXBElement<GetCardsResponse>(_GetCardsResponse_QNAME, GetCardsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserSMSSettingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getUserSMSSettingResponse")
    public JAXBElement<GetUserSMSSettingResponse> createGetUserSMSSettingResponse(GetUserSMSSettingResponse value) {
        return new JAXBElement<GetUserSMSSettingResponse>(_GetUserSMSSettingResponse_QNAME, GetUserSMSSettingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChBranchBalanceSearchBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chBranchBalanceSearchBean")
    public JAXBElement<ChBranchBalanceSearchBean> createChBranchBalanceSearchBean(ChBranchBalanceSearchBean value) {
        return new JAXBElement<ChBranchBalanceSearchBean>(_ChBranchBalanceSearchBean_QNAME, ChBranchBalanceSearchBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MenuEntryDto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "MenuEntryDto")
    public JAXBElement<MenuEntryDto> createMenuEntryDto(MenuEntryDto value) {
        return new JAXBElement<MenuEntryDto>(_MenuEntryDto_QNAME, MenuEntryDto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAutoTransferListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getAutoTransferListResponse")
    public JAXBElement<GetAutoTransferListResponse> createGetAutoTransferListResponse(GetAutoTransferListResponse value) {
        return new JAXBElement<GetAutoTransferListResponse>(_GetAutoTransferListResponse_QNAME, GetAutoTransferListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChPaymentRequestBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chPaymentRequestBean")
    public JAXBElement<ChPaymentRequestBean> createChPaymentRequestBean(ChPaymentRequestBean value) {
        return new JAXBElement<ChPaymentRequestBean>(_ChPaymentRequestBean_QNAME, ChPaymentRequestBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardChangePinResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "cardChangePinResponse")
    public JAXBElement<CardChangePinResponse> createCardChangePinResponse(CardChangePinResponse value) {
        return new JAXBElement<CardChangePinResponse>(_CardChangePinResponse_QNAME, CardChangePinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCreditDossiersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "getCreditDossiersResponse")
    public JAXBElement<GetCreditDossiersResponse> createGetCreditDossiersResponse(GetCreditDossiersResponse value) {
        return new JAXBElement<GetCreditDossiersResponse>(_GetCreditDossiersResponse_QNAME, GetCreditDossiersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChDestinationCardOwnerRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "bean", name = "chDestinationCardOwnerRequest")
    public JAXBElement<ChDestinationCardOwnerRequest> createChDestinationCardOwnerRequest(ChDestinationCardOwnerRequest value) {
        return new JAXBElement<ChDestinationCardOwnerRequest>(_ChDestinationCardOwnerRequest_QNAME, ChDestinationCardOwnerRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayBatchBillResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/", name = "payBatchBillResponse")
    public JAXBElement<PayBatchBillResponse> createPayBatchBillResponse(PayBatchBillResponse value) {
        return new JAXBElement<PayBatchBillResponse>(_PayBatchBillResponse_QNAME, PayBatchBillResponse.class, null, value);
    }

}
