/**
 * ChNormalTransferRequestBean.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class NormalTransferRequestDto implements java.io.Serializable {
    private java.lang.String additionalDocumentDesc;

    private java.math.BigDecimal amount;

    @ApiModelProperty(value = "secondPasswordType")
    private com.caspco.channel.dtos.SecondPasswordType chSecondPasswordType;

    private java.lang.String cif;

    private java.lang.String currencyCode;

    private java.lang.String destinationComment;

    private java.lang.String destinationDeposit;

    private java.lang.String email;

    private java.lang.String referenceNumber;

    private java.lang.String secondPassword;

    private java.lang.String sourceComment;

    private java.lang.String sourceDeposit;


    public String getAdditionalDocumentDesc() {
        return additionalDocumentDesc;
    }

    public void setAdditionalDocumentDesc(String additionalDocumentDesc) {
        this.additionalDocumentDesc = additionalDocumentDesc;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public SecondPasswordType getChSecondPasswordType() {
        return chSecondPasswordType;
    }

    public void setChSecondPasswordType(SecondPasswordType chSecondPasswordType) {
        this.chSecondPasswordType = chSecondPasswordType;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getDestinationComment() {
        return destinationComment;
    }

    public void setDestinationComment(String destinationComment) {
        this.destinationComment = destinationComment;
    }

    public String getDestinationDeposit() {
        return destinationDeposit;
    }

    public void setDestinationDeposit(String destinationDeposit) {
        this.destinationDeposit = destinationDeposit;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getSecondPassword() {
        return secondPassword;
    }

    public void setSecondPassword(String secondPassword) {
        this.secondPassword = secondPassword;
    }

    public String getSourceComment() {
        return sourceComment;
    }

    public void setSourceComment(String sourceComment) {
        this.sourceComment = sourceComment;
    }

    public String getSourceDeposit() {
        return sourceDeposit;
    }

    public void setSourceDeposit(String sourceDeposit) {
        this.sourceDeposit = sourceDeposit;
    }
}
