
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for suspendAchTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="suspendAchTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAchTransferKeyRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAchTransferKeyRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "suspendAchTransfer", propOrder = {
    "chAchTransferKeyRequestBean"
})
public class SuspendAchTransfer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAchTransferKeyRequestBean chAchTransferKeyRequestBean;

    /**
     * Gets the value of the chAchTransferKeyRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAchTransferKeyRequestBean }
     *     
     */
    public ChAchTransferKeyRequestBean getChAchTransferKeyRequestBean() {
        return chAchTransferKeyRequestBean;
    }

    /**
     * Sets the value of the chAchTransferKeyRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAchTransferKeyRequestBean }
     *     
     */
    public void setChAchTransferKeyRequestBean(ChAchTransferKeyRequestBean value) {
        this.chAchTransferKeyRequestBean = value;
    }

}
