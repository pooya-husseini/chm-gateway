
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chProcessCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chProcessCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="GOODS_AND_SERVICE"/>
 *     &lt;enumeration value="CASH"/>
 *     &lt;enumeration value="DEBIT_ADJUSTMENT"/>
 *     &lt;enumeration value="CHEQUE_GUARANTEE"/>
 *     &lt;enumeration value="CHEQUE_VERIFICATION"/>
 *     &lt;enumeration value="EURO_CHEQUE"/>
 *     &lt;enumeration value="TRAVELLER_CHEQUE"/>
 *     &lt;enumeration value="LETTER_OF_CREDIT"/>
 *     &lt;enumeration value="GIRO"/>
 *     &lt;enumeration value="GOODS_AND_SERVICE_WITH_CASH_DISBURSEMENT"/>
 *     &lt;enumeration value="NON_CASH_FINANCIAL_INSTRUMENT"/>
 *     &lt;enumeration value="QUASI_CASH_AND_SCRIP"/>
 *     &lt;enumeration value="BILL_PAYMENT"/>
 *     &lt;enumeration value="RETURNS"/>
 *     &lt;enumeration value="DEPOSITS"/>
 *     &lt;enumeration value="CREDIT_ADJUSTMENT"/>
 *     &lt;enumeration value="CHEQUE_DEPOSIT_GUARANTEE"/>
 *     &lt;enumeration value="CHEQUE_DEPOSIT"/>
 *     &lt;enumeration value="DEPOSIT_CONFIRM"/>
 *     &lt;enumeration value="RETRACT_CONFIRM"/>
 *     &lt;enumeration value="AVAILABLE_FUNDS_INQUIRY"/>
 *     &lt;enumeration value="BALANCE_INQUIRY"/>
 *     &lt;enumeration value="BILL_INQUIRY"/>
 *     &lt;enumeration value="ACCOUNT_CHEQUE_INQUIRY"/>
 *     &lt;enumeration value="CUSTOMER_PAN_INQUIRY"/>
 *     &lt;enumeration value="CUSTOMER_DEPOSIT_INQUIRY"/>
 *     &lt;enumeration value="BILL"/>
 *     &lt;enumeration value="ACCOUNT_LIST_INQUIRY"/>
 *     &lt;enumeration value="MIN_BILL"/>
 *     &lt;enumeration value="TRANSFER"/>
 *     &lt;enumeration value="TRANSFER_FROM"/>
 *     &lt;enumeration value="TRANSFER_TO"/>
 *     &lt;enumeration value="TRANSFER_TO_PAN"/>
 *     &lt;enumeration value="PAYMENT"/>
 *     &lt;enumeration value="GENERAL"/>
 *     &lt;enumeration value="CHARGE_BACK"/>
 *     &lt;enumeration value="CARD_CAPTURED"/>
 *     &lt;enumeration value="CHANGE_PIN_2"/>
 *     &lt;enumeration value="AMBIGUOUS"/>
 *     &lt;enumeration value="UN_BLOCK"/>
 *     &lt;enumeration value="ATM_REPLENISH"/>
 *     &lt;enumeration value="ATM_BALANCE"/>
 *     &lt;enumeration value="SUPERVISOR_BALANCE"/>
 *     &lt;enumeration value="MERCHANT_BALANCE"/>
 *     &lt;enumeration value="MERCHANT_RESET"/>
 *     &lt;enumeration value="CHANGE_FLOOR"/>
 *     &lt;enumeration value="CHARGE"/>
 *     &lt;enumeration value="CHANGE_PIN"/>
 *     &lt;enumeration value="CARD_AND_PIN_VERIFICATION"/>
 *     &lt;enumeration value="RETRACT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chProcessCode")
@XmlEnum
public enum ChProcessCode {

    GOODS_AND_SERVICE,
    CASH,
    DEBIT_ADJUSTMENT,
    CHEQUE_GUARANTEE,
    CHEQUE_VERIFICATION,
    EURO_CHEQUE,
    TRAVELLER_CHEQUE,
    LETTER_OF_CREDIT,
    GIRO,
    GOODS_AND_SERVICE_WITH_CASH_DISBURSEMENT,
    NON_CASH_FINANCIAL_INSTRUMENT,
    QUASI_CASH_AND_SCRIP,
    BILL_PAYMENT,
    RETURNS,
    DEPOSITS,
    CREDIT_ADJUSTMENT,
    CHEQUE_DEPOSIT_GUARANTEE,
    CHEQUE_DEPOSIT,
    DEPOSIT_CONFIRM,
    RETRACT_CONFIRM,
    AVAILABLE_FUNDS_INQUIRY,
    BALANCE_INQUIRY,
    BILL_INQUIRY,
    ACCOUNT_CHEQUE_INQUIRY,
    CUSTOMER_PAN_INQUIRY,
    CUSTOMER_DEPOSIT_INQUIRY,
    BILL,
    ACCOUNT_LIST_INQUIRY,
    MIN_BILL,
    TRANSFER,
    TRANSFER_FROM,
    TRANSFER_TO,
    TRANSFER_TO_PAN,
    PAYMENT,
    GENERAL,
    CHARGE_BACK,
    CARD_CAPTURED,
    CHANGE_PIN_2,
    AMBIGUOUS,
    UN_BLOCK,
    ATM_REPLENISH,
    ATM_BALANCE,
    SUPERVISOR_BALANCE,
    MERCHANT_BALANCE,
    MERCHANT_RESET,
    CHANGE_FLOOR,
    CHARGE,
    CHANGE_PIN,
    CARD_AND_PIN_VERIFICATION,
    RETRACT;

    public String value() {
        return name();
    }

    public static ChProcessCode fromValue(String v) {
        return valueOf(v);
    }

}
