/**
 * ChDepositStatus.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public
enum DepositStatus implements java.io.Serializable {
    CLOSE,
    OPEN,
    NEGATIVE_BLOCK,
    POSITIVE_BLOCK,
    BLOCK,
    RESTING_AND_NEGATIVE_BLOCK,
    RESTING_AND_POSITIVE_BLOCK,
    RESTING_AND_BLOCK,
    RESTING,
    OLD,
    OPENING;
}
