/**
 * ChStatementBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

import java.math.BigDecimal;
import java.util.Date;

public class StatementDto  implements java.io.Serializable {

    private String agentBranchCode;

    private String agentBranchName;

    private BigDecimal balance;

    private String branchCode;

    private String branchName;

    private String customerDesc;

    private Date date;

    private String description;

    private String referenceNumber;

    private Long registrationNumber;

    private Integer sequence;

    private String serial;

    private String serialNumber;

    private BigDecimal transferAmount;

    public String getAgentBranchCode() {
        return agentBranchCode;
    }

    public void setAgentBranchCode(String agentBranchCode) {
        this.agentBranchCode = agentBranchCode;
    }

    public String getAgentBranchName() {
        return agentBranchName;
    }

    public void setAgentBranchName(String agentBranchName) {
        this.agentBranchName = agentBranchName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getCustomerDesc() {
        return customerDesc;
    }

    public void setCustomerDesc(String customerDesc) {
        this.customerDesc = customerDesc;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public Long getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(Long registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }

    @Override
    public String toString() {
        return "StatementDto{" +
                "agentBranchCode='" + agentBranchCode + '\'' +
                ", agentBranchName='" + agentBranchName + '\'' +
                ", balance=" + balance +
                ", branchCode='" + branchCode + '\'' +
                ", branchName='" + branchName + '\'' +
                ", customerDesc='" + customerDesc + '\'' +
                ", date=" + date +
                ", description='" + description + '\'' +
                ", referenceNumber='" + referenceNumber + '\'' +
                ", registrationNumber=" + registrationNumber +
                ", sequence=" + sequence +
                ", serial='" + serial + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                ", transferAmount=" + transferAmount +
                '}';
    }
}
