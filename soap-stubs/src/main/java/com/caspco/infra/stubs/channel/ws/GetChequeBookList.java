
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getChequeBookList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getChequeBookList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBaseSearchChequeBookRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBaseSearchChequeBookRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getChequeBookList", propOrder = {
    "chBaseSearchChequeBookRequestBean"
})
public class GetChequeBookList
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBaseSearchChequeBookRequestBean chBaseSearchChequeBookRequestBean;

    /**
     * Gets the value of the chBaseSearchChequeBookRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBaseSearchChequeBookRequestBean }
     *     
     */
    public ChBaseSearchChequeBookRequestBean getChBaseSearchChequeBookRequestBean() {
        return chBaseSearchChequeBookRequestBean;
    }

    /**
     * Sets the value of the chBaseSearchChequeBookRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBaseSearchChequeBookRequestBean }
     *     
     */
    public void setChBaseSearchChequeBookRequestBean(ChBaseSearchChequeBookRequestBean value) {
        this.chBaseSearchChequeBookRequestBean = value;
    }

}
