
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for accountTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="accountTypes">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Internals"/>
 *     &lt;enumeration value="CurrentNostros"/>
 *     &lt;enumeration value="DepositNostros"/>
 *     &lt;enumeration value="Vostros"/>
 *     &lt;enumeration value="Currents"/>
 *     &lt;enumeration value="Savings"/>
 *     &lt;enumeration value="LongTermDeposits"/>
 *     &lt;enumeration value="ShortTermDeposits"/>
 *     &lt;enumeration value="Loans"/>
 *     &lt;enumeration value="EPayments"/>
 *     &lt;enumeration value="BankCertifiedCheques"/>
 *     &lt;enumeration value="InterBankCertifiedCheques"/>
 *     &lt;enumeration value="Others"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "accountTypes")
@XmlEnum
public enum AccountTypes {

    @XmlEnumValue("Internals")
    INTERNALS("Internals"),
    @XmlEnumValue("CurrentNostros")
    CURRENT_NOSTROS("CurrentNostros"),
    @XmlEnumValue("DepositNostros")
    DEPOSIT_NOSTROS("DepositNostros"),
    @XmlEnumValue("Vostros")
    VOSTROS("Vostros"),
    @XmlEnumValue("Currents")
    CURRENTS("Currents"),
    @XmlEnumValue("Savings")
    SAVINGS("Savings"),
    @XmlEnumValue("LongTermDeposits")
    LONG_TERM_DEPOSITS("LongTermDeposits"),
    @XmlEnumValue("ShortTermDeposits")
    SHORT_TERM_DEPOSITS("ShortTermDeposits"),
    @XmlEnumValue("Loans")
    LOANS("Loans"),
    @XmlEnumValue("EPayments")
    E_PAYMENTS("EPayments"),
    @XmlEnumValue("BankCertifiedCheques")
    BANK_CERTIFIED_CHEQUES("BankCertifiedCheques"),
    @XmlEnumValue("InterBankCertifiedCheques")
    INTER_BANK_CERTIFIED_CHEQUES("InterBankCertifiedCheques"),
    @XmlEnumValue("Others")
    OTHERS("Others");
    private final String value;

    AccountTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AccountTypes fromValue(String v) {
        for (AccountTypes c: AccountTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
