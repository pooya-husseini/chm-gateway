
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateUserBillSetting complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateUserBillSetting">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBillSettingRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillSettingRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateUserBillSetting", propOrder = {
    "chBillSettingRequestBean"
})
public class UpdateUserBillSetting
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBillSettingRequestBean chBillSettingRequestBean;

    /**
     * Gets the value of the chBillSettingRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillSettingRequestBean }
     *     
     */
    public ChBillSettingRequestBean getChBillSettingRequestBean() {
        return chBillSettingRequestBean;
    }

    /**
     * Sets the value of the chBillSettingRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillSettingRequestBean }
     *     
     */
    public void setChBillSettingRequestBean(ChBillSettingRequestBean value) {
        this.chBillSettingRequestBean = value;
    }

}
