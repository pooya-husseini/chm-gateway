package com.caspco.channel.exceptions;

import java.util.Map;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/26/17
 * Time: 3:48 PM
 */
public class SystemException extends OpenApiException {

    public SystemException(String message, Map<String,?> args) {
        super(message, args);
    }

    public SystemException(String message) {
        super(message);
    }

    public SystemException(ExceptionDescription description) {
        super(description);
    }

    public SystemException(String message, ExceptionDescription description) {
        super(message, description);
    }

    public SystemException(String message, Throwable cause, ExceptionDescription description) {
        super(message, cause, description);
    }

    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public SystemException(Throwable cause, ExceptionDescription description) {
        super(cause, description);
    }

    public SystemException(Throwable cause) {
        super(cause);
    }

    public SystemException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ExceptionDescription description) {
        super(message, cause, enableSuppression, writableStackTrace, description);
    }
}
