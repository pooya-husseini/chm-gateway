
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for achTransferReportResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="achTransferReportResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAchTransfersResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAchTransfersResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "achTransferReportResponse", propOrder = {
    "chAchTransfersResponseBean"
})
public class AchTransferReportResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAchTransfersResponseBean chAchTransfersResponseBean;

    /**
     * Gets the value of the chAchTransfersResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAchTransfersResponseBean }
     *     
     */
    public ChAchTransfersResponseBean getChAchTransfersResponseBean() {
        return chAchTransfersResponseBean;
    }

    /**
     * Sets the value of the chAchTransfersResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAchTransfersResponseBean }
     *     
     */
    public void setChAchTransfersResponseBean(ChAchTransfersResponseBean value) {
        this.chAchTransfersResponseBean = value;
    }

}
