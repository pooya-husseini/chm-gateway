package com.caspco.channel.configuration;

import com.caspco.channel.swagger.MethodParameterFilter;
import com.caspco.infra.stubs.channel.ws.CoreBankingService;
import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/11/17
 * Time: 2:36 PM
 */

@Configuration
public class BaseConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public Docket api() {
        List<ResolvedType> resolvedTypes = Arrays.stream(CoreBankingService.class.getDeclaredMethods())
                .filter(i -> i.getParameterCount() <= 1).flatMap(i -> {
//            if (i.getParameters().length > 1) {
//                throw new IllegalStateException("Method " + i.getName() + " should have only one parameter");
//            }
                    return resolveMethodTypes(i).stream();
                }).collect(Collectors.toList());

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.caspco.channel.services"))
                .paths(PathSelectors.regex("/.*"))
                .build()
                .additionalModels(
                        resolvedTypes.get(0), resolvedTypes.toArray(new ResolvedType[resolvedTypes.size()])
                );
    }

    private List<ResolvedType> resolveMethodTypes(Method method) {
        return resolveMethodTypes(method, parameter -> true);
    }

    private List<ResolvedType> resolveMethodTypes(Method method,MethodParameterFilter methodParameterFilter) {
        TypeResolver typeResolver = new TypeResolver();
        List<ResolvedType> types = new ArrayList<>();
        List<Parameter> parameters=new ArrayList<>();
        if (method.getParameters() != null) {
            parameters=Arrays.stream(method.getParameters())
                    .filter(methodParameterFilter::isValid)
                    .collect(Collectors.toList());
        }
        if (Collection.class.isAssignableFrom(method.getReturnType())) {
            Class<?> actualType = (Class<?>) ((ParameterizedTypeImpl) method.getGenericReturnType()).getActualTypeArguments()[0];
            types.add(typeResolver.resolve(actualType));
        } else {
            types.add(typeResolver.resolve(method.getReturnType()));
        }

        if (method.getParameters().length > 0) {
            types.add(typeResolver.resolve(parameters.get(0).getType()));
        }
        return types;
    }

    private ApiInfo metadata() {
        return new ApiInfo(
                "Spring Boot REST API",
                "Spring Boot REST API for Online Store",
                "1.0",
                "Terms of service",
                new Contact("John Thompson", "https://springframework.guru/about/", "john@springfrmework.guru"),
                "Apache License Version 2.0",
                "https://www.apache.org/licenses/LICENSE-2.0", Collections.EMPTY_LIST);
    }


}