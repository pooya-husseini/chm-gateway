package com.caspco.channel.mapper;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/15/17
 * Time: 4:35 PM
 */
public class SecuredMapper extends ObjectMapper {
    private Set<String> ignoredProps = new HashSet<>();


    public SecuredMapper() {
        super();
        try {
            InputStream resource = this.getClass().getClassLoader().getResourceAsStream("ignored-props.txt");
            String s = new String(FileCopyUtils.copyToByteArray(resource), StandardCharsets.UTF_8);
            ignoredProps = Arrays.stream(s.split(",")).collect(Collectors.toSet());
            SimpleModule module = new SimpleModule("MyModule", new Version(1, 1, 1, "Snapshot"));
            module.addSerializer(new JsonSerializer<String>() {
                @Override
                public void serialize(String value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
                    if (ignoredProps.contains(gen.getOutputContext().getCurrentName())) {
                        gen.writeString("***********");
                    } else {
                        gen.writeString(value);
                    }
                }

                @Override
                public Class<String> handledType() {
                    return String.class;
                }
            });
            registerModule(module);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
