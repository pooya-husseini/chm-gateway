
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chDefaultStatementRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chDefaultStatementRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="billPageSize" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="depositNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastN" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="viewType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDefaultBillStatementBeanType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chDefaultStatementRequestBean", propOrder = {
    "billPageSize",
    "depositNumber",
    "lastN",
    "viewType"
})
public class ChDefaultStatementRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected Short billPageSize;
    protected String depositNumber;
    protected Short lastN;
    @XmlSchemaType(name = "string")
    protected ChDefaultBillStatementBeanType viewType;

    /**
     * Gets the value of the billPageSize property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getBillPageSize() {
        return billPageSize;
    }

    /**
     * Sets the value of the billPageSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setBillPageSize(Short value) {
        this.billPageSize = value;
    }

    /**
     * Gets the value of the depositNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepositNumber() {
        return depositNumber;
    }

    /**
     * Sets the value of the depositNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepositNumber(String value) {
        this.depositNumber = value;
    }

    /**
     * Gets the value of the lastN property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getLastN() {
        return lastN;
    }

    /**
     * Sets the value of the lastN property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setLastN(Short value) {
        this.lastN = value;
    }

    /**
     * Gets the value of the viewType property.
     * 
     * @return
     *     possible object is
     *     {@link ChDefaultBillStatementBeanType }
     *     
     */
    public ChDefaultBillStatementBeanType getViewType() {
        return viewType;
    }

    /**
     * Sets the value of the viewType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDefaultBillStatementBeanType }
     *     
     */
    public void setViewType(ChDefaultBillStatementBeanType value) {
        this.viewType = value;
    }

}
