
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBatchBillInfoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBatchBillInfoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBatchBillInfoResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBatchBillInfoResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBatchBillInfoResponse", propOrder = {
    "chBatchBillInfoResponseBean"
})
public class GetBatchBillInfoResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBatchBillInfoResponseBean chBatchBillInfoResponseBean;

    /**
     * Gets the value of the chBatchBillInfoResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBatchBillInfoResponseBean }
     *     
     */
    public ChBatchBillInfoResponseBean getChBatchBillInfoResponseBean() {
        return chBatchBillInfoResponseBean;
    }

    /**
     * Sets the value of the chBatchBillInfoResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBatchBillInfoResponseBean }
     *     
     */
    public void setChBatchBillInfoResponseBean(ChBatchBillInfoResponseBean value) {
        this.chBatchBillInfoResponseBean = value;
    }

}
