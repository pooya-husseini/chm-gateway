import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dtos.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 8/19/17
 * Time: 4:35 PM
 */

public class TestLauncher {
    private static final String URL = "http://192.168.104.129:8989/core/";

    private static RestTemplate restTemplate = new RestTemplate();
    private static String sessionId;
    private static ObjectMapper objectMapper = new ObjectMapper();

    private static AtomicReference<String> achRefId=new AtomicReference<>();

    @BeforeClass
    public static void init() throws IOException {
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        restTemplate.setMessageConverters(Collections.singletonList(new MappingJackson2HttpMessageConverter(objectMapper)));
        LoginDto loginDto = new LoginDto();
        loginDto.setChannelServiceType(ChannelServiceType.INTERNET_BANK);
        UserInfoRequestDto bean = new UserInfoRequestDto();
        bean.setUsername("rezazadeh");
        bean.setPassword("32899631");
        loginDto.setRequestBean(bean);
        LoginResponseDto login = callMethod(loginDto, "login", LoginResponseDto.class);
//        LoginResponseDto loginResponseDto = restTemplate.postForObject(URL + "login", loginDto, LoginResponseDto.class);
        sessionId = login.getSessionId();
    }

    @Test
    public void testUserProfile() {
        UserRequestDto dto = new UserRequestDto();
        dto.setCif("36340073");
        callMethod(dto, "getCustomerInfo");
//        UserResponseDto post = restTemplate.postForObject(URL + "getCustomerInfo", getEntity(dto), UserResponseDto.class);
//        System.out.println(post);
    }

    @Test
    public void getDeposits() {
        DepositSearchRequestDto dto = new DepositSearchRequestDto();
        dto.setCif("36340073");
        callMethod(dto, "getDeposits");
//        DepositResponseDto post = restTemplate.postForObject(URL + "getDeposits", getEntity(dto), DepositResponseDto.class);
//        System.out.println(post);
    }

    @Test
    public void getCardStatementInquiry() {
        CardStatementRequestDto dto = new CardStatementRequestDto();
        dto.setPan("6221061073792683");
        callMethod(dto, "getCardStatementInquiry");
//        List<CardStatementDto> result = restTemplate.exchange(URL + "getCardStatementInquiry", HttpMethod.POST, getEntity(dto), new ParameterizedTypeReference<List<CardStatementDto>>() {
//        }).getBody();
//        System.out.println(result);
    }


    @Test
    public void getStatement() {
        StatementSearchRequestDto dto = new StatementSearchRequestDto();
        dto.setDepositNumber("80001412700005");
        dto.setLength(10L);
        dto.setOffset(1L);
        callMethod(dto, "getStatement");
    }

    @Test
    public void normalTransfer() throws JsonProcessingException {
        NormalTransferRequestDto dto = new NormalTransferRequestDto();
        dto.setSourceDeposit("80001876653005");
        dto.setDestinationDeposit("80001412700005");
        dto.setAmount(BigDecimal.valueOf(1L));
        callMethod(dto, "normalTransfer");
    }


    @Test
    public void achNormalTransfer() throws JsonProcessingException {
        if (achRefId.get() != null) {
            return;
        }
        System.out.println("Hello");
        AchNormalTransferRequestDto dto = new AchNormalTransferRequestDto();
        dto.setSourceDepositNumber("47000470593600");
        dto.setIbanNumber("IR380540100147000470593600");
        dto.setAmount(BigDecimal.valueOf(1L));
        dto.setSecondPassword("22999269");
        dto.setOwnerName("someone");
        NormalAchTransferResultDto responseDto = callMethod(dto, "achNormalTransfer", NormalAchTransferResultDto.class);
        achRefId.set(responseDto.getReferenceId());
    }

    @Test
    public void reportAchTransferSummary() throws JsonProcessingException {
        if (achRefId.get() == null) {
            achNormalTransfer();
        }
        AchTransferSummeryFilterDto dto = new AchTransferSummeryFilterDto();
        dto.setReferenceId(achRefId.get());
        callMethod(dto, "reportAchTransferSummary");
    }

    @Test
    public void reportAchTransferSummaryWithCif() throws JsonProcessingException {
        if (achRefId.get() == null) {
            achNormalTransfer();
        }
        AchTransferSummeryFilterDto dto = new AchTransferSummeryFilterDto();
        dto.setCif("36340073");
        callMethod(dto, "reportAchTransferSummary");
    }

    @Test
    public void cardTransfer() throws JsonProcessingException {
        FundTransferRequestDto dto = new FundTransferRequestDto();
        dto.setAmount(BigDecimal.valueOf(1L));
        dto.setCvv2("411");
        dto.setDestination("6221061101871327");
        dto.setDestinationType(DepositOrPan.PAN);
        dto.setExpDate("9905");
//        dto.setPan("6221061207243116");
        dto.setPan("6221061207242936");
        dto.setPin("58761");
        dto.setPinType(PinType.EPAY);
        System.out.println(objectMapper.writeValueAsString(dto));
        callMethod(dto, "cardTransfer");
    }

    @Test(expected = Exception.class)
    public void hotCard() throws JsonProcessingException {
        PanRequestDto dto = new PanRequestDto();
        dto.setPan("6221061207243116");
        callMethod(dto, "hotCard");
    }


    private void callMethod(Object dto, String address) {
        callMethod(dto, address, Object.class);
    }

    private static <T> T callMethod(Object dto, String address, Class<T> resultClass) {
        try {
            FileWriter stream = new FileWriter(new File(address+".txt"),true);
            stream.append("/").append(address).append(" :\n\n");
            stream.append("Input is: \n");
            stream.append(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(dto)).append("\n\n");
            T result = restTemplate.postForObject(URL + address, getEntity(dto), resultClass);
            stream.append("Output is: \n");
            stream.append(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result)).append("\n");
            stream.append("\n\n######################################################################################\n\n\n");
            stream.close();
            return result;
        } catch (Exception x) {
            throw new RuntimeException(x);
        }
    }

    private static <T> HttpEntity<T> getEntity(T dto) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", sessionId);
        return new HttpEntity<>(dto, headers);
    }

    @AfterClass
    public static void cleanUp() throws IOException {

    }
}
