
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chChangeUsernameRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chChangeUsernameRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currentUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="newUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chChangeUsernameRequestBean", propOrder = {
    "currentUsername",
    "newUsername"
})
public class ChChangeUsernameRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String currentUsername;
    protected String newUsername;

    /**
     * Gets the value of the currentUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentUsername() {
        return currentUsername;
    }

    /**
     * Sets the value of the currentUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentUsername(String value) {
        this.currentUsername = value;
    }

    /**
     * Gets the value of the newUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewUsername() {
        return newUsername;
    }

    /**
     * Sets the value of the newUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewUsername(String value) {
        this.newUsername = value;
    }

}
