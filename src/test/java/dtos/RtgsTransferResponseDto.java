/**
 * ChRtgsTransferResponseBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import java.util.List;

public class RtgsTransferResponseDto  implements java.io.Serializable {

    private java.lang.Long totalRecord;

    private List<TransferDetailDto> transferDetailsDtos;

    public Long getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(Long totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<TransferDetailDto> getTransferDetailsDtos() {
        return transferDetailsDtos;
    }

    public void setTransferDetailsDtos(List<TransferDetailDto> transferDetailsDtos) {
        this.transferDetailsDtos = transferDetailsDtos;
    }
}
