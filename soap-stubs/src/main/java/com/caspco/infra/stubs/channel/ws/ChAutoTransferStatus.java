
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chAutoTransferStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chAutoTransferStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FINISHED"/>
 *     &lt;enumeration value="ACTIVE"/>
 *     &lt;enumeration value="CANCELED"/>
 *     &lt;enumeration value="UNSUCCESSFUL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chAutoTransferStatus")
@XmlEnum
public enum ChAutoTransferStatus {

    FINISHED,
    ACTIVE,
    CANCELED,
    UNSUCCESSFUL;

    public String value() {
        return name();
    }

    public static ChAutoTransferStatus fromValue(String v) {
        return valueOf(v);
    }

}
