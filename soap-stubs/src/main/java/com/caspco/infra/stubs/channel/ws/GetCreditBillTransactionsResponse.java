
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCreditBillTransactionsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCreditBillTransactionsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chTransactionResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chTransactionResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCreditBillTransactionsResponse", propOrder = {
    "chTransactionResponseBean"
})
public class GetCreditBillTransactionsResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChTransactionResponseBean chTransactionResponseBean;

    /**
     * Gets the value of the chTransactionResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChTransactionResponseBean }
     *     
     */
    public ChTransactionResponseBean getChTransactionResponseBean() {
        return chTransactionResponseBean;
    }

    /**
     * Sets the value of the chTransactionResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChTransactionResponseBean }
     *     
     */
    public void setChTransactionResponseBean(ChTransactionResponseBean value) {
        this.chTransactionResponseBean = value;
    }

}
