
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reportAchTransferSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reportAchTransferSummary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAchTransferSummeryFilterBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAchTransferSummeryFilterBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reportAchTransferSummary", propOrder = {
    "chAchTransferSummeryFilterBean"
})
public class ReportAchTransferSummary
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAchTransferSummeryFilterBean chAchTransferSummeryFilterBean;

    /**
     * Gets the value of the chAchTransferSummeryFilterBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAchTransferSummeryFilterBean }
     *     
     */
    public ChAchTransferSummeryFilterBean getChAchTransferSummeryFilterBean() {
        return chAchTransferSummeryFilterBean;
    }

    /**
     * Sets the value of the chAchTransferSummeryFilterBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAchTransferSummeryFilterBean }
     *     
     */
    public void setChAchTransferSummeryFilterBean(ChAchTransferSummeryFilterBean value) {
        this.chAchTransferSummeryFilterBean = value;
    }

}
