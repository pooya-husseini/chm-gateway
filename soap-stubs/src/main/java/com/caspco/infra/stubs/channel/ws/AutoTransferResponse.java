
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for autoTransferResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="autoTransferResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAutoTransferResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAutoTransferResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "autoTransferResponse", propOrder = {
    "chAutoTransferResponseBean"
})
public class AutoTransferResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAutoTransferResponseBean chAutoTransferResponseBean;

    /**
     * Gets the value of the chAutoTransferResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAutoTransferResponseBean }
     *     
     */
    public ChAutoTransferResponseBean getChAutoTransferResponseBean() {
        return chAutoTransferResponseBean;
    }

    /**
     * Sets the value of the chAutoTransferResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAutoTransferResponseBean }
     *     
     */
    public void setChAutoTransferResponseBean(ChAutoTransferResponseBean value) {
        this.chAutoTransferResponseBean = value;
    }

}
