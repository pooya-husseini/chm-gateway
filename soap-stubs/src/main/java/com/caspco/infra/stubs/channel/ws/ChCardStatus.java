
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCardStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chCardStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OK"/>
 *     &lt;enumeration value="HOT"/>
 *     &lt;enumeration value="WARM"/>
 *     &lt;enumeration value="BLOCKED"/>
 *     &lt;enumeration value="CAPTURED"/>
 *     &lt;enumeration value="EXPIRED"/>
 *     &lt;enumeration value="INACTIVE"/>
 *     &lt;enumeration value="SETTLEMENT"/>
 *     &lt;enumeration value="CLOSED"/>
 *     &lt;enumeration value="PRE_ACTIVE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chCardStatus")
@XmlEnum
public enum ChCardStatus {

    OK,
    HOT,
    WARM,
    BLOCKED,
    CAPTURED,
    EXPIRED,
    INACTIVE,
    SETTLEMENT,
    CLOSED,
    PRE_ACTIVE;

    public String value() {
        return name();
    }

    public static ChCardStatus fromValue(String v) {
        return valueOf(v);
    }

}
