
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardInfoRetrievationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardInfoRetrievationResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardInfoRetrievationResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardInfoRetrievationResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardInfoRetrievationResponse", propOrder = {
    "chCardInfoRetrievationResponseBean"
})
public class CardInfoRetrievationResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardInfoRetrievationResponseBean chCardInfoRetrievationResponseBean;

    /**
     * Gets the value of the chCardInfoRetrievationResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardInfoRetrievationResponseBean }
     *     
     */
    public ChCardInfoRetrievationResponseBean getChCardInfoRetrievationResponseBean() {
        return chCardInfoRetrievationResponseBean;
    }

    /**
     * Sets the value of the chCardInfoRetrievationResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardInfoRetrievationResponseBean }
     *     
     */
    public void setChCardInfoRetrievationResponseBean(ChCardInfoRetrievationResponseBean value) {
        this.chCardInfoRetrievationResponseBean = value;
    }

}
