
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBranchBalance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBranchBalance">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBranchBalanceSearchBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBranchBalanceSearchBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBranchBalance", propOrder = {
    "chBranchBalanceSearchBean"
})
public class GetBranchBalance
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBranchBalanceSearchBean chBranchBalanceSearchBean;

    /**
     * Gets the value of the chBranchBalanceSearchBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBranchBalanceSearchBean }
     *     
     */
    public ChBranchBalanceSearchBean getChBranchBalanceSearchBean() {
        return chBranchBalanceSearchBean;
    }

    /**
     * Sets the value of the chBranchBalanceSearchBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBranchBalanceSearchBean }
     *     
     */
    public void setChBranchBalanceSearchBean(ChBranchBalanceSearchBean value) {
        this.chBranchBalanceSearchBean = value;
    }

}
