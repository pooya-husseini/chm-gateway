package com.caspco.channel.configuration;

import com.caspco.channel.interceptor.AbstractServicesInterceptor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/11/17
 * Time: 11:35 AM
 */

@Component
public class HandlersPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (AbstractServicesInterceptor.class.isAssignableFrom(bean.getClass())) {
            if (bean.getClass().isAnnotationPresent(UrlPattern.class)) {
                UrlPattern annotation = bean.getClass().getAnnotation(UrlPattern.class);
                ((AbstractServicesInterceptor) bean).setPrefixPath(annotation.value());
            } else {
                throw new IllegalStateException("AbstractServicesInterceptor inherited class " + bean.getClass().getName() + " should annotate with UrlPattern annotation");
            }
        }
        return bean;
    }
}