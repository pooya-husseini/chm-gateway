package com.caspco.channel.configuration;

import io.swagger.models.Path;

import java.util.Map;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/16/17
 * Time: 11:49 AM
 */
public interface SwaggerOperationFilter {
    boolean filter(Map.Entry<String, Path> path);
}
