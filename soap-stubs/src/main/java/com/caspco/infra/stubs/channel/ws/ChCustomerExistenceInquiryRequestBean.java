
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCustomerExistenceInquiryRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chCustomerExistenceInquiryRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="customerNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="foreignerNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nationalCodeNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chCustomerExistenceInquiryRequestBean", propOrder = {
    "customerNo",
    "foreignerNo",
    "nationalCodeNo"
})
public class ChCustomerExistenceInquiryRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String customerNo;
    protected String foreignerNo;
    protected String nationalCodeNo;

    /**
     * Gets the value of the customerNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerNo() {
        return customerNo;
    }

    /**
     * Sets the value of the customerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerNo(String value) {
        this.customerNo = value;
    }

    /**
     * Gets the value of the foreignerNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignerNo() {
        return foreignerNo;
    }

    /**
     * Sets the value of the foreignerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignerNo(String value) {
        this.foreignerNo = value;
    }

    /**
     * Gets the value of the nationalCodeNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalCodeNo() {
        return nationalCodeNo;
    }

    /**
     * Sets the value of the nationalCodeNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalCodeNo(String value) {
        this.nationalCodeNo = value;
    }

}
