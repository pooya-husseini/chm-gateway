
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStatement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStatement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chStatementSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chStatementSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStatement", propOrder = {
    "chStatementSearchRequestBean"
})
public class GetStatement
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChStatementSearchRequestBean chStatementSearchRequestBean;

    /**
     * Gets the value of the chStatementSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChStatementSearchRequestBean }
     *     
     */
    public ChStatementSearchRequestBean getChStatementSearchRequestBean() {
        return chStatementSearchRequestBean;
    }

    /**
     * Sets the value of the chStatementSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChStatementSearchRequestBean }
     *     
     */
    public void setChStatementSearchRequestBean(ChStatementSearchRequestBean value) {
        this.chStatementSearchRequestBean = value;
    }

}
