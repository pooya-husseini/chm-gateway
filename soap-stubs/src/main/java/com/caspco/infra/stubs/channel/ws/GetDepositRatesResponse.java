
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDepositRatesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDepositRatesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDepositRateInquiryResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositRateInquiryResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDepositRatesResponse", propOrder = {
    "chDepositRateInquiryResponseBean"
})
public class GetDepositRatesResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDepositRateInquiryResponseBean chDepositRateInquiryResponseBean;

    /**
     * Gets the value of the chDepositRateInquiryResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositRateInquiryResponseBean }
     *     
     */
    public ChDepositRateInquiryResponseBean getChDepositRateInquiryResponseBean() {
        return chDepositRateInquiryResponseBean;
    }

    /**
     * Sets the value of the chDepositRateInquiryResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositRateInquiryResponseBean }
     *     
     */
    public void setChDepositRateInquiryResponseBean(ChDepositRateInquiryResponseBean value) {
        this.chDepositRateInquiryResponseBean = value;
    }

}
