
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardStatementInquiry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardStatementInquiry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardStatementRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardStatementRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardStatementInquiry", propOrder = {
    "chCardStatementRequestBean"
})
public class GetCardStatementInquiry
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardStatementRequestBean chCardStatementRequestBean;

    /**
     * Gets the value of the chCardStatementRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardStatementRequestBean }
     *     
     */
    public ChCardStatementRequestBean getChCardStatementRequestBean() {
        return chCardStatementRequestBean;
    }

    /**
     * Sets the value of the chCardStatementRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardStatementRequestBean }
     *     
     */
    public void setChCardStatementRequestBean(ChCardStatementRequestBean value) {
        this.chCardStatementRequestBean = value;
    }

}
