
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chChequeFieldOrder.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chChequeFieldOrder">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CHECK_NUMBER"/>
 *     &lt;enumeration value="CHECK_DATE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chChequeFieldOrder")
@XmlEnum
public enum ChChequeFieldOrder {

    CHECK_NUMBER,
    CHECK_DATE;

    public String value() {
        return name();
    }

    public static ChChequeFieldOrder fromValue(String v) {
        return valueOf(v);
    }

}
