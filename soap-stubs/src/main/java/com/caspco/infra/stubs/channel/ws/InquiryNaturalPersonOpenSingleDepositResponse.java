
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for inquiryNaturalPersonOpenSingleDepositResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="inquiryNaturalPersonOpenSingleDepositResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chInquiryNaturalPersonOpenSingleDepositResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chInquiryNaturalPersonOpenSingleDepositResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "inquiryNaturalPersonOpenSingleDepositResponse", propOrder = {
    "chInquiryNaturalPersonOpenSingleDepositResponseBean"
})
public class InquiryNaturalPersonOpenSingleDepositResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChInquiryNaturalPersonOpenSingleDepositResponseBean chInquiryNaturalPersonOpenSingleDepositResponseBean;

    /**
     * Gets the value of the chInquiryNaturalPersonOpenSingleDepositResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChInquiryNaturalPersonOpenSingleDepositResponseBean }
     *     
     */
    public ChInquiryNaturalPersonOpenSingleDepositResponseBean getChInquiryNaturalPersonOpenSingleDepositResponseBean() {
        return chInquiryNaturalPersonOpenSingleDepositResponseBean;
    }

    /**
     * Sets the value of the chInquiryNaturalPersonOpenSingleDepositResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChInquiryNaturalPersonOpenSingleDepositResponseBean }
     *     
     */
    public void setChInquiryNaturalPersonOpenSingleDepositResponseBean(ChInquiryNaturalPersonOpenSingleDepositResponseBean value) {
        this.chInquiryNaturalPersonOpenSingleDepositResponseBean = value;
    }

}
