/**
 * ChFundTransferRequestBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class FundTransferRequestDto  implements java.io.Serializable {

    private java.math.BigDecimal amount;

    private java.lang.String cvv2;

    private java.lang.String destination;

    @ApiModelProperty(value = "destinationType")
    private com.caspco.channel.dtos.DepositOrPan destinationType;

    private java.lang.String email;

    private java.lang.String expDate;

    private java.lang.String pan;

    private java.lang.String pin;

    @ApiModelProperty(value = "pinType")
    private com.caspco.channel.dtos.PinType pinType;

    private java.lang.String track2;


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCvv2() {
        return cvv2;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public DepositOrPan getDestinationType() {
        return destinationType;
    }

    public void setDestinationType(DepositOrPan destinationType) {
        this.destinationType = destinationType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public PinType getPinType() {
        return pinType;
    }

    public void setPinType(PinType pinType) {
        this.pinType = pinType;
    }

    public String getTrack2() {
        return track2;
    }

    public void setTrack2(String track2) {
        this.track2 = track2;
    }
}
