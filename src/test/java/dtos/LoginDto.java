package com.caspco.channel.configuration;

import com.caspco.channel.dtos.ChannelServiceType;
import com.caspco.channel.dtos.UserInfoRequestDto;
import io.swagger.annotations.ApiModel;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 8/15/17
 * Time: 2:15 PM
 */


@ApiModel
public class LoginDto {
    private UserInfoRequestDto requestBean;
    private ChannelServiceType channelServiceType;


    public UserInfoRequestDto getRequestBean() {
        return requestBean;
    }

    public void setRequestBean(UserInfoRequestDto requestBean) {
        this.requestBean = requestBean;
    }

    public ChannelServiceType getChannelServiceType() {
        return channelServiceType;
    }

    public void setChannelServiceType(ChannelServiceType channelServiceType) {
        this.channelServiceType = channelServiceType;
    }
}
