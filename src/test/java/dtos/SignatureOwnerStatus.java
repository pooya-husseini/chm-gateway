/**
 * ChSignatureOwnerStatus.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.HashMap;

public @io.swagger.annotations.ApiModel
enum SignatureOwnerStatus implements java.io.Serializable {
    DEPOSIT_OWNER,
    OWNER_OF_DEPOSIT_AND_SIGNATURE,
    SIGNATURE_OWNER,
    BROKER;

    @JsonCreator
    public static SignatureOwnerStatus fromStringOperator(HashMap<String, String> map) {
        if (map != null) {
            for (SignatureOwnerStatus operator : SignatureOwnerStatus.values()) {
                if (map.get("value").equals(operator.name())) {
                    return operator;
                }
            }
        }
        return null;
    }
}
