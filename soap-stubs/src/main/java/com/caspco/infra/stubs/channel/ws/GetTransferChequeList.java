
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getTransferChequeList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTransferChequeList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSearchTransferChequeRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSearchTransferChequeRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransferChequeList", propOrder = {
    "chSearchTransferChequeRequestBean"
})
public class GetTransferChequeList
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSearchTransferChequeRequestBean chSearchTransferChequeRequestBean;

    /**
     * Gets the value of the chSearchTransferChequeRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChSearchTransferChequeRequestBean }
     *     
     */
    public ChSearchTransferChequeRequestBean getChSearchTransferChequeRequestBean() {
        return chSearchTransferChequeRequestBean;
    }

    /**
     * Sets the value of the chSearchTransferChequeRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSearchTransferChequeRequestBean }
     *     
     */
    public void setChSearchTransferChequeRequestBean(ChSearchTransferChequeRequestBean value) {
        this.chSearchTransferChequeRequestBean = value;
    }

}
