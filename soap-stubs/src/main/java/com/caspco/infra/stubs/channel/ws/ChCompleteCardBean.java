
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chCompleteCardBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chCompleteCardBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardIssueCause" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardIssueCause" minOccurs="0"/>
 *         &lt;element name="cardStatusCause" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardStatusCause" minOccurs="0"/>
 *         &lt;element name="expireDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="pan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardStatus" minOccurs="0"/>
 *         &lt;element name="type" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chCompleteCardBean", propOrder = {
    "cardIssueCause",
    "cardStatusCause",
    "expireDate",
    "issueDate",
    "pan",
    "status",
    "type"
})
public class ChCompleteCardBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlSchemaType(name = "string")
    protected ChCardIssueCause cardIssueCause;
    @XmlSchemaType(name = "string")
    protected ChCardStatusCause cardStatusCause;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date expireDate;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date issueDate;
    protected String pan;
    @XmlSchemaType(name = "string")
    protected ChCardStatus status;
    @XmlSchemaType(name = "string")
    protected ChCardType type;

    /**
     * Gets the value of the cardIssueCause property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardIssueCause }
     *     
     */
    public ChCardIssueCause getCardIssueCause() {
        return cardIssueCause;
    }

    /**
     * Sets the value of the cardIssueCause property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardIssueCause }
     *     
     */
    public void setCardIssueCause(ChCardIssueCause value) {
        this.cardIssueCause = value;
    }

    /**
     * Gets the value of the cardStatusCause property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardStatusCause }
     *     
     */
    public ChCardStatusCause getCardStatusCause() {
        return cardStatusCause;
    }

    /**
     * Sets the value of the cardStatusCause property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardStatusCause }
     *     
     */
    public void setCardStatusCause(ChCardStatusCause value) {
        this.cardStatusCause = value;
    }

    /**
     * Gets the value of the expireDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getExpireDate() {
        return expireDate;
    }

    /**
     * Sets the value of the expireDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpireDate(Date value) {
        this.expireDate = value;
    }

    /**
     * Gets the value of the issueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * Sets the value of the issueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssueDate(Date value) {
        this.issueDate = value;
    }

    /**
     * Gets the value of the pan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPan() {
        return pan;
    }

    /**
     * Sets the value of the pan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPan(String value) {
        this.pan = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardStatus }
     *     
     */
    public ChCardStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardStatus }
     *     
     */
    public void setStatus(ChCardStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardType }
     *     
     */
    public ChCardType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardType }
     *     
     */
    public void setType(ChCardType value) {
        this.type = value;
    }

}
