
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchThirdParty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchThirdParty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chThirdPartySearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chThirdPartySearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchThirdParty", propOrder = {
    "chThirdPartySearchRequestBean"
})
public class SearchThirdParty
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChThirdPartySearchRequestBean chThirdPartySearchRequestBean;

    /**
     * Gets the value of the chThirdPartySearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChThirdPartySearchRequestBean }
     *     
     */
    public ChThirdPartySearchRequestBean getChThirdPartySearchRequestBean() {
        return chThirdPartySearchRequestBean;
    }

    /**
     * Sets the value of the chThirdPartySearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChThirdPartySearchRequestBean }
     *     
     */
    public void setChThirdPartySearchRequestBean(ChThirdPartySearchRequestBean value) {
        this.chThirdPartySearchRequestBean = value;
    }

}
