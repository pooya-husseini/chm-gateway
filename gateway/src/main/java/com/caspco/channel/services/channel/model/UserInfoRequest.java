
package com.caspco.channel.services.channel.model;

import java.io.Serializable;


public class UserInfoRequest implements Serializable {

    private String password;
    private String username;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

