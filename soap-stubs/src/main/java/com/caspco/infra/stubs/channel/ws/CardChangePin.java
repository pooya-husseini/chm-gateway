
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardChangePin complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardChangePin">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardChangePinRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardChangePinRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardChangePin", propOrder = {
    "chCardChangePinRequestBean"
})
public class CardChangePin
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardChangePinRequestBean chCardChangePinRequestBean;

    /**
     * Gets the value of the chCardChangePinRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardChangePinRequestBean }
     *     
     */
    public ChCardChangePinRequestBean getChCardChangePinRequestBean() {
        return chCardChangePinRequestBean;
    }

    /**
     * Sets the value of the chCardChangePinRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardChangePinRequestBean }
     *     
     */
    public void setChCardChangePinRequestBean(ChCardChangePinRequestBean value) {
        this.chCardChangePinRequestBean = value;
    }

}
