/**
 * ChDepositResponseBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import java.util.List;

public class DepositResponseDto  implements java.io.Serializable {

    private List<DepositBeanDto> depositBeans;

    private java.lang.Long totalRecord;

    public List<DepositBeanDto> getDepositBeans() {
        return depositBeans;
    }

    public void setDepositBeans(List<DepositBeanDto> depositBeans) {
        this.depositBeans = depositBeans;
    }

    public Long getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(Long totalRecord) {
        this.totalRecord = totalRecord;
    }
}
