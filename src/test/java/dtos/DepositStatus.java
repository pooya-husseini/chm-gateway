/**
 * ChDepositStatus.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.HashMap;

public @io.swagger.annotations.ApiModel
enum DepositStatus implements java.io.Serializable {
    CLOSE,
    OPEN,
    NEGATIVE_BLOCK,
    POSITIVE_BLOCK,
    BLOCK,
    RESTING_AND_NEGATIVE_BLOCK,
    RESTING_AND_POSITIVE_BLOCK,
    RESTING_AND_BLOCK,
    RESTING,
    OLD,
    OPENING;


    @JsonCreator
    public static DepositStatus fromStringOperator(HashMap<String, String> map) {
        if (map != null) {
            for (DepositStatus operator : DepositStatus.values()) {
                if (map.get("value").equals(operator.name())) {
                    return operator;
                }
            }
        }
        return null;
    }
}
