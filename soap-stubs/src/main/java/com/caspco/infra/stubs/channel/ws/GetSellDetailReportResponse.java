
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getSellDetailReportResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getSellDetailReportResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSellDetailReportResponse" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSellDetailReportResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSellDetailReportResponse", propOrder = {
    "chSellDetailReportResponse"
})
public class GetSellDetailReportResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSellDetailReportResponse chSellDetailReportResponse;

    /**
     * Gets the value of the chSellDetailReportResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ChSellDetailReportResponse }
     *     
     */
    public ChSellDetailReportResponse getChSellDetailReportResponse() {
        return chSellDetailReportResponse;
    }

    /**
     * Sets the value of the chSellDetailReportResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSellDetailReportResponse }
     *     
     */
    public void setChSellDetailReportResponse(ChSellDetailReportResponse value) {
        this.chSellDetailReportResponse = value;
    }

}
