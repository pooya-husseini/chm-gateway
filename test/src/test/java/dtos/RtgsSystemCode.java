/**
 * ChRtgsSystemCode.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public  enum RtgsSystemCode implements java.io.Serializable {
    TAVASOT_KARBAR_SHOBE,
    TAVASOT_SHOBE_TAEED_KONANDE,
    TAVASOT_KARBAR_SHOBE_SANTA,
    TAVASOT_BANKDARI_MODERN,
    TAVASOT_FERESTANDE_GIRANDE_SATNA;


}
