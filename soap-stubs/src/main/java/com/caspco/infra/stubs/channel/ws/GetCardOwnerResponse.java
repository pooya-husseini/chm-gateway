
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardOwnerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardOwnerResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardOwnerResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardOwnerResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardOwnerResponse", propOrder = {
    "chCardOwnerResponseBean"
})
public class GetCardOwnerResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardOwnerResponseBean chCardOwnerResponseBean;

    /**
     * Gets the value of the chCardOwnerResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardOwnerResponseBean }
     *     
     */
    public ChCardOwnerResponseBean getChCardOwnerResponseBean() {
        return chCardOwnerResponseBean;
    }

    /**
     * Sets the value of the chCardOwnerResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardOwnerResponseBean }
     *     
     */
    public void setChCardOwnerResponseBean(ChCardOwnerResponseBean value) {
        this.chCardOwnerResponseBean = value;
    }

}
