
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for achBatchTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="achBatchTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBatchAchTransferRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBatchAchTransferRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "achBatchTransfer", propOrder = {
    "chBatchAchTransferRequestBean"
})
public class AchBatchTransfer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBatchAchTransferRequestBean chBatchAchTransferRequestBean;

    /**
     * Gets the value of the chBatchAchTransferRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBatchAchTransferRequestBean }
     *     
     */
    public ChBatchAchTransferRequestBean getChBatchAchTransferRequestBean() {
        return chBatchAchTransferRequestBean;
    }

    /**
     * Sets the value of the chBatchAchTransferRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBatchAchTransferRequestBean }
     *     
     */
    public void setChBatchAchTransferRequestBean(ChBatchAchTransferRequestBean value) {
        this.chBatchAchTransferRequestBean = value;
    }

}
