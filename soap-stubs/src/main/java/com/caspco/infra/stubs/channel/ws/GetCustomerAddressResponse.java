
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCustomerAddressResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCustomerAddressResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCustomerAddressResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCustomerAddressResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCustomerAddressResponse", propOrder = {
    "chCustomerAddressResponseBean"
})
public class GetCustomerAddressResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCustomerAddressResponseBean chCustomerAddressResponseBean;

    /**
     * Gets the value of the chCustomerAddressResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCustomerAddressResponseBean }
     *     
     */
    public ChCustomerAddressResponseBean getChCustomerAddressResponseBean() {
        return chCustomerAddressResponseBean;
    }

    /**
     * Sets the value of the chCustomerAddressResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCustomerAddressResponseBean }
     *     
     */
    public void setChCustomerAddressResponseBean(ChCustomerAddressResponseBean value) {
        this.chCustomerAddressResponseBean = value;
    }

}
