
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chPaymentServiceInputStyleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chPaymentServiceInputStyleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MANUAL"/>
 *     &lt;enumeration value="BARCODE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chPaymentServiceInputStyleType")
@XmlEnum
public enum ChPaymentServiceInputStyleType {

    MANUAL,
    BARCODE;

    public String value() {
        return name();
    }

    public static ChPaymentServiceInputStyleType fromValue(String v) {
        return valueOf(v);
    }

}
