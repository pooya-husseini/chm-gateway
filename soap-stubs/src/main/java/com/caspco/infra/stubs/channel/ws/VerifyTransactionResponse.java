
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for verifyTransactionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="verifyTransactionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chVerifyTransactionResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chVerifyTransactionResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "verifyTransactionResponse", propOrder = {
    "chVerifyTransactionResponseBean"
})
public class VerifyTransactionResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChVerifyTransactionResponseBean chVerifyTransactionResponseBean;

    /**
     * Gets the value of the chVerifyTransactionResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChVerifyTransactionResponseBean }
     *     
     */
    public ChVerifyTransactionResponseBean getChVerifyTransactionResponseBean() {
        return chVerifyTransactionResponseBean;
    }

    /**
     * Sets the value of the chVerifyTransactionResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChVerifyTransactionResponseBean }
     *     
     */
    public void setChVerifyTransactionResponseBean(ChVerifyTransactionResponseBean value) {
        this.chVerifyTransactionResponseBean = value;
    }

}
