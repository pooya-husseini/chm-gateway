
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBranches complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBranches">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBranchSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBranchSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBranches", propOrder = {
    "chBranchSearchRequestBean"
})
public class GetBranches
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBranchSearchRequestBean chBranchSearchRequestBean;

    /**
     * Gets the value of the chBranchSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBranchSearchRequestBean }
     *     
     */
    public ChBranchSearchRequestBean getChBranchSearchRequestBean() {
        return chBranchSearchRequestBean;
    }

    /**
     * Sets the value of the chBranchSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBranchSearchRequestBean }
     *     
     */
    public void setChBranchSearchRequestBean(ChBranchSearchRequestBean value) {
        this.chBranchSearchRequestBean = value;
    }

}
