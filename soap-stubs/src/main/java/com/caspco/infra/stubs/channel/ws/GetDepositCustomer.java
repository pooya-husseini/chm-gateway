
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDepositCustomer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDepositCustomer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDepositCustomerRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositCustomerRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDepositCustomer", propOrder = {
    "chDepositCustomerRequestBean"
})
public class GetDepositCustomer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDepositCustomerRequestBean chDepositCustomerRequestBean;

    /**
     * Gets the value of the chDepositCustomerRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositCustomerRequestBean }
     *     
     */
    public ChDepositCustomerRequestBean getChDepositCustomerRequestBean() {
        return chDepositCustomerRequestBean;
    }

    /**
     * Sets the value of the chDepositCustomerRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositCustomerRequestBean }
     *     
     */
    public void setChDepositCustomerRequestBean(ChDepositCustomerRequestBean value) {
        this.chDepositCustomerRequestBean = value;
    }

}
