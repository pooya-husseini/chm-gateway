
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getTransferChequeListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTransferChequeListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chTransferChequesResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chTransferChequesResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransferChequeListResponse", propOrder = {
    "chTransferChequesResponseBean"
})
public class GetTransferChequeListResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChTransferChequesResponseBean chTransferChequesResponseBean;

    /**
     * Gets the value of the chTransferChequesResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChTransferChequesResponseBean }
     *     
     */
    public ChTransferChequesResponseBean getChTransferChequesResponseBean() {
        return chTransferChequesResponseBean;
    }

    /**
     * Sets the value of the chTransferChequesResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChTransferChequesResponseBean }
     *     
     */
    public void setChTransferChequesResponseBean(ChTransferChequesResponseBean value) {
        this.chTransferChequesResponseBean = value;
    }

}
