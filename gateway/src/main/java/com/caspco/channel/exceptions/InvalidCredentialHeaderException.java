package com.caspco.channel.exceptions;

import java.util.Map;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/22/17
 * Time: 3:58 PM
 */
public class InvalidCredentialHeaderException extends OpenApiException {

    public InvalidCredentialHeaderException(String message, Map<String,?> args) {
        super(message, args);
    }

    public InvalidCredentialHeaderException(String message) {
        super(message);
    }

    public InvalidCredentialHeaderException(ExceptionDescription description) {
        super(description);
    }

    public InvalidCredentialHeaderException(String message, ExceptionDescription description) {
        super(message, description);
    }

    public InvalidCredentialHeaderException(String message, Throwable cause, ExceptionDescription description) {
        super(message, cause, description);
    }

    public InvalidCredentialHeaderException(Throwable cause, ExceptionDescription description) {
        super(cause, description);
    }

    public InvalidCredentialHeaderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ExceptionDescription description) {
        super(message, cause, enableSuppression, writableStackTrace, description);
    }
}
