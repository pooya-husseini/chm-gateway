/**
 * ChCardStatementRequestBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public class CardStatementRequestDto  implements java.io.Serializable {

    private CardAuthorizeParamsDto cardAuthorizeParams;

    private String depositNumber;

    private String pan;

    public CardAuthorizeParamsDto getCardAuthorizeParams() {
        return cardAuthorizeParams;
    }

    public void setCardAuthorizeParams(CardAuthorizeParamsDto cardAuthorizeParams) {
        this.cardAuthorizeParams = cardAuthorizeParams;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }
}
