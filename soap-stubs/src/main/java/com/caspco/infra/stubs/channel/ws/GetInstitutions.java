
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getInstitutions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getInstitutions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chInstitutionRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chInstitutionRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInstitutions", propOrder = {
    "chInstitutionRequestBean"
})
public class GetInstitutions
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChInstitutionRequestBean chInstitutionRequestBean;

    /**
     * Gets the value of the chInstitutionRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChInstitutionRequestBean }
     *     
     */
    public ChInstitutionRequestBean getChInstitutionRequestBean() {
        return chInstitutionRequestBean;
    }

    /**
     * Sets the value of the chInstitutionRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChInstitutionRequestBean }
     *     
     */
    public void setChInstitutionRequestBean(ChInstitutionRequestBean value) {
        this.chInstitutionRequestBean = value;
    }

}
