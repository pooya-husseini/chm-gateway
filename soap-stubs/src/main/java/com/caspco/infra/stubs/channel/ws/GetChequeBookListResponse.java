
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getChequeBookListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getChequeBookListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chChequeBooksResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chChequeBooksResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getChequeBookListResponse", propOrder = {
    "chChequeBooksResponseBean"
})
public class GetChequeBookListResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChChequeBooksResponseBean chChequeBooksResponseBean;

    /**
     * Gets the value of the chChequeBooksResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChChequeBooksResponseBean }
     *     
     */
    public ChChequeBooksResponseBean getChChequeBooksResponseBean() {
        return chChequeBooksResponseBean;
    }

    /**
     * Sets the value of the chChequeBooksResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChChequeBooksResponseBean }
     *     
     */
    public void setChChequeBooksResponseBean(ChChequeBooksResponseBean value) {
        this.chChequeBooksResponseBean = value;
    }

}
