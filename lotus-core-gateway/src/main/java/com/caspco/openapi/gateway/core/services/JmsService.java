package com.caspco.openapi.gateway.core.services;

/*
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 5/22/17
 * Time: 10:39 AM
 */

import com.caspco.openapi.gateway.core.exceptions.CoreException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.jndi.JndiTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.jms.*;
import javax.naming.NamingException;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.IllegalStateException;
import java.util.Random;
import java.util.zip.GZIPInputStream;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/20/17
 *         Time: 3:04 PM
 */

@Service
public class JmsService {

    @Autowired
    private JndiTemplate jndiTemplate;
    @Autowired
    private ObjectMapper objectMapper;
    private ConnectionFactory connectionFactory;
    private Destination requestQueue;
    private Destination responseQueue;
    private JmsTemplate template;

    @PostConstruct
    public void init() throws NamingException {
        JndiObjectFactoryBean factoryBean = new JndiObjectFactoryBean();
        factoryBean.setJndiTemplate(jndiTemplate);
        factoryBean.setJndiName("com.casp.jms.mainConnectionFactory");
        factoryBean.setExpectedType(javax.jms.ConnectionFactory.class);
        factoryBean.afterPropertiesSet();
        connectionFactory = (ConnectionFactory) factoryBean.getObject();


        JndiObjectFactoryBean bean1 = new JndiObjectFactoryBean();
        bean1.setJndiTemplate(jndiTemplate);
        bean1.setJndiName("com.casp.jms.queue.LotusQueueRequest");
        bean1.setExpectedType(javax.jms.Destination.class);
        bean1.afterPropertiesSet();
        requestQueue = (Destination) bean1.getObject();

        JndiObjectFactoryBean bean2 = new JndiObjectFactoryBean();
        bean2.setJndiTemplate(jndiTemplate);
        bean2.setJndiName("com.casp.jms.queue.LotusQueueResponse");
        bean2.setExpectedType(javax.jms.Destination.class);
        bean2.afterPropertiesSet();
        responseQueue = (Destination) bean2.getObject();

        template = new JmsTemplate();

        template.setConnectionFactory(connectionFactory);
        template.setDefaultDestination(requestQueue);
        template.setReceiveTimeout(30000);
        template.setSessionTransacted(true);
        template.setSessionAcknowledgeMode(1);
        template.afterPropertiesSet();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String send(String serviceId, RequestType requestType, Object input) throws JMSException, IOException, NamingException {

        final String RANDOM_STRING = createRandomString();
        Random random = new Random(System.currentTimeMillis());
        long randomLong = random.nextLong();

        template.send(requestQueue, session -> {
            BytesMessage message = session.createBytesMessage();
            message.setJMSCorrelationID(RANDOM_STRING);
            message.setStringProperty("channel", "BRANCH");
            message.setStringProperty("clientVersion", "1.3.10.0");
            message.setStringProperty("gatewayVersion", "1.3.10.0");
            message.setStringProperty("filter", "ServerNode-PRELIVE");
            message.setStringProperty("serviceId", serviceId);
            message.setStringProperty("payloadSchema", "object/json");
            message.setStringProperty("messageType", MessageType.REQUEST.name());
            message.setStringProperty("requestType", requestType.name());
            message.setStringProperty("securityAliasName", "lotus-host");
            message.setStringProperty("transactionType", "INPUT");
            message.setStringProperty("userCredentials", "javide@5010:IRR");
            message.setStringProperty("transactionId", Long.toHexString(randomLong));

            try {
//                message.writeBytes(("{\"customerDTO\":{" +
//                        "\"id\": \"18\"}," +
//                        "\"start\": \"1\"," +
//                        "\"end\": \"100\"" +
//                        "}").getBytes("utf-8"));
//
                message.writeBytes(objectMapper.writeValueAsString(input).getBytes("utf-8"));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            return message;
        });
        return RANDOM_STRING;

    }

    private static String createRandomString() {
        return Long.toHexString(new Random(System.currentTimeMillis()).nextLong());
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public <T> T receive(String correlationId, Class<T> resultClass) throws JMSException, IOException {
        Message receive = template.receiveSelected(responseQueue, String.format("JMSCorrelationID='%s'", correlationId));
        if (receive instanceof BytesMessage) {
            String responseType = receive.getStringProperty("responseType");
            if ("FAILED".equals(responseType)) {
                String s = extractMessageBody(receive);
                throw new CoreException(s);
            } else {
                String s = extractMessageBody(receive);
                return objectMapper.readValue(s, resultClass);
            }
        }
        return null;
    }

    private String extractMessageBody(Message receive) throws JMSException, IOException {
        if (receive instanceof BytesMessage) {
            BytesMessage bytesMessage = (BytesMessage) receive;
            byte[] buffer = new byte[(int) bytesMessage.getBodyLength()];
            bytesMessage.readBytes(buffer);
            if (receive.getBooleanProperty("compressed")) {
                ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
                final GZIPInputStream gis = new GZIPInputStream(bais);
                BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));

                StringBuilder outStr = new StringBuilder();
                String line;
                while ((line = bf.readLine()) != null) {
                    outStr.append(line);
                }
                bais.close();
                gis.close();
                bf.close();
                return outStr.toString();
            }
            return new String(buffer, "utf-8");
        } else {
            throw new IllegalStateException("Text message not supported");
        }
    }
}
