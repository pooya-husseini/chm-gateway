
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for payLoanResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="payLoanResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chLoanPaymentResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chLoanPaymentResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "payLoanResponse", propOrder = {
    "chLoanPaymentResponseBean"
})
public class PayLoanResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChLoanPaymentResponseBean chLoanPaymentResponseBean;

    /**
     * Gets the value of the chLoanPaymentResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChLoanPaymentResponseBean }
     *     
     */
    public ChLoanPaymentResponseBean getChLoanPaymentResponseBean() {
        return chLoanPaymentResponseBean;
    }

    /**
     * Sets the value of the chLoanPaymentResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChLoanPaymentResponseBean }
     *     
     */
    public void setChLoanPaymentResponseBean(ChLoanPaymentResponseBean value) {
        this.chLoanPaymentResponseBean = value;
    }

}
