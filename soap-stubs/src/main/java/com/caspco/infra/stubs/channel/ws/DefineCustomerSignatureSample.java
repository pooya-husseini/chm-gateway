
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for defineCustomerSignatureSample complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="defineCustomerSignatureSample">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDefineCustomerSignatureSampleRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDefineCustomerSignatureSampleRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "defineCustomerSignatureSample", propOrder = {
    "chDefineCustomerSignatureSampleRequestBean"
})
public class DefineCustomerSignatureSample
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDefineCustomerSignatureSampleRequestBean chDefineCustomerSignatureSampleRequestBean;

    /**
     * Gets the value of the chDefineCustomerSignatureSampleRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDefineCustomerSignatureSampleRequestBean }
     *     
     */
    public ChDefineCustomerSignatureSampleRequestBean getChDefineCustomerSignatureSampleRequestBean() {
        return chDefineCustomerSignatureSampleRequestBean;
    }

    /**
     * Sets the value of the chDefineCustomerSignatureSampleRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDefineCustomerSignatureSampleRequestBean }
     *     
     */
    public void setChDefineCustomerSignatureSampleRequestBean(ChDefineCustomerSignatureSampleRequestBean value) {
        this.chDefineCustomerSignatureSampleRequestBean = value;
    }

}
