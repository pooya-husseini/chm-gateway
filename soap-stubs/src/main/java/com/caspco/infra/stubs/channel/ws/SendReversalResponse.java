
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sendReversalResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sendReversalResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSendReversalResponse" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSendReversalResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendReversalResponse", propOrder = {
    "chSendReversalResponse"
})
public class SendReversalResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSendReversalResponse chSendReversalResponse;

    /**
     * Gets the value of the chSendReversalResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ChSendReversalResponse }
     *     
     */
    public ChSendReversalResponse getChSendReversalResponse() {
        return chSendReversalResponse;
    }

    /**
     * Sets the value of the chSendReversalResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSendReversalResponse }
     *     
     */
    public void setChSendReversalResponse(ChSendReversalResponse value) {
        this.chSendReversalResponse = value;
    }

}
