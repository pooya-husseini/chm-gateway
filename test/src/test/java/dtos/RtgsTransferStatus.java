/**
 * ChRtgsTransferStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public  enum RtgsTransferStatus implements java.io.Serializable {
    SABT_SHODE,
    TAEED_SHOBE_SHODE,
    ADAM_TAEED_SHOBE,
    HAZF_SHODE,
    TAEED_SHOBE_SATNA,
    ADAM_TAEED_SHOBE_SATNA,
    ERSAL_SHODE,
    TASFIYEH_SHODE,
    TASFIYEH_NASHODE;


}
