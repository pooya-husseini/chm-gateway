
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for blockDepositAmount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="blockDepositAmount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCoreBlockDepositAmountRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "blockDepositAmount", propOrder = {
    "arg0"
})
public class BlockDepositAmount
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCoreBlockDepositAmountRequestBean arg0;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link ChCoreBlockDepositAmountRequestBean }
     *     
     */
    public ChCoreBlockDepositAmountRequestBean getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCoreBlockDepositAmountRequestBean }
     *     
     */
    public void setArg0(ChCoreBlockDepositAmountRequestBean value) {
        this.arg0 = value;
    }

}
