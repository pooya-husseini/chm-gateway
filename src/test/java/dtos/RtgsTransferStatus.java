/**
 * ChRtgsTransferStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.HashMap;

public @io.swagger.annotations.ApiModel enum RtgsTransferStatus implements java.io.Serializable {
    SABT_SHODE,
    TAEED_SHOBE_SHODE,
    ADAM_TAEED_SHOBE,
    HAZF_SHODE,
    TAEED_SHOBE_SATNA,
    ADAM_TAEED_SHOBE_SATNA,
    ERSAL_SHODE,
    TASFIYEH_SHODE,
    TASFIYEH_NASHODE;

    @JsonCreator
    public static RtgsTransferStatus fromStringOperator(HashMap<String, String> map) {
        if (map != null) {
            for (RtgsTransferStatus operator : RtgsTransferStatus.values()) {
                if (map.get("value").equals(operator.name())) {
                    return operator;
                }
            }
        }
        return null;
    }

}
