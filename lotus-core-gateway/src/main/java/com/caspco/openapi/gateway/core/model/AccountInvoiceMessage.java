package com.caspco.openapi.gateway.core.model;

import com.caspian.moderngateway.core.coreservice.dto.ChOrderStatus;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/22/17
 *         Time: 4:08 PM
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountInvoiceMessage {

    private String depositNumber;
    private String description;
    private Date fromDate;
    private Long length;
    private Long offset;
    private ChOrderStatus order;
    private Date toDate;
    private BigDecimal fromAmount;
    private BigDecimal toAmount;
    private Date fromDateTime;
    private Date toDateTime;

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }
}
