
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBillInfoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBillInfoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBillInfoResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillInfoResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBillInfoResponse", propOrder = {
    "chBillInfoResponseBean"
})
public class GetBillInfoResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBillInfoResponseBean chBillInfoResponseBean;

    /**
     * Gets the value of the chBillInfoResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillInfoResponseBean }
     *     
     */
    public ChBillInfoResponseBean getChBillInfoResponseBean() {
        return chBillInfoResponseBean;
    }

    /**
     * Sets the value of the chBillInfoResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillInfoResponseBean }
     *     
     */
    public void setChBillInfoResponseBean(ChBillInfoResponseBean value) {
        this.chBillInfoResponseBean = value;
    }

}
