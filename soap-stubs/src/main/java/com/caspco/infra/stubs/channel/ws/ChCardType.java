
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCardType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chCardType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DEBIT"/>
 *     &lt;enumeration value="CREDIT"/>
 *     &lt;enumeration value="WALLET"/>
 *     &lt;enumeration value="PREPAID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chCardType")
@XmlEnum
public enum ChCardType {

    DEBIT,
    CREDIT,
    WALLET,
    PREPAID;

    public String value() {
        return name();
    }

    public static ChCardType fromValue(String v) {
        return valueOf(v);
    }

}
