
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chOrderDirection.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chOrderDirection">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DESC"/>
 *     &lt;enumeration value="ASC"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chOrderDirection")
@XmlEnum
public enum ChOrderDirection {

    DESC,
    ASC;

    public String value() {
        return name();
    }

    public static ChOrderDirection fromValue(String v) {
        return valueOf(v);
    }

}
