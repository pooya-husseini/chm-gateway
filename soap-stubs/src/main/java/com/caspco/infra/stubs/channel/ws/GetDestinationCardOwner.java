
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDestinationCardOwner complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDestinationCardOwner">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDestinationCardOwnerRequest" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDestinationCardOwnerRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDestinationCardOwner", propOrder = {
    "chDestinationCardOwnerRequest"
})
public class GetDestinationCardOwner
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDestinationCardOwnerRequest chDestinationCardOwnerRequest;

    /**
     * Gets the value of the chDestinationCardOwnerRequest property.
     * 
     * @return
     *     possible object is
     *     {@link ChDestinationCardOwnerRequest }
     *     
     */
    public ChDestinationCardOwnerRequest getChDestinationCardOwnerRequest() {
        return chDestinationCardOwnerRequest;
    }

    /**
     * Sets the value of the chDestinationCardOwnerRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDestinationCardOwnerRequest }
     *     
     */
    public void setChDestinationCardOwnerRequest(ChDestinationCardOwnerRequest value) {
        this.chDestinationCardOwnerRequest = value;
    }

}
