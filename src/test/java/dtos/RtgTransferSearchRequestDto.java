/**
 * ChRtgTransferSearchRequestBean.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import io.swagger.annotations.ApiModelProperty;

public class RtgTransferSearchRequestDto extends com.caspco.channel.dtos.SearchBeanDto implements java.io.Serializable {
    private java.lang.String cif;
    @ApiModelProperty(value = "rtgsTransferStatus")
    private com.caspco.channel.dtos.RtgsTransferStatus status;

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public RtgsTransferStatus getStatus() {
        return status;
    }

    public void setStatus(RtgsTransferStatus status) {
        this.status = status;
    }
}
