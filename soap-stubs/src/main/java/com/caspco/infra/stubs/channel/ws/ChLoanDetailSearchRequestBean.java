
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chLoanDetailSearchRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chLoanDetailSearchRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cbLoanNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hasDetail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="loanNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="offset" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="payStatus" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPayStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chLoanDetailSearchRequestBean", propOrder = {
    "cbLoanNumber",
    "hasDetail",
    "length",
    "loanNumber",
    "offset",
    "payStatus"
})
public class ChLoanDetailSearchRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String cbLoanNumber;
    protected boolean hasDetail;
    protected Long length;
    protected String loanNumber;
    protected Long offset;
    @XmlSchemaType(name = "string")
    protected ChPayStatus payStatus;

    /**
     * Gets the value of the cbLoanNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCbLoanNumber() {
        return cbLoanNumber;
    }

    /**
     * Sets the value of the cbLoanNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCbLoanNumber(String value) {
        this.cbLoanNumber = value;
    }

    /**
     * Gets the value of the hasDetail property.
     * 
     */
    public boolean isHasDetail() {
        return hasDetail;
    }

    /**
     * Sets the value of the hasDetail property.
     * 
     */
    public void setHasDetail(boolean value) {
        this.hasDetail = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLength(Long value) {
        this.length = value;
    }

    /**
     * Gets the value of the loanNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanNumber() {
        return loanNumber;
    }

    /**
     * Sets the value of the loanNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanNumber(String value) {
        this.loanNumber = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOffset(Long value) {
        this.offset = value;
    }

    /**
     * Gets the value of the payStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ChPayStatus }
     *     
     */
    public ChPayStatus getPayStatus() {
        return payStatus;
    }

    /**
     * Sets the value of the payStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPayStatus }
     *     
     */
    public void setPayStatus(ChPayStatus value) {
        this.payStatus = value;
    }

}
