
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for switchFundTransferFromPhysicalTerminalResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="switchFundTransferFromPhysicalTerminalResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardTransferResponse" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardTransferResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "switchFundTransferFromPhysicalTerminalResponse", propOrder = {
    "chCardTransferResponse"
})
public class SwitchFundTransferFromPhysicalTerminalResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardTransferResponse chCardTransferResponse;

    /**
     * Gets the value of the chCardTransferResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardTransferResponse }
     *     
     */
    public ChCardTransferResponse getChCardTransferResponse() {
        return chCardTransferResponse;
    }

    /**
     * Sets the value of the chCardTransferResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardTransferResponse }
     *     
     */
    public void setChCardTransferResponse(ChCardTransferResponse value) {
        this.chCardTransferResponse = value;
    }

}
