
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getLoanDetailResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getLoanDetailResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chLoanDetailResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chLoanDetailResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getLoanDetailResponse", propOrder = {
    "chLoanDetailResponseBean"
})
public class GetLoanDetailResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChLoanDetailResponseBean chLoanDetailResponseBean;

    /**
     * Gets the value of the chLoanDetailResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChLoanDetailResponseBean }
     *     
     */
    public ChLoanDetailResponseBean getChLoanDetailResponseBean() {
        return chLoanDetailResponseBean;
    }

    /**
     * Sets the value of the chLoanDetailResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChLoanDetailResponseBean }
     *     
     */
    public void setChLoanDetailResponseBean(ChLoanDetailResponseBean value) {
        this.chLoanDetailResponseBean = value;
    }

}
