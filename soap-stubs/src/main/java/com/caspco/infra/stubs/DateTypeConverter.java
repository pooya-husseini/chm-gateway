package com.caspco.infra.stubs;

import javax.xml.bind.DatatypeConverter;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/10/17
 * Time: 4:45 PM
 */
public class DateTypeConverter {
    public static Date unmarshal(String dateTime) {
        return DatatypeConverter.parseDate(dateTime).getTime();
    }

    public static String marshal(Date date) {
        final GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return DatatypeConverter.printDateTime(calendar);
    }

    public static String marshalDateTime(Date dateTime) {
        final GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(dateTime);
        return DatatypeConverter.printDateTime(calendar);
    }
}
