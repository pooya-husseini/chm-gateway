/**
 * ChFinancialServiceResponseBean.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import java.util.Date;

public class FinancialServiceResponseDto implements java.io.Serializable {

    private com.caspco.channel.dtos.CardDepositDto deposit;

    private java.lang.String switchResponseRRN;

    private java.util.Date transactionDate;

    private java.lang.String transactionNumber;

    public CardDepositDto getDeposit() {
        return deposit;
    }

    public void setDeposit(CardDepositDto deposit) {
        this.deposit = deposit;
    }

    public String getSwitchResponseRRN() {
        return switchResponseRRN;
    }

    public void setSwitchResponseRRN(String switchResponseRRN) {
        this.switchResponseRRN = switchResponseRRN;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }
}
