
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chChargeCardErrorType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chChargeCardErrorType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SERVICE_EXCEPTION"/>
 *     &lt;enumeration value="PERMISSION_EXCEPTION"/>
 *     &lt;enumeration value="INVALID_DEPOSIT_EXCEPTION"/>
 *     &lt;enumeration value="INSUFFICIENT_FUNDS_EXCEPTION"/>
 *     &lt;enumeration value="INVALID_CARD_EXCEPTION"/>
 *     &lt;enumeration value="INVALID_CARD_STATUS_EXCEPTION"/>
 *     &lt;enumeration value="AMOUNT_COSTRAINT_EXCEPTION"/>
 *     &lt;enumeration value="MULTI_CURRENCY_NOT_SUPPORTED_EXCEPTION"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chChargeCardErrorType")
@XmlEnum
public enum ChChargeCardErrorType {

    SERVICE_EXCEPTION,
    PERMISSION_EXCEPTION,
    INVALID_DEPOSIT_EXCEPTION,
    INSUFFICIENT_FUNDS_EXCEPTION,
    INVALID_CARD_EXCEPTION,
    INVALID_CARD_STATUS_EXCEPTION,
    AMOUNT_COSTRAINT_EXCEPTION,
    MULTI_CURRENCY_NOT_SUPPORTED_EXCEPTION;

    public String value() {
        return name();
    }

    public static ChChargeCardErrorType fromValue(String v) {
        return valueOf(v);
    }

}
