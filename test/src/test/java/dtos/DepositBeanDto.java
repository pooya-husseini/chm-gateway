/**
 * ChDepositBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

import java.math.BigDecimal;
import java.util.Date;

public class DepositBeanDto  implements java.io.Serializable {

    private BigDecimal availableBalance;

    private BigDecimal balance;

    private BigDecimal blockedAmount;

    private String branchCode;

    private String creditDeposit;

    private BigDecimal creditLoanRemainAmount;

    private Double creditRateAmount;

    private BigDecimal creditRemainAmount;

    private String currency;

    private String dayOfDepositInterest;

    private String depositNumber;

    private DepositStatus depositStatus;

    private String depositTitle;

    private Date expireDate;

    private BigDecimal extraAvailableBalance;

    private DepositGroupType group;

    private Date inaugurationDate;

    private String interestAccount;

    private Boolean lotusFlag;

    private BigDecimal maximumBalance;

    private BigDecimal minimumBalance;

    private DepositOwnerType owner;

    private String owners;

    private PersonalityType personality;

    private SignatureOwnerStatus signature;

    private String supportCurrency;

    private String supportDepositNumber;

    private DepositStatus supportDepositStatus;

    private SupportStatus supportStatus;

    private WithdrawalOption withdrawalOption;


    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBlockedAmount() {
        return blockedAmount;
    }

    public void setBlockedAmount(BigDecimal blockedAmount) {
        this.blockedAmount = blockedAmount;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getCreditDeposit() {
        return creditDeposit;
    }

    public void setCreditDeposit(String creditDeposit) {
        this.creditDeposit = creditDeposit;
    }

    public BigDecimal getCreditLoanRemainAmount() {
        return creditLoanRemainAmount;
    }

    public void setCreditLoanRemainAmount(BigDecimal creditLoanRemainAmount) {
        this.creditLoanRemainAmount = creditLoanRemainAmount;
    }

    public Double getCreditRateAmount() {
        return creditRateAmount;
    }

    public void setCreditRateAmount(Double creditRateAmount) {
        this.creditRateAmount = creditRateAmount;
    }

    public BigDecimal getCreditRemainAmount() {
        return creditRemainAmount;
    }

    public void setCreditRemainAmount(BigDecimal creditRemainAmount) {
        this.creditRemainAmount = creditRemainAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDayOfDepositInterest() {
        return dayOfDepositInterest;
    }

    public void setDayOfDepositInterest(String dayOfDepositInterest) {
        this.dayOfDepositInterest = dayOfDepositInterest;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public DepositStatus getDepositStatus() {
        return depositStatus;
    }

    public void setDepositStatus(DepositStatus depositStatus) {
        this.depositStatus = depositStatus;
    }

    public String getDepositTitle() {
        return depositTitle;
    }

    public void setDepositTitle(String depositTitle) {
        this.depositTitle = depositTitle;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public BigDecimal getExtraAvailableBalance() {
        return extraAvailableBalance;
    }

    public void setExtraAvailableBalance(BigDecimal extraAvailableBalance) {
        this.extraAvailableBalance = extraAvailableBalance;
    }

    public DepositGroupType getGroup() {
        return group;
    }

    public void setGroup(DepositGroupType group) {
        this.group = group;
    }

    public Date getInaugurationDate() {
        return inaugurationDate;
    }

    public void setInaugurationDate(Date inaugurationDate) {
        this.inaugurationDate = inaugurationDate;
    }

    public String getInterestAccount() {
        return interestAccount;
    }

    public void setInterestAccount(String interestAccount) {
        this.interestAccount = interestAccount;
    }

    public Boolean getLotusFlag() {
        return lotusFlag;
    }

    public void setLotusFlag(Boolean lotusFlag) {
        this.lotusFlag = lotusFlag;
    }

    public BigDecimal getMaximumBalance() {
        return maximumBalance;
    }

    public void setMaximumBalance(BigDecimal maximumBalance) {
        this.maximumBalance = maximumBalance;
    }

    public BigDecimal getMinimumBalance() {
        return minimumBalance;
    }

    public void setMinimumBalance(BigDecimal minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

    public DepositOwnerType getOwner() {
        return owner;
    }

    public void setOwner(DepositOwnerType owner) {
        this.owner = owner;
    }

    public String getOwners() {
        return owners;
    }

    public void setOwners(String owners) {
        this.owners = owners;
    }

    public PersonalityType getPersonality() {
        return personality;
    }

    public void setPersonality(PersonalityType personality) {
        this.personality = personality;
    }

    public SignatureOwnerStatus getSignature() {
        return signature;
    }

    public void setSignature(SignatureOwnerStatus signature) {
        this.signature = signature;
    }

    public String getSupportCurrency() {
        return supportCurrency;
    }

    public void setSupportCurrency(String supportCurrency) {
        this.supportCurrency = supportCurrency;
    }

    public String getSupportDepositNumber() {
        return supportDepositNumber;
    }

    public void setSupportDepositNumber(String supportDepositNumber) {
        this.supportDepositNumber = supportDepositNumber;
    }

    public DepositStatus getSupportDepositStatus() {
        return supportDepositStatus;
    }

    public void setSupportDepositStatus(DepositStatus supportDepositStatus) {
        this.supportDepositStatus = supportDepositStatus;
    }

    public SupportStatus getSupportStatus() {
        return supportStatus;
    }

    public void setSupportStatus(SupportStatus supportStatus) {
        this.supportStatus = supportStatus;
    }

    public WithdrawalOption getWithdrawalOption() {
        return withdrawalOption;
    }

    public void setWithdrawalOption(WithdrawalOption withdrawalOption) {
        this.withdrawalOption = withdrawalOption;
    }

    @Override
    public String toString() {
        return "DepositBeanDto{" +
                "availableBalance=" + availableBalance +
                ", balance=" + balance +
                ", blockedAmount=" + blockedAmount +
                ", branchCode='" + branchCode + '\'' +
                ", creditDeposit='" + creditDeposit + '\'' +
                ", creditLoanRemainAmount=" + creditLoanRemainAmount +
                ", creditRateAmount=" + creditRateAmount +
                ", creditRemainAmount=" + creditRemainAmount +
                ", currency='" + currency + '\'' +
                ", dayOfDepositInterest='" + dayOfDepositInterest + '\'' +
                ", depositNumber='" + depositNumber + '\'' +
                ", depositStatus=" + depositStatus +
                ", depositTitle='" + depositTitle + '\'' +
                ", expireDate=" + expireDate +
                ", extraAvailableBalance=" + extraAvailableBalance +
                ", group=" + group +
                ", inaugurationDate=" + inaugurationDate +
                ", interestAccount='" + interestAccount + '\'' +
                ", lotusFlag=" + lotusFlag +
                ", maximumBalance=" + maximumBalance +
                ", minimumBalance=" + minimumBalance +
                ", owner=" + owner +
                ", owners='" + owners + '\'' +
                ", personality=" + personality +
                ", signature=" + signature +
                ", supportCurrency='" + supportCurrency + '\'' +
                ", supportDepositNumber='" + supportDepositNumber + '\'' +
                ", supportDepositStatus=" + supportDepositStatus +
                ", supportStatus=" + supportStatus +
                ", withdrawalOption=" + withdrawalOption +
                '}';
    }
}
