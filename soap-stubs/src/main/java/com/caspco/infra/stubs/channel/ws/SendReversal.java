
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sendReversal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sendReversal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSendReversalRequest" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSendReversalRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendReversal", propOrder = {
    "chSendReversalRequest"
})
public class SendReversal
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSendReversalRequest chSendReversalRequest;

    /**
     * Gets the value of the chSendReversalRequest property.
     * 
     * @return
     *     possible object is
     *     {@link ChSendReversalRequest }
     *     
     */
    public ChSendReversalRequest getChSendReversalRequest() {
        return chSendReversalRequest;
    }

    /**
     * Sets the value of the chSendReversalRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSendReversalRequest }
     *     
     */
    public void setChSendReversalRequest(ChSendReversalRequest value) {
        this.chSendReversalRequest = value;
    }

}
