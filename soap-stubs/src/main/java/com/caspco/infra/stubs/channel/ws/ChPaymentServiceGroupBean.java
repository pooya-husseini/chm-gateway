
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chPaymentServiceGroupBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chPaymentServiceGroupBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="childGroups" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPaymentServiceGroupBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="foreignTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentServices" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPaymentServiceBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chPaymentServiceGroupBean", propOrder = {
    "childGroups",
    "foreignTitle",
    "id",
    "paymentServices",
    "title"
})
public class ChPaymentServiceGroupBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(nillable = true)
    protected List<ChPaymentServiceGroupBean> childGroups;
    protected String foreignTitle;
    protected String id;
    @XmlElement(nillable = true)
    protected List<ChPaymentServiceBean> paymentServices;
    protected String title;

    /**
     * Gets the value of the childGroups property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the childGroups property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChildGroups().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChPaymentServiceGroupBean }
     * 
     * 
     */
    public List<ChPaymentServiceGroupBean> getChildGroups() {
        if (childGroups == null) {
            childGroups = new ArrayList<ChPaymentServiceGroupBean>();
        }
        return this.childGroups;
    }

    /**
     * Gets the value of the foreignTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignTitle() {
        return foreignTitle;
    }

    /**
     * Sets the value of the foreignTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignTitle(String value) {
        this.foreignTitle = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the paymentServices property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentServices property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentServices().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChPaymentServiceBean }
     * 
     * 
     */
    public List<ChPaymentServiceBean> getPaymentServices() {
        if (paymentServices == null) {
            paymentServices = new ArrayList<ChPaymentServiceBean>();
        }
        return this.paymentServices;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

}
