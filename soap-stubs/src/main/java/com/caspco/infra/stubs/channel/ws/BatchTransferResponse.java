
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for batchTransferResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="batchTransferResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBatchTransferResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBatchTransferResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "batchTransferResponse", propOrder = {
    "chBatchTransferResponseBean"
})
public class BatchTransferResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBatchTransferResponseBean chBatchTransferResponseBean;

    /**
     * Gets the value of the chBatchTransferResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBatchTransferResponseBean }
     *     
     */
    public ChBatchTransferResponseBean getChBatchTransferResponseBean() {
        return chBatchTransferResponseBean;
    }

    /**
     * Sets the value of the chBatchTransferResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBatchTransferResponseBean }
     *     
     */
    public void setChBatchTransferResponseBean(ChBatchTransferResponseBean value) {
        this.chBatchTransferResponseBean = value;
    }

}
