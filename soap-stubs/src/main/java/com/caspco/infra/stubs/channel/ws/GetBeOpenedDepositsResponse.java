
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBeOpenedDepositsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBeOpenedDepositsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBeOpenedDepositResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBeOpenedDepositResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBeOpenedDepositsResponse", propOrder = {
    "chBeOpenedDepositResponseBean"
})
public class GetBeOpenedDepositsResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBeOpenedDepositResponseBean chBeOpenedDepositResponseBean;

    /**
     * Gets the value of the chBeOpenedDepositResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBeOpenedDepositResponseBean }
     *     
     */
    public ChBeOpenedDepositResponseBean getChBeOpenedDepositResponseBean() {
        return chBeOpenedDepositResponseBean;
    }

    /**
     * Sets the value of the chBeOpenedDepositResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBeOpenedDepositResponseBean }
     *     
     */
    public void setChBeOpenedDepositResponseBean(ChBeOpenedDepositResponseBean value) {
        this.chBeOpenedDepositResponseBean = value;
    }

}
