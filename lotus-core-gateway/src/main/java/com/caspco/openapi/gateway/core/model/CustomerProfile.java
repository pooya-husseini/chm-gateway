package com.caspco.openapi.gateway.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/23/17
 *         Time: 9:40 AM
 */


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerProfile {
    private Character clientStatus;
    private Character clientType;
    private Integer branchCode;
    private Date deathDate;
    private String englishFirstName;
    private String englishLastName;
    private Long externalRef;
    private String firstName;
    private String lastName;
    private String mainCurrency;
    private String nickName;
    private Date startDate;
    private String swiftId;
    private String status;
    private Boolean isAmlEnable;
    private String description;
    private String nationality;
    private String nationalCode;
    private String shahabCode;
    private String oldNationalCode;
    private String foreignerNo;
    private Character creditType;
    private Character enable;
    private Date enableDate;
    private String tradeCardIssuancePlace;
    private Boolean isForeignerCustomer;
    private String defaultMobile;
    private String defaultEmail;
    private String defaultWebsite;


    public Character getClientStatus() {
        return clientStatus;
    }

    public void setClientStatus(Character clientStatus) {
        this.clientStatus = clientStatus;
    }

    public Character getClientType() {
        return clientType;
    }

    public void setClientType(Character clientType) {
        this.clientType = clientType;
    }

    public Integer getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(Integer branchCode) {
        this.branchCode = branchCode;
    }

    public Date getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(Date deathDate) {
        this.deathDate = deathDate;
    }

    public String getEnglishFirstName() {
        return englishFirstName;
    }

    public void setEnglishFirstName(String englishFirstName) {
        this.englishFirstName = englishFirstName;
    }

    public String getEnglishLastName() {
        return englishLastName;
    }

    public void setEnglishLastName(String englishLastName) {
        this.englishLastName = englishLastName;
    }

    public Long getExternalRef() {
        return externalRef;
    }

    public void setExternalRef(Long externalRef) {
        this.externalRef = externalRef;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMainCurrency() {
        return mainCurrency;
    }

    public void setMainCurrency(String mainCurrency) {
        this.mainCurrency = mainCurrency;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getSwiftId() {
        return swiftId;
    }

    public void setSwiftId(String swiftId) {
        this.swiftId = swiftId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getAmlEnable() {
        return isAmlEnable;
    }

    public void setAmlEnable(Boolean amlEnable) {
        isAmlEnable = amlEnable;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getShahabCode() {
        return shahabCode;
    }

    public void setShahabCode(String shahabCode) {
        this.shahabCode = shahabCode;
    }

    public String getOldNationalCode() {
        return oldNationalCode;
    }

    public void setOldNationalCode(String oldNationalCode) {
        this.oldNationalCode = oldNationalCode;
    }

    public String getForeignerNo() {
        return foreignerNo;
    }

    public void setForeignerNo(String foreignerNo) {
        this.foreignerNo = foreignerNo;
    }

    public Character getCreditType() {
        return creditType;
    }

    public void setCreditType(Character creditType) {
        this.creditType = creditType;
    }

    public Character getEnable() {
        return enable;
    }

    public void setEnable(Character enable) {
        this.enable = enable;
    }

    public Date getEnableDate() {
        return enableDate;
    }

    public void setEnableDate(Date enableDate) {
        this.enableDate = enableDate;
    }

    public String getTradeCardIssuancePlace() {
        return tradeCardIssuancePlace;
    }

    public void setTradeCardIssuancePlace(String tradeCardIssuancePlace) {
        this.tradeCardIssuancePlace = tradeCardIssuancePlace;
    }

    public Boolean getForeignerCustomer() {
        return isForeignerCustomer;
    }

    public void setForeignerCustomer(Boolean foreignerCustomer) {
        isForeignerCustomer = foreignerCustomer;
    }

    public String getDefaultMobile() {
        return defaultMobile;
    }

    public void setDefaultMobile(String defaultMobile) {
        this.defaultMobile = defaultMobile;
    }

    public String getDefaultEmail() {
        return defaultEmail;
    }

    public void setDefaultEmail(String defaultEmail) {
        this.defaultEmail = defaultEmail;
    }

    public String getDefaultWebsite() {
        return defaultWebsite;
    }

    public void setDefaultWebsite(String defaultWebsite) {
        this.defaultWebsite = defaultWebsite;
    }
}
