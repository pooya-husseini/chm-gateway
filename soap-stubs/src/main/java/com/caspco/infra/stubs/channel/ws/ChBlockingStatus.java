
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBlockingStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chBlockingStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SUCCESSFULlY"/>
 *     &lt;enumeration value="NOT_FOUND"/>
 *     &lt;enumeration value="INVALID_STATE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chBlockingStatus")
@XmlEnum
public enum ChBlockingStatus {

    @XmlEnumValue("SUCCESSFULlY")
    SUCCESSFU_LL_Y("SUCCESSFULlY"),
    NOT_FOUND("NOT_FOUND"),
    INVALID_STATE("INVALID_STATE");
    private final String value;

    ChBlockingStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ChBlockingStatus fromValue(String v) {
        for (ChBlockingStatus c: ChBlockingStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
