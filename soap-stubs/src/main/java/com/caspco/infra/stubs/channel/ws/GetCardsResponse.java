
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardsResponse", propOrder = {
    "chCardResponseBean"
})
public class GetCardsResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardResponseBean chCardResponseBean;

    /**
     * Gets the value of the chCardResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardResponseBean }
     *     
     */
    public ChCardResponseBean getChCardResponseBean() {
        return chCardResponseBean;
    }

    /**
     * Sets the value of the chCardResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardResponseBean }
     *     
     */
    public void setChCardResponseBean(ChCardResponseBean value) {
        this.chCardResponseBean = value;
    }

}
