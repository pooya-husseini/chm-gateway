
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getSellDetailReport complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getSellDetailReport">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSellDetailRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSellDetailRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSellDetailReport", propOrder = {
    "chSellDetailRequestBean"
})
public class GetSellDetailReport
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSellDetailRequestBean chSellDetailRequestBean;

    /**
     * Gets the value of the chSellDetailRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChSellDetailRequestBean }
     *     
     */
    public ChSellDetailRequestBean getChSellDetailRequestBean() {
        return chSellDetailRequestBean;
    }

    /**
     * Sets the value of the chSellDetailRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSellDetailRequestBean }
     *     
     */
    public void setChSellDetailRequestBean(ChSellDetailRequestBean value) {
        this.chSellDetailRequestBean = value;
    }

}
