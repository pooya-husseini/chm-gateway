package com.caspco.openapi.gateway.core.model;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/23/17
 *         Time: 4:29 PM
 */
public class ResponseObject {
    private final Object result;

    public ResponseObject(Object result) {
        this.result = result;
    }

    public Object getResult() {
        return result;
    }
}
