
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAutoTransferListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAutoTransferListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAutoTransferDetailsResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAutoTransferDetailsResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAutoTransferListResponse", propOrder = {
    "chAutoTransferDetailsResponseBean"
})
public class GetAutoTransferListResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAutoTransferDetailsResponseBean chAutoTransferDetailsResponseBean;

    /**
     * Gets the value of the chAutoTransferDetailsResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAutoTransferDetailsResponseBean }
     *     
     */
    public ChAutoTransferDetailsResponseBean getChAutoTransferDetailsResponseBean() {
        return chAutoTransferDetailsResponseBean;
    }

    /**
     * Sets the value of the chAutoTransferDetailsResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAutoTransferDetailsResponseBean }
     *     
     */
    public void setChAutoTransferDetailsResponseBean(ChAutoTransferDetailsResponseBean value) {
        this.chAutoTransferDetailsResponseBean = value;
    }

}
