
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCurrencyRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCurrencyRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCurrencyRateRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCurrencyRateRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCurrencyRate", propOrder = {
    "chCurrencyRateRequestBean"
})
public class GetCurrencyRate
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCurrencyRateRequestBean chCurrencyRateRequestBean;

    /**
     * Gets the value of the chCurrencyRateRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCurrencyRateRequestBean }
     *     
     */
    public ChCurrencyRateRequestBean getChCurrencyRateRequestBean() {
        return chCurrencyRateRequestBean;
    }

    /**
     * Sets the value of the chCurrencyRateRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCurrencyRateRequestBean }
     *     
     */
    public void setChCurrencyRateRequestBean(ChCurrencyRateRequestBean value) {
        this.chCurrencyRateRequestBean = value;
    }

}
