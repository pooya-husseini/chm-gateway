
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addCardDeposit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addCardDeposit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAddCardDepositRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAddCardDepositRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addCardDeposit", propOrder = {
    "chAddCardDepositRequestBean"
})
public class AddCardDeposit
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAddCardDepositRequestBean chAddCardDepositRequestBean;

    /**
     * Gets the value of the chAddCardDepositRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAddCardDepositRequestBean }
     *     
     */
    public ChAddCardDepositRequestBean getChAddCardDepositRequestBean() {
        return chAddCardDepositRequestBean;
    }

    /**
     * Sets the value of the chAddCardDepositRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAddCardDepositRequestBean }
     *     
     */
    public void setChAddCardDepositRequestBean(ChAddCardDepositRequestBean value) {
        this.chAddCardDepositRequestBean = value;
    }

}
