
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for normalTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="normalTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="normalTransferRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chNormalTransferRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "normalTransfer", propOrder = {
    "normalTransferRequestBean"
})
public class NormalTransfer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChNormalTransferRequestBean normalTransferRequestBean;

    /**
     * Gets the value of the normalTransferRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChNormalTransferRequestBean }
     *     
     */
    public ChNormalTransferRequestBean getNormalTransferRequestBean() {
        return normalTransferRequestBean;
    }

    /**
     * Sets the value of the normalTransferRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChNormalTransferRequestBean }
     *     
     */
    public void setNormalTransferRequestBean(ChNormalTransferRequestBean value) {
        this.normalTransferRequestBean = value;
    }

}
