
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for loginWithMobileNumber complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loginWithMobileNumber">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chMobileLoginRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chMobileLoginRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loginWithMobileNumber", propOrder = {
    "chMobileLoginRequestBean"
})
public class LoginWithMobileNumber
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChMobileLoginRequestBean chMobileLoginRequestBean;

    /**
     * Gets the value of the chMobileLoginRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChMobileLoginRequestBean }
     *     
     */
    public ChMobileLoginRequestBean getChMobileLoginRequestBean() {
        return chMobileLoginRequestBean;
    }

    /**
     * Sets the value of the chMobileLoginRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChMobileLoginRequestBean }
     *     
     */
    public void setChMobileLoginRequestBean(ChMobileLoginRequestBean value) {
        this.chMobileLoginRequestBean = value;
    }

}
