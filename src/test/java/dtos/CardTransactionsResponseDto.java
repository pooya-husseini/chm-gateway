/**
 * ChCardTransactionsResponseBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import java.util.List;

public class CardTransactionsResponseDto  implements java.io.Serializable {

    private java.lang.Long totalRecordCount;

    private List<CardTransactionResponseDto> transactions;

    public Long getTotalRecordCount() {
        return totalRecordCount;
    }

    public void setTotalRecordCount(Long totalRecordCount) {
        this.totalRecordCount = totalRecordCount;
    }

    public List<CardTransactionResponseDto> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<CardTransactionResponseDto> transactions) {
        this.transactions = transactions;
    }
}
