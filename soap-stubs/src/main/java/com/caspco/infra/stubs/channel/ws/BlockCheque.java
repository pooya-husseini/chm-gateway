
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for blockCheque complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="blockCheque">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBlockChequeRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBlockChequeRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "blockCheque", propOrder = {
    "chBlockChequeRequestBean"
})
public class BlockCheque
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBlockChequeRequestBean chBlockChequeRequestBean;

    /**
     * Gets the value of the chBlockChequeRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBlockChequeRequestBean }
     *     
     */
    public ChBlockChequeRequestBean getChBlockChequeRequestBean() {
        return chBlockChequeRequestBean;
    }

    /**
     * Sets the value of the chBlockChequeRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBlockChequeRequestBean }
     *     
     */
    public void setChBlockChequeRequestBean(ChBlockChequeRequestBean value) {
        this.chBlockChequeRequestBean = value;
    }

}
