
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chDefaultBillStatementType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chDefaultBillStatementType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FROM_LAST_VIEW_BILL_DATE"/>
 *     &lt;enumeration value="LAST_N_DAY"/>
 *     &lt;enumeration value="LAST_N_COUNT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chDefaultBillStatementType")
@XmlEnum
public enum ChDefaultBillStatementType {

    FROM_LAST_VIEW_BILL_DATE,
    LAST_N_DAY,
    LAST_N_COUNT;

    public String value() {
        return name();
    }

    public static ChDefaultBillStatementType fromValue(String v) {
        return valueOf(v);
    }

}
