
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chPaymentPreviewRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chPaymentPreviewRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inputParameters" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPaymentInputParameterBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="paymentServiceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requireVerification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="verificationExpirationTimeOut" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chPaymentPreviewRequestBean", propOrder = {
    "inputParameters",
    "paymentServiceId",
    "requireVerification",
    "verificationExpirationTimeOut"
})
public class ChPaymentPreviewRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(nillable = true)
    protected List<ChPaymentInputParameterBean> inputParameters;
    protected String paymentServiceId;
    protected Boolean requireVerification;
    protected Long verificationExpirationTimeOut;

    /**
     * Gets the value of the inputParameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inputParameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInputParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChPaymentInputParameterBean }
     * 
     * 
     */
    public List<ChPaymentInputParameterBean> getInputParameters() {
        if (inputParameters == null) {
            inputParameters = new ArrayList<ChPaymentInputParameterBean>();
        }
        return this.inputParameters;
    }

    /**
     * Gets the value of the paymentServiceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentServiceId() {
        return paymentServiceId;
    }

    /**
     * Sets the value of the paymentServiceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentServiceId(String value) {
        this.paymentServiceId = value;
    }

    /**
     * Gets the value of the requireVerification property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequireVerification() {
        return requireVerification;
    }

    /**
     * Sets the value of the requireVerification property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequireVerification(Boolean value) {
        this.requireVerification = value;
    }

    /**
     * Gets the value of the verificationExpirationTimeOut property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVerificationExpirationTimeOut() {
        return verificationExpirationTimeOut;
    }

    /**
     * Sets the value of the verificationExpirationTimeOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVerificationExpirationTimeOut(Long value) {
        this.verificationExpirationTimeOut = value;
    }

}
