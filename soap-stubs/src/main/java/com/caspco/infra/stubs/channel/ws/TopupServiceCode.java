
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for topupServiceCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="topupServiceCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NONE"/>
 *     &lt;enumeration value="MCI"/>
 *     &lt;enumeration value="RIGHTEL"/>
 *     &lt;enumeration value="IRANCELL_PREPAID_SIMCARD"/>
 *     &lt;enumeration value="IRANCELL_MAGICAL_PREPAID_SIMCARD"/>
 *     &lt;enumeration value="IRANCELL_POSTPAID_SIMCARD"/>
 *     &lt;enumeration value="IRANCELL_PREPAID_WIMAX"/>
 *     &lt;enumeration value="IRANCELL_POSTPAID_WIMAX"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "topupServiceCode")
@XmlEnum
public enum TopupServiceCode {

    NONE,
    MCI,
    RIGHTEL,
    IRANCELL_PREPAID_SIMCARD,
    IRANCELL_MAGICAL_PREPAID_SIMCARD,
    IRANCELL_POSTPAID_SIMCARD,
    IRANCELL_PREPAID_WIMAX,
    IRANCELL_POSTPAID_WIMAX;

    public String value() {
        return name();
    }

    public static TopupServiceCode fromValue(String v) {
        return valueOf(v);
    }

}
