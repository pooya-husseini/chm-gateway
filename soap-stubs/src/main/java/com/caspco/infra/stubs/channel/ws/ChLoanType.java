
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chLoanType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chLoanType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MOZAREBE"/>
 *     &lt;enumeration value="MOSHAREKATE_MADANI"/>
 *     &lt;enumeration value="MOSHAREKATE_HOGHOGHY"/>
 *     &lt;enumeration value="FOROOSHE_AGHSATY"/>
 *     &lt;enumeration value="EJARE_BE_SHARTE_TAMLIK"/>
 *     &lt;enumeration value="SALAF"/>
 *     &lt;enumeration value="JOALE"/>
 *     &lt;enumeration value="SARMAYEGOZARIE_MOSTAGHIM"/>
 *     &lt;enumeration value="GHARZOLHASANE"/>
 *     &lt;enumeration value="ZEMAN"/>
 *     &lt;enumeration value="KHARIDE_DEYN"/>
 *     &lt;enumeration value="MOZARE_EH"/>
 *     &lt;enumeration value="MOSAGHAT"/>
 *     &lt;enumeration value="KHADAMATE_ETEBARY"/>
 *     &lt;enumeration value="MOZAREBE_AAM"/>
 *     &lt;enumeration value="MOSHAREKAT_AAM"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chLoanType")
@XmlEnum
public enum ChLoanType {

    MOZAREBE,
    MOSHAREKATE_MADANI,
    MOSHAREKATE_HOGHOGHY,
    FOROOSHE_AGHSATY,
    EJARE_BE_SHARTE_TAMLIK,
    SALAF,
    JOALE,
    SARMAYEGOZARIE_MOSTAGHIM,
    GHARZOLHASANE,
    ZEMAN,
    KHARIDE_DEYN,
    MOZARE_EH,
    MOSAGHAT,
    KHADAMATE_ETEBARY,
    MOZAREBE_AAM,
    MOSHAREKAT_AAM;

    public String value() {
        return name();
    }

    public static ChLoanType fromValue(String v) {
        return valueOf(v);
    }

}
