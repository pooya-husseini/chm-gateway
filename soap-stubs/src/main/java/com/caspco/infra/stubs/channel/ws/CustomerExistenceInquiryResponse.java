
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for customerExistenceInquiryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="customerExistenceInquiryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCustomerInquiryResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCustomerExistenceInquiryResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "customerExistenceInquiryResponse", propOrder = {
    "chCustomerInquiryResponseBean"
})
public class CustomerExistenceInquiryResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCustomerExistenceInquiryResponseBean chCustomerInquiryResponseBean;

    /**
     * Gets the value of the chCustomerInquiryResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCustomerExistenceInquiryResponseBean }
     *     
     */
    public ChCustomerExistenceInquiryResponseBean getChCustomerInquiryResponseBean() {
        return chCustomerInquiryResponseBean;
    }

    /**
     * Sets the value of the chCustomerInquiryResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCustomerExistenceInquiryResponseBean }
     *     
     */
    public void setChCustomerInquiryResponseBean(ChCustomerExistenceInquiryResponseBean value) {
        this.chCustomerInquiryResponseBean = value;
    }

}
