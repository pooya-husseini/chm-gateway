
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for suspendAchTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="suspendAchTransaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAchTransactionKeysRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAchTransactionKeysRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "suspendAchTransaction", propOrder = {
    "chAchTransactionKeysRequestBean"
})
public class SuspendAchTransaction
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAchTransactionKeysRequestBean chAchTransactionKeysRequestBean;

    /**
     * Gets the value of the chAchTransactionKeysRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAchTransactionKeysRequestBean }
     *     
     */
    public ChAchTransactionKeysRequestBean getChAchTransactionKeysRequestBean() {
        return chAchTransactionKeysRequestBean;
    }

    /**
     * Sets the value of the chAchTransactionKeysRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAchTransactionKeysRequestBean }
     *     
     */
    public void setChAchTransactionKeysRequestBean(ChAchTransactionKeysRequestBean value) {
        this.chAchTransactionKeysRequestBean = value;
    }

}
