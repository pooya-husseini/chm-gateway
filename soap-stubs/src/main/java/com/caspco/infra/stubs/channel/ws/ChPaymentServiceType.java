
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chPaymentServiceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chPaymentServiceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Bill"/>
 *     &lt;enumeration value="Charity"/>
 *     &lt;enumeration value="CHARGE_SIM_CARD"/>
 *     &lt;enumeration value="SPECIAL_PAYMENT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chPaymentServiceType")
@XmlEnum
public enum ChPaymentServiceType {

    @XmlEnumValue("Bill")
    BILL("Bill"),
    @XmlEnumValue("Charity")
    CHARITY("Charity"),
    CHARGE_SIM_CARD("CHARGE_SIM_CARD"),
    SPECIAL_PAYMENT("SPECIAL_PAYMENT");
    private final String value;

    ChPaymentServiceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ChPaymentServiceType fromValue(String v) {
        for (ChPaymentServiceType c: ChPaymentServiceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
