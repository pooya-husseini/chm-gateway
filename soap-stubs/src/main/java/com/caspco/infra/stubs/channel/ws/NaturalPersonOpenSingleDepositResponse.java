
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for naturalPersonOpenSingleDepositResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="naturalPersonOpenSingleDepositResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chNaturalPersonOpenSingleDepositResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chNaturalPersonOpenSingleDepositResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "naturalPersonOpenSingleDepositResponse", propOrder = {
    "chNaturalPersonOpenSingleDepositResponseBean"
})
public class NaturalPersonOpenSingleDepositResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChNaturalPersonOpenSingleDepositResponseBean chNaturalPersonOpenSingleDepositResponseBean;

    /**
     * Gets the value of the chNaturalPersonOpenSingleDepositResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChNaturalPersonOpenSingleDepositResponseBean }
     *     
     */
    public ChNaturalPersonOpenSingleDepositResponseBean getChNaturalPersonOpenSingleDepositResponseBean() {
        return chNaturalPersonOpenSingleDepositResponseBean;
    }

    /**
     * Sets the value of the chNaturalPersonOpenSingleDepositResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChNaturalPersonOpenSingleDepositResponseBean }
     *     
     */
    public void setChNaturalPersonOpenSingleDepositResponseBean(ChNaturalPersonOpenSingleDepositResponseBean value) {
        this.chNaturalPersonOpenSingleDepositResponseBean = value;
    }

}
