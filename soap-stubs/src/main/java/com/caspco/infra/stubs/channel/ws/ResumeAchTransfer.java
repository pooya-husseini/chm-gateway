
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for resumeAchTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="resumeAchTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chResumeAchTransferRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chResumeAchTransferRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resumeAchTransfer", propOrder = {
    "chResumeAchTransferRequestBean"
})
public class ResumeAchTransfer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChResumeAchTransferRequestBean chResumeAchTransferRequestBean;

    /**
     * Gets the value of the chResumeAchTransferRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChResumeAchTransferRequestBean }
     *     
     */
    public ChResumeAchTransferRequestBean getChResumeAchTransferRequestBean() {
        return chResumeAchTransferRequestBean;
    }

    /**
     * Sets the value of the chResumeAchTransferRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChResumeAchTransferRequestBean }
     *     
     */
    public void setChResumeAchTransferRequestBean(ChResumeAchTransferRequestBean value) {
        this.chResumeAchTransferRequestBean = value;
    }

}
