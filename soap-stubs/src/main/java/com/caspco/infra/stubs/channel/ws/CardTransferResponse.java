
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardTransferResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardTransferResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chFinancialServiceResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chFinancialServiceResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardTransferResponse", propOrder = {
    "chFinancialServiceResponseBean"
})
public class CardTransferResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChFinancialServiceResponseBean chFinancialServiceResponseBean;

    /**
     * Gets the value of the chFinancialServiceResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChFinancialServiceResponseBean }
     *     
     */
    public ChFinancialServiceResponseBean getChFinancialServiceResponseBean() {
        return chFinancialServiceResponseBean;
    }

    /**
     * Sets the value of the chFinancialServiceResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChFinancialServiceResponseBean }
     *     
     */
    public void setChFinancialServiceResponseBean(ChFinancialServiceResponseBean value) {
        this.chFinancialServiceResponseBean = value;
    }

}
