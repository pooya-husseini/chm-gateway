
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createNaturalCustomer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createNaturalCustomer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCreateNaturalCustomerRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCreateNaturalCustomerRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createNaturalCustomer", propOrder = {
    "chCreateNaturalCustomerRequestBean"
})
public class CreateNaturalCustomer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCreateNaturalCustomerRequestBean chCreateNaturalCustomerRequestBean;

    /**
     * Gets the value of the chCreateNaturalCustomerRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCreateNaturalCustomerRequestBean }
     *     
     */
    public ChCreateNaturalCustomerRequestBean getChCreateNaturalCustomerRequestBean() {
        return chCreateNaturalCustomerRequestBean;
    }

    /**
     * Sets the value of the chCreateNaturalCustomerRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCreateNaturalCustomerRequestBean }
     *     
     */
    public void setChCreateNaturalCustomerRequestBean(ChCreateNaturalCustomerRequestBean value) {
        this.chCreateNaturalCustomerRequestBean = value;
    }

}
