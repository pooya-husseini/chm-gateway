
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCards complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCards">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardsSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardsSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCards", propOrder = {
    "chCardsSearchRequestBean"
})
public class GetCards
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardsSearchRequestBean chCardsSearchRequestBean;

    /**
     * Gets the value of the chCardsSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardsSearchRequestBean }
     *     
     */
    public ChCardsSearchRequestBean getChCardsSearchRequestBean() {
        return chCardsSearchRequestBean;
    }

    /**
     * Sets the value of the chCardsSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardsSearchRequestBean }
     *     
     */
    public void setChCardsSearchRequestBean(ChCardsSearchRequestBean value) {
        this.chCardsSearchRequestBean = value;
    }

}
