
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rtgsNormalTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rtgsNormalTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chRtgsNormalTransferRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chRtgsNormalTransferRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rtgsNormalTransfer", propOrder = {
    "chRtgsNormalTransferRequestBean"
})
public class RtgsNormalTransfer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChRtgsNormalTransferRequestBean chRtgsNormalTransferRequestBean;

    /**
     * Gets the value of the chRtgsNormalTransferRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChRtgsNormalTransferRequestBean }
     *     
     */
    public ChRtgsNormalTransferRequestBean getChRtgsNormalTransferRequestBean() {
        return chRtgsNormalTransferRequestBean;
    }

    /**
     * Sets the value of the chRtgsNormalTransferRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChRtgsNormalTransferRequestBean }
     *     
     */
    public void setChRtgsNormalTransferRequestBean(ChRtgsNormalTransferRequestBean value) {
        this.chRtgsNormalTransferRequestBean = value;
    }

}
