
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rtgsTransferReportResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rtgsTransferReportResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chRtgsTransferResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chRtgsTransferResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rtgsTransferReportResponse", propOrder = {
    "chRtgsTransferResponseBean"
})
public class RtgsTransferReportResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChRtgsTransferResponseBean chRtgsTransferResponseBean;

    /**
     * Gets the value of the chRtgsTransferResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChRtgsTransferResponseBean }
     *     
     */
    public ChRtgsTransferResponseBean getChRtgsTransferResponseBean() {
        return chRtgsTransferResponseBean;
    }

    /**
     * Sets the value of the chRtgsTransferResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChRtgsTransferResponseBean }
     *     
     */
    public void setChRtgsTransferResponseBean(ChRtgsTransferResponseBean value) {
        this.chRtgsTransferResponseBean = value;
    }

}
