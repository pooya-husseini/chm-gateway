
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for inquiryCardIssuingResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="inquiryCardIssuingResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChInquiryCardIssuingResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chInquiryCardIssuingResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "inquiryCardIssuingResponse", propOrder = {
    "chInquiryCardIssuingResponseBean"
})
public class InquiryCardIssuingResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ChInquiryCardIssuingResponseBean")
    protected ChInquiryCardIssuingResponseBean chInquiryCardIssuingResponseBean;

    /**
     * Gets the value of the chInquiryCardIssuingResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChInquiryCardIssuingResponseBean }
     *     
     */
    public ChInquiryCardIssuingResponseBean getChInquiryCardIssuingResponseBean() {
        return chInquiryCardIssuingResponseBean;
    }

    /**
     * Sets the value of the chInquiryCardIssuingResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChInquiryCardIssuingResponseBean }
     *     
     */
    public void setChInquiryCardIssuingResponseBean(ChInquiryCardIssuingResponseBean value) {
        this.chInquiryCardIssuingResponseBean = value;
    }

}
