/**
 * ChUserRequestBean.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public class UserRequestDto implements java.io.Serializable {

    private String cif;


    /**
     * Gets the cif value for this ChUserRequestBean.
     *
     * @return cif
     */
    public String getCif() {
        return cif;
    }


    /**
     * Sets the cif value for this ChUserRequestBean.
     *
     * @param cif
     */
    public void setCif(String cif) {
        this.cif = cif;
    }
}
