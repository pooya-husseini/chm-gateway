
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getGuarantyListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getGuarantyListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chGuarantiesResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chGuarantiesResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getGuarantyListResponse", propOrder = {
    "chGuarantiesResponseBean"
})
public class GetGuarantyListResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChGuarantiesResponseBean chGuarantiesResponseBean;

    /**
     * Gets the value of the chGuarantiesResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChGuarantiesResponseBean }
     *     
     */
    public ChGuarantiesResponseBean getChGuarantiesResponseBean() {
        return chGuarantiesResponseBean;
    }

    /**
     * Sets the value of the chGuarantiesResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChGuarantiesResponseBean }
     *     
     */
    public void setChGuarantiesResponseBean(ChGuarantiesResponseBean value) {
        this.chGuarantiesResponseBean = value;
    }

}
