
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for removeCardDeposit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="removeCardDeposit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chRemoveCardDepositRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chRemoveCardDepositRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeCardDeposit", propOrder = {
    "chRemoveCardDepositRequestBean"
})
public class RemoveCardDeposit
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChRemoveCardDepositRequestBean chRemoveCardDepositRequestBean;

    /**
     * Gets the value of the chRemoveCardDepositRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChRemoveCardDepositRequestBean }
     *     
     */
    public ChRemoveCardDepositRequestBean getChRemoveCardDepositRequestBean() {
        return chRemoveCardDepositRequestBean;
    }

    /**
     * Sets the value of the chRemoveCardDepositRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChRemoveCardDepositRequestBean }
     *     
     */
    public void setChRemoveCardDepositRequestBean(ChRemoveCardDepositRequestBean value) {
        this.chRemoveCardDepositRequestBean = value;
    }

}
