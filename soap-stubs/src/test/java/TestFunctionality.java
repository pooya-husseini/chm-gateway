import com.caspco.infra.stubs.channel.ws.ChNormalTransferRequestBean;
import com.caspco.infra.stubs.channel.ws.ChUserInfoRequestBean;
import com.caspco.infra.stubs.channel.ws.ChannelServiceType;
import com.caspco.infra.stubs.channel.ws.WebserviceGatewayFaultException_Exception;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 12/2/17
 * Time: 9:45 AM
 */


public class TestFunctionality {
    private static final String PREFIX_PATH = "/home/pooya/results/";
    private Set<String> stringSet = new HashSet<>();
    private ObjectMapper objectMapper = new ObjectMapper();
    private com.caspco.infra.stubs.channel.ws.CoreBankingService actualService = new com.caspco.infra.stubs.channel.ws.WebserviceGatewayCoreBankingServiceImplService(new URL("http://192.168.17.95:8080/webservice-gateway/ModernGatewayServices/CorebankingService?wsdl"))
            .getWebserviceGatewayCoreBankingServiceImplPort();

    private final Logger logger = Logger.getLogger(TestFunctionality.class);
    private Map<String, Object> objectMap = new HashMap<>();

    public TestFunctionality() throws MalformedURLException {

        initSet();
        ChNormalTransferRequestBean bean = new ChNormalTransferRequestBean();
        bean.setSourceDeposit("47000424005609");
        objectMap.put("normalTransfer", bean);
    }

    private void initSet() {
        stringSet.add("normalTransfer");
//        if (1 == 1) {
//            return;
//        }
        stringSet.add("getDepositOwnerName");
        stringSet.add("doPayment");
        stringSet.add("getUserSMSSetting");
        stringSet.add("registerCheque");
        stringSet.add("cancelAchTransfer");
        stringSet.add("getUserInfo");
        stringSet.add("getCards");
        stringSet.add("rtgsNormalTransfer");
        stringSet.add("changePassword");
        stringSet.add("achTransactionReport");
        stringSet.add("getDepositRates");
        stringSet.add("payBill");
        stringSet.add("getCheque");
        stringSet.add("getPaymentServices");
        stringSet.add("cardBalanceInquiry");
        stringSet.add("hotCard");
        stringSet.add("logout");
        stringSet.add("ibanEnquiry");
        stringSet.add("acceptAchTransfer");
        stringSet.add("cardChangePin");
        stringSet.add("getLoans");
        stringSet.add("getAutoTransferList");
        stringSet.add("getCardDeposits");
        stringSet.add("getCardStatementInquiry");
        stringSet.add("getPaymentServicesByType");
        stringSet.add("achNormalTransfer");
        stringSet.add("disableTransfer");
        stringSet.add("rtgsTransferReport");
        stringSet.add("cardTransfer");
        stringSet.add("getChequeBookList");
        stringSet.add("login");
        stringSet.add("getBranches");
        stringSet.add("payBillByAccount");
        stringSet.add("getBeOpenedDeposits");
        stringSet.add("payLoan");
        stringSet.add("saveOrUpdateSecondPassword");
        stringSet.add("getDeposits");
        stringSet.add("extendSMSSetting");
        stringSet.add("getEcho");
        stringSet.add("getStatement");
        stringSet.add("getCurrencyRate");
        stringSet.add("topupChargeFromVirtualTerminal");
        stringSet.add("achTransferReport");
        stringSet.add("autoTransfer");
        stringSet.add("cardLessIssueVoucher");
        stringSet.add("getInstitutions");
        stringSet.add("searchThirdParty");
        stringSet.add("changeUsername");
        stringSet.add("getCardOwner");
        stringSet.add("getTransferChequeList");
        stringSet.add("institutionalTransfer");
        stringSet.add("achAutoTransfer");
        stringSet.add("cardLessListVoucherSearch");
        stringSet.add("cardLessCancelVoucher");
        stringSet.add("cardLessAmendVoucher");
        stringSet.add("getLoanOwnerName");
        stringSet.add("bookTurn");
    }

    @Test
    public void generate() {
        AtomicInteger integer = new AtomicInteger(0);
        Arrays.stream(actualService.getClass().getMethods())
                .filter(i -> stringSet.contains(i.getName()))
                .forEach(method -> {
                    System.out.println(("processing : " + method.getName()));
                    FileWriter writer = createStream(method.getName());
                    Parameter[] parameters = method.getParameters();
                    if (parameters.length == 1) {
                        Parameter parameter = parameters[0];
                        Class<?> type = parameter.getType();
                        try {
                            if (type.isEnum()) {
                                integer.incrementAndGet();
                                invokeMethod(method, actualService, type.getEnumConstants()[0]);
                            } else {
                                final Object o;
                                try {
                                    o = type.newInstance();
                                } catch (InstantiationException | IllegalAccessException e) {
                                    throw new RuntimeException("Can not get instance", e);
                                }
                                BeanInfo beanInfo = Introspector.getBeanInfo(type);
                                try {
                                    integer.incrementAndGet();
                                    invokeMethod(method, actualService, o);
                                } catch (Exception e) {
                                    appendLog(writer, e, o);
                                }
                                Arrays.stream(beanInfo.getPropertyDescriptors()).forEach(prop -> {
                                    try {
                                        if (prop.getPropertyType().isAssignableFrom(String.class)) {
                                            setValue(o, prop, "1");
                                        } else if (prop.getPropertyType().isAssignableFrom(Integer.class) || prop.getPropertyType().isAssignableFrom(Short.class)) {
                                            setValue(o, prop, 1);
                                        } else if (prop.getPropertyType().isAssignableFrom(BigDecimal.class)) {
                                            setValue(o, prop, new BigDecimal(1));
                                        } else if (prop.getPropertyType().isAssignableFrom(Boolean.class)) {
                                            setValue(o, prop, true);
                                        } else if (prop.getPropertyType().isAssignableFrom(Long.class)) {
                                            setValue(o, prop, 1L);
                                        } else if (prop.getPropertyType().isAssignableFrom(Date.class)) {
                                            setValue(o, prop, new Date());
                                        } else if (prop.getPropertyType().isEnum()) {
                                            setValue(o, prop, prop.getPropertyType().getEnumConstants()[0]);
                                        } else if (prop.getPropertyType().isAssignableFrom(Class.class)) {
                                            return;
                                        } else if (prop.getPropertyType().isAssignableFrom(List.class)) {
                                            System.out.println(prop.getPropertyType());
                                        } else {
                                            System.out.println(prop.getPropertyType());
                                        }
                                        integer.incrementAndGet();
                                        if (this.objectMap.get(method.getName()) != null) {
                                            updateObject(o, this.objectMap.get(method.getName()));
                                        }
                                        invokeMethod(method, actualService, o);
                                    } catch (Exception e) {
                                        appendLog(writer, e, o);
                                    }
                                });
                            }
                        } catch (Exception e) {
                            appendLog(writer, e, type.getEnumConstants()[0]);
                        }
                    } else {
                        System.out.println("Method does not have 1 parameter " + method.getName());
                    }
                    try {
                        writer.flush();
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        System.out.println("Integer " + integer.get());
    }

    private String getToken() {
        System.out.println("Get token");
        try {
            ChUserInfoRequestBean bean = new ChUserInfoRequestBean();
            bean.setUsername("afshar_96");
            bean.setPassword("71761296");
            return actualService.login(bean, ChannelServiceType.INTERNET_BANK).getSessionId();
        } catch (WebserviceGatewayFaultException_Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void appendLog(FileWriter writer, Exception e, Object input) {
        try {
            writer.append("Input is :\n");
            writer.append(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(input));
            writer.append("\n");
            writer.append("\n");
            writer.append("\n");
            writer.append("Exception is: ");
            writer.append("\n");
            writer.append(extractString(e)).append("\n");
            writer.append(String.join("", Collections.nCopies(200, "=")));
            writer.append("\n");


        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private void setValue(Object o, PropertyDescriptor prop, Object value) {
        try {
            prop.getWriteMethod().invoke(o, value);
        } catch (InvocationTargetException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
    }

    private void invokeMethod(Method method, Object actualService, Object args) {
        try {
            setHeader(actualService);
            method.invoke(actualService, args);
        } catch (Exception x) {
            throw new RuntimeException(x);
        }
    }

    private void setHeader(Object actualService) {
        Map<String, List<String>> maps = new Hashtable<>();
        maps.put("token", Collections.singletonList(getToken()));
        Map<String, Object> requestContext = ((BindingProvider) (actualService)).getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, maps);
    }

    private FileWriter createStream(String path) {
        File file = new File(PREFIX_PATH + path);
        try {
            return new FileWriter(file, true);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String extractString(Exception x) {
        StringWriter out = new StringWriter();
        PrintWriter pw = new PrintWriter(out);
        x.printStackTrace(pw);
        try {
            out.close();
            pw.close();
        } catch (IOException e) {
            throw new RuntimeException(e);

        }
        return out.toString();
    }


    public static void updateObject(Object target, Object source)
            throws InvocationTargetException, IllegalAccessException {
        if (source == null || target == null) {
            throw new NullPointerException("A null paramter was passed into updateObject");
        }


        //Only go through the process if the objects are not the same reference
        if (source != target) {
            Class sourceClass = source.getClass();
            Class targetClass = target.getClass();
            //you may want to work this check if you need to handle polymorphic relations
            if (!sourceClass.equals(targetClass)) {
                throw new IllegalArgumentException("Received parameters are not the same type of class, but must be");
            }


            PropertyDescriptor[] descriptors = PropertyUtils.getPropertyDescriptors(sourceClass);
            for (PropertyDescriptor descriptor : descriptors) {

                Method readMethod = descriptor.getReadMethod();
                if (readMethod.getName().equals("getClass")) {
                    continue;

                }
                Object value = readMethod.invoke(source);

                if (value != null) {
                    Method writeMethod = descriptor.getWriteMethod();
                    writeMethod.invoke(target, value);
                }
            }
        }
    }
}