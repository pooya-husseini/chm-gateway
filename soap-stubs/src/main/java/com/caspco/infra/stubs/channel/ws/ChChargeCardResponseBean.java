
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chChargeCardResponseBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chChargeCardResponseBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="doneCompletely" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="errorType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chChargeCardErrorType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chChargeCardResponseBean", propOrder = {
    "doneCompletely",
    "errorType"
})
public class ChChargeCardResponseBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected boolean doneCompletely;
    @XmlSchemaType(name = "string")
    protected ChChargeCardErrorType errorType;

    /**
     * Gets the value of the doneCompletely property.
     * 
     */
    public boolean isDoneCompletely() {
        return doneCompletely;
    }

    /**
     * Sets the value of the doneCompletely property.
     * 
     */
    public void setDoneCompletely(boolean value) {
        this.doneCompletely = value;
    }

    /**
     * Gets the value of the errorType property.
     * 
     * @return
     *     possible object is
     *     {@link ChChargeCardErrorType }
     *     
     */
    public ChChargeCardErrorType getErrorType() {
        return errorType;
    }

    /**
     * Sets the value of the errorType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChChargeCardErrorType }
     *     
     */
    public void setErrorType(ChChargeCardErrorType value) {
        this.errorType = value;
    }

}
