
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for achNormalTransferResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="achNormalTransferResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chNormalAchTransferResultBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chNormalAchTransferResultBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "achNormalTransferResponse", propOrder = {
    "chNormalAchTransferResultBean"
})
public class AchNormalTransferResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChNormalAchTransferResultBean chNormalAchTransferResultBean;

    /**
     * Gets the value of the chNormalAchTransferResultBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChNormalAchTransferResultBean }
     *     
     */
    public ChNormalAchTransferResultBean getChNormalAchTransferResultBean() {
        return chNormalAchTransferResultBean;
    }

    /**
     * Sets the value of the chNormalAchTransferResultBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChNormalAchTransferResultBean }
     *     
     */
    public void setChNormalAchTransferResultBean(ChNormalAchTransferResultBean value) {
        this.chNormalAchTransferResultBean = value;
    }

}
