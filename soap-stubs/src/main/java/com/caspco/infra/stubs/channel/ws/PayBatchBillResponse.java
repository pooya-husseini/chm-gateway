
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for payBatchBillResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="payBatchBillResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBatchBillPaymentResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBatchBillPaymentResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "payBatchBillResponse", propOrder = {
    "chBatchBillPaymentResponseBean"
})
public class PayBatchBillResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBatchBillPaymentResponseBean chBatchBillPaymentResponseBean;

    /**
     * Gets the value of the chBatchBillPaymentResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBatchBillPaymentResponseBean }
     *     
     */
    public ChBatchBillPaymentResponseBean getChBatchBillPaymentResponseBean() {
        return chBatchBillPaymentResponseBean;
    }

    /**
     * Sets the value of the chBatchBillPaymentResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBatchBillPaymentResponseBean }
     *     
     */
    public void setChBatchBillPaymentResponseBean(ChBatchBillPaymentResponseBean value) {
        this.chBatchBillPaymentResponseBean = value;
    }

}
