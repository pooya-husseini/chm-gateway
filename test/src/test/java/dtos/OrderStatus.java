/**
 * ChOrderStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public  enum OrderStatus implements java.io.Serializable {
    DESC ,
    ASC

}
