
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getSellReportResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getSellReportResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSellReportResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSellReportResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSellReportResponse", propOrder = {
    "chSellReportResponseBean"
})
public class GetSellReportResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSellReportResponseBean chSellReportResponseBean;

    /**
     * Gets the value of the chSellReportResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChSellReportResponseBean }
     *     
     */
    public ChSellReportResponseBean getChSellReportResponseBean() {
        return chSellReportResponseBean;
    }

    /**
     * Sets the value of the chSellReportResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSellReportResponseBean }
     *     
     */
    public void setChSellReportResponseBean(ChSellReportResponseBean value) {
        this.chSellReportResponseBean = value;
    }

}
