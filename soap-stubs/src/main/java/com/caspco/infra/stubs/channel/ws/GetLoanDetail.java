
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getLoanDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getLoanDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chLoanDetailSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chLoanDetailSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getLoanDetail", propOrder = {
    "chLoanDetailSearchRequestBean"
})
public class GetLoanDetail
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChLoanDetailSearchRequestBean chLoanDetailSearchRequestBean;

    /**
     * Gets the value of the chLoanDetailSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChLoanDetailSearchRequestBean }
     *     
     */
    public ChLoanDetailSearchRequestBean getChLoanDetailSearchRequestBean() {
        return chLoanDetailSearchRequestBean;
    }

    /**
     * Sets the value of the chLoanDetailSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChLoanDetailSearchRequestBean }
     *     
     */
    public void setChLoanDetailSearchRequestBean(ChLoanDetailSearchRequestBean value) {
        this.chLoanDetailSearchRequestBean = value;
    }

}
