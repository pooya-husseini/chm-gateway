package com.caspco.channel.exceptions;

import java.util.Map;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/22/17
 * Time: 3:57 PM
 */
public class OpenApiException extends RuntimeException {
    private ExceptionDescription description;
    private Map<String,?> args;

    public OpenApiException(String message,Map<String,?> args) {
        super(message);
        this.args = args;
    }

    public OpenApiException(String message) {
        super(message);
    }

    public OpenApiException(ExceptionDescription description) {
        this.description = description;
    }

    public OpenApiException(String message,ExceptionDescription description) {
        super(message);
        this.description = description;
    }

    public OpenApiException(String message,Throwable cause, ExceptionDescription description) {
        super(message, cause);
        this.description = description;
    }

    public OpenApiException(String message,Throwable cause) {
        super(message, cause);
    }

    public OpenApiException(Throwable cause, ExceptionDescription description) {
        super(cause);
        this.description = description;
    }

    public OpenApiException(Throwable cause) {
        super(cause);
    }

    public OpenApiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ExceptionDescription description) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.description = description;
    }

    public ExceptionDescription getDescription() {
        return description;
    }

    public Map<String, ?> getArgs() {
        return args;
    }
}