
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chFullCustomerInfoRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chFullCustomerInfoRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="customerModificationType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}customerModificationType" minOccurs="0"/>
 *         &lt;element name="modifyDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chFullCustomerInfoRequestBean", propOrder = {
    "customerModificationType",
    "modifyDate"
})
public class ChFullCustomerInfoRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlSchemaType(name = "string")
    protected CustomerModificationType customerModificationType;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date modifyDate;

    /**
     * Gets the value of the customerModificationType property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerModificationType }
     *     
     */
    public CustomerModificationType getCustomerModificationType() {
        return customerModificationType;
    }

    /**
     * Sets the value of the customerModificationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerModificationType }
     *     
     */
    public void setCustomerModificationType(CustomerModificationType value) {
        this.customerModificationType = value;
    }

    /**
     * Gets the value of the modifyDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * Sets the value of the modifyDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModifyDate(Date value) {
        this.modifyDate = value;
    }

}
