
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPaymentBillStatement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPaymentBillStatement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chPaymentBillStatementRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPaymentBillStatementRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPaymentBillStatement", propOrder = {
    "chPaymentBillStatementRequestBean"
})
public class GetPaymentBillStatement
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChPaymentBillStatementRequestBean chPaymentBillStatementRequestBean;

    /**
     * Gets the value of the chPaymentBillStatementRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChPaymentBillStatementRequestBean }
     *     
     */
    public ChPaymentBillStatementRequestBean getChPaymentBillStatementRequestBean() {
        return chPaymentBillStatementRequestBean;
    }

    /**
     * Sets the value of the chPaymentBillStatementRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPaymentBillStatementRequestBean }
     *     
     */
    public void setChPaymentBillStatementRequestBean(ChPaymentBillStatementRequestBean value) {
        this.chPaymentBillStatementRequestBean = value;
    }

}
