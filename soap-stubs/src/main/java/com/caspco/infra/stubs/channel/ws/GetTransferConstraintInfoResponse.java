
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getTransferConstraintInfoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTransferConstraintInfoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBriefTransferConstraintInfoResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBriefTransferConstraintInfoResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransferConstraintInfoResponse", propOrder = {
    "chBriefTransferConstraintInfoResponseBean"
})
public class GetTransferConstraintInfoResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBriefTransferConstraintInfoResponseBean chBriefTransferConstraintInfoResponseBean;

    /**
     * Gets the value of the chBriefTransferConstraintInfoResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBriefTransferConstraintInfoResponseBean }
     *     
     */
    public ChBriefTransferConstraintInfoResponseBean getChBriefTransferConstraintInfoResponseBean() {
        return chBriefTransferConstraintInfoResponseBean;
    }

    /**
     * Sets the value of the chBriefTransferConstraintInfoResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBriefTransferConstraintInfoResponseBean }
     *     
     */
    public void setChBriefTransferConstraintInfoResponseBean(ChBriefTransferConstraintInfoResponseBean value) {
        this.chBriefTransferConstraintInfoResponseBean = value;
    }

}
