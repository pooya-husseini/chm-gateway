
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sendMail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sendMail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chMailRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chMailRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendMail", propOrder = {
    "chMailRequestBean"
})
public class SendMail
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChMailRequestBean chMailRequestBean;

    /**
     * Gets the value of the chMailRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChMailRequestBean }
     *     
     */
    public ChMailRequestBean getChMailRequestBean() {
        return chMailRequestBean;
    }

    /**
     * Sets the value of the chMailRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChMailRequestBean }
     *     
     */
    public void setChMailRequestBean(ChMailRequestBean value) {
        this.chMailRequestBean = value;
    }

}
