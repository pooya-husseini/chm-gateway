
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getLoans complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getLoans">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chLoansSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chLoansSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getLoans", propOrder = {
    "chLoansSearchRequestBean"
})
public class GetLoans
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChLoansSearchRequestBean chLoansSearchRequestBean;

    /**
     * Gets the value of the chLoansSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChLoansSearchRequestBean }
     *     
     */
    public ChLoansSearchRequestBean getChLoansSearchRequestBean() {
        return chLoansSearchRequestBean;
    }

    /**
     * Sets the value of the chLoansSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChLoansSearchRequestBean }
     *     
     */
    public void setChLoansSearchRequestBean(ChLoansSearchRequestBean value) {
        this.chLoansSearchRequestBean = value;
    }

}
