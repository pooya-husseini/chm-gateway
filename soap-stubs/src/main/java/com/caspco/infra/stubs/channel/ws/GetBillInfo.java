
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBillInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBillInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBillInfoSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillInfoSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBillInfo", propOrder = {
    "chBillInfoSearchRequestBean"
})
public class GetBillInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBillInfoSearchRequestBean chBillInfoSearchRequestBean;

    /**
     * Gets the value of the chBillInfoSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillInfoSearchRequestBean }
     *     
     */
    public ChBillInfoSearchRequestBean getChBillInfoSearchRequestBean() {
        return chBillInfoSearchRequestBean;
    }

    /**
     * Sets the value of the chBillInfoSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillInfoSearchRequestBean }
     *     
     */
    public void setChBillInfoSearchRequestBean(ChBillInfoSearchRequestBean value) {
        this.chBillInfoSearchRequestBean = value;
    }

}
