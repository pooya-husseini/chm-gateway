
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBatchBillInfoBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chBatchBillInfoBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="billId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="billType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillType" minOccurs="0"/>
 *         &lt;element name="doneCompletely" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="foreignTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="problemType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chProblemType" minOccurs="0"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chBatchBillInfoBean", propOrder = {
    "amount",
    "billId",
    "billType",
    "doneCompletely",
    "foreignTitle",
    "payId",
    "problemType",
    "title"
})
public class ChBatchBillInfoBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigDecimal amount;
    protected String billId;
    @XmlSchemaType(name = "string")
    protected ChBillType billType;
    protected boolean doneCompletely;
    protected String foreignTitle;
    protected String payId;
    @XmlSchemaType(name = "string")
    protected ChProblemType problemType;
    protected String title;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the billId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillId() {
        return billId;
    }

    /**
     * Sets the value of the billId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillId(String value) {
        this.billId = value;
    }

    /**
     * Gets the value of the billType property.
     * 
     * @return
     *     possible object is
     *     {@link ChBillType }
     *     
     */
    public ChBillType getBillType() {
        return billType;
    }

    /**
     * Sets the value of the billType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBillType }
     *     
     */
    public void setBillType(ChBillType value) {
        this.billType = value;
    }

    /**
     * Gets the value of the doneCompletely property.
     * 
     */
    public boolean isDoneCompletely() {
        return doneCompletely;
    }

    /**
     * Sets the value of the doneCompletely property.
     * 
     */
    public void setDoneCompletely(boolean value) {
        this.doneCompletely = value;
    }

    /**
     * Gets the value of the foreignTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignTitle() {
        return foreignTitle;
    }

    /**
     * Sets the value of the foreignTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignTitle(String value) {
        this.foreignTitle = value;
    }

    /**
     * Gets the value of the payId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayId() {
        return payId;
    }

    /**
     * Sets the value of the payId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayId(String value) {
        this.payId = value;
    }

    /**
     * Gets the value of the problemType property.
     * 
     * @return
     *     possible object is
     *     {@link ChProblemType }
     *     
     */
    public ChProblemType getProblemType() {
        return problemType;
    }

    /**
     * Sets the value of the problemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChProblemType }
     *     
     */
    public void setProblemType(ChProblemType value) {
        this.problemType = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

}
