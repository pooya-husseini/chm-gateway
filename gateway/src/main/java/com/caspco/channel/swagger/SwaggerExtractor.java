package com.caspco.channel.swagger;


import com.caspco.infra.stubs.channel.ws.CoreBankingService;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ApiListingBuilderPlugin;
import springfox.documentation.spi.service.contexts.ApiListingContext;
import springfox.documentation.swagger.common.SwaggerPluginSupport;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/9/17
 * Time: 4:08 PM
 */
@Component
@Order(value = SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER)
public class SwaggerExtractor implements ApiListingBuilderPlugin {

    private static final Logger LOGGER = Logger.getLogger(SwaggerExtractor.class);

    @Autowired
    private RequestMappingHandlerMapping handlerMapping;
    private Set<String> definedMethodsSet;

    @Override
    public void apply(ApiListingContext apiListingContext) {
        LOGGER.info("Collecting swagger data started!!");

        Map<RequestMappingInfo, HandlerMethod> handledMethodsMap = handlerMapping.getHandlerMethods();
//        Set<RequestMappingInfo> handlerMethods = handledMethodsMap.keySet();
//        ProgressBar pb = new ProgressBar("Test", handledMethodsMap.size()); // name, initial max
//        pb.start();
        definedMethodsSet = handledMethodsMap.keySet().stream().map(i -> i.getPatternsCondition().toString().replace("[", "").replace("]", "")).collect(Collectors.toSet());

//        TypeResolver typeResolver = new TypeResolver();
//        Map<String, Model> modelsMap = new HashMap<>();
//        Map<String, ModelProperty> propertyMap = new HashMap<>();
//        ModelPropertyBuilder modelPropertyBuilder = new ModelPropertyBuilder();

//        modelPropertyBuilder.name("name");
//        modelPropertyBuilder.qualifiedType("java.lang.String");
//        modelPropertyBuilder.type(typeResolver.resolve(String.class));
//        modelPropertyBuilder.required(true);
//        modelPropertyBuilder.readOnly(false);
//        modelPropertyBuilder.pattern("");
//        modelPropertyBuilder.extensions(newArrayList());
//
//        ModelBuilder modelBuilder = new ModelBuilder();

//        modelBuilder
//                .id("com.caspco.channel.services.channel.dtos.BlockChequeRequest")
//                .name("BlockChequeRequest")
//                .qualifiedType("com.caspco.channel.services.channel.dtos.BlockChequeRequest")
//                .type(typeResolver.resolve(com.caspco.channel.services.channel.dtos.BlockChequeRequest.class))
//                .properties(propertyMap);

//        modelsMap.put("com.caspco.channel.services.channel.dtos.BlockChequeRequest", modelBuilder.build());
        Stream<ApiDescription> restMethods = handledMethodsMap.entrySet()
                .stream()
                .filter(i -> i.getKey().getPatternsCondition().toString().replace("[", "").startsWith("/core"))
                .map(Map.Entry::getValue)
                .map(HandlerMethod::getMethod)
                .map(i -> extractDescription(i, parameter -> parameter.isAnnotationPresent(RequestBody.class)));
        Stream<ApiDescription> dynamicMethods = Arrays.stream(CoreBankingService.class.getMethods())
                .map(i -> Pair.of("/core/" + i.getName(), i))
                .filter(i -> !definedMethodsSet.contains(i.getLeft()))
                .map(i -> extractDescription(i.getRight()));
        List<ApiDescription> apiDescriptionList = Stream.concat(restMethods, dynamicMethods).collect(Collectors.toList());
        apiListingContext
                .apiListingBuilder()
                .apis(apiDescriptionList);
    }

    private ApiDescription extractDescription(Method method) {
        return extractDescription(method, parameter -> true);
    }

    private ApiDescription extractDescription(Method method, MethodParameterFilter methodParameterFilter) {
//        java.lang.reflect.Parameter methodParameter = null;
        List<Parameter> parameters = new ArrayList<>();

        ModelRef responseModel;
        if (Collection.class.isAssignableFrom(method.getReturnType())) {
            Class<?> actualType = (Class<?>) ((ParameterizedTypeImpl) method.getGenericReturnType()).getActualTypeArguments()[0];
            responseModel = new ModelRef("Array", new ModelRef(actualType.getSimpleName()));
        } else {
            responseModel = new ModelRef(method.getReturnType().getSimpleName());
        }

        List<java.lang.reflect.Parameter> methodParameters = new ArrayList<>();

        if (method.getParameters() != null) {
            methodParameters = Arrays
                    .stream(method.getParameters())
                    .filter(methodParameterFilter::isValid)
                    .collect(Collectors.toList());
        }

        java.lang.reflect.Parameter methodParameter = null;
        if (methodParameters.size() == 1) {
            methodParameter = methodParameters.get(0);
            ModelRef modelRef;

            if (Collection.class.isAssignableFrom(methodParameter.getType())) {
                modelRef = new ModelRef("Array", new ModelRef(methodParameter.getType().getSimpleName()));
            } else {
                modelRef = new ModelRef(methodParameter.getType().getSimpleName());
            }

            parameters.add(new Parameter(
                            methodParameter.getName(),
                            "",
                            "",
                            true,
                            false,
                            modelRef,
                            null,
                            new AllowableListValues(newArrayList(), "String"),
                            "body",
                            "", false, Collections.emptyList()
                    )
            );
        } else if (method.getParameterCount() > 1) {
            throw new IllegalStateException("Method " + method.getName() + " should have only one parameter");
        }

        if (methodParameter != null && methodParameter.getType().isEnum()) {
            throw new IllegalStateException("Method " + method.getName() + " should not have enum as input parameter!");
        }

        return new ApiDescription(
                "/core/" + method.getName(),
                "",
                Lists.newArrayList(
                        new Operation(
                                HttpMethod.POST,
                                "",
                                "",
                                null,
                                method.getName(),
                                0,
                                Collections.emptySet(),
                                newHashSet(MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE),
                                newHashSet(MediaType.APPLICATION_JSON_VALUE),
                                Collections.emptySet(),
                                newArrayList(),

                                parameters,
                                newHashSet(
                                        new ResponseMessage(
                                                HttpStatus.OK.value(),
                                                HttpStatus.OK.getReasonPhrase(),
                                                responseModel,
                                                Collections.emptyMap(), Collections.emptyList()
                                        )
                                ),
                                null,
                                false,
                                newArrayList()
                        )
                ),
                false
        );
    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }
}