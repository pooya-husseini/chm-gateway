/**
 * ChRtgsNormalTransferRequestBean.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

import java.math.BigDecimal;

public class RtgsNormalTransferRequestDto implements java.io.Serializable {

    private BigDecimal amount;

    private SecondPasswordType chSecondPasswordType;

    private String cif;

    private String description;

    private String destinationIbanNumber;

    private String email;

    private String factorNumber;

    private String receiverFamily;

    private String receiverName;

    private String receiverTelephoneNumber;

    private String secondPassword;

    private String sourceDepositNumber;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public SecondPasswordType getChSecondPasswordType() {
        return chSecondPasswordType;
    }

    public void setChSecondPasswordType(SecondPasswordType chSecondPasswordType) {
        this.chSecondPasswordType = chSecondPasswordType;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDestinationIbanNumber() {
        return destinationIbanNumber;
    }

    public void setDestinationIbanNumber(String destinationIbanNumber) {
        this.destinationIbanNumber = destinationIbanNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFactorNumber() {
        return factorNumber;
    }

    public void setFactorNumber(String factorNumber) {
        this.factorNumber = factorNumber;
    }

    public String getReceiverFamily() {
        return receiverFamily;
    }

    public void setReceiverFamily(String receiverFamily) {
        this.receiverFamily = receiverFamily;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverTelephoneNumber() {
        return receiverTelephoneNumber;
    }

    public void setReceiverTelephoneNumber(String receiverTelephoneNumber) {
        this.receiverTelephoneNumber = receiverTelephoneNumber;
    }

    public String getSecondPassword() {
        return secondPassword;
    }

    public void setSecondPassword(String secondPassword) {
        this.secondPassword = secondPassword;
    }

    public String getSourceDepositNumber() {
        return sourceDepositNumber;
    }

    public void setSourceDepositNumber(String sourceDepositNumber) {
        this.sourceDepositNumber = sourceDepositNumber;
    }
}
