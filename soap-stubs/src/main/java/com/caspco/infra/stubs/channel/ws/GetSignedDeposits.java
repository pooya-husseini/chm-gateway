
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getSignedDeposits complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getSignedDeposits">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSignedDepositRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSignedDepositRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSignedDeposits", propOrder = {
    "chSignedDepositRequestBean"
})
public class GetSignedDeposits
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSignedDepositRequestBean chSignedDepositRequestBean;

    /**
     * Gets the value of the chSignedDepositRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChSignedDepositRequestBean }
     *     
     */
    public ChSignedDepositRequestBean getChSignedDepositRequestBean() {
        return chSignedDepositRequestBean;
    }

    /**
     * Sets the value of the chSignedDepositRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSignedDepositRequestBean }
     *     
     */
    public void setChSignedDepositRequestBean(ChSignedDepositRequestBean value) {
        this.chSignedDepositRequestBean = value;
    }

}
