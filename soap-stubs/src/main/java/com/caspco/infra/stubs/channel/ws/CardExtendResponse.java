
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardExtendResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardExtendResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardExtendResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardExtendResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardExtendResponse", propOrder = {
    "chCardExtendResponseBean"
})
public class CardExtendResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardExtendResponseBean chCardExtendResponseBean;

    /**
     * Gets the value of the chCardExtendResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardExtendResponseBean }
     *     
     */
    public ChCardExtendResponseBean getChCardExtendResponseBean() {
        return chCardExtendResponseBean;
    }

    /**
     * Sets the value of the chCardExtendResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardExtendResponseBean }
     *     
     */
    public void setChCardExtendResponseBean(ChCardExtendResponseBean value) {
        this.chCardExtendResponseBean = value;
    }

}
