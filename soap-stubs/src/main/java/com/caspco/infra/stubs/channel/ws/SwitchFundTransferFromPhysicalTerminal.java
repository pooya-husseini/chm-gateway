
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for switchFundTransferFromPhysicalTerminal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="switchFundTransferFromPhysicalTerminal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chPhysicalCardTransferRequest" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPhysicalCardTransferRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "switchFundTransferFromPhysicalTerminal", propOrder = {
    "chPhysicalCardTransferRequest"
})
public class SwitchFundTransferFromPhysicalTerminal
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChPhysicalCardTransferRequest chPhysicalCardTransferRequest;

    /**
     * Gets the value of the chPhysicalCardTransferRequest property.
     * 
     * @return
     *     possible object is
     *     {@link ChPhysicalCardTransferRequest }
     *     
     */
    public ChPhysicalCardTransferRequest getChPhysicalCardTransferRequest() {
        return chPhysicalCardTransferRequest;
    }

    /**
     * Sets the value of the chPhysicalCardTransferRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPhysicalCardTransferRequest }
     *     
     */
    public void setChPhysicalCardTransferRequest(ChPhysicalCardTransferRequest value) {
        this.chPhysicalCardTransferRequest = value;
    }

}
