
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDepositCustomerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDepositCustomerResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDepositCustomerResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositCustomerResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDepositCustomerResponse", propOrder = {
    "chDepositCustomerResponseBean"
})
public class GetDepositCustomerResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDepositCustomerResponseBean chDepositCustomerResponseBean;

    /**
     * Gets the value of the chDepositCustomerResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositCustomerResponseBean }
     *     
     */
    public ChDepositCustomerResponseBean getChDepositCustomerResponseBean() {
        return chDepositCustomerResponseBean;
    }

    /**
     * Sets the value of the chDepositCustomerResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositCustomerResponseBean }
     *     
     */
    public void setChDepositCustomerResponseBean(ChDepositCustomerResponseBean value) {
        this.chDepositCustomerResponseBean = value;
    }

}
