package com.caspco.openapi.gateway.core.services;

import com.caspco.openapi.gateway.core.exceptions.InvalidUserException;
import com.caspian.banking.cif.dto.CustomerDTO;
import com.caspian.banking.cif.message.CustomerSimpleSearchMsg;
import com.caspian.banking.deposit.account.dto.AccountDTO;
import com.caspian.banking.deposit.account.message.LoadAllValidAccountsOfAccountMsg;
import com.caspian.banking.ebanking.dto.*;
import com.caspian.banking.ebanking.message.MGCardBalanceInquiryMsg;
import com.caspian.banking.ebanking.message.MGGetCardDepositsMsg;
import com.caspian.banking.ebanking.message.MGGetCardsMsg;
import com.caspian.moderngateway.core.coreservice.dto.ChStatementBean;
import com.caspian.moderngateway.core.coreservice.dto.ChStatementSearchRequestBean;
import com.caspian.moderngateway.core.message.GetStatementMsg;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.jms.JMSException;
import javax.naming.NamingException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/22/17
 *         Time: 10:41 AM
 */


@Service
public class CustomerService {

    @Autowired
    private JmsService jmsService;
    private Cache<Long, Set<String>> cache;

    @PostConstruct
    public void init() {
        cache = CacheBuilder.newBuilder().weakKeys().build();
    }

    public MGCardBalanceInquiryMsg.Outbound getCardBalance(CardBalanceDto cardBalanceDto) throws JMSException, NamingException, IOException {
        MGCardBalanceInquiryMsg.Inbound inbound=new MGCardBalanceInquiryMsg.Inbound();
        inbound.setCardBalanceDto(cardBalanceDto);
        String send = jmsService.send("channelManagement.CardBalanceRequestMsg", RequestType.TRANSACTION, inbound);
        return jmsService.receive(send, MGCardBalanceInquiryMsg.Outbound.class);
    }


    public MGGetCardDepositsMsg.Outbound getCardDeposit(Long id, String cardNumber) throws JMSException, NamingException, IOException {
        MGGetCardDepositsMsg.Inbound  inbound=new MGGetCardDepositsMsg.Inbound();
        CardDepositDto depositDto = new CardDepositDto();
        depositDto.setCif(String.valueOf(id));
        depositDto.setPan(cardNumber);

        inbound.setCardDepositDto(depositDto);
        String send = jmsService.send("channelManagement.getCardDepositsMsg", RequestType.TRANSACTION, inbound);
        return jmsService.receive(send, MGGetCardDepositsMsg.Outbound.class);
    }
    public List<ChCardDto> getCards(Long id) throws JMSException, NamingException, IOException {
        MGGetCardsMsg.Inbound inbound = new MGGetCardsMsg.Inbound();
        CardsSearchDto cardsSearchDto = new CardsSearchDto();
        cardsSearchDto.setCif(String.valueOf(id));
        cardsSearchDto.setCardStatus(ChCardStatus.OK);
        inbound.setCardsSearchDto(cardsSearchDto);
        String send = jmsService.send("channelManagement.CardsSearchRequestMsg", RequestType.TRANSACTION, inbound);
        MGGetCardsMsg.Outbound outbound = jmsService.receive(send, MGGetCardsMsg.Outbound.class);
        return outbound.getChCardDtos();
    }

    public List<ChStatementBean> getStatement(ChStatementSearchRequestBean inbound) throws JMSException, NamingException, IOException {
//        GetAccountInvoiceMsg.Inbound inbound = new GetAccountInvoiceMsg.Inbound();
        try {
            String send = jmsService.send("GetStatementMsg", RequestType.INQUIRY, inbound);
            GetStatementMsg.Outbound outbound = jmsService.receive(send, GetStatementMsg.Outbound.class);
            return outbound.getStatementResponseBean().getStatementBeans();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    public List<AccountDTO> getAccounts(Long id) throws IOException, JMSException, NamingException {
        LoadAllValidAccountsOfAccountMsg.Inbound inbound = new LoadAllValidAccountsOfAccountMsg.Inbound();
        inbound.setClientId(id);
        String send = jmsService.send("deposit.account.message.loadAllValidAccountsOfClient", RequestType.TRANSACTION, inbound);
        LoadAllValidAccountsOfAccountMsg.Outbound outbound = jmsService.receive(send, LoadAllValidAccountsOfAccountMsg.Outbound.class);
        List<AccountDTO> accountListDTO = outbound.getAccountListDTO();
//        if (accountListDTO != null && accountListDTO.size() > 0) {
//            return accountListDTO.stream().map(AccountDTO::getAccountId).collect(Collectors.toList());
//        }
        return accountListDTO;
    }

    public Set<String> getAccountsExternalRefs(Long id) throws IOException, JMSException, NamingException {
        Set<String> result = cache.getIfPresent(id);
        if (result != null) {
            return result;
        }
        List<AccountDTO> accounts = getAccounts(id);
        if (accounts != null) {
            Set<String> externalRefs = accounts.stream().map(AccountDTO::getExternalRef).collect(Collectors.toSet());
            cache.put(id, externalRefs);
            return externalRefs;
        }
        return null;
    }


    public CustomerDTO getCustomerById(Long id) throws JMSException, NamingException, IOException {
        CustomerSimpleSearchMsg.Inbound inbound = new CustomerSimpleSearchMsg.Inbound();

        inbound.setEnd(10);
        inbound.setStart(1);
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setExternalRef(id);
        inbound.setCustomerDTO(customerDTO);
        String send = jmsService.send("cif.CustomerSimpleSearchMsg", RequestType.INQUIRY, inbound);
        CustomerSimpleSearchMsg.Outbound outbound = jmsService.receive(send, CustomerSimpleSearchMsg.Outbound.class);
        if (outbound == null || outbound.getCustomers().size() > 1) {
            throw new InvalidUserException();
        }
        return outbound.getCustomers().get(0);
    }


    public Long getCustomerId(String username) {
        RestTemplate restTemplate=new RestTemplate();
        Map<String, String> input = new HashMap<>();
        input.put("payload", username);

        Map map = restTemplate.postForObject("http://localhost:9900/profile", input, Map.class);
        if (map == null) {
            return null;
        }
        Object payload = map.get("payload");
        return (Long)(payload);
    }
}