
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDepositRates complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDepositRates">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDepositRateInquiryRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositRateInquiryRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDepositRates", propOrder = {
    "chDepositRateInquiryRequestBean"
})
public class GetDepositRates
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDepositRateInquiryRequestBean chDepositRateInquiryRequestBean;

    /**
     * Gets the value of the chDepositRateInquiryRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositRateInquiryRequestBean }
     *     
     */
    public ChDepositRateInquiryRequestBean getChDepositRateInquiryRequestBean() {
        return chDepositRateInquiryRequestBean;
    }

    /**
     * Sets the value of the chDepositRateInquiryRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositRateInquiryRequestBean }
     *     
     */
    public void setChDepositRateInquiryRequestBean(ChDepositRateInquiryRequestBean value) {
        this.chDepositRateInquiryRequestBean = value;
    }

}
