
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for customerModificationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="customerModificationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Created"/>
 *     &lt;enumeration value="Edited"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "customerModificationType")
@XmlEnum
public enum CustomerModificationType {

    @XmlEnumValue("Created")
    CREATED("Created"),
    @XmlEnumValue("Edited")
    EDITED("Edited");
    private final String value;

    CustomerModificationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CustomerModificationType fromValue(String v) {
        for (CustomerModificationType c: CustomerModificationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
