/**
 * ChDepositSearchRequestBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class DepositSearchRequestDto  implements java.io.Serializable {

    private java.lang.String cif;

    private List<String> depositNumbers;

    private com.caspco.channel.dtos.DepositStatus depositStatus;

    private List<java.lang.String> excludeCurrency;

    @ApiModelProperty(value = "excludeType")
    private List<com.caspco.channel.dtos.DepositGroupType> excludeType;

    private java.lang.Boolean forSearch;

    private java.lang.Boolean includeCreditAccount;

    private List<java.lang.String> includeCurrency;

    private java.lang.Boolean includeSupportAccount;

    @ApiModelProperty(value = "includeType")
    private List<com.caspco.channel.dtos.DepositGroupType> includeType;

    private java.lang.Long length;

    private java.lang.Long offset;

    @ApiModelProperty(value = "personalityType")
    private List<com.caspco.channel.dtos.PersonalityType> personality;

    @ApiModelProperty(value = "serviceCode")
    private com.caspco.channel.dtos.ServicesCode serviceCode;

    @ApiModelProperty(value = "signatureOwnerStatus")
    private List<com.caspco.channel.dtos.SignatureOwnerStatus> signatureStatus;

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public List<String> getDepositNumbers() {
        return depositNumbers;
    }

    public void setDepositNumbers(List<String> depositNumbers) {
        this.depositNumbers = depositNumbers;
    }

    public DepositStatus getDepositStatus() {
        return depositStatus;
    }

    public void setDepositStatus(DepositStatus depositStatus) {
        this.depositStatus = depositStatus;
    }

    public List<String> getExcludeCurrency() {
        return excludeCurrency;
    }

    public void setExcludeCurrency(List<String> excludeCurrency) {
        this.excludeCurrency = excludeCurrency;
    }

    public List<DepositGroupType> getExcludeType() {
        return excludeType;
    }

    public void setExcludeType(List<DepositGroupType> excludeType) {
        this.excludeType = excludeType;
    }

    public Boolean getForSearch() {
        return forSearch;
    }

    public void setForSearch(Boolean forSearch) {
        this.forSearch = forSearch;
    }

    public Boolean getIncludeCreditAccount() {
        return includeCreditAccount;
    }

    public void setIncludeCreditAccount(Boolean includeCreditAccount) {
        this.includeCreditAccount = includeCreditAccount;
    }

    public List<String> getIncludeCurrency() {
        return includeCurrency;
    }

    public void setIncludeCurrency(List<String> includeCurrency) {
        this.includeCurrency = includeCurrency;
    }

    public Boolean getIncludeSupportAccount() {
        return includeSupportAccount;
    }

    public void setIncludeSupportAccount(Boolean includeSupportAccount) {
        this.includeSupportAccount = includeSupportAccount;
    }

    public List<DepositGroupType> getIncludeType() {
        return includeType;
    }

    public void setIncludeType(List<DepositGroupType> includeType) {
        this.includeType = includeType;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public List<PersonalityType> getPersonality() {
        return personality;
    }

    public void setPersonality(List<PersonalityType> personality) {
        this.personality = personality;
    }

    public ServicesCode getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(ServicesCode serviceCode) {
        this.serviceCode = serviceCode;
    }

    public List<SignatureOwnerStatus> getSignatureStatus() {
        return signatureStatus;
    }

    public void setSignatureStatus(List<SignatureOwnerStatus> signatureStatus) {
        this.signatureStatus = signatureStatus;
    }
}
