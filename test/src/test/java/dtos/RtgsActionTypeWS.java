/**
 * RtgsActionTypeWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public  enum RtgsActionTypeWS implements java.io.Serializable {
    DASTUR_SABT_SHODE ,
    VIRAYESH_SHODE ,
    TALIGH_DAEM ,
    TAEED_SHODE ,
    ADAM_TAEED_SHOBE ,
    TAEED_SHOBE_SATNA ,
    ADAM_TAEED_SHOBE_SATNA ,
    ERSAL_SHODE_BE_BANK_MAGHSAD ,
    VARIZ_SHODE_BE_BANK_MAGHSAD ,
    VARIZ_NASHODE_BE_BANK_MAGHSAD

}
