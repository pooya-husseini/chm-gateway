
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rtgsNormalTransferResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rtgsNormalTransferResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chNormalRtgsTransferResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chNormalRtgsTransferResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rtgsNormalTransferResponse", propOrder = {
    "chNormalRtgsTransferResponseBean"
})
public class RtgsNormalTransferResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChNormalRtgsTransferResponseBean chNormalRtgsTransferResponseBean;

    /**
     * Gets the value of the chNormalRtgsTransferResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChNormalRtgsTransferResponseBean }
     *     
     */
    public ChNormalRtgsTransferResponseBean getChNormalRtgsTransferResponseBean() {
        return chNormalRtgsTransferResponseBean;
    }

    /**
     * Sets the value of the chNormalRtgsTransferResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChNormalRtgsTransferResponseBean }
     *     
     */
    public void setChNormalRtgsTransferResponseBean(ChNormalRtgsTransferResponseBean value) {
        this.chNormalRtgsTransferResponseBean = value;
    }

}
