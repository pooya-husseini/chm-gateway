/**
 * ChSignatureOwnerStatus.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public
enum SignatureOwnerStatus implements java.io.Serializable {
    DEPOSIT_OWNER,
    OWNER_OF_DEPOSIT_AND_SIGNATURE,
    SIGNATURE_OWNER,
    BROKER;


}
