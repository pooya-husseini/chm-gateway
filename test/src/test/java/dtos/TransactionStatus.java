/**
 * ChTransactionStatus.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package dtos;

public  enum TransactionStatus {
    READY_FOR_PROCESS,
    SUSPENDED,
    CANCELED,
    PROCESS_FAIL,
    READY_TO_TRANSFER,
    TRANSFERRED,
    SETTLED,
    NOT_SETTLED,
    REJECTED;


}
