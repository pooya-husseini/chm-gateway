
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getRequestStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getRequestStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chReqStatusResponseBeans" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chReqStatusResponseBeans" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getRequestStatusResponse", propOrder = {
    "chReqStatusResponseBeans"
})
public class GetRequestStatusResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChReqStatusResponseBeans chReqStatusResponseBeans;

    /**
     * Gets the value of the chReqStatusResponseBeans property.
     * 
     * @return
     *     possible object is
     *     {@link ChReqStatusResponseBeans }
     *     
     */
    public ChReqStatusResponseBeans getChReqStatusResponseBeans() {
        return chReqStatusResponseBeans;
    }

    /**
     * Sets the value of the chReqStatusResponseBeans property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChReqStatusResponseBeans }
     *     
     */
    public void setChReqStatusResponseBeans(ChReqStatusResponseBeans value) {
        this.chReqStatusResponseBeans = value;
    }

}
