package com.caspco.channel.exceptions;

import com.caspco.channel.services.ExceptionTranslationService;
import com.caspco.spl.wrench.beans.transformer.SmartTransformer;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.stringtemplate.v4.ST;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 9/5/17
 * Time: 11:26 AM
 */

@ControllerAdvice
@Service
public class ServiceExceptionHandler implements BaseExceptionHandler{
    private final static Logger LOGGER = LoggerFactory.getLogger(ServiceExceptionHandler.class);
    @Autowired
    private SmartTransformer smartTransformer;

    //    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "An error occurred on server!")
    @ExceptionHandler(Exception.class)
    public void logError(Exception e, HttpServletResponse response) {
        if (e instanceof OpenApiException) {
            String message = e.getMessage();

            ErrorResponse translationMap = new ErrorResponse();
            Map<String, ?> args = ((OpenApiException) e).getArgs();
            Map<String, String> translations = ExceptionTranslationService.getTranslations(message);
            if (args != null && args.size() > 0) {
                translations = translations
                        .entrySet()
                        .stream()
                        .map(i -> {
                            ST st = new ST(i.getValue());
                            args.forEach(st::add);
                            return Pair.of(i.getKey(), st.render());
                        })
                        .collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
            }

            translationMap.setDescription(translations);
            translationMap.setMessage(message);
            translationMap.setCode(-1);
            if (e instanceof InvalidCredentialsException || e instanceof InvalidCredentialHeaderException) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            } else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
            translationMap.setTimestamp(new Date());

            response.setContentType("application/json");
            writeToWriter(response, translationMap);
            LOGGER.error(e.getMessage() + " with args " + args);
        } else if (e instanceof MethodArgumentNotValidException) {
            MethodArgumentNotValidException exception = (MethodArgumentNotValidException) e;
            ErrorResponse translationMap = new ErrorResponse();
            List<ErrorDetail> errorDetails = exception.getBindingResult()
                    .getFieldErrors()
                    .stream()
                    .map(i -> new ErrorDetail(-1, i.getField(), ExceptionTranslationService.getTranslations(i.getDefaultMessage())))
                    .collect(Collectors.toList());

            translationMap.setMessage("validation.error");
            translationMap.setCode(-200);
            translationMap.setDescription(ExceptionTranslationService.getTranslations("validation.error"));
            translationMap.setTimestamp(new Date());
            translationMap.setErrorDetails(errorDetails);

            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.setContentType("application/json");
            writeToWriter(response, translationMap);

        } else {
            ErrorResponse translationMap = new ErrorResponse();
            translationMap.setMessage("system error");
            translationMap.setCode(-100);
            translationMap.setTimestamp(new Date());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.setContentType("application/json");
            writeToWriter(response, translationMap);
//            response.getWriter().write(smartTransformer.toJson(translationMap));
//            response.getWriter().flush();
            LOGGER.error("System error", e);
        }
//  response.getWriter().print();
    }

    public void writeToWriter(HttpServletResponse response,Object o){
        try {
            response.getWriter().write(smartTransformer.toJson(o));
            response.getWriter().flush();
        } catch (IOException e) {
            throw new SystemException(e);
        }

    }
}

