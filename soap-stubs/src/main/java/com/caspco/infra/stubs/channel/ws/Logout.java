
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for logout complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="logout">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="channelServiceType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}channelServiceType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "logout", propOrder = {
    "channelServiceType"
})
public class Logout
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlSchemaType(name = "string")
    protected ChannelServiceType channelServiceType;

    /**
     * Gets the value of the channelServiceType property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelServiceType }
     *     
     */
    public ChannelServiceType getChannelServiceType() {
        return channelServiceType;
    }

    /**
     * Sets the value of the channelServiceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelServiceType }
     *     
     */
    public void setChannelServiceType(ChannelServiceType value) {
        this.channelServiceType = value;
    }

}
