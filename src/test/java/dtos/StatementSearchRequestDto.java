/**
 * ChStatementSearchRequestBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

public class StatementSearchRequestDto  implements java.io.Serializable {

    @ApiModelProperty(value = "actionType")
    private com.caspco.channel.dtos.ActionType action;

    @ApiModelProperty(value = "statementSearchDirection")
    private com.caspco.channel.dtos.StatementSearchDirection chStatementSearchDirection;

    private java.lang.String customerDescription;

    private java.lang.String dealReference;

    private java.lang.String depositNumber;

    private java.lang.String description;

    private java.math.BigDecimal fromAmount;

    private java.util.Date fromDate;

    private java.util.Date fromDateTime;

    private java.lang.Long length;

    private java.lang.Long offset;

    @ApiModelProperty(value = "orderStatus")
    private com.caspco.channel.dtos.OrderStatus order;

    private java.lang.String serial;

    private java.lang.String serialNumber;

    private java.math.BigDecimal toAmount;

    private java.util.Date toDate;

    private java.util.Date toDateTime;


    public ActionType getAction() {
        return action;
    }

    public void setAction(ActionType action) {
        this.action = action;
    }

    public StatementSearchDirection getChStatementSearchDirection() {
        return chStatementSearchDirection;
    }

    public void setChStatementSearchDirection(StatementSearchDirection chStatementSearchDirection) {
        this.chStatementSearchDirection = chStatementSearchDirection;
    }

    public String getCustomerDescription() {
        return customerDescription;
    }

    public void setCustomerDescription(String customerDescription) {
        this.customerDescription = customerDescription;
    }

    public String getDealReference() {
        return dealReference;
    }

    public void setDealReference(String dealReference) {
        this.dealReference = dealReference;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(BigDecimal fromAmount) {
        this.fromAmount = fromAmount;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getFromDateTime() {
        return fromDateTime;
    }

    public void setFromDateTime(Date fromDateTime) {
        this.fromDateTime = fromDateTime;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public OrderStatus getOrder() {
        return order;
    }

    public void setOrder(OrderStatus order) {
        this.order = order;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public BigDecimal getToAmount() {
        return toAmount;
    }

    public void setToAmount(BigDecimal toAmount) {
        this.toAmount = toAmount;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Date getToDateTime() {
        return toDateTime;
    }

    public void setToDateTime(Date toDateTime) {
        this.toDateTime = toDateTime;
    }
}
