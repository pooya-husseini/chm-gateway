package com.caspco.channel.services.channel.model;

import com.caspco.infra.stubs.channel.ws.ChUserInfoRequestBean;
import com.caspco.infra.stubs.channel.ws.ChannelServiceType;
import io.swagger.annotations.ApiModel;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 8/15/17
 * Time: 2:15 PM
 */


@ApiModel

public class LoginDto {
    private ChUserInfoRequestBean requestBean;
    private ChannelServiceType channelServiceType;

    public ChUserInfoRequestBean getRequestBean() {
        return requestBean;
    }

    public void setRequestBean(ChUserInfoRequestBean requestBean) {
        this.requestBean = requestBean;
    }

    public ChannelServiceType getChannelServiceType() {
        return channelServiceType;
    }

    public void setChannelServiceType(ChannelServiceType channelServiceType) {
        this.channelServiceType = channelServiceType;
    }
}
