
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCustomerSignatures complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCustomerSignatures">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCustomerSignaturesRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCustomerSignaturesRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCustomerSignatures", propOrder = {
    "chCustomerSignaturesRequestBean"
})
public class GetCustomerSignatures
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCustomerSignaturesRequestBean chCustomerSignaturesRequestBean;

    /**
     * Gets the value of the chCustomerSignaturesRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCustomerSignaturesRequestBean }
     *     
     */
    public ChCustomerSignaturesRequestBean getChCustomerSignaturesRequestBean() {
        return chCustomerSignaturesRequestBean;
    }

    /**
     * Sets the value of the chCustomerSignaturesRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCustomerSignaturesRequestBean }
     *     
     */
    public void setChCustomerSignaturesRequestBean(ChCustomerSignaturesRequestBean value) {
        this.chCustomerSignaturesRequestBean = value;
    }

}
