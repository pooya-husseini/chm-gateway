
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBlockReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chBlockReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EXCHANGE"/>
 *     &lt;enumeration value="REGULATORY_ORDER"/>
 *     &lt;enumeration value="ACCOUNT_OWNER_REQUEST"/>
 *     &lt;enumeration value="CUSTOMER_DEATH"/>
 *     &lt;enumeration value="TRADE_FINANCE"/>
 *     &lt;enumeration value="MISSING"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chBlockReason")
@XmlEnum
public enum ChBlockReason {

    EXCHANGE,
    REGULATORY_ORDER,
    ACCOUNT_OWNER_REQUEST,
    CUSTOMER_DEATH,
    TRADE_FINANCE,
    MISSING;

    public String value() {
        return name();
    }

    public static ChBlockReason fromValue(String v) {
        return valueOf(v);
    }

}
