package com.caspco.channel.jms;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/22/17
 *         Time: 11:52 AM
 */
public enum RequestType {
    TRANSACTION(100),
    INQUIRY(200);

    private final Integer code;

    private RequestType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }
}
