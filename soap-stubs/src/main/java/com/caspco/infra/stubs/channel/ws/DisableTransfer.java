
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for disableTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="disableTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDisableTransferRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDisableTransferRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "disableTransfer", propOrder = {
    "chDisableTransferRequestBean"
})
public class DisableTransfer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDisableTransferRequestBean chDisableTransferRequestBean;

    /**
     * Gets the value of the chDisableTransferRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDisableTransferRequestBean }
     *     
     */
    public ChDisableTransferRequestBean getChDisableTransferRequestBean() {
        return chDisableTransferRequestBean;
    }

    /**
     * Sets the value of the chDisableTransferRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDisableTransferRequestBean }
     *     
     */
    public void setChDisableTransferRequestBean(ChDisableTransferRequestBean value) {
        this.chDisableTransferRequestBean = value;
    }

}
