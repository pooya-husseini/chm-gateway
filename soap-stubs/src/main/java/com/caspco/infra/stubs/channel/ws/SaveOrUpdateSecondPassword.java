
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for saveOrUpdateSecondPassword complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="saveOrUpdateSecondPassword">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chSecondPasswordRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSecondPasswordRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "saveOrUpdateSecondPassword", propOrder = {
    "chSecondPasswordRequestBean"
})
public class SaveOrUpdateSecondPassword
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChSecondPasswordRequestBean chSecondPasswordRequestBean;

    /**
     * Gets the value of the chSecondPasswordRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChSecondPasswordRequestBean }
     *     
     */
    public ChSecondPasswordRequestBean getChSecondPasswordRequestBean() {
        return chSecondPasswordRequestBean;
    }

    /**
     * Sets the value of the chSecondPasswordRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSecondPasswordRequestBean }
     *     
     */
    public void setChSecondPasswordRequestBean(ChSecondPasswordRequestBean value) {
        this.chSecondPasswordRequestBean = value;
    }

}
