
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chLoanDetailResponseBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chLoanDetailResponseBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="automaticPaymentAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cbLoanNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chCustomersInfo" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCustomerInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="countOfMaturedUnpaid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="countOfPaid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="countOfUnpaid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="loanRows" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chLoanRowBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="penalty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="totalMaturedUnpaidAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="totalPaidAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="totalRecord" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="totalUnpaidAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chLoanDetailResponseBean", propOrder = {
    "amount",
    "automaticPaymentAccountNumber",
    "cbLoanNumber",
    "chCustomersInfo",
    "countOfMaturedUnpaid",
    "countOfPaid",
    "countOfUnpaid",
    "discount",
    "loanRows",
    "penalty",
    "totalMaturedUnpaidAmount",
    "totalPaidAmount",
    "totalRecord",
    "totalUnpaidAmount"
})
public class ChLoanDetailResponseBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigDecimal amount;
    protected String automaticPaymentAccountNumber;
    protected String cbLoanNumber;
    @XmlElement(nillable = true)
    protected List<ChCustomerInfo> chCustomersInfo;
    protected Integer countOfMaturedUnpaid;
    protected Integer countOfPaid;
    protected Integer countOfUnpaid;
    protected BigDecimal discount;
    @XmlElement(nillable = true)
    protected List<ChLoanRowBean> loanRows;
    protected BigDecimal penalty;
    protected BigDecimal totalMaturedUnpaidAmount;
    protected BigDecimal totalPaidAmount;
    protected Integer totalRecord;
    protected BigDecimal totalUnpaidAmount;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the automaticPaymentAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutomaticPaymentAccountNumber() {
        return automaticPaymentAccountNumber;
    }

    /**
     * Sets the value of the automaticPaymentAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutomaticPaymentAccountNumber(String value) {
        this.automaticPaymentAccountNumber = value;
    }

    /**
     * Gets the value of the cbLoanNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCbLoanNumber() {
        return cbLoanNumber;
    }

    /**
     * Sets the value of the cbLoanNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCbLoanNumber(String value) {
        this.cbLoanNumber = value;
    }

    /**
     * Gets the value of the chCustomersInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chCustomersInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChCustomersInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChCustomerInfo }
     * 
     * 
     */
    public List<ChCustomerInfo> getChCustomersInfo() {
        if (chCustomersInfo == null) {
            chCustomersInfo = new ArrayList<ChCustomerInfo>();
        }
        return this.chCustomersInfo;
    }

    /**
     * Gets the value of the countOfMaturedUnpaid property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCountOfMaturedUnpaid() {
        return countOfMaturedUnpaid;
    }

    /**
     * Sets the value of the countOfMaturedUnpaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCountOfMaturedUnpaid(Integer value) {
        this.countOfMaturedUnpaid = value;
    }

    /**
     * Gets the value of the countOfPaid property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCountOfPaid() {
        return countOfPaid;
    }

    /**
     * Sets the value of the countOfPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCountOfPaid(Integer value) {
        this.countOfPaid = value;
    }

    /**
     * Gets the value of the countOfUnpaid property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCountOfUnpaid() {
        return countOfUnpaid;
    }

    /**
     * Sets the value of the countOfUnpaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCountOfUnpaid(Integer value) {
        this.countOfUnpaid = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscount(BigDecimal value) {
        this.discount = value;
    }

    /**
     * Gets the value of the loanRows property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the loanRows property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLoanRows().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChLoanRowBean }
     * 
     * 
     */
    public List<ChLoanRowBean> getLoanRows() {
        if (loanRows == null) {
            loanRows = new ArrayList<ChLoanRowBean>();
        }
        return this.loanRows;
    }

    /**
     * Gets the value of the penalty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPenalty() {
        return penalty;
    }

    /**
     * Sets the value of the penalty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPenalty(BigDecimal value) {
        this.penalty = value;
    }

    /**
     * Gets the value of the totalMaturedUnpaidAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalMaturedUnpaidAmount() {
        return totalMaturedUnpaidAmount;
    }

    /**
     * Sets the value of the totalMaturedUnpaidAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalMaturedUnpaidAmount(BigDecimal value) {
        this.totalMaturedUnpaidAmount = value;
    }

    /**
     * Gets the value of the totalPaidAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalPaidAmount() {
        return totalPaidAmount;
    }

    /**
     * Sets the value of the totalPaidAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalPaidAmount(BigDecimal value) {
        this.totalPaidAmount = value;
    }

    /**
     * Gets the value of the totalRecord property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalRecord() {
        return totalRecord;
    }

    /**
     * Sets the value of the totalRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalRecord(Integer value) {
        this.totalRecord = value;
    }

    /**
     * Gets the value of the totalUnpaidAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalUnpaidAmount() {
        return totalUnpaidAmount;
    }

    /**
     * Sets the value of the totalUnpaidAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalUnpaidAmount(BigDecimal value) {
        this.totalUnpaidAmount = value;
    }

}
