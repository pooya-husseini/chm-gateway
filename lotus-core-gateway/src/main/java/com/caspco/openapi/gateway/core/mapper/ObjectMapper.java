package com.caspco.openapi.gateway.core.mapper;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/22/17
 *         Time: 12:11 PM
 */

@Component
public class ObjectMapper extends com.fasterxml.jackson.databind.ObjectMapper {
    @PostConstruct
    public void init(){
        this.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S"));
    }
}