
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chPeriodicDurationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chPeriodicDurationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DAILY"/>
 *     &lt;enumeration value="MONTHLY"/>
 *     &lt;enumeration value="WEEKLY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chPeriodicDurationType")
@XmlEnum
public enum ChPeriodicDurationType {

    DAILY,
    MONTHLY,
    WEEKLY;

    public String value() {
        return name();
    }

    public static ChPeriodicDurationType fromValue(String v) {
        return valueOf(v);
    }

}
