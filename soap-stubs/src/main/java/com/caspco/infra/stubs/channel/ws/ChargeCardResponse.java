
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chargeCardResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chargeCardResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chChargeCardResponseBeans" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chChargeCardResponseBeans" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chargeCardResponse", propOrder = {
    "chChargeCardResponseBeans"
})
public class ChargeCardResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChChargeCardResponseBeans chChargeCardResponseBeans;

    /**
     * Gets the value of the chChargeCardResponseBeans property.
     * 
     * @return
     *     possible object is
     *     {@link ChChargeCardResponseBeans }
     *     
     */
    public ChChargeCardResponseBeans getChChargeCardResponseBeans() {
        return chChargeCardResponseBeans;
    }

    /**
     * Sets the value of the chChargeCardResponseBeans property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChChargeCardResponseBeans }
     *     
     */
    public void setChChargeCardResponseBeans(ChChargeCardResponseBeans value) {
        this.chChargeCardResponseBeans = value;
    }

}
