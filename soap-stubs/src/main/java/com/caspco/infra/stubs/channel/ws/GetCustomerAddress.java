
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCustomerAddress complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCustomerAddress">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCustomerAddressRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCustomerAddressRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCustomerAddress", propOrder = {
    "chCustomerAddressRequestBean"
})
public class GetCustomerAddress
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCustomerAddressRequestBean chCustomerAddressRequestBean;

    /**
     * Gets the value of the chCustomerAddressRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCustomerAddressRequestBean }
     *     
     */
    public ChCustomerAddressRequestBean getChCustomerAddressRequestBean() {
        return chCustomerAddressRequestBean;
    }

    /**
     * Sets the value of the chCustomerAddressRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCustomerAddressRequestBean }
     *     
     */
    public void setChCustomerAddressRequestBean(ChCustomerAddressRequestBean value) {
        this.chCustomerAddressRequestBean = value;
    }

}
