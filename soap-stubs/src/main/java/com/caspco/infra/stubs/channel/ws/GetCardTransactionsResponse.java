
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardTransactionsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardTransactionsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardTransactionsResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardTransactionsResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardTransactionsResponse", propOrder = {
    "chCardTransactionsResponseBean"
})
public class GetCardTransactionsResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardTransactionsResponseBean chCardTransactionsResponseBean;

    /**
     * Gets the value of the chCardTransactionsResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardTransactionsResponseBean }
     *     
     */
    public ChCardTransactionsResponseBean getChCardTransactionsResponseBean() {
        return chCardTransactionsResponseBean;
    }

    /**
     * Sets the value of the chCardTransactionsResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardTransactionsResponseBean }
     *     
     */
    public void setChCardTransactionsResponseBean(ChCardTransactionsResponseBean value) {
        this.chCardTransactionsResponseBean = value;
    }

}
