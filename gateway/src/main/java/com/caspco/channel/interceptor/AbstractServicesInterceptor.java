package com.caspco.channel.interceptor;

import com.caspco.channel.exceptions.ServiceException;
import com.caspco.channel.services.channel.CoreService;
import com.caspco.channel.swagger.DefinedRestServiceExtractor;
import com.caspco.channel.swagger.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

//import org.apache.axis.client.Stub;
//import org.apache.axis.transport.http.HTTPConstants;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/3/17
 * Time: 4:23 PM
 */


public class AbstractServicesInterceptor extends DynamicFilter {

    @Autowired
    private CoreService coreService;
    private Map<String, Method> methodMap;

    private @Autowired
    DefinedRestServiceExtractor definedRestServiceExtractor;
    private String prefixPath;

    public AbstractServicesInterceptor(Class<?> service) {
        try {
            methodMap = Arrays.stream(service.getDeclaredMethods())
                    .map(i -> Pair.of(i.getName(), i)).collect(Collectors.toMap(Pair::getLeft, Pair::getRight,
                            (u, u2) -> u)
                    );
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getPrefixPath() {
        return prefixPath;
    }

    public void setPrefixPath(String prefixPath) {
        this.prefixPath = prefixPath;
    }

    @Override
    public Object createResponse(HttpServletRequest request) throws IOException, ServletException, InvocationTargetException, IllegalAccessException {
        String requestURI = request.getRequestURI();
        if (!requestURI.startsWith(prefixPath) || definedRestServiceExtractor.getDefinedMethods().contains(requestURI)) {
            return SHOULD_NEXT;
        }

        String content = new String(FileCopyUtils.copyToByteArray(request.getInputStream()), StandardCharsets.UTF_8);

        String urlWithoutPrefix = requestURI.replace(prefixPath, "");
        String s = urlWithoutPrefix.substring(urlWithoutPrefix.lastIndexOf("/") + 1);
        Method method = methodMap.get(s);
        Parameter[] parameters = method.getParameters();
        Object methodParams = null;

        if (parameters != null) {
            if (parameters.length > 1) {
                throw new ServiceException("Service.invalid.parameter");
//                throw new InterceptionException("Method " + method.getName() + " in class " + method.getDeclaringClass().getName() + " should have one parameter");
            }
            if (parameters.length > 0) {
                Class<?> type = parameters[0].getType();
                methodParams = getObjectMapper().readValue(content, type);
            }
        }

        if (methodParams != null) {
            return method.invoke(coreService.getService(request), methodParams);
        } else {
            return method.invoke(coreService.getService(request));
        }
    }
}