
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for batchTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="batchTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="batchTransferRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBatchTransferRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "batchTransfer", propOrder = {
    "batchTransferRequestBean"
})
public class BatchTransfer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBatchTransferRequestBean batchTransferRequestBean;

    /**
     * Gets the value of the batchTransferRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBatchTransferRequestBean }
     *     
     */
    public ChBatchTransferRequestBean getBatchTransferRequestBean() {
        return batchTransferRequestBean;
    }

    /**
     * Sets the value of the batchTransferRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBatchTransferRequestBean }
     *     
     */
    public void setBatchTransferRequestBean(ChBatchTransferRequestBean value) {
        this.batchTransferRequestBean = value;
    }

}
