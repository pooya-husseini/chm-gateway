
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDefaultBillStatementResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDefaultBillStatementResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDefaultStatementResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDefaultStatementResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDefaultBillStatementResponse", propOrder = {
    "chDefaultStatementResponseBean"
})
public class GetDefaultBillStatementResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDefaultStatementResponseBean chDefaultStatementResponseBean;

    /**
     * Gets the value of the chDefaultStatementResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDefaultStatementResponseBean }
     *     
     */
    public ChDefaultStatementResponseBean getChDefaultStatementResponseBean() {
        return chDefaultStatementResponseBean;
    }

    /**
     * Sets the value of the chDefaultStatementResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDefaultStatementResponseBean }
     *     
     */
    public void setChDefaultStatementResponseBean(ChDefaultStatementResponseBean value) {
        this.chDefaultStatementResponseBean = value;
    }

}
