
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getMerchantInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getMerchantInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chMerchantInfoSearchRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chMerchantInfoSearchRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMerchantInfo", propOrder = {
    "chMerchantInfoSearchRequestBean"
})
public class GetMerchantInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChMerchantInfoSearchRequestBean chMerchantInfoSearchRequestBean;

    /**
     * Gets the value of the chMerchantInfoSearchRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChMerchantInfoSearchRequestBean }
     *     
     */
    public ChMerchantInfoSearchRequestBean getChMerchantInfoSearchRequestBean() {
        return chMerchantInfoSearchRequestBean;
    }

    /**
     * Sets the value of the chMerchantInfoSearchRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChMerchantInfoSearchRequestBean }
     *     
     */
    public void setChMerchantInfoSearchRequestBean(ChMerchantInfoSearchRequestBean value) {
        this.chMerchantInfoSearchRequestBean = value;
    }

}
