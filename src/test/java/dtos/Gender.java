/**
 * ChGender.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;

import java.util.HashMap;

@ApiModel
public enum Gender implements java.io.Serializable {

    MALE,
    FEMALE,
    LEGAL,
    PUBLIC;


    @JsonCreator
    public static Gender fromStringOperator(HashMap<String,String> map) {
        if (map != null) {
            for (Gender operator : Gender.values()) {
                if (map.get("value").equals( operator.name())) {
                    return operator;
                }
            }
        }
        return null;
    }

}