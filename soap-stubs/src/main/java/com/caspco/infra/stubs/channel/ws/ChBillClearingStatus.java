
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBillClearingStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chBillClearingStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FULL_SETTLED"/>
 *     &lt;enumeration value="PARTIAL_SETTLED"/>
 *     &lt;enumeration value="NO_SETTLED"/>
 *     &lt;enumeration value="FULL_SETTLED_STF"/>
 *     &lt;enumeration value="SETTLED_BY_LOAN"/>
 *     &lt;enumeration value="FULL_SETTLED_IN_NEXT_CYCLE"/>
 *     &lt;enumeration value="SETTLED_BY_SUSPENDING"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chBillClearingStatus")
@XmlEnum
public enum ChBillClearingStatus {

    FULL_SETTLED,
    PARTIAL_SETTLED,
    NO_SETTLED,
    FULL_SETTLED_STF,
    SETTLED_BY_LOAN,
    FULL_SETTLED_IN_NEXT_CYCLE,
    SETTLED_BY_SUSPENDING;

    public String value() {
        return name();
    }

    public static ChBillClearingStatus fromValue(String v) {
        return valueOf(v);
    }

}
