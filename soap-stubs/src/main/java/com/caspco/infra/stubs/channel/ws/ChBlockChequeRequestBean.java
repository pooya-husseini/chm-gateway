
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBlockChequeRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chBlockChequeRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="blockedReason" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBlockedChequeReasonType" minOccurs="0"/>
 *         &lt;element name="chequeNumbers" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="depositNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chBlockChequeRequestBean", propOrder = {
    "blockedReason",
    "chequeNumbers",
    "depositNumber"
})
public class ChBlockChequeRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlSchemaType(name = "string")
    protected ChBlockedChequeReasonType blockedReason;
    @XmlElement(nillable = true)
    protected List<String> chequeNumbers;
    protected String depositNumber;

    /**
     * Gets the value of the blockedReason property.
     * 
     * @return
     *     possible object is
     *     {@link ChBlockedChequeReasonType }
     *     
     */
    public ChBlockedChequeReasonType getBlockedReason() {
        return blockedReason;
    }

    /**
     * Sets the value of the blockedReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBlockedChequeReasonType }
     *     
     */
    public void setBlockedReason(ChBlockedChequeReasonType value) {
        this.blockedReason = value;
    }

    /**
     * Gets the value of the chequeNumbers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chequeNumbers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChequeNumbers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getChequeNumbers() {
        if (chequeNumbers == null) {
            chequeNumbers = new ArrayList<String>();
        }
        return this.chequeNumbers;
    }

    /**
     * Gets the value of the depositNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepositNumber() {
        return depositNumber;
    }

    /**
     * Sets the value of the depositNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepositNumber(String value) {
        this.depositNumber = value;
    }

}
