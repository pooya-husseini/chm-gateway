
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for chSearchCreditDossierRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chSearchCreditDossierRequestBean">
 *   &lt;complexContent>
 *     &lt;extension base="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardPaginationBean">
 *       &lt;sequence>
 *         &lt;element name="cif" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customer" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSearchCustomerBean" minOccurs="0"/>
 *         &lt;element name="defaultCard" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chSearchCardBean" minOccurs="0"/>
 *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fromAvailableFund" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="fromExpireDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fromOpenDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nationalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderDirection" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chOrderDirection" minOccurs="0"/>
 *         &lt;element name="orderField" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCreditDossierOrderField" minOccurs="0"/>
 *         &lt;element name="showTotalRecord" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="status" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCreditDossierStatus" minOccurs="0"/>
 *         &lt;element name="toAvailableFund" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="toExpireDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="toOpenDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chSearchCreditDossierRequestBean", propOrder = {
    "cif",
    "customer",
    "defaultCard",
    "firstName",
    "fromAvailableFund",
    "fromExpireDate",
    "fromOpenDate",
    "lastName",
    "nationalCode",
    "orderDirection",
    "orderField",
    "showTotalRecord",
    "status",
    "toAvailableFund",
    "toExpireDate",
    "toOpenDate"
})
public class ChSearchCreditDossierRequestBean
    extends ChCardPaginationBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String cif;
    protected ChSearchCustomerBean customer;
    protected ChSearchCardBean defaultCard;
    protected String firstName;
    protected BigDecimal fromAvailableFund;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date fromExpireDate;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date fromOpenDate;
    protected String lastName;
    protected String nationalCode;
    @XmlSchemaType(name = "string")
    protected ChOrderDirection orderDirection;
    @XmlSchemaType(name = "string")
    protected ChCreditDossierOrderField orderField;
    protected boolean showTotalRecord;
    @XmlSchemaType(name = "string")
    protected ChCreditDossierStatus status;
    protected BigDecimal toAvailableFund;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date toExpireDate;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date toOpenDate;

    /**
     * Gets the value of the cif property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCif() {
        return cif;
    }

    /**
     * Sets the value of the cif property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCif(String value) {
        this.cif = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link ChSearchCustomerBean }
     *     
     */
    public ChSearchCustomerBean getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSearchCustomerBean }
     *     
     */
    public void setCustomer(ChSearchCustomerBean value) {
        this.customer = value;
    }

    /**
     * Gets the value of the defaultCard property.
     * 
     * @return
     *     possible object is
     *     {@link ChSearchCardBean }
     *     
     */
    public ChSearchCardBean getDefaultCard() {
        return defaultCard;
    }

    /**
     * Sets the value of the defaultCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChSearchCardBean }
     *     
     */
    public void setDefaultCard(ChSearchCardBean value) {
        this.defaultCard = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the fromAvailableFund property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFromAvailableFund() {
        return fromAvailableFund;
    }

    /**
     * Sets the value of the fromAvailableFund property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFromAvailableFund(BigDecimal value) {
        this.fromAvailableFund = value;
    }

    /**
     * Gets the value of the fromExpireDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getFromExpireDate() {
        return fromExpireDate;
    }

    /**
     * Sets the value of the fromExpireDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromExpireDate(Date value) {
        this.fromExpireDate = value;
    }

    /**
     * Gets the value of the fromOpenDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getFromOpenDate() {
        return fromOpenDate;
    }

    /**
     * Sets the value of the fromOpenDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromOpenDate(Date value) {
        this.fromOpenDate = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the nationalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalCode() {
        return nationalCode;
    }

    /**
     * Sets the value of the nationalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalCode(String value) {
        this.nationalCode = value;
    }

    /**
     * Gets the value of the orderDirection property.
     * 
     * @return
     *     possible object is
     *     {@link ChOrderDirection }
     *     
     */
    public ChOrderDirection getOrderDirection() {
        return orderDirection;
    }

    /**
     * Sets the value of the orderDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChOrderDirection }
     *     
     */
    public void setOrderDirection(ChOrderDirection value) {
        this.orderDirection = value;
    }

    /**
     * Gets the value of the orderField property.
     * 
     * @return
     *     possible object is
     *     {@link ChCreditDossierOrderField }
     *     
     */
    public ChCreditDossierOrderField getOrderField() {
        return orderField;
    }

    /**
     * Sets the value of the orderField property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCreditDossierOrderField }
     *     
     */
    public void setOrderField(ChCreditDossierOrderField value) {
        this.orderField = value;
    }

    /**
     * Gets the value of the showTotalRecord property.
     * 
     */
    public boolean isShowTotalRecord() {
        return showTotalRecord;
    }

    /**
     * Sets the value of the showTotalRecord property.
     * 
     */
    public void setShowTotalRecord(boolean value) {
        this.showTotalRecord = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link ChCreditDossierStatus }
     *     
     */
    public ChCreditDossierStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCreditDossierStatus }
     *     
     */
    public void setStatus(ChCreditDossierStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the toAvailableFund property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getToAvailableFund() {
        return toAvailableFund;
    }

    /**
     * Sets the value of the toAvailableFund property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setToAvailableFund(BigDecimal value) {
        this.toAvailableFund = value;
    }

    /**
     * Gets the value of the toExpireDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getToExpireDate() {
        return toExpireDate;
    }

    /**
     * Sets the value of the toExpireDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToExpireDate(Date value) {
        this.toExpireDate = value;
    }

    /**
     * Gets the value of the toOpenDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getToOpenDate() {
        return toOpenDate;
    }

    /**
     * Sets the value of the toOpenDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToOpenDate(Date value) {
        this.toOpenDate = value;
    }

}
