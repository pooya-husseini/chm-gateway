
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBillSettingRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chBillSettingRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="billPageSize" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="defaultBillStatementType" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDefaultBillStatementType" minOccurs="0"/>
 *         &lt;element name="defaultLastN" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="fields" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBillField" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="order" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chOrderDirection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chBillSettingRequestBean", propOrder = {
    "billPageSize",
    "defaultBillStatementType",
    "defaultLastN",
    "fields",
    "order"
})
public class ChBillSettingRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected Short billPageSize;
    @XmlSchemaType(name = "string")
    protected ChDefaultBillStatementType defaultBillStatementType;
    protected Short defaultLastN;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "string")
    protected List<ChBillField> fields;
    @XmlSchemaType(name = "string")
    protected ChOrderDirection order;

    /**
     * Gets the value of the billPageSize property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getBillPageSize() {
        return billPageSize;
    }

    /**
     * Sets the value of the billPageSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setBillPageSize(Short value) {
        this.billPageSize = value;
    }

    /**
     * Gets the value of the defaultBillStatementType property.
     * 
     * @return
     *     possible object is
     *     {@link ChDefaultBillStatementType }
     *     
     */
    public ChDefaultBillStatementType getDefaultBillStatementType() {
        return defaultBillStatementType;
    }

    /**
     * Sets the value of the defaultBillStatementType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDefaultBillStatementType }
     *     
     */
    public void setDefaultBillStatementType(ChDefaultBillStatementType value) {
        this.defaultBillStatementType = value;
    }

    /**
     * Gets the value of the defaultLastN property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getDefaultLastN() {
        return defaultLastN;
    }

    /**
     * Sets the value of the defaultLastN property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setDefaultLastN(Short value) {
        this.defaultLastN = value;
    }

    /**
     * Gets the value of the fields property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fields property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFields().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChBillField }
     * 
     * 
     */
    public List<ChBillField> getFields() {
        if (fields == null) {
            fields = new ArrayList<ChBillField>();
        }
        return this.fields;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link ChOrderDirection }
     *     
     */
    public ChOrderDirection getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChOrderDirection }
     *     
     */
    public void setOrder(ChOrderDirection value) {
        this.order = value;
    }

}
