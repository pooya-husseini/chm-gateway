
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for naturalPersonOpenSingleDeposit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="naturalPersonOpenSingleDeposit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chNaturalPersonOpenSingleDepositRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chNaturalPersonOpenSingleDepositRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "naturalPersonOpenSingleDeposit", propOrder = {
    "chNaturalPersonOpenSingleDepositRequestBean"
})
public class NaturalPersonOpenSingleDeposit
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChNaturalPersonOpenSingleDepositRequestBean chNaturalPersonOpenSingleDepositRequestBean;

    /**
     * Gets the value of the chNaturalPersonOpenSingleDepositRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChNaturalPersonOpenSingleDepositRequestBean }
     *     
     */
    public ChNaturalPersonOpenSingleDepositRequestBean getChNaturalPersonOpenSingleDepositRequestBean() {
        return chNaturalPersonOpenSingleDepositRequestBean;
    }

    /**
     * Sets the value of the chNaturalPersonOpenSingleDepositRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChNaturalPersonOpenSingleDepositRequestBean }
     *     
     */
    public void setChNaturalPersonOpenSingleDepositRequestBean(ChNaturalPersonOpenSingleDepositRequestBean value) {
        this.chNaturalPersonOpenSingleDepositRequestBean = value;
    }

}
