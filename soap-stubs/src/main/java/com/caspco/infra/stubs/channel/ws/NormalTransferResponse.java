
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for normalTransferResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="normalTransferResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chNormalTransferResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chNormalTransferResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "normalTransferResponse", propOrder = {
    "chNormalTransferResponseBean"
})
public class NormalTransferResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChNormalTransferResponseBean chNormalTransferResponseBean;

    /**
     * Gets the value of the chNormalTransferResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChNormalTransferResponseBean }
     *     
     */
    public ChNormalTransferResponseBean getChNormalTransferResponseBean() {
        return chNormalTransferResponseBean;
    }

    /**
     * Sets the value of the chNormalTransferResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChNormalTransferResponseBean }
     *     
     */
    public void setChNormalTransferResponseBean(ChNormalTransferResponseBean value) {
        this.chNormalTransferResponseBean = value;
    }

}
