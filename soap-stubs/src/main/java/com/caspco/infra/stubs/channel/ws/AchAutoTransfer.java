
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for achAutoTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="achAutoTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chAutoAchTransferRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAutoAchTransferRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "achAutoTransfer", propOrder = {
    "chAutoAchTransferRequestBean"
})
public class AchAutoTransfer
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChAutoAchTransferRequestBean chAutoAchTransferRequestBean;

    /**
     * Gets the value of the chAutoAchTransferRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAutoAchTransferRequestBean }
     *     
     */
    public ChAutoAchTransferRequestBean getChAutoAchTransferRequestBean() {
        return chAutoAchTransferRequestBean;
    }

    /**
     * Sets the value of the chAutoAchTransferRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAutoAchTransferRequestBean }
     *     
     */
    public void setChAutoAchTransferRequestBean(ChAutoAchTransferRequestBean value) {
        this.chAutoAchTransferRequestBean = value;
    }

}
