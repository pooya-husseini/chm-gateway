
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for customerExistenceInquiry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="customerExistenceInquiry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCustomerExistenceInquiryRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCustomerExistenceInquiryRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "customerExistenceInquiry", propOrder = {
    "chCustomerExistenceInquiryRequestBean"
})
public class CustomerExistenceInquiry
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCustomerExistenceInquiryRequestBean chCustomerExistenceInquiryRequestBean;

    /**
     * Gets the value of the chCustomerExistenceInquiryRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCustomerExistenceInquiryRequestBean }
     *     
     */
    public ChCustomerExistenceInquiryRequestBean getChCustomerExistenceInquiryRequestBean() {
        return chCustomerExistenceInquiryRequestBean;
    }

    /**
     * Sets the value of the chCustomerExistenceInquiryRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCustomerExistenceInquiryRequestBean }
     *     
     */
    public void setChCustomerExistenceInquiryRequestBean(ChCustomerExistenceInquiryRequestBean value) {
        this.chCustomerExistenceInquiryRequestBean = value;
    }

}
