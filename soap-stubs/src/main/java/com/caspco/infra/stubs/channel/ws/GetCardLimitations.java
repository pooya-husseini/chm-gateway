
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardLimitations complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardLimitations">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardLimitRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardLimitRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardLimitations", propOrder = {
    "chCardLimitRequestBean"
})
public class GetCardLimitations
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardLimitRequestBean chCardLimitRequestBean;

    /**
     * Gets the value of the chCardLimitRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardLimitRequestBean }
     *     
     */
    public ChCardLimitRequestBean getChCardLimitRequestBean() {
        return chCardLimitRequestBean;
    }

    /**
     * Sets the value of the chCardLimitRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardLimitRequestBean }
     *     
     */
    public void setChCardLimitRequestBean(ChCardLimitRequestBean value) {
        this.chCardLimitRequestBean = value;
    }

}
