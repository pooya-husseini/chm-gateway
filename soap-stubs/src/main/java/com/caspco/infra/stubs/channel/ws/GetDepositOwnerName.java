
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDepositOwnerName complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDepositOwnerName">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chDepositOwnerRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chDepositOwnerRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDepositOwnerName", propOrder = {
    "chDepositOwnerRequestBean"
})
public class GetDepositOwnerName
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChDepositOwnerRequestBean chDepositOwnerRequestBean;

    /**
     * Gets the value of the chDepositOwnerRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChDepositOwnerRequestBean }
     *     
     */
    public ChDepositOwnerRequestBean getChDepositOwnerRequestBean() {
        return chDepositOwnerRequestBean;
    }

    /**
     * Sets the value of the chDepositOwnerRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChDepositOwnerRequestBean }
     *     
     */
    public void setChDepositOwnerRequestBean(ChDepositOwnerRequestBean value) {
        this.chDepositOwnerRequestBean = value;
    }

}
