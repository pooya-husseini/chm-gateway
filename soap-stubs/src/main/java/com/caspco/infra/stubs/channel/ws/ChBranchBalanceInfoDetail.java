
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chBranchBalanceInfoDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chBranchBalanceInfoDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valuableItem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valuableItemType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valuableItemTypeCount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chBranchBalanceInfoDetail", propOrder = {
    "amount",
    "currency",
    "valuableItem",
    "valuableItemType",
    "valuableItemTypeCount"
})
public class ChBranchBalanceInfoDetail
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigDecimal amount;
    protected String currency;
    protected String valuableItem;
    protected String valuableItemType;
    protected Long valuableItemTypeCount;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the valuableItem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValuableItem() {
        return valuableItem;
    }

    /**
     * Sets the value of the valuableItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValuableItem(String value) {
        this.valuableItem = value;
    }

    /**
     * Gets the value of the valuableItemType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValuableItemType() {
        return valuableItemType;
    }

    /**
     * Sets the value of the valuableItemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValuableItemType(String value) {
        this.valuableItemType = value;
    }

    /**
     * Gets the value of the valuableItemTypeCount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getValuableItemTypeCount() {
        return valuableItemTypeCount;
    }

    /**
     * Sets the value of the valuableItemTypeCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setValuableItemTypeCount(Long value) {
        this.valuableItemTypeCount = value;
    }

}
