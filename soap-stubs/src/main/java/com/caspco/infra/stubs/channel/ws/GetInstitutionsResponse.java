
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getInstitutionsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getInstitutionsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="depositNo" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chInstitutionsResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInstitutionsResponse", propOrder = {
    "depositNo"
})
public class GetInstitutionsResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChInstitutionsResponseBean depositNo;

    /**
     * Gets the value of the depositNo property.
     * 
     * @return
     *     possible object is
     *     {@link ChInstitutionsResponseBean }
     *     
     */
    public ChInstitutionsResponseBean getDepositNo() {
        return depositNo;
    }

    /**
     * Sets the value of the depositNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChInstitutionsResponseBean }
     *     
     */
    public void setDepositNo(ChInstitutionsResponseBean value) {
        this.depositNo = value;
    }

}
