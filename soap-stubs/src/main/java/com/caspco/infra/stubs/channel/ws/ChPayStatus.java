
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chPayStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chPayStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PAID"/>
 *     &lt;enumeration value="NOT_PAID_BEFORE_MATURITY"/>
 *     &lt;enumeration value="NOT_PAID_AFTER_MATURITY"/>
 *     &lt;enumeration value="UNKNOWN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chPayStatus")
@XmlEnum
public enum ChPayStatus {

    PAID,
    NOT_PAID_BEFORE_MATURITY,
    NOT_PAID_AFTER_MATURITY,
    UNKNOWN;

    public String value() {
        return name();
    }

    public static ChPayStatus fromValue(String v) {
        return valueOf(v);
    }

}
