/**
 * ChAchTransferSummeryBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import java.util.List;

public class AchTransferSummeryDto  implements java.io.Serializable {

    private java.lang.String currency;

    private List<BriefAchTransactionDto> transactions;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<BriefAchTransactionDto> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<BriefAchTransactionDto> transactions) {
        this.transactions = transactions;
    }
}
