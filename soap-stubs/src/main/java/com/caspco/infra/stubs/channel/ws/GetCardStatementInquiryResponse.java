
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardStatementInquiryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardStatementInquiryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chCardStatementResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCardStatementResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardStatementInquiryResponse", propOrder = {
    "chCardStatementResponseBean"
})
public class GetCardStatementInquiryResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCardStatementResponseBean chCardStatementResponseBean;

    /**
     * Gets the value of the chCardStatementResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCardStatementResponseBean }
     *     
     */
    public ChCardStatementResponseBean getChCardStatementResponseBean() {
        return chCardStatementResponseBean;
    }

    /**
     * Sets the value of the chCardStatementResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCardStatementResponseBean }
     *     
     */
    public void setChCardStatementResponseBean(ChCardStatementResponseBean value) {
        this.chCardStatementResponseBean = value;
    }

}
