
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for extendSMSSettingResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="extendSMSSettingResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chUserSMSSettingResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chUserSMSSettingResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "extendSMSSettingResponse", propOrder = {
    "chUserSMSSettingResponseBean"
})
public class ExtendSMSSettingResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChUserSMSSettingResponseBean chUserSMSSettingResponseBean;

    /**
     * Gets the value of the chUserSMSSettingResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChUserSMSSettingResponseBean }
     *     
     */
    public ChUserSMSSettingResponseBean getChUserSMSSettingResponseBean() {
        return chUserSMSSettingResponseBean;
    }

    /**
     * Sets the value of the chUserSMSSettingResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChUserSMSSettingResponseBean }
     *     
     */
    public void setChUserSMSSettingResponseBean(ChUserSMSSettingResponseBean value) {
        this.chUserSMSSettingResponseBean = value;
    }

}
