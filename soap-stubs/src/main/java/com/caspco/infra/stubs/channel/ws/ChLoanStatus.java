
package com.caspco.infra.stubs.channel.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chLoanStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="chLoanStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UNPAID"/>
 *     &lt;enumeration value="PAID_INCOMPLETE"/>
 *     &lt;enumeration value="ACTIVE"/>
 *     &lt;enumeration value="DOUBTFUL_RECEIPT"/>
 *     &lt;enumeration value="SETTLEMENT_READY"/>
 *     &lt;enumeration value="FREE"/>
 *     &lt;enumeration value="OTHER"/>
 *     &lt;enumeration value="INCOMPLETE_COLLATERAL"/>
 *     &lt;enumeration value="ARREARS"/>
 *     &lt;enumeration value="PAST_DUE"/>
 *     &lt;enumeration value="CURRENT_DUE"/>
 *     &lt;enumeration value="REJECTED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "chLoanStatus")
@XmlEnum
public enum ChLoanStatus {

    UNPAID,
    PAID_INCOMPLETE,
    ACTIVE,
    DOUBTFUL_RECEIPT,
    SETTLEMENT_READY,
    FREE,
    OTHER,
    INCOMPLETE_COLLATERAL,
    ARREARS,
    PAST_DUE,
    CURRENT_DUE,
    REJECTED;

    public String value() {
        return name();
    }

    public static ChLoanStatus fromValue(String v) {
        return valueOf(v);
    }

}
