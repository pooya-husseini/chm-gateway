
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chServiceConstraintInformationResponseBeans complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chServiceConstraintInformationResponseBeans">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceConstraintInformationBeans" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chServiceConstraintInformationBean" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chServiceConstraintInformationResponseBeans", propOrder = {
    "serviceConstraintInformationBeans"
})
public class ChServiceConstraintInformationResponseBeans
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(nillable = true)
    protected List<ChServiceConstraintInformationBean> serviceConstraintInformationBeans;

    /**
     * Gets the value of the serviceConstraintInformationBeans property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceConstraintInformationBeans property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceConstraintInformationBeans().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChServiceConstraintInformationBean }
     * 
     * 
     */
    public List<ChServiceConstraintInformationBean> getServiceConstraintInformationBeans() {
        if (serviceConstraintInformationBeans == null) {
            serviceConstraintInformationBeans = new ArrayList<ChServiceConstraintInformationBean>();
        }
        return this.serviceConstraintInformationBeans;
    }

}
