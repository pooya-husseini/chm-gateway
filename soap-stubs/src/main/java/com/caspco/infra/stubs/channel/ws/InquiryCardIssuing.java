
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for inquiryCardIssuing complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="inquiryCardIssuing">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chInquiryCardIssuingRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chInquiryCardIssuingRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "inquiryCardIssuing", propOrder = {
    "chInquiryCardIssuingRequestBean"
})
public class InquiryCardIssuing
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChInquiryCardIssuingRequestBean chInquiryCardIssuingRequestBean;

    /**
     * Gets the value of the chInquiryCardIssuingRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChInquiryCardIssuingRequestBean }
     *     
     */
    public ChInquiryCardIssuingRequestBean getChInquiryCardIssuingRequestBean() {
        return chInquiryCardIssuingRequestBean;
    }

    /**
     * Sets the value of the chInquiryCardIssuingRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChInquiryCardIssuingRequestBean }
     *     
     */
    public void setChInquiryCardIssuingRequestBean(ChInquiryCardIssuingRequestBean value) {
        this.chInquiryCardIssuingRequestBean = value;
    }

}
