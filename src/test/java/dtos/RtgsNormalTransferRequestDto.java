/**
 * ChRtgsNormalTransferRequestBean.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class RtgsNormalTransferRequestDto implements java.io.Serializable {

    private java.math.BigDecimal amount;

    @ApiModelProperty(value = "secondPasswordType")
    private com.caspco.channel.dtos.SecondPasswordType chSecondPasswordType;

    private java.lang.String cif;

    private java.lang.String description;

    private java.lang.String destinationIbanNumber;

    private java.lang.String email;

    private java.lang.String factorNumber;

    private java.lang.String receiverFamily;

    private java.lang.String receiverName;

    private java.lang.String receiverTelephoneNumber;

    private java.lang.String secondPassword;

    private java.lang.String sourceDepositNumber;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public SecondPasswordType getChSecondPasswordType() {
        return chSecondPasswordType;
    }

    public void setChSecondPasswordType(SecondPasswordType chSecondPasswordType) {
        this.chSecondPasswordType = chSecondPasswordType;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDestinationIbanNumber() {
        return destinationIbanNumber;
    }

    public void setDestinationIbanNumber(String destinationIbanNumber) {
        this.destinationIbanNumber = destinationIbanNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFactorNumber() {
        return factorNumber;
    }

    public void setFactorNumber(String factorNumber) {
        this.factorNumber = factorNumber;
    }

    public String getReceiverFamily() {
        return receiverFamily;
    }

    public void setReceiverFamily(String receiverFamily) {
        this.receiverFamily = receiverFamily;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverTelephoneNumber() {
        return receiverTelephoneNumber;
    }

    public void setReceiverTelephoneNumber(String receiverTelephoneNumber) {
        this.receiverTelephoneNumber = receiverTelephoneNumber;
    }

    public String getSecondPassword() {
        return secondPassword;
    }

    public void setSecondPassword(String secondPassword) {
        this.secondPassword = secondPassword;
    }

    public String getSourceDepositNumber() {
        return sourceDepositNumber;
    }

    public void setSourceDepositNumber(String sourceDepositNumber) {
        this.sourceDepositNumber = sourceDepositNumber;
    }
}
