
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCardIssuingAllowedDepositsToOpenRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chCardIssuingAllowedDepositsToOpenRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardProductCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mainAccountRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chCardIssuingAllowedDepositsToOpenRequestBean", propOrder = {
    "cardProductCode",
    "customerNumber",
    "mainAccountRequired"
})
public class ChCardIssuingAllowedDepositsToOpenRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String cardProductCode;
    protected String customerNumber;
    protected boolean mainAccountRequired;

    /**
     * Gets the value of the cardProductCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardProductCode() {
        return cardProductCode;
    }

    /**
     * Sets the value of the cardProductCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardProductCode(String value) {
        this.cardProductCode = value;
    }

    /**
     * Gets the value of the customerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    /**
     * Sets the value of the customerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerNumber(String value) {
        this.customerNumber = value;
    }

    /**
     * Gets the value of the mainAccountRequired property.
     * 
     */
    public boolean isMainAccountRequired() {
        return mainAccountRequired;
    }

    /**
     * Sets the value of the mainAccountRequired property.
     * 
     */
    public void setMainAccountRequired(boolean value) {
        this.mainAccountRequired = value;
    }

}
