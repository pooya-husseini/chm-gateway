/**
 * ChDepositGroupType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.HashMap;

public @io.swagger.annotations.ApiModel enum DepositGroupType implements java.io.Serializable {
    JARI_ACCOUNT,
    SPECIAL_LONG_ACCOUNT,
    OTHERS,
    EDARE_SHODE,
    ANDOKHTE_ACCOUNT,
    SPECIAL_SHORT_ACCOUNT,
    PASANDAZ,
    SHORT_ACCOUNT,
    LONG_ACCOUNT;


    @JsonCreator
    public static DepositGroupType fromStringOperator(HashMap<String, String> map) {
        if (map != null) {
            for (DepositGroupType operator : DepositGroupType.values()) {
                if (map.get("value").equals(operator.name())) {
                    return operator;
                }
            }
        }
        return null;
    }
}
