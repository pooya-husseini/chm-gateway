
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFavoriteAccountSettingResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFavoriteAccountSettingResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chFavoriteDepositNumberResponseBeans" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chFavoriteDepositNumberResponseBeans" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFavoriteAccountSettingResponse", propOrder = {
    "chFavoriteDepositNumberResponseBeans"
})
public class GetFavoriteAccountSettingResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChFavoriteDepositNumberResponseBeans chFavoriteDepositNumberResponseBeans;

    /**
     * Gets the value of the chFavoriteDepositNumberResponseBeans property.
     * 
     * @return
     *     possible object is
     *     {@link ChFavoriteDepositNumberResponseBeans }
     *     
     */
    public ChFavoriteDepositNumberResponseBeans getChFavoriteDepositNumberResponseBeans() {
        return chFavoriteDepositNumberResponseBeans;
    }

    /**
     * Sets the value of the chFavoriteDepositNumberResponseBeans property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChFavoriteDepositNumberResponseBeans }
     *     
     */
    public void setChFavoriteDepositNumberResponseBeans(ChFavoriteDepositNumberResponseBeans value) {
        this.chFavoriteDepositNumberResponseBeans = value;
    }

}
