package com.caspco.channel.exceptions;

import java.util.Map;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/26/17
 * Time: 3:48 PM
 */
public class ServiceException extends OpenApiException {

    public ServiceException(String message, Map<String,?> args) {
        super(message, args);
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(ExceptionDescription description) {
        super(description);
    }

    public ServiceException(String message, ExceptionDescription description) {
        super(message, description);
    }

    public ServiceException(String message, Throwable cause, ExceptionDescription description) {
        super(message, cause, description);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause, ExceptionDescription description) {
        super(cause, description);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ExceptionDescription description) {
        super(message, cause, enableSuppression, writableStackTrace, description);
    }
}
