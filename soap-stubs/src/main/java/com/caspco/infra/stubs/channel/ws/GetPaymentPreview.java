
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPaymentPreview complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPaymentPreview">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chPaymentPreviewRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chPaymentPreviewRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPaymentPreview", propOrder = {
    "chPaymentPreviewRequestBean"
})
public class GetPaymentPreview
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChPaymentPreviewRequestBean chPaymentPreviewRequestBean;

    /**
     * Gets the value of the chPaymentPreviewRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChPaymentPreviewRequestBean }
     *     
     */
    public ChPaymentPreviewRequestBean getChPaymentPreviewRequestBean() {
        return chPaymentPreviewRequestBean;
    }

    /**
     * Sets the value of the chPaymentPreviewRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChPaymentPreviewRequestBean }
     *     
     */
    public void setChPaymentPreviewRequestBean(ChPaymentPreviewRequestBean value) {
        this.chPaymentPreviewRequestBean = value;
    }

}
