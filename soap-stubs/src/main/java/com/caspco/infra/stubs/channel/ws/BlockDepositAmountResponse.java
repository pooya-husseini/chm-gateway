
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for blockDepositAmountResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="blockDepositAmountResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBlockAmountDepositResponseBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chCoreBlockDepositAmountResponseBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "blockDepositAmountResponse", propOrder = {
    "chBlockAmountDepositResponseBean"
})
public class BlockDepositAmountResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChCoreBlockDepositAmountResponseBean chBlockAmountDepositResponseBean;

    /**
     * Gets the value of the chBlockAmountDepositResponseBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChCoreBlockDepositAmountResponseBean }
     *     
     */
    public ChCoreBlockDepositAmountResponseBean getChBlockAmountDepositResponseBean() {
        return chBlockAmountDepositResponseBean;
    }

    /**
     * Sets the value of the chBlockAmountDepositResponseBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChCoreBlockDepositAmountResponseBean }
     *     
     */
    public void setChBlockAmountDepositResponseBean(ChCoreBlockDepositAmountResponseBean value) {
        this.chBlockAmountDepositResponseBean = value;
    }

}
