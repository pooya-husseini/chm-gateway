/**
 * ChDestinationTransactionStatus.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.caspco.channel.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.HashMap;

public @io.swagger.annotations.ApiModel
enum DestinationTransactionStatus implements java.io.Serializable {
    ACCEPTED,
    ERROR_DUPLICATE,
    ERROR_GENERAL;

    @JsonCreator
    public static DestinationTransactionStatus fromStringOperator(HashMap<String, String> map) {
        if (map != null) {
            for (DestinationTransactionStatus operator : DestinationTransactionStatus.values()) {
                if (map.get("value").equals(operator.name())) {
                    return operator;
                }
            }
        }
        return null;
    }

}
