
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for payBatchBill complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="payBatchBill">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chBatchBillPaymentRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chBatchBillPaymentRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "payBatchBill", propOrder = {
    "chBatchBillPaymentRequestBean"
})
public class PayBatchBill
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChBatchBillPaymentRequestBean chBatchBillPaymentRequestBean;

    /**
     * Gets the value of the chBatchBillPaymentRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChBatchBillPaymentRequestBean }
     *     
     */
    public ChBatchBillPaymentRequestBean getChBatchBillPaymentRequestBean() {
        return chBatchBillPaymentRequestBean;
    }

    /**
     * Sets the value of the chBatchBillPaymentRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChBatchBillPaymentRequestBean }
     *     
     */
    public void setChBatchBillPaymentRequestBean(ChBatchBillPaymentRequestBean value) {
        this.chBatchBillPaymentRequestBean = value;
    }

}
