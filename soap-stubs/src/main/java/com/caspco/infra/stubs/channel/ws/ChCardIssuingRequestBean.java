
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for chCardIssuingRequestBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="chCardIssuingRequestBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardBranchCodeIssuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardLinkedAccounts" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="cardMainAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardProductCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payrollAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="referenceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chCardIssuingRequestBean", propOrder = {
    "cardBranchCodeIssuer",
    "cardLinkedAccounts",
    "cardMainAccount",
    "cardProductCode",
    "customerNumber",
    "payrollAccount",
    "referenceCode"
})
public class ChCardIssuingRequestBean
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String cardBranchCodeIssuer;
    @XmlElement(nillable = true)
    protected List<String> cardLinkedAccounts;
    protected String cardMainAccount;
    protected String cardProductCode;
    protected String customerNumber;
    protected String payrollAccount;
    protected String referenceCode;

    /**
     * Gets the value of the cardBranchCodeIssuer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardBranchCodeIssuer() {
        return cardBranchCodeIssuer;
    }

    /**
     * Sets the value of the cardBranchCodeIssuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardBranchCodeIssuer(String value) {
        this.cardBranchCodeIssuer = value;
    }

    /**
     * Gets the value of the cardLinkedAccounts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cardLinkedAccounts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCardLinkedAccounts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCardLinkedAccounts() {
        if (cardLinkedAccounts == null) {
            cardLinkedAccounts = new ArrayList<String>();
        }
        return this.cardLinkedAccounts;
    }

    /**
     * Gets the value of the cardMainAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardMainAccount() {
        return cardMainAccount;
    }

    /**
     * Sets the value of the cardMainAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardMainAccount(String value) {
        this.cardMainAccount = value;
    }

    /**
     * Gets the value of the cardProductCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardProductCode() {
        return cardProductCode;
    }

    /**
     * Sets the value of the cardProductCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardProductCode(String value) {
        this.cardProductCode = value;
    }

    /**
     * Gets the value of the customerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    /**
     * Sets the value of the customerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerNumber(String value) {
        this.customerNumber = value;
    }

    /**
     * Gets the value of the payrollAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayrollAccount() {
        return payrollAccount;
    }

    /**
     * Sets the value of the payrollAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayrollAccount(String value) {
        this.payrollAccount = value;
    }

    /**
     * Gets the value of the referenceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceCode() {
        return referenceCode;
    }

    /**
     * Sets the value of the referenceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceCode(String value) {
        this.referenceCode = value;
    }

}
