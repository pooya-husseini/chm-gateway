
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for inquiryNaturalPersonOpenSingleDeposit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="inquiryNaturalPersonOpenSingleDeposit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chInquiryNaturalPersonOpenSingleDepositRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chInquiryNaturalPersonOpenSingleDepositRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "inquiryNaturalPersonOpenSingleDeposit", propOrder = {
    "chInquiryNaturalPersonOpenSingleDepositRequestBean"
})
public class InquiryNaturalPersonOpenSingleDeposit
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected ChInquiryNaturalPersonOpenSingleDepositRequestBean chInquiryNaturalPersonOpenSingleDepositRequestBean;

    /**
     * Gets the value of the chInquiryNaturalPersonOpenSingleDepositRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChInquiryNaturalPersonOpenSingleDepositRequestBean }
     *     
     */
    public ChInquiryNaturalPersonOpenSingleDepositRequestBean getChInquiryNaturalPersonOpenSingleDepositRequestBean() {
        return chInquiryNaturalPersonOpenSingleDepositRequestBean;
    }

    /**
     * Sets the value of the chInquiryNaturalPersonOpenSingleDepositRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChInquiryNaturalPersonOpenSingleDepositRequestBean }
     *     
     */
    public void setChInquiryNaturalPersonOpenSingleDepositRequestBean(ChInquiryNaturalPersonOpenSingleDepositRequestBean value) {
        this.chInquiryNaturalPersonOpenSingleDepositRequestBean = value;
    }

}
