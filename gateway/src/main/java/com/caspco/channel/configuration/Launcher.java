package com.caspco.channel.configuration;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.rmi.RemoteException;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 8/15/17
 * Time: 12:43 PM
 */

@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class
})
@EnableSwagger2
@ComponentScan(basePackages = "com.caspco.channel")
@EnableCaching
@PropertySource("classpath:application.properties")
@PropertySource("classpath:gateway-jms.properties")
@ImportResource("classpath:applicationContext-gateway-spi.xml")
public class Launcher implements ApplicationRunner {
//    @Autowired
//    private JmsService jmsService;
    public static void main(String[] args) throws RemoteException {
        SpringApplication.run(Launcher.class, args);
    }
//    @Bean
//    public Filter logFilter() throws IOException {
//        LoggerFilter filter = new LoggerFilter();
//        filter.setIncludeQueryString(true);
//        filter.setIncludePayload(true);
//        filter.setMaxPayloadLength(5120);
//        return filter;
//    }
//
//    @Bean
//    public JndiTemplate jndiTemplate(@Value("${core.jms.url}") String jmsUrl) {
//        JndiTemplate template = new JndiTemplate();
//        Properties properties = new Properties();
//        properties.setProperty("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
//        properties.setProperty("java.naming.provider.url", jmsUrl);
//        template.setEnvironment(properties);
//        return template;
//    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
//        String id = jmsService.send("ECHO", RequestType.INQUIRY, new Echo.Inbound());
//        Echo.Outbound outbound = jmsService.receive(id, Echo.Outbound.class);
//        System.out.println(outbound);
//        ChUserInfoRequestBean chUserInfoRequestBean = new ChUserInfoRequestBean();
//        chUserInfoRequestBean.setUsername("rezaam");
//        chUserInfoRequestBean.setPassword("57380099");
//
//        LoginStaticMsg.Inbound loginInbound = new LoginStaticMsg.Inbound();
//        loginInbound.setChUserInfoRequestBean(chUserInfoRequestBean);
//
//        LoginStaticMsg.Outbound execute1 = provider.execute( loginInbound, LoginStaticMsg.Outbound.class);
//        String sessionId = execute1.getChLoginResponseBean().getSessionId();
//        System.out.println(execute1.getChLoginResponseBean().getSessionId());
//
//        CardLessIssueVoucherMsg.Inbound msg = new CardLessIssueVoucherMsg.Inbound();
//        ChCardLessIssueVoucherRequestBean bean = new ChCardLessIssueVoucherRequestBean();
//        bean.setAccountNumber("003000000042008");
//        bean.setAmount(BigDecimal.valueOf(500000));
//        bean.setMobileNumber("09125168150");
//        bean.setDescription("");
//
//        bean.setChSecondPasswordType(ChSecondPasswordType.TRANSFER_SECOND_PASS);
//        bean.setEmail("");
//
//
//        msg.setRequestBean(bean);
//
//        CardLessIssueVoucherMsg.Outbound execute = provider.execute(msg, CardLessIssueVoucherMsg.Outbound.class,sessionId);
//        System.out.println(execute);
    }
}