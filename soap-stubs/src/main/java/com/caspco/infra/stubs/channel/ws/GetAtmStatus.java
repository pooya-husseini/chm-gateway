
package com.caspco.infra.stubs.channel.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAtmStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAtmStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChAtmStatusRequestBean" type="{http://corebankingservice.endpoint.webservicegateway.core.channelmanager.caspian.com/}chAtmStatusRequestBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAtmStatus", propOrder = {
    "chAtmStatusRequestBean"
})
public class GetAtmStatus
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ChAtmStatusRequestBean")
    protected ChAtmStatusRequestBean chAtmStatusRequestBean;

    /**
     * Gets the value of the chAtmStatusRequestBean property.
     * 
     * @return
     *     possible object is
     *     {@link ChAtmStatusRequestBean }
     *     
     */
    public ChAtmStatusRequestBean getChAtmStatusRequestBean() {
        return chAtmStatusRequestBean;
    }

    /**
     * Sets the value of the chAtmStatusRequestBean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChAtmStatusRequestBean }
     *     
     */
    public void setChAtmStatusRequestBean(ChAtmStatusRequestBean value) {
        this.chAtmStatusRequestBean = value;
    }

}
